import { data } from "jquery";
import {SET_EDITOR, SET_EQUATION, SET_EXPRESSION, SET_MATHML, SET_MATHQUILL, REMOVE_MATHML, SET_COPY_EQUATION} from "../constants/mathml.constant";

// Define Actions for Latex. Store Expression in Application reducer. 
export const mathMLActions = {
  setMathEditorAction,
  setMathEquationAction,
  setMathMLExpressionAction,
  removeMathMLExpressionAction,
  setCopyMathMLExpressionAction
};

function setMathEditorAction(editor) {
  return dispatch => {
    dispatch(success(editor))
  };

  function success(editor) {
    return {type: SET_EDITOR, editor}
  }

}
function setMathEquationAction(equation) {
  return dispatch => {
    dispatch(success(equation))
  };

  function success(equation) {
    return {type: SET_EQUATION, equation}
  }

}

function setMathMLExpressionAction(mathml) {
  return dispatch => {
    dispatch(success(mathml))
  };

  function success(mathml) {
    return {type: SET_MATHML, mathml}
  }
}

function removeMathMLExpressionAction() {
  return dispatch => {
    dispatch(success())
  };

  function success() {
    return {type: REMOVE_MATHML}
  }
}


function setCopyMathMLExpressionAction(copy_equation) {
  return dispatch => {
    dispatch(success(copy_equation))
  };

  function success(copy_equation) {
    return {type: SET_COPY_EQUATION, copy_equation}
  }
}