//Define Constants for Latex Actions
export const SET_EDITOR = "SET_EDITOR"
export const SET_EQUATION = "SET_EQUATION"
export const SET_MATHML = "SET_MATHML"
export const SET_COPY_EQUATION = "SET_COPY_EQUATION"
export const REMOVE_MATHML = "REMOVE_MATHML"

export const ITERATION = "ITERATION"
export const CONDITION = "CONDITION"
export const ASSIGNMENT = "ASSIGNMENT"
