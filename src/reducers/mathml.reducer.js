import { SET_EDITOR, SET_EQUATION, SET_MATHML, REMOVE_MATHML, SET_COPY_EQUATION } from "../constants/mathml.constant";


export function mathmlReducer(state = { mathml: null, equation: null, editor: null, copy_equation: [] }, action) {
  switch (action.type) {
    case SET_EDITOR:
      return { ...state, editor: action.editor }
    case SET_EQUATION:
      return { ...state, equation: action.equation }
    case SET_MATHML:
      return { ...state, mathml: action.mathml }
    case REMOVE_MATHML:
      return { ...state, mathml: '' };
    case SET_COPY_EQUATION:
      return { ...state, copy_equation: action.copy_equation }
    default:
      return state
  }
}
