import { combineReducers } from 'redux';
import { mathmlReducer } from "./mathml.reducer";
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
//Combine all the Resucers of all application which are passed to the components using Provider
const allReducers = combineReducers({
  mathmlReducer,
});

const rootReducer = (state, action) => {
  if (action.type === LOGOUT_SUCCESS) {
    state = undefined
  }
  return allReducers(state, action)
};
export default rootReducer