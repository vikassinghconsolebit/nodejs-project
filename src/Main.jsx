import React, {Component} from 'react';
import {Redirect, Route} from "react-router-dom";
import {isLoggedIn} from "./controller/localStorageHandler";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";


function Main({component: Component, ...rest}) {
  return (
    <Route
      {...rest}
      render={routeProps => (
        isLoggedIn() ?
          <>
            <Header/>
            <Sidebar/>
            <Component {...routeProps} />
          </> :
          <Redirect to={{pathname: '/', state: {from: routeProps.location}}}/>
      )}
    />
  );
}

export default Main;
