import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Routes from "./controller/routes";
import Login from "./components/Login";
import {Dashboard} from "./components/Dashboard";
import ExpressionsEditor from "./components/Calculation/ExpressionEditor";
import Entity from "./components/Entity";
import CreateEntity from "./components/Entity/CreateEntity";
import Attribute from "./components/Attribute";
import CreateAttribute from "./components/Attribute/CreateAttribute";
import Iteration from "./components/Iteration";
import CreateIteration from "./components/Iteration/CreateIteration";
import Calculation from "./components/Calculation";
import CreateCalculation from "./components/Calculation/CreateCalculation";
import Schedule from "./components/Schedule";
import CreateSchedule from "./components/Schedule/CreateSchedule";
import Log from "./components/Log";
import CreateLog from "./components/Log/CreateLog";
import Profile from "./components/Profile";
import Setting from "./components/Setting";
import Help from "./components/Help";
import Main from "./Main";
import history from "./controller/history";
import EntityDetail from "./components/Entity/EntityDetail";
import ImportExport from "./components/ImportExport";
import AttributeDetail from "./components/Attribute/AttributeDetail";
import ScrollToTop from "./controller/ScrollToTop";
import LocalVariable from './components/Calculation/ExpressionEditor/LocalVariable';
import CreatelocalVariable from './components/Calculation/ExpressionEditor/CreateLocalVariable';
import GlobalVariable from './components/Calculation/ExpressionEditor/LocalVariable';
import CreateGlobalVariable from './components/Calculation/ExpressionEditor/CreateGlobalVariable';
import EditCalculation from './components/Calculation/EditCalculation';
import CreateEquation from './components/Calculation/ExpressionEditor/CreateEquation';
import EditLocalVariable from './components/Calculation/ExpressionEditor/EditLocalVariable';
import EditGlobalVariable from './components/Calculation/ExpressionEditor/EditGlobalVariable';
import SetVariable from './components/Calculation/ExpressionEditor/SetVariable';
import CreateSetVariable from './components/Calculation/ExpressionEditor/CreateSetVariable';
import EditSetVariable from './components/Calculation/ExpressionEditor/EditSetVariable';


import EquationTable from './components/Calculation/ExpressionEditor/EquationTable';

import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
    render() {
        return (
            // <Router>
            <Router history={history}>
                <ScrollToTop>
                    <Route exact path={Routes.login} component={Login}/>
                    {/*<Route path={Routes.dashboard.self} component={Main}/>*/}
                    {/*<Redirect exact from="/" to={Routes.dashboard.self}/>*/}
                    <Main exact path={Routes.dashboard.self} component={Dashboard}/>
                    <Main exact path={Routes.dashboard.entity.self} component={Entity}/>
                    <Main exact path={Routes.dashboard.entity.detail} component={EntityDetail}/>
                    <Main exact path={Routes.dashboard.entity.create} component={CreateEntity}/>
                    <Main exact path={Routes.dashboard.attribute.self} component={Attribute}/>
                    <Main exact path={Routes.dashboard.attribute.create} component={CreateAttribute}/>
                    <Main exact path={Routes.dashboard.attribute.detail} component={AttributeDetail}/>

                    <Main exact path={Routes.dashboard.localvariable.self} component={LocalVariable}/>
                    <Main exact path={Routes.dashboard.localvariable.create} component={CreatelocalVariable}/>
                    <Main exact path={Routes.dashboard.localvariable.detail} component={EditLocalVariable}/>
                    <Main exact path={Routes.dashboard.globalvariable.self} component={GlobalVariable}/>
                    <Main exact path={Routes.dashboard.globalvariable.create} component={CreateGlobalVariable}/>
                    <Main exact path={Routes.dashboard.globalvariable.detail} component={EditGlobalVariable}/>

                    <Main exact path={Routes.dashboard.setvariable.self} component={SetVariable}/>
                    <Main exact path={Routes.dashboard.setvariable.create} component={CreateSetVariable}/>
                    <Main exact path={Routes.dashboard.setvariable.detail} component={EditSetVariable}/>


                    <Main exact path={Routes.dashboard.iteration.self} component={Iteration}/>
                    <Main exact path={Routes.dashboard.iteration.create} component={CreateIteration}/>
                    <Main exact path={Routes.dashboard.calculation.self} component={Calculation}/>
                    <Main exact path={Routes.dashboard.calculation.create} component={CreateCalculation}/>
                    <Main exact path={Routes.dashboard.calculation.detail} component={EditCalculation}/>

                    <Main exact path={Routes.dashboard.calculation.equationBuilder} component={ExpressionsEditor}/>

                    <Main exact path={Routes.dashboard.calculation.equation.self} component={EquationTable}/>
                    <Main exact path={Routes.dashboard.calculation.equation.create} component={CreateEquation}/>

                    <Main exact path={Routes.dashboard.schedule.self} component={Schedule}/>
                    <Main exact path={Routes.dashboard.schedule.create} component={CreateSchedule}/>
                    <Main exact path={Routes.dashboard.log.self} component={Log}/>
                    <Main exact path={Routes.dashboard.log.create} component={CreateLog}/>
                    <Main exact path={Routes.dashboard.profile} component={Profile}/>
                    <Main exact path={Routes.dashboard.setting} component={Setting}/>
                    <Main exact path={Routes.dashboard.help} component={Help}/>
                    <Main exact path={Routes.dashboard.import_export} component={ImportExport}/>
                </ScrollToTop>
            </Router>
        );
    }
}

export default App;