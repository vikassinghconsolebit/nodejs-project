//The history library lets you easily manage session history anywhere JavaScript 
import {createBrowserHistory} from 'history';
const history = createBrowserHistory();

export default history