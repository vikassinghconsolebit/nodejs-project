import {getAPIUrl} from "../global";
import {Get, Post,} from "../headerIntercepter";

export function login(data) {
    const url = getAPIUrl('auth.login');
    return Post(url, data, false)
}

export function getProfile() {
    const url = getAPIUrl('auth.profile');
    return Get(url)
}

