import {getAPIUrl} from "../global";
import {Get, Post, Patch} from "../headerIntercepter";


export function addCalculation(data) {
    const url = getAPIUrl('auth.calculation.self');
    return Post(url, data);
}
export function getCalculation(data) {
    const url = getAPIUrl('auth.calculation.self');
    return Get(url, data);
}

export function fetchRunCalculation(id, params = {}) {
    const url = getAPIUrl('auth.run_calculation.one', {id})
    return Get(url, params)
}

export function deleteCalculation(data) {
    const url = getAPIUrl('auth.calculation.delete');
    return Post(url, data);
}

export function updateCalculation(id, data) {
    const url = getAPIUrl('auth.calculation.one', {id})
    return Patch(url, data)
}

export function getCalculationById(id, params = {}) {
    const url = getAPIUrl('auth.calculation.one', {id})
    return Get(url, params)
}

// export function fetchCalculationStatus(id, params = {}) {
//     const url = getAPIUrl('auth.task_time.one', {id});
//     return Get(url, params);
// }
export function fetchCalculationStatus(params = {}) {
    const url = getAPIUrl('auth.task_time.self');
    return Get(url, params);
}