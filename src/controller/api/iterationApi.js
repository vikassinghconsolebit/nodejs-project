import {getAPIUrl} from "../global";
import { Post, Get} from "../headerIntercepter";


export function addIteration(data) {
    const url = getAPIUrl('auth.iteration.self');
    return Post(url, data);
}
export function getIteration(params={}) {
    const url = getAPIUrl('auth.iteration.self');
    return Get(url, params);
}
