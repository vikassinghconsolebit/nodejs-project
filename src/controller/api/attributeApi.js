import {getAPIUrl} from "../global";
import {Get, Patch, Post,} from "../headerIntercepter";


export function addAttribute(data) {
    const url = getAPIUrl('auth.attribute.self');
    return Post(url, data);
}

export function updateAttribute(id, data) {
    const url = getAPIUrl('auth.attribute.one', {id})
    return Patch(url, data)
}

export function getAttribute(params = {}) {
    const url = getAPIUrl('auth.attribute.self');
    return Get(url, params);
}

export function getOneAttribute(id, params = {}) {
    const url = getAPIUrl('auth.attribute.one', {id})
    return Get(url, params)
}

export function fetchAttributeHistory(id, params = {}) {
    const url = getAPIUrl('auth.attribute.history', {id})
    return Get(url, params)
}

export function deleteAttribute(data) {
    const url = getAPIUrl('auth.attribute.delete');
    return Post(url, data);
}