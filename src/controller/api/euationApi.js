import {getAPIUrl} from "../global";
import {Get, Post, Patch} from "../headerIntercepter";


export function getEquation(data) {
    const url = getAPIUrl('auth.equation.self');
    return Get(url, data);
}

export function addEquation(data) {
    const url = getAPIUrl('auth.equation.self');
    return Post(url, data);
}

export function updateEquation(id, params){
    const url = getAPIUrl('auth.equation.one', {id})
    return Patch(url, params)
}

export function deleteEquation(data) {
    const url = getAPIUrl('auth.equation.delete');
    return Post(url, data);
}
