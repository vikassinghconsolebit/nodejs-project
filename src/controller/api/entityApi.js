import {getAPIUrl} from "../global";
import {Get, Patch, Post, Remove} from "../headerIntercepter";
import {reverse} from "named-urls/dist/index.es";
import { param } from "jquery";


export function postEntity(data) {
    const url = getAPIUrl('auth.entity.self');
    return Post(url, data);
}

export function postEntityStructure(data) {
    const url = getAPIUrl('auth.createEntity');
    return Post(url, {"entity_id": "u2bOTJYSjQALQbipHRprgAmVItwKFk7v"});
}

export function postEntityAttribute(data) {
    const url = getAPIUrl('auth.entity.attribute.self');
    return Post(url, data);
}

export function deleteEntityAttribute(data) {
    const url = getAPIUrl('auth.entity.attribute.delete');
    return Post(url, data);
}

export function postEntityRelation(data) {
    const url = getAPIUrl('auth.entity.entity_relation.self');
    return Post(url, data);
}

export function deleteEntityRelation(data) {
    const url = getAPIUrl('auth.entity.entity_relation.delete');
    return Post(url, data);
}

export function getEntity(params = {}) {
    const url = getAPIUrl('auth.entity.self');
    return Get(url, params);
}

export function deleteEntity(data) {
    const url = getAPIUrl('auth.entity.delete');
    return Post(url, data);
}

export function getEntityAttribute(params = {}) {
    const url = getAPIUrl('auth.entity.attribute.self');
    return Get(url, params);
}

export function getEntityRelation(params = {}) {
    const url = getAPIUrl('auth.entity.entity_relation.self');
    return Get(url, params);
}

export function getSingleEntity(id) {
    const url = reverse(getAPIUrl('auth.entity.one', {id}));
    return Get(url);
}

export function getEntityData(id, params) {
    const url = getAPIUrl('auth.entity.entity_data.one', {id})
    return Get(url, params)
}

export function updateEntityData(id, params){
    const url = getAPIUrl('auth.entity.entity_data.one', {id})
    return Patch(url, params)
}

export function deleteEntityData(data){
    const url = getAPIUrl('auth.entity.entity_data.delete')
    return Post(url, data)
}

export function importXML(data) {
    const url = getAPIUrl('auth.import_export.import')
    return Post(url, data)
}

export function exportXML(data) {
    const url = getAPIUrl('auth.import_export.export')
    return Post(url, data)
}

export function exportSample(data) {
    const url = getAPIUrl('auth.import_export.sample')
    return Post(url, data)
}

export function getEntityFunction(id, params) {
    const url = getAPIUrl('auth.entity.entity_function.one', {id})
    return Get(url, params)
}

export function getEntityAllFunction(params = {}) {
    const url = getAPIUrl('auth.entity.entity_function.self')
    return Get(url, params)
}