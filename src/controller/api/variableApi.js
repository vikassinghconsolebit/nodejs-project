import {getAPIUrl} from "../global";
import {Get, Post, Patch} from "../headerIntercepter";


export function addVariable(data) {
    const url = getAPIUrl('auth.variable.self');
    return Post(url, data);
}
export function getVariable(params = {}) {
    const url = getAPIUrl('auth.variable.self');
    return Get(url, params);
}

export function deleteVariable(data) {
    const url = getAPIUrl('auth.variable.delete');
    return Post(url, data);
}

export function getVariableById(id, params = {}) {
    const url = getAPIUrl('auth.variable.one', {id})
    return Get(url, params)
}
export function updateVariable(id, data) {
    const url = getAPIUrl('auth.variable.one', {id})
    return Patch(url, data)
}
// export function fetchVariableHistory(id, params = {}) {
//     const url = getAPIUrl('auth.variable.history', {id})
//     return Get(url, params)
// }