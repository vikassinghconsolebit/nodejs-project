import {getAPIUrl} from "../global";
import {Get, Post,} from "../headerIntercepter";


export function addAttributeTemplate(data) {
    const url = getAPIUrl('auth.attributeTemplate.self');
    return Post(url, data);
}
export function getAttributeTemplate(params={}) {
    const url = getAPIUrl('auth.attributeTemplate.self');
    return Get(url, params);
}
