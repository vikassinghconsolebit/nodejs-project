import {include} from 'named-urls'
// import {getEnvValue} from "./environment";

// Define Routes Path 
export default {
    login: '/',
    logout: '/logout/',
    dashboard: include('/dashboard', {
        self: '',
        profile: 'profile/',
        setting: 'setting/',
        entity: include('entity/', {
            self: '',
            create: 'create/',
            detail: 'detail/:id/'
        }),
        attribute: include('attribute/', {
            self: '',
            create: 'create/',
            detail: 'detail/:id/',
        }),
        localvariable: include('localvariable/',{
            self: '',
            create: 'create/',
            detail: 'detail/:id/',
        }),
        globalvariable: include('globalvariable/',{
            self: '',
            create: 'create/',
            detail: 'detail/:id/',
        }),
        setvariable: include('setvariable/',{
            self: '',
            create: 'create/',
            detail: 'detail/:id/',
        }),
        attributeTemplate: include('attribute-template/', {
            self: '',
            create: 'create/'
        }),
        iteration: include('iteration/', {
            self: '',
            create: 'create/'
        }),
       
        calculation: include('calculation/', {
            self: '',
            equationBuilder: 'equation-editor/:id/',
            create: 'create/',
            detail: 'detail/:id/',
            equation: include('equation/',{
                self: '',
                create: 'create/'
            }),
        }),
        schedule: include('schedule/', {
            self: '',
            create: 'create/'
        }),
        log: include('log/', {
            self: '',
            create: 'create/'
        }),
        help: 'help/',
        import_export: 'import-export/'
    }),
}
// export default Routes;