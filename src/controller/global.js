import {getEnvValue} from "./environment";
import {include, reverse} from "named-urls";
import {message} from "antd";

const endpoint = {
    auth: include('api/v1/', {
        login: 'auth/login/',
        profile: 'auth/profile/',
        import_export: include('import-export/', {
            import: 'import/',
            export: 'export/',
            sample: 'sample-data/'
        }),
        entity: include('entity/', {
            self: '',
            one: ':id/',
            delete: 'multiple-delete/',
            attribute: include('entity-attribute/', {
                self: '',
                one: ':id/',
                delete: 'multiple-delete/',
            }),
            entity_relation: include('entity-relation/', {
                self: '',
                one: ':id/',
                delete: 'multiple-delete/',
            }),
            entity_data: include('entity-data/',{
                self: '',
                one: ':id/',
                delete: 'multiple-delete/',
            }),
            entity_function:include('function/',{
                self: '',
                one: ':id/',
            })
        }),
        createEntity: 'create-entity',
        calculation: include('calculation/calculation/', {
            self: '',
            one: ':id/',
            delete: 'multiple-delete/'
        }),
        attribute: include('attribute/', {
            self: '',
            one: ':id/',
            history: ':id/history/',
            delete: 'multiple-delete/'
        }),
        attributeTemplate: include('attribute-template/', {
            self: '',
            one: ':id/'
        }),
        equation: include('calculation/equation/', {
            self: '',
            one: ':id/',
            delete: 'multiple-delete/'
        }),
        iteration: include('calculation/iteration/',{
            self: '',
            one: ':id/'
        }),
        variable: include('calculation/variable/',{
            self:'',
            one: ':id/',
            delete: 'multiple-delete/'
        }),
        run_calculation: include('calculation/run-calculation/',{
            self: '',
            one: ':id/'
        }),
        task_time: include('calculation/task-time/',{
            self: '',
            one: ':id/',
        })
    }),
};

export function getAPIUrl(url, params = null) {
    const path = reverse(url.split('.').reduce((o, i) => o[i], endpoint), params);
    return getEnvValue('API_URL') + path;
}

export function formatDate(date) {
    const options = {year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit'};
    return date ? new Date(date).toLocaleString("en", options) : "-"
}

export function handleError(err) {
    if (err.response.status === 500) {
        message.error('Something went wrong')
    } else {
        if (err.response) {
            Object.keys(err.response.data).map((e) => {
                message.error(`${e}:${err.response.data[e]}`)
            })
        }
    }
}

