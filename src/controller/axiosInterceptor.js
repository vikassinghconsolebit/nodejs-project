import Axios from 'axios';
import {getEnvValue} from "./environment";
import {getRefreshToken, getUserToken, setUserToken} from "./localStorageHandler";
import Routes from "./routes";
import history from "./history";

export const axios = Axios.create({
  baseURL: getEnvValue('API_URL')
});


axios.interceptors.response.use((response) => {
  return response
}, function (error) {
  const originalRequest = error.config;

  if (error.response.status === 403 && !originalRequest._retry) {

    originalRequest._retry = true;
    const refreshToken = getRefreshToken();
    return axios.post('/api/v1/auth/api/v1/token/refresh/',
      {
        "refresh": refreshToken
      })
      .then(async res => {
        if (res.status === 200) {
          await setUserToken(res.data.access);
          originalRequest.headers['Authorization'] = 'Bearer ' + getUserToken();
          return axios(originalRequest);
        }
      }).catch(e => {
        console.log(e, 'refresh error');
        history.push(Routes.logout)
      })
  }
  return Promise.reject(error);
});
