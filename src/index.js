import 'antd/dist/antd.css'
import './assets/css/responsive.css'
import "./assets/css/custom.css"

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {pStore, store} from "./store";
import {PersistGate} from 'redux-persist/lib/integration/react';
// import Variable from './components/Calculation/Keyboard/Custom/Variable';


ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={pStore}>
      <App/>
      {/* <Variable /> */}
    </PersistGate>
  </Provider>
  ,
  document.querySelector("#root")
);


