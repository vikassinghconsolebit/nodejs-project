import function_icon from '../assets/img/function_icon.svg'
import constant_icon from '../assets/img/constant_icon.svg'
import operators_icon from '../assets/img/operator_icon.svg'
import fields_icon from '../assets/img/table_icon.svg'

// Contains all the images/icons used in application
export let Image = {
    function_icon, constant_icon, operators_icon, fields_icon,
};