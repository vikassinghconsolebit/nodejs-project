import React, { Component } from 'react'
import { Button, Table, Tabs, Popconfirm, Input, Space, message } from 'antd';
import Routes from '../../controller/routes';
import { getEntity, deleteEntity } from "../../controller/api/entityApi";
import { reverse } from "named-urls/dist/index.es";
import { formatDate, handleError } from "../../controller/global";
import { DatabaseTwoTone } from '@ant-design/icons';
import { QuestionCircleOutlined } from '@ant-design/icons';
import RecordStatus from "./RecordStatus";
import { SearchOutlined } from '@ant-design/icons';


const { TabPane } = Tabs;

export default class Entity extends Component {
    state = {
        selectedRowKeys: [],
        loading: false,
        data: [],
        pagination: {
            current: 1,
            pageSize: 25,
        },
    }

    componentDidMount() {
        this.fetchEntity()
    }

    fetchEntity = (params = {}) => {
        this.setState({ loading: true })
        getEntity(params)
            .then(res => {
                this.setState({
                    loading: false,
                    data: res.data.results,
                    pagination: {
                        ...this.state.pagination,
                        current: params.page || 1,
                        total: res.data.count,
                    },
                })
            }).catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }

    getColumnSearchProps = dataIndex => ({

        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (

            <div style={{ padding: 8 }}>

                <Input ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}

                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <Space>
                    <Button type="primary" onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />} size="small" style={{ width: 90 }} >
                        Search </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>Reset</Button>
                </Space>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        this.fetchEntity({ search: selectedKeys[0], page: 1 })
    };

    handleReset = clearFilters => {
        clearFilters();
        this.fetchEntity({ page: 1 })

    };

    onRowClick = (row) => {
        this.props.history.push(
            {
                pathname: reverse(Routes.dashboard.entity.detail, { id: row.id }),
                state: { allEntity: this.state.data, selectedData: row }
            }
        )
    }

    deleteItem = (rowKeys) => {
        deleteEntity({
            id_list: rowKeys,
            type: "entity"
        })
            .then(res => {
                message.success('Entity Deleted Successfully ')
                this.setState({ selectedRowKeys: [] })
                this.fetchEntity()
            })
    }

    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys });
    };

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend')
            symbol = '-';
        let params = {
            page: pagination.current,
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.fetchEntity(params);
    };

    render() {
        const { data, pagination, loading, selectedRowKeys } = this.state;
        const hasSelected = selectedRowKeys.length > 0;

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                sorter: true,
                ...this.getColumnSearchProps('name')
            },
            {
                title: 'Description',
                dataIndex: 'description',
                key: 'description',
                sorter: true,
                ...this.getColumnSearchProps('description')
            },
            // {
            //     title: 'Start Date',
            //     dataIndex: 'start_date',
            //     key: 'start_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            //     sorter: true,
            // },
            // {
            //     title: 'End Date',
            //     dataIndex: 'end_date',
            //     key: 'end_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            //     sorter: true
            // },
            // {
            //     title: 'Created By',
            //     dataIndex: 'created_by',
            //     key: 'created_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,
            //     sorter: true
            // },
            // {
            //     title: 'Updated By',
            //     dataIndex: 'updated_by',
            //     key: 'updated_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,
            //     sorter: true
            // },
            {
                title: 'Table Generated',
                dataIndex: 'is_table_generated',
                key: 'generated',
                render: (data) => <div className="text-center"><DatabaseTwoTone
                    style={{ fontSize: '20px' }} twoToneColor={data ? "#008000" : "#FF0000"} /></div>,
            }
        ];
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Button type="primary" className="ml-2"
                            onClick={() => this.props.history.push(Routes.dashboard.entity.create)}> Create New
                            Entity</Button>

                        <Popconfirm
                            title="Are you sure to delete this Entity?"
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                            onConfirm={() => this.deleteItem(selectedRowKeys)}
                            onCancel={() => this.fetchEntity()}
                            okText="Yes"
                            cancelText="No"
                            placement="right"
                            disabled={!hasSelected}>

                            <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

                        </Popconfirm>
                        <div className="table-container mt-4">
                            <Table loading={loading} className="table-cursor" onChange={this.handleTableChange} pagination={pagination} rowSelection={rowSelection}
                                onRow={(record, rowIndex) => {
                                    return {
                                        onClick: event => {
                                            this.onRowClick(record)
                                        }
                                    };
                                }}
                                rowKey={record => record.id}
                                dataSource={data} bordered columns={columns} />
                        </div>
                        <div>
                            <Tabs type="card">
                                <TabPane tab="Record Status" key="1"><RecordStatus /></TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
