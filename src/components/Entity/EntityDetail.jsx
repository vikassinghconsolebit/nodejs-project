import React, { Component } from 'react';
import { exportSample, exportXML, getSingleEntity, importXML } from "../../controller/api/entityApi";
import AttachAttribute from "./components/AttachAttribute";
import EntityRelation from "./components/EntityRelation";
import { Upload, message, Spin, Select, Button, Alert } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { handleError, formatDate } from "../../controller/global";
import { Tabs } from 'antd';
import EntityData from "./components/EntityData";
import History from "./components/History";

const { TabPane } = Tabs;

const { Dragger } = Upload;

class EntityDetail extends Component {
    state = {
        data: null,
        format: null,
        sampleFormat: null,
    }

    componentDidMount() {
        this.fetchEntity()
    }

    fetchEntity = () => {
        getSingleEntity(this.props.match.params.id)
            .then(response => {
                this.setState({ data: response.data })
            })
    }

    handleImport = (file, newData = false) => {
        let form_data = new FormData()
        form_data.append('file', file.file)
        form_data.append('entity', this.props.match.params.id)
        if (newData) {
            form_data.append('restructured', true)
        }
        importXML(form_data).then(res => {
            message.success("Data Imported successfully!")
            this.fetchEntity()
        }).catch(err => {
            handleError(err)
        })
    }

    handleSampleFile = () => {
        let data = {
            file_type: this.state.sampleFormat,
            entity: this.props.match.params.id
        }
        exportSample(data).then(res => {
            let downloadLink = document.createElement("a");
            let blob = new Blob([res.data]);
            let url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = `${this.state.data.name}.${this.state.sampleFormat.toLowerCase()}`;
            downloadLink.click();
        }).catch(err => {
            handleError(err)
        })
    }

    handleExport = () => {
        let data = {
            file_type: this.state.format,
            entity: this.props.match.params.id
        }
        exportXML(data).then(res => {
            let downloadLink = document.createElement("a");
            let blob = new Blob([res.data]);
            let url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = `${this.state.data.name}.${this.state.format.toLowerCase()}`;
            downloadLink.click();
        }).catch(err => {
            handleError(err)
        })
    }

    handleCallToRouter = (value) => {
        this.props.history.push(value);
    }

    render() {
        const { data } = this.state;
        if (!data) {
            return <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <div className='text-center'><Spin size='large' /></div>
                    </div>
                </div>
            </div>
        }
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        {data.is_table_generated && data.is_structure_modified &&
                            <Alert className='mb-3' message="Please import data" type="error" />}
                        <Tabs type='card' size='large'>
                            {/*  value={this.props.history.location.pathname} */}
                            {/*  onChange={this.handleCallToRouter}> */}
                            <TabPane tab="Summary" key="1">
                                <div className="card">
                                    <div className="card-body">
                                        <h5>Entity Details</h5>
                                        <div className="row">
                                            <div className="col-sm-4 col-12">
                                                <p><span>Name:</span>{data.name}</p>
                                            </div>
                                            <div className="col-sm-4 col-12">
                                                <p><span>Start Date:</span> {formatDate(data.start_date)}</p>
                                            </div>
                                            <div className="col-sm-4 col-12">
                                                <p><span>End Date:</span> {formatDate(data.end_date)}</p>
                                            </div>
                                            <div className="col-12">
                                                <p className="mb-0"><span>Description:</span> {data.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tab="Entity Attributes" key="2" value='/attribute'>
                                <AttachAttribute fetchEntity={this.fetchEntity} id={data.id} />
                            </TabPane>
                            <TabPane tab="Entity Relations" key="3" value='/relations'>
                                <EntityRelation fetchEntity={this.fetchEntity} data={data} />
                            </TabPane>
                            <TabPane
                                disabled={data.is_table_generated === false || (data.is_table_generated === true && data.is_structure_modified === true)}
                                tab="Entity Data" key="4" value='/data'>
                                <EntityData />
                            </TabPane>
                            <TabPane tab="Imports" key="5" value='/import'>
                                <div className="card">
                                    {!data.is_structure_modified && <div className="card-body">
                                        <h5 className='mb-5'>Import XML/CSV</h5>
                                        <h6>{data.is_table_generated ? "Add More Data" : "Add Data"}</h6>
                                        <Dragger showUploadList={false} customRequest={(f) => this.handleImport(f)}
                                            accept=".csv,.xml">
                                            <p className="ant-upload-drag-icon">
                                                <InboxOutlined />
                                            </p>
                                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                            <p className="ant-upload-hint">
                                                Only CSV and XML files are accepted.
                                            </p>
                                        </Dragger>
                                    </div>}
                                    {data.is_table_generated &&
                                        <div className='card-body'>
                                            <h6>Restructure Table</h6>
                                            <Dragger showUploadList={false}
                                                customRequest={(f) => this.handleImport(f, true)}
                                                accept=".csv,.xml">
                                                <p className="ant-upload-drag-icon">
                                                    <InboxOutlined />
                                                </p>
                                                <p className="ant-upload-text">Click or drag file to this area to
                                                    upload</p>
                                                <p className="ant-upload-hint">
                                                    Only CSV and XML files are accepted.
                                                </p>
                                                <p className='text-danger'>Once Uploaded , whole table will be restructured
                                                    and old data is deleted
                                                    forever!</p>
                                            </Dragger>
                                        </div>}
                                    <div className="card-body">
                                        <h6>Download Sample File</h6>
                                        <Select placeholder="Select Format"
                                            onChange={v => this.setState({ sampleFormat: v })}>
                                            <Select.Option value='CSV'>CSV</Select.Option>
                                            <Select.Option value='XML'>XML</Select.Option>
                                        </Select>
                                        <Button className='ml-3' type='primary' disabled={!this.state.sampleFormat}
                                            onClick={this.handleSampleFile}>Download
                                            Sample File</Button>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane
                                disabled={data.is_table_generated === false || (data.is_table_generated === true && data.is_structure_modified === true)}
                                tab="Exports" key="6" value='/exports'>
                                <Select placeholder="Select Format" onChange={v => this.setState({ format: v })}>
                                    <Select.Option value='CSV'>CSV</Select.Option>
                                    <Select.Option value='XML'>XML</Select.Option>
                                </Select>
                                <Button className='ml-3' type='primary' disabled={!this.state.format}
                                    onClick={this.handleExport}>Export
                                    Data</Button>
                            </TabPane>
                            <TabPane tab="History" key="7" value='/history'>
                                <History />
                            </TabPane>
                        </Tabs>
                    </div>
                </div>
            </div>
        );
    }
}

export default EntityDetail;