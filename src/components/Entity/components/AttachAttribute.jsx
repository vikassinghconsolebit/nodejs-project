import React, {Component} from 'react';
import {Button, Checkbox, Form, message, Select, Spin, Popconfirm} from "antd";
import {getAttribute} from "../../../controller/api/attributeApi";
import {deleteEntityAttribute, getEntityAttribute, postEntityAttribute} from "../../../controller/api/entityApi";
import {handleError} from "../../../controller/global";
import {withRouter} from "react-router-dom";
import {DeleteTwoTone, QuestionCircleOutlined} from "@ant-design/icons";

const {Option} = Select;

class AttachAttribute extends Component {
    state = {
        data: [],
        attributeList: [],
        fetching: false,
        selectedAttributes: []
    }
    formRef = React.createRef()

    componentDidMount() {
        this.getAttributes()
    }

    getAllAttributes = (params = {}) => {
        this.setState({fetching: true})
        getAttribute(params)
            .then(response => {
                this.setState({data: response.data.results, fetching: false})
            }).catch(err => {
            handleError(err)
            this.setState({fetching: false})
        })
    }

    getAttributes = () => {
        getEntityAttribute({entity: this.props.match.params.id})
            .then(response => {
                this.setState({attributeList: response.data.results})
            })
    }

    onSubmit = (value) => {
        value['entity'] = this.props.id;
        postEntityAttribute(value)
            .then(response => {
                this.formRef.current.resetFields()
                this.getAttributes()
                this.props.fetchEntity()
            }).catch(err => {
            handleError(err)
        })
    }

    deleteSelected = (rowKeys) => {
        deleteEntityAttribute({
            id_list: rowKeys,
            type: 'entity-attribute'
        }).then(res=>{
            message.success('Attribute deleted Successfully')
            this.getAttributes()
            this.props.fetchEntity()
            this.setState({ selectedAttributes:[] })
        }).catch(err => {
            handleError(err)
        })
    }

    handleCheckbox = (e, id) => {
        if (e.target.checked) {
            let newAttributes = this.state.selectedAttributes;
            newAttributes.push(id)
            this.setState({selectedAttributes: newAttributes})
        } else {
            this.setState(prevState => ({
                selectedAttributes: prevState.selectedAttributes.filter(i => i !== id)
            }))
        }
    }

    render() {
        const {data, attributeList, fetching, selectedAttributes} = this.state;
        return (
            <div>
                <div className="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-sm-6 col-12">
                                <h5>Attach Attributes</h5>
                                <Form ref={this.formRef} className="attribute-form" onFinish={this.onSubmit}>
                                    <Form.Item rules={[{message: 'Attribute is required', required: true}]}
                                               label="Select Attribute"
                                               name="attribute">
                                        <Select
                                            notFoundContent={fetching ? <Spin size="small"/> : null}
                                            filterOption={false}
                                            showSearch
                                            onFocus={() => this.getAllAttributes()}
                                            onSearch={(e) => this.getAllAttributes({search: e})}
                                            placeholder="Select Type" style={{width: "100%"}}>
                                            {data.length > 0 &&
                                            data.map((attribute, index) => <Option key={attribute.id}
                                                                                   value={attribute.id}>{attribute.name}</Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Button className="add-attr-btn" type='primary' htmlType="submit">Add</Button>
                                </Form>
                            </div>
                            <div className="col-sm-6 col-12">
                                <h5>Attached Attributes</h5>
                                <ul className="attached-list">
                                    {attributeList.map((attribute, index) => {
                                        return <li key={index} className='d-flex justify-content-between'>
                                            <Checkbox checked={selectedAttributes.includes(attribute.id)}
                                                      onChange={(e) => this.handleCheckbox(e, attribute.id)}/>
                                            <div>{attribute.attribute.name}</div>
                                            <div><DeleteTwoTone onClick={() => this.deleteSelected([attribute.id])}
                                                                style={{
                                                                    fontSize: '17px',
                                                                    verticalAlign: 'top',
                                                                    cursor: 'pointer'
                                                                }}
                                                                twoToneColor="#FF0000"/></div>
                                        </li>
                                    })}
                                </ul>
                                {selectedAttributes.length > 0 &&
                                    <Popconfirm
                                        title="Are you sure to delete Attribute?"
                                        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                        onConfirm={() => this.deleteSelected(selectedAttributes)}
                                        onCancel={() => this.getAttributes()}
                                        okText="Yes"
                                        cancelText="No"
                                        placement="right">

                                            <Button type="danger">Delete All Selected</Button>

                                    </Popconfirm>}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default withRouter(AttachAttribute);