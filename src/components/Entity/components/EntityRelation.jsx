import React, {Component} from 'react';
import {Button, Checkbox, Form, Input, message, Popconfirm, Select, Spin} from "antd";
import {
    deleteEntityRelation,
    getEntity,
    getEntityRelation,
    postEntityRelation
} from "../../../controller/api/entityApi";
import {handleError} from "../../../controller/global";
import {withRouter} from "react-router-dom";
import {DeleteTwoTone, QuestionCircleOutlined} from '@ant-design/icons';

const {Option} = Select;

class EntityRelation extends Component {
    state = {
        allEntity: [],
        relationList: [],
        selectedRelations: [],
        loading: false,
        fetching: false

    }
    formRef = React.createRef()

    componentDidMount() {
        this.getEntity()
        this.getRelations()
        this.formRef.current.setFieldsValue({
            entity1: this.props.data.id
        })
    }

    getEntity = (params = {}) => {
        getEntity(params)
            .then(response => {
                this.setState({allEntity: response.data.results})
            })
    }

    getRelations = () => {
        getEntityRelation({entity1: this.props.match.params.id})
            .then(response => {
                this.setState({relationList: response.data.results})
            })
    }

    onSubmit = (value) => {
        this.setState({loading: true})
        postEntityRelation(value)
            .then(response => {
                message.success('Added Successfully')
                this.getEntity()
                this.getRelations()
                this.formRef.current.resetFields()
                this.formRef.current.setFieldsValue({
                    entity1: this.props.data.id
                })
                this.setState({loading: false})
            }).catch(err => {
            handleError(err)
            this.setState({loading: false})
        })
    }

    handleCheckbox = (e, id) => {
        if (e.target.checked) {
            let newRelation = this.state.selectedRelations;
            newRelation.push(id)
            this.setState({selectedRelations: newRelation})
        } else {
            this.setState(prevState => ({
                selectedRelations: prevState.selectedRelations.filter(i => i !== id)
            }))
        }
    }

    deleteSelected = (rowKeys) => {
        deleteEntityRelation({
            id_list: rowKeys,
            type: 'entity-relation',
        })
        .then(res=>{
            message.success('Relation Deleted Successfully ')
            this.getRelations()
            this.setState({ selectedRelations: [] })
        }).catch(err => {
            handleError(err)
        })
    }

    render() {
        const {allEntity, relationList, fetching, selectedRelations} = this.state;
        return (
            <div className="entity-card">
                <div className="card">
                    <div className="card-body">
                        <h5>Entity Relation</h5>
                        <Form className="entity-form" ref={this.formRef} onFinish={this.onSubmit}>
                            <div className="row">
                                <div className="col-sm-3">
                                    <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                               className="w-100" label="Name" name="name">
                                        <Input placeholder="Name"/>
                                    </Form.Item>
                                </div>
                                <div className="col-sm-3">
                                    <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                               className="w-100" label="Selected Entity" name="entity1">
                                        <Select disabled placeholder="Select Type" style={{width: "100%"}}>
                                            {allEntity.length > 0 &&
                                            allEntity.map((attribute, index) => <Option key={index}
                                                                                        value={attribute.id}>{attribute.name}</Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                                <div className="col-sm-3">
                                    <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                               className="w-100" label="Another Entity" name="entity2">
                                        <Select notFoundContent={fetching ? <Spin size="small"/> : null}
                                                filterOption={false}
                                                showSearch
                                                onFocus={() => this.getEntity()}
                                                onSearch={(e) => this.getEntity({search: e})} placeholder="Select Type"
                                                style={{width: "100%"}}>
                                            {allEntity.length > 0 &&
                                            allEntity.filter(i => i.id !== this.props.data.id && i.is_table_generated).map((attribute, index) =>
                                                <Option
                                                    key={index}
                                                    value={attribute.id}>{attribute.name}</Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                                <div className="col-sm-3">
                                    <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                               className="w-100" label="Select Relation" name="relation_type">
                                        <Select placeholder="Select Type" style={{width: "100%"}}>
                                            <Option key='rel1' value='FOREIGN_KEY'>Foreign Keys</Option>
                                            <Option key='rel2' value='ONE_TO_ONE'>One to One</Option>
                                            {/*<Option key='rel3' value='MANY_TO_MANY'>Many to Many</Option>*/}
                                        </Select>
                                    </Form.Item>
                                </div>
                                <div className="col-sm-3">
                                    <Form.Item className="w-100">
                                        <Button loading={this.state.loading} className="add-attr-btn"
                                                type='primary'
                                                htmlType="submit">Add Relation</Button>
                                    </Form.Item>
                                </div>
                            </div>


                        </Form>
                        <h5 className='mt-4'>Added Relation</h5>
                        {relationList.map(item => (
                            <ul key={item.id}>
                                <li><Checkbox className="w-100" checked={selectedRelations.includes(item.id)}
                                              onChange={(e) => this.handleCheckbox(e, item.id)}>{item.name}</Checkbox>
                                </li>
                                <li><span>Entity (1) :</span>{item.entity1.name}</li>
                                <li><span>Entity (2) :</span>{item.entity2.name}</li>
                                <li><span>Relation :</span>{item.relation_type.split('_').join(' ')}
                                </li>
                                <li><DeleteTwoTone onClick={() => this.deleteSelected([item.id])}
                                                   style={{fontSize: '17px', verticalAlign: 'top', cursor: 'pointer'}}
                                                   twoToneColor="#FF0000"/></li>
                            </ul>
                        ))}
                        {selectedRelations.length > 0 &&
                            <Popconfirm
                                title="Are you sure to delete Relation?"
                                icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                                onConfirm={() => this.deleteSelected(selectedRelations)}
                                onCancel={() => this.getRelations()}
                                okText="Yes"
                                cancelText="No"
                                placement="right">

                                    <Button type="danger">Delete All Selected</Button>

                            </Popconfirm>}
                    </div>
                </div>

            </div>
        );
    }
}

export default withRouter(EntityRelation);