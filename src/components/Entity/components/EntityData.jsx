import React, { Component } from 'react';
import { getEntityData, updateEntityData, deleteEntityData } from "../../../controller/api/entityApi";
import { handleError } from "../../../controller/global";
import { withRouter } from "react-router-dom";
import pagination from "../../../controller/Pagination"
import { Table, Input, Button, Space, Form, Empty, Popconfirm, DatePicker, message } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { useContext, useState, useEffect, useRef } from 'react';
import { QuestionCircleOutlined } from '@ant-design/icons';
import moment from 'moment';


const datetime = "YYYY-MM-DDTHH:mm:ssZ"
const date = "YYYY-MM-DD"

const EditableContext = React.createContext(null);
const EditableRow = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
        <Form form={form} component={false}>

            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};


const EditableCell = ({
    title,
    editable,
    children,
    dataIndex,
    record,
    handleSave,
    ...restProps

}) => {

    const [editing, setEditing] = useState(false);
    const inputRef = useRef(null);
    const form = useContext(EditableContext);

    useEffect(() => {
        if (editing) {
            inputRef.current.focus();
        }

    }, [editing]);

    const toggleEdit = () => {
        setEditing(!editing);
        if (moment(record[dataIndex], datetime, true).isValid() ||
            moment(record[dataIndex], date, true).isValid()) {
            form.setFieldsValue({
                [dataIndex]: moment(record[dataIndex]),
            });
        }
        else {
            form.setFieldsValue({
                [dataIndex]: record[dataIndex],
            });
        }
    };


    const save = async () => {
        try {
            setEditing(!editing)
            let values = await form.validateFields();
            const keys = Object.keys(values);
            keys.forEach((key, index) => {
                if (values[key] instanceof moment) {
                    values[key] = values[key]._i
                }
            });
            // Handle logic if data is same then should not call handleSave
            const new_record = keys.filter(key => values[key] !== record[key])
            if (new_record.length)
                handleSave({ ...record, ...values }, dataIndex);


        } catch (errInfo) {
            console.log("error", errInfo)
        }
    };

    const onChange = (a, b) => {
        form.setFieldsValue({
            [dataIndex]: moment(b),
        });
    }
    let childNode = children;

    if (editable) {

        childNode = editing ? (moment(record[dataIndex], date, true).isValid() ? (
            <Form.Item
                name={dataIndex}
                rules={[
                    {
                        required: true,
                        message: `${title} is required.`,
                    },
                ]}>
                <DatePicker ref={inputRef} format={date} showToday={false} onBlur={save} onChange={onChange} />

            </Form.Item>
        ) : (
            (moment(record[dataIndex], datetime, true).isValid()) ?
                (<Form.Item
                    name={dataIndex}
                    rules={[
                        {
                            required: true,
                            message: `${title} is required.`,
                        },
                    ]}>
                    <DatePicker ref={inputRef} format={datetime} showTime showNow={false} onBlur={save} onChange={onChange} />

                </Form.Item>
                ) : (
                    <Form.Item style={{ margin: 0, }}
                        name={dataIndex}
                        rules={[
                            {
                                required: true,
                                message: `${title} is required.`,
                            },
                        ]}>

                        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
                    </Form.Item>
                ))) : (
            <div
                className="editable-cell-value-wrap"
                style={{
                    paddingRight: 24,
                }}
                onClick={toggleEdit}
            >
                {children}
            </div>
        );
    }

    return <td {...restProps}>{childNode}</td>;
};


class EntityData extends Component {

    state = {
        loading: false,
        data: [],
        columns: [],
        pagination: pagination,
        params: {},
        noData: false,
        selectedRowKeys: [],

    }

    componentDidMount() {
        this.getData()
    }

    getColumnSearchProps = dataIndex => ({

        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (

            <div style={{ padding: 8 }}>

                <Input ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}

                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <Space>
                    <Button type="primary" onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />} size="small" style={{ width: 90 }} >
                        Search </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>Reset</Button>
                </Space>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        this.getData({ [dataIndex]: selectedKeys[0], page: 1 })
    };

    handleReset = clearFilters => {
        clearFilters();
        this.getData({ page: 1 })

    };

    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys });
    };


    deleteItem = (rowKeys) => {
        deleteEntityData({
            id_list: rowKeys,
            entity_id: this.props.match.params.id,
            type: "entity-data"
        })
            .then(res => {
                message.success('Data Deleted Successfully ')
                this.setState({ selectedRowKeys: [] })
                this.getData()
            })
    }


    getData = (params = {}) => {
        this.setState({ loading: true, params })
        getEntityData(this.props.match.params.id, params).then(res => {
            let columns = []

            if (res.data.results.length > 0) {

                columns = Object.keys(res.data.results[0]).map((item) => {

                    let obj = {
                        title: item,
                        dataIndex: item,
                        key: item,

                        ...this.getColumnSearchProps(item),
                        sorter: true,
                        editable: true,

                        render: (data) =>
                            <>
                                <div className={typeof data === "string" && data.indexOf("+") < 0 ? "left-align" : "right-align"}>
                                    {typeof data === "boolean" ? (data ? 'True' : "False") : data}

                                </div>
                            </>

                    }
                    return obj
                })
                this.setState({ noData: false })
            } else {
                this.setState({ noData: true })
            }
            const pagination = { ...this.state.pagination }
            pagination.total = res.data.count
            this.setState({
                data: res.data.results,
                columns,
                pagination,
                loading: false,

            })
        }).catch(err => {
            handleError(err)

            this.setState({ loading: false })
        })

    };

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend')
            symbol = '-';
        let params = {
            page: pagination.current,
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.getData({ ...this.state.params, ...params });
    };

    handleSave = (row, column_name) => {
        updateEntityData(this.props.match.params.id, { column_name, data_id: row.id, value: row[column_name] })
            .then(response => {
                this.getData()
            })
    };


    render() {
        let { data, columns, loading, pagination, noData, selectedRowKeys } = this.state;
        const hasSelected = selectedRowKeys.length > 0;

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const components = {
            body: {
                row: EditableRow,
                cell: EditableCell,
            },
        };

        const column = columns.map((col) => {
            if (col.dataIndex === "id") {
                return col;
            }

            return {
                ...col,
                onCell: (record) => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    handleSave: this.handleSave,
                }),
            };
        });


        if (noData) {
            return (
                <>
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} >
                        <p className='text-danger editable-cell-value-wrap' onClick={() => this.getData()}>Click here to go Back</p>
                    </Empty>
                </>)
        }
        return (
            <>
                <Popconfirm
                    title="Are you sure to delete this Entity Data?"
                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                    onConfirm={() => this.deleteItem(selectedRowKeys)}
                    onCancel={() => this.getData()}
                    okText="Yes"
                    cancelText="No"
                    placement="right"
                    disabled={!hasSelected}>

                    <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

                </Popconfirm>
                <div className="table-container mt-4">
                    <Table components={components} onChange={this.handleTableChange} loading={loading} pagination={pagination} bordered
                        dataSource={data} rowKey={column => column.id} columns={column} rowSelection={rowSelection} />
                </div>
            </>
        );
    }
}


export default withRouter(EntityData);