import React, {Component} from 'react'
import {Button, DatePicker, Form, Input} from 'antd'
import {postEntity, postEntityStructure} from "../../controller/api/entityApi";
import Routes from "../../controller/routes";
import {handleError} from "../../controller/global";

export default class CreateEntity extends Component {
    state = {
        attributeData: [],
        loading: false
    }
    formRef = React.createRef()
    onSubmit = (value) => {
        this.setState({loading: true})
        postEntity(value)
            .then(response => {
                this.props.history.push(Routes.dashboard.entity.self)
                this.setState({loading: false})
            })
            .catch(error => {
                handleError(error)
                this.setState({loading: false})
            })
    }
    

    render() {
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Entity</h5>
                        <Form ref={this.formRef} onFinish={this.onSubmit}>
                            <Form.Item label="Name" name="name"
                                       rules={[{required: true, message: 'This field is required'}]}>
                                <Input placeholder="Entity Name"/>
                            </Form.Item>
                            <Form.Item label="Description" name="description"
                                       rules={[{required: true, message: 'This field is required'}]}>
                                <Input placeholder="Description"/>
                            </Form.Item>
                            <Form.Item label="Start Date" name="start_date"
                                       rules={[{required: true, message: 'This field is required'}]}>
                                <DatePicker showTime placeholder="Select Date"/>
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date"
                                       rules={[{required: true, message: 'This field is required'}]}>
                                <DatePicker showTime placeholder="Select Date"/>
                            </Form.Item>
                            {/*<Form.Item label="Select Attribute" name="attributes">*/}
                            {/*    <Select mode="multiple" placeholder="Select Type" style={{width: "100%"}}>*/}
                            {/*        {attributeData.length>0 &&*/}
                            {/*        attributeData.map((attribute,index)=><Option key={index} value={attribute.id}>{attribute.name}</Option>) }*/}
                            {/*    </Select>*/}
                            {/*</Form.Item>*/}
                            <div className="d-block mt-4">
                                {/*<Button type="primary" onClick={()=>this.setState({draft:true})} htmlType="submit" className="mr-3">Save As Draft</Button>*/}
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                        className="mr-3">Submit</Button>
                                <Button onClick={() => this.formRef.current.resetFields()} type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
