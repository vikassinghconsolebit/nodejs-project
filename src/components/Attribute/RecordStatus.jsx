import React, { Component } from 'react';
import { Spin } from 'antd';
import {getAttribute} from "../../controller/api/attributeApi";
import { handleError, formatDate } from '../../controller/global';
import { connect } from "react-redux";


class RecordStatus extends Component {
    state = {
        data: [],
        loading: false,
        recordData: [],
    }

    componentDidMount() {
        this.fetchEquation()
    }

    fetchEquation = () => {
        this.setState({ loading: true })

        getAttribute()
            .then(res => {
                this.setState({
                    loading: false,
                    data: res.data.results,
                }, () => {
                    this.setState({ recordData: [...this.state.data].shift() })
                })
            })
            .catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }

    render() {
        const { data, recordData } = this.state;
        if (!data) {
            return <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <div className='text-center'><Spin size='large' /></div>
                    </div>
                </div>
            </div>
        }

        return (
            <div>
                <div>
                    <div className="row">
                        <div className="col-md-4">
                            <label>Created by : </label>
                            <span className="wid-30"> {recordData.created_by ? `${recordData.created_by.first_name} ${recordData.created_by.last_name}` : '-'}</span>
                        </div>
                        {/* <div className="col-md-4 p-0">
                            <label>Comapany : </label>
                            <span className="wid-30"> {recordData.company && recordData.company.name}</span>
                        </div> */}
                        <div className="col-md-4">
                            <label>Start Date : </label>
                            <span className="wid-30"> {recordData.start_date && formatDate(recordData.start_date)}</span>
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-md-4">
                            <label>Last Updated by : </label>
                            <span className="wid-30"> {recordData.updated_by ? `${recordData.updated_by.first_name} ${recordData.updated_by.last_name}` : '-'}</span>
                        </div>
                        {/* <div className="col-md-4 p-0">
                        <label>Day Time : </label>
                            <span className="wid-30"> {recordData.day_time && formatDate(recordData.day_time)}</span>
                        </div> */}
                        <div className="col-md-4">
                            <label>End Date : </label>
                            <span className="wid-30"> {recordData.end_date && formatDate(recordData.end_date)}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const actionCreators = {
};
export default connect(null, actionCreators)(RecordStatus);
