import React, {Component} from 'react'
import {Button, Select, DatePicker, Form, Input, TimePicker, message} from 'antd'
import {addAttribute, getOneAttribute, updateAttribute} from "../../controller/api/attributeApi";
import Routes from "../../controller/routes";
import {handleError} from "../../controller/global";

const {Option} = Select;
export default class CreateAttribute extends Component {
    state = {
        loading: false
    }
    formRef = React.createRef()
    onSubmit = (value) => {
        this.setState({loading: true})
        addAttribute(value)
            .then(response => {
                this.props.history.push(Routes.dashboard.attribute.self);
                this.setState({loading: false})
            }).catch(err => {
            handleError(err)
            this.setState({loading: false})
        })
    }


    render() {
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>{`Create Attribute`}</h5>
                        <Form ref={this.formRef} onFinish={this.onSubmit}>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Name"
                                       name="name">
                                <Input placeholder="Attribute name"/>
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input placeholder="Description"/>
                            </Form.Item>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Start Date"
                                       name="start_date">
                                <DatePicker showTime/>
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker showTime/>
                            </Form.Item>
                            {/*<Form.Item label="Time" name="day_time">*/}
                            {/*    <TimePicker/>*/}
                            {/*</Form.Item>*/}
                            <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Type"
                                       name="type">
                                <Select placeholder="Select Type" style={{width: "100%"}}>
                                    <Option value="CHAR">CHAR</Option>
                                    <Option value="INT">INT</Option>
                                    <Option value="EMAIL">EMAIL</Option>
                                    <Option value="DECIMAL">DECIMAL</Option>
                                    <Option value="BOOLEAN">BOOLEAN</Option>
                                    <Option value="DATE">DATE</Option>
                                    <Option value="DATETIME">DATETIME</Option>

                                </Select>
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                        className="mr-3">Submit</Button>
                                <Button onClick={(e) => {
                                    this.formRef.current.resetFields()
                                }} type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
