import React, { Component } from 'react';
import { Button, Table, Tabs, Popconfirm, Input, Space, message } from 'antd';
import Routes from '../../controller/routes';
import { getAttribute, deleteAttribute } from "../../controller/api/attributeApi";
import { formatDate, handleError } from "../../controller/global";
import { reverse } from "named-urls/dist/index.es";
import { QuestionCircleOutlined } from '@ant-design/icons';
import RecordStatus from "./RecordStatus";
import { SearchOutlined } from '@ant-design/icons';

const { TabPane } = Tabs;

export default class Attribute extends Component {
    state = {
        attributeData: [],
        loading: false,
        pagination: {
            current: 1,
            pageSize: 25,
        },
        selectedRowKeys: [],
    }

    componentDidMount() {
        this.fetchData()
    }

    fetchData = (params = {}) => {
        this.setState({ loading: true })
        getAttribute(params)
            .then(response => {
                this.setState({
                    attributeData: response.data.results, loading: false, pagination: {
                        ...this.state.pagination,
                        current: params.page || 1,
                        total: response.data.count,
                    }
                })
            }).catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }

    getColumnSearchProps = dataIndex => ({

        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (

            <div style={{ padding: 8 }}>

                <Input ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}

                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <Space>
                    <Button type="primary" onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />} size="small" style={{ width: 90 }} >
                        Search </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>Reset</Button>
                </Space>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        this.fetchData({ search: selectedKeys[0], page: 1 })
    };

    handleReset = clearFilters => {
        clearFilters();
        this.fetchData({ page: 1 })

    };

    onRowClick = (row) => {
        this.props.history.push(
            {
                pathname: reverse(Routes.dashboard.attribute.detail, { id: row.id }),
                state: { allEntity: this.state.data, selectedData: row }
            }
        )
    }

    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys, });
    };

    deleteItem = (rowKeys) => {
        deleteAttribute({
            id_list: rowKeys,
            type: "attribute",
        })
            .then(res => {
                message.success('Attribute Deleted Successfully ')
                this.setState({ selectedRowKeys: [] })
                this.fetchData()
            })
    }

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend')
            symbol = '-';
        let params = {
            page: pagination.current,
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.fetchData(params);
    };

    render() {
        const { attributeData, pagination, selectedRowKeys, loading } = this.state;
        const rowSelection = { selectedRowKeys, onChange: this.onSelectChange, };
        const hasSelected = selectedRowKeys.length > 0;

        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                sorter: true,
                ...this.getColumnSearchProps('name')
            },
            {
                title: 'Description',
                dataIndex: 'description',
                key: 'description',
                sorter: true,
                ...this.getColumnSearchProps('description')

            },
            // {
            //     title: 'Start Date',
            //     dataIndex: 'start_date',
            //     key: 'start_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            //     sorter: true
            // },
            // {
            //     title: 'End Date',
            //     dataIndex: 'end_date',
            //     key: 'end_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            //     sorter: true
            // },
            // {
            //     title: 'Created By',
            //     dataIndex: 'created_by',
            //     key: 'created_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,
            //     sorter: true
            // },
            // {
            //     title: 'Updated By',
            //     dataIndex: 'updated_by',
            //     key: 'updated_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,
            //     sorter: true
            // },
            {
                title: 'Type',
                dataIndex: 'type',
                key: 'type',
                sorter: true,
                ...this.getColumnSearchProps('type')
            }
        ];

        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Button type="primary" className="ml-2"
                            onClick={() => this.props.history.push(Routes.dashboard.attribute.create)}>Create New
                            Attribute</Button>

                        <Popconfirm
                            title="Are you sure to delete Attributes?"
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                            onConfirm={() => this.deleteItem(selectedRowKeys)}
                            onCancel={() => this.fetchData()}
                            okText="Yes"
                            cancelText="No"
                            placement="right"
                            disabled={!hasSelected}>

                            <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

                        </Popconfirm>

                        <div className="table-container mt-4">
                            <Table onChange={this.handleTableChange} onRow={(record) => {
                                return {
                                    onClick: event => {
                                        this.onRowClick(record)
                                    }
                                };
                            }}
                                className="table-cursor"
                                rowKey={record => record.id}
                                dataSource={attributeData} bordered columns={columns} pagination={pagination}
                                rowSelection={rowSelection} loading={loading} />
                        </div>
                        <div>
                            <Tabs type="card">
                                <TabPane tab="Record Status" key="1"><RecordStatus /></TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
