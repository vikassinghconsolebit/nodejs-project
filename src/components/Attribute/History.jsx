import React, {Component} from 'react';
import {Alert, Empty, Table} from "antd";
import {fetchAttributeHistory} from "../../controller/api/attributeApi";
import {formatDate, handleError} from "../../controller/global";
import {withRouter} from "react-router-dom";


class History extends Component {

    state = {
        loading: false,
        data: [],
        pagination: {
            current: 1,
            pageSize: 25,
        },
        selectedRecord: null
    }

    componentDidMount() {
        this.fetchHistory()
    }

    fetchHistory = (params = {}) => {
        this.setState({loading: true})
        fetchAttributeHistory(this.props.match.params.id, params).then(response => {
            this.setState({
                data: response.data.results, loading: false, pagination: {
                    current: params.page || 1,
                    total: response.data.count,
                }
            })
        }).catch(err => {
            handleError(err)
            this.setState({loading: false})
        })

    }
    handleTableChange = (pagination, filters, sorter) => {
        this.fetchHistory({
            // sortField: sorter.field,
            // sortOrder: sorter.order,
            page: pagination.current,
            // ...filters,
        });
    };

    handleRecord = (record) => {

        this.setState({selectedRecord: record})
    }

    render() {
        let {pagination, loading, data, selectedRecord} = this.state
        const columns = [
            {
                title: 'User',
                dataIndex: 'history_user',
                key: 'history_user',
                render: (data) => <div>{`${data.first_name} ${data.last_name}`}</div>
            },
            {
                title: 'History Type',
                dataIndex: 'history_type',
                key: 'history_type',
                render: (data) => <div>{data === "~" ? "Changed" : data === "+" ? "Added" : "Deleted"}</div>
            },
            {
                title: 'Created',
                dataIndex: 'history_date',
                key: 'history_date',
                render: (data) => <div>{formatDate(data)}</div>
            },
        ];
        return (
            <>
                <div>
                    <Table onChange={this.handleTableChange} bordered={true} onRow={(record, rowIndex) => {
                        return {
                            onClick: event => {
                                this.handleRecord(record)
                            }
                        };
                    }}
                    rowKey={record => record.history_id}
                     loading={loading} pagination={pagination} dataSource={data} columns={columns}/>
                </div>
                <div>
                    <div className="card">
                        <div className="card-body">
                            <h5>History Detail</h5>
                            {selectedRecord ?
                                <div>
                                    {selectedRecord.message.split('\n').filter(p => p).map(i => (
                                        <Alert className='my-1' showIcon key={i} message={i} type="info"/>
                                    ))}
                                </div> :
                                <Empty
                                    description={
                                    <>
                                        <span className='text-danger'>No History selected!</span><br/>
                                        <span>Click on Row to view details.</span>
                                    </>}
                                />}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(History);