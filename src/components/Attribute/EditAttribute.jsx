import React, {Component} from 'react';
import {Button, DatePicker, Form, Input, message, Select} from "antd";
import moment from "moment";
import {addAttribute, updateAttribute} from "../../controller/api/attributeApi";
import Routes from "../../controller/routes";
import {handleError} from "../../controller/global";
import {withRouter} from "react-router-dom";

const {Option} = Select

class EditAttribute extends Component {
    state = {
        loading: false
    }
    formRef = React.createRef()

    componentDidMount() {
        this.formRef.current.setFieldsValue({
            ...this.props.data,
            start_date: moment(this.props.data.start_date),
            end_date: moment(this.props.data.end_date)
        })
    }

    onSubmit = (value) => {
        this.setState({loading: true})
        updateAttribute(this.props.match.params.id, value).then(res => {
            message.success("Attribute Updated Successfully")
            this.props.history.push(Routes.dashboard.attribute.self);
            this.setState({loading: false})
        }).catch(err => {
            handleError(err)
            this.setState({loading: false})
        })
    }

    render() {
        return (
            <div>
                <Form ref={this.formRef} onFinish={this.onSubmit}>
                    <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Name"
                               name="name">
                        <Input disabled={true} placeholder="Attribute name"/>
                    </Form.Item>
                    <Form.Item label="Description" name="description">
                        <Input placeholder="Description"/>
                    </Form.Item>
                    <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Start Date"
                               name="start_date">
                        <DatePicker showTime/>
                    </Form.Item>
                    <Form.Item label="End Date" name="end_date">
                        <DatePicker showTime/>
                    </Form.Item>
                    {/*<Form.Item label="Time" name="day_time">*/}
                    {/*    <TimePicker/>*/}
                    {/*</Form.Item>*/}
                    <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Type"
                               name="type">
                        <Select placeholder="Select Type" style={{width: "100%"}}>
                            <Option value="CHAR">CHAR</Option>
                            <Option value="INT">INT</Option>
                            <Option value="EMAIL">EMAIL</Option>
                            <Option value="DECIMAL">DECIMAL</Option>
                            <Option value="BOOLEAN">BOOLEAN</Option>
                            <Option value="DATE">DATE</Option>
                            <Option value="DATETIME">DATETIME</Option>

                        </Select>
                    </Form.Item>
                    <div className="d-block mt-4">
                        <Button loading={this.state.loading} type="primary" htmlType="submit"
                                className="mr-3">Update</Button>
                    </div>
                </Form>
            </div>
        );
    }
}

export default withRouter(EditAttribute);