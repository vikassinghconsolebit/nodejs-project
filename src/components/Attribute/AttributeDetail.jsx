import React, {Component} from 'react';
import {Spin, Tabs, Tag} from "antd";
import {getOneAttribute} from "../../controller/api/attributeApi";
import { handleError} from "../../controller/global";
import History from "./History";
import {getEntity} from "../../controller/api/entityApi";
import {withRouter} from "react-router-dom";
import EditAttribute from "./EditAttribute";

const {TabPane} = Tabs;

class AttributeDetail extends Component {
    state = {
        data: null,
        entity: []
    }

    componentDidMount() {
        getOneAttribute(this.props.match.params.id).then(res => {
            this.setState({data: res.data})
        }).catch(err => {
            handleError(err)
        })
        getEntity({entity_attribute__attribute_id: this.props.match.params.id}).then(res => {
            this.setState({entity: res.data.results})
        }).catch(err => {
            handleError(err)
        })
    }

    render() {
        const {data, entity} = this.state;
        let colors = ['magenta', 'red', 'volcano', 'orange', 'gold', 'lime', 'green', 'cyan', 'blue', 'geekblue', 'purple']
        if (!data) {
            return <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <div className='text-center'><Spin size='large'/></div>
                    </div>
                </div>
            </div>
        }
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Tabs type='card' size='large'>
                            <TabPane tab="Summary" key="1">
                                <div className="card">
                                    <div className="card-body">
                                        <h5>Attribute Details</h5>
                                        <EditAttribute data={data}/>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tab="Entity" key="3">
                                <h5>Entities</h5>
                                {entity.map((item, index) => (
                                    <Tag key={index} color={colors[Math.floor(Math.random() * colors.length)]}>{item.name}</Tag>
                                ))}
                            </TabPane>
                            <TabPane tab="History" key="2">
                                <History/>
                            </TabPane>
                        </Tabs>

                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(AttributeDetail);