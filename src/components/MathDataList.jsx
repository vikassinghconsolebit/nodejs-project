import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Tooltip} from "antd";

class MathDataList extends Component {

  onButtonClick = (e, value) => {
    // e.currentTarget.classList.add("selected");
  }

  render() {
    const mathList = []
    return (
      <div className="col-12 col-sm-6 px-0">
        <div className="row exp-right-data-row mx-0">
          <div className="col-12">
            <ul className="list-inline mb-0">
              {mathList.map((item, index) => {
                return <li key={index}>
                  <Tooltip overlayClassName="tol-tip-main-div" color={"#d5d5d5"} title={
                    <div className="toltip-design-hover">
                      {item.description}
                    </div>
                  }>
                    <Button onClick={(e) => this.onButtonClick(e, item.latex_code)}
                            className="border-0 shadow-none text-left w-100">{item.title}</Button>
                  </Tooltip>
                </li>
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {...state};
}


export default connect(mapStateToProps,)(MathDataList);
