import React, {Component} from 'react'
import {getProfile} from "../../controller/api/login";
import {formatDate, handleError} from "../../controller/global";
import {Spin} from "antd";

export default class indes extends Component {
    state = {
        data: null
    }

    componentDidMount() {
        getProfile().then(res => {
            this.setState({data: res.data})
        }).catch(err => {
            handleError(err)
        })
    }

    render() {
        let {data} = this.state
        if (!data) {
            return <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <div className='text-center'><Spin size='large'/></div>
                    </div>
                </div>
            </div>
        }
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">

                        <div className="card">
                            <div className="card-body">
                                <h5>Profile</h5>
                                <div className="row">
                                    <div className="col-sm-4 col-12">
                                        <p><span>First Name:</span>{data.first_name}</p>
                                    </div>
                                    <div className="col-sm-4 col-12">
                                        <p><span>Last Name:</span>{data.last_name}</p>
                                    </div>
                                    <div className="col-sm-4 col-12">
                                        <p><span>Email:</span>{data.email}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
