import React, {Component} from 'react';
import {Button, Table} from 'antd';
import Routes from '../../controller/routes';
import {getIteration} from "../../controller/api/iterationApi";

export default class Iteration extends Component {
    state = {
        data: [],
    }
    componentDidMount() {
        getIteration()
            .then(response => {
                this.setState({data: response.data.results})
            })
    }
    render() {
        const {data} = this.state;
        const columns = [
            {
                title: 'Eqn #',
                dataIndex: 'eqn',
                key: 'eqn',
            },
            {
                title: 'Doc',
                dataIndex: 'doc',
                key: 'doc',
            },
            {
                title: 'Iterations',
                dataIndex: 'iteration',
                key: 'iteration',
            },
        ];
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Button type="primary" className="ml-2"
                                onClick={() => this.props.history.push(Routes.dashboard.iteration.create)}>Create New
                            Iteration</Button>
                        <div className="table-container mt-4">
                            <Table dataSource={data} bordered columns={columns} pagination={false}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
