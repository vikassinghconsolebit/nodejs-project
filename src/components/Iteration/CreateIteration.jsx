import React, { Component } from 'react'
import { Button, Form, Input, message } from 'antd'
import { addIteration } from '../../controller/api/iterationApi';
import Routes from '../../controller/routes';

export default class CreateIteration extends Component {
     onSubmit=(value)=>{
         console.log(value, "value")
        // addIteration(value)
        // .then(response => {
        //     message.success('Added Successfully')
        //     this.props.history.push(Routes.dashboard.iteration.self)
        // })
    }
    render() {
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Iteration</h5>
                        <Form onFinish={this.onSubmit}>
                        <Form.Item label="Data" name="data">
                                <Input />
                            </Form.Item>
                            {/* <Form.Item label="Name" name="name">
                                <Input />
                            </Form.Item> */}
                            {/* <Form.Item label="Equation" name="equation">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Iteration" name="iteration">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Condition" name="condition">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Doc" name="document">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Disable" name="disable">
                                <Input />
                            </Form.Item> */}
                            <div className="d-block mt-4">
                                <Button type="primary" htmlType="submit" className="mr-3">Submit</Button>
                                <Button type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
