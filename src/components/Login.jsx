import React, {Component} from 'react';
import {Button, Form, Input} from "antd";
import {login} from "../controller/api/login";
import {setRefreshToken, setUser, setUserToken} from "../controller/localStorageHandler";
import Routes from "../controller/routes";

class Login extends Component {
    submitForm = (value) => {
        login(value)
            .then(response=>{
                const user = response.data;
                setUserToken(user.access)
                setRefreshToken(user.refresh)
                setUser(JSON.stringify(user.user))
                this.props.history.push(Routes.dashboard.self)
            })
    }

    render() {
        return (
            <div className="container-fluid login-fluid">
                <div className="container">
                    <div className="login-div">
                        <div className="row">
                            <div className="col-12">
                                <h3>Login</h3>
                            </div>
                            <Form className="col-12" onFinish={this.submitForm}>
                                <div className="col-12">
                                    <Form.Item label="Email" name="email">
                                        <Input plcaholder="Enter Here"/>
                                    </Form.Item>
                                </div>
                                <div className="col-12">
                                    <Form.Item label="Password" name="password">
                                        <Input.Password plcaholder="Enter Here" autoComplete="off"/>
                                    </Form.Item>
                                </div>
                                <div className="col-12">
                                    <Button type="primary" htmlType="submit">Login</Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;