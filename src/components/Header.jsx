import React, {Component} from 'react';
import {Input, Menu, Dropdown} from 'antd';
import {clearUserToken} from "../controller/localStorageHandler";
import Routes from "../controller/routes";
import {withRouter} from 'react-router-dom'
const {Search} = Input;

class Header extends Component {
    logout = () => {
        clearUserToken()
        this.props.history.push(Routes.login)
    }

    render() {
        const menu = (
            <Menu style={{width: "150px"}}>
                <Menu.Item key="0">
                    <a href="#">Profile</a>
                </Menu.Item>
                <Menu.Item key="1">
                    <a href="#">Account</a>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="3" onClick={() => this.logout()}>Logout</Menu.Item>
            </Menu>
        );
        return (
            <div className="main-header-div position-fixed">
                <div className="row mx-0 h-100 align-items-center justify-content-between">
                    <div className="search-bar-main">
                        <Search
                            placeholder="Search here..."
                            onSearch={value => console.log(value)}
                        />
                    </div>
                    <div className="user-icon-main">
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                MB
                            </a>
                        </Dropdown>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Header);