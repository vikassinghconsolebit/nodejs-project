import React from 'react'

export const Dashboard = () => {
    return (
        <div className="container-fluid exp-main-fluid">
            <div className="row mx-0">
                <div className="dashboard-container">
                    <h6 className="pl-3">Dashboard</h6>
                </div>
            </div>
        </div>
    )
}
