import React, { Component } from 'react'
import { Button, Form, Input } from 'antd'

export default class CreateSchedule extends Component {
    render() {
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Schedule</h5>
                        <Form>
                            <Form.Item label="Name">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Equation">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Iteration">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Condition">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Doc">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Disable">
                                <Input />
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button type="primary" className="mr-3">Submit</Button>
                                <Button type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
