import React, { Component } from 'react';
import { Button, Table } from 'antd';
import Routes from '../../controller/routes';

const dataSource = [
    {
        key: '1',
        eqn: '1',
        doc: "-",
        iteration: "-",
        condition: "?",
        equation: "a+b/c"
    },
    {
        key: '2',
        eqn: '2',
        doc: "-",
        iteration: "-",
        condition: "?",
        equation: "a+b/c"
    }
];

const columns = [
    {
        title: 'Eqn #',
        dataIndex: 'eqn',
        key: 'eqn',
    },
    {
        title: 'Doc',
        dataIndex: 'doc',
        key: 'doc',
    },
    {
        title: 'Iterations',
        dataIndex: 'iteration',
        key: 'iteration',
    },
];
export default class Schedule extends Component {
    render() {
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Button type="primary" className="ml-2" onClick={() => this.props.history.push(Routes.dashboard.schedule.create)}>Create New Schedule</Button>
                        <div className="table-container mt-4">
                            <Table dataSource={dataSource} bordered columns={columns} pagination={false} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
