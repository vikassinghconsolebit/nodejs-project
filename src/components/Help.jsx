import React, { Component } from 'react'

export default class Help extends Component {
    render() {
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Help centre</h5>
                        <h6 className="mt-3">Questions You may have</h6>
                        <p className="mt-3">1. How to create an equation ?</p>
                        <p>2. How to add a variable ?</p>
                        <p>3. How to change equation ?</p>
                        <div className="mt-4">
                            <h6>Cancel Premium</h6>
                            <p>Important: You can only cancel your Premium subscription from the desktop site or mobile browser, but not from the mobile app. To cancel your Premium subscription: Click the Me icon at the top of your homepage</p>
                            <h6 className="mt-3">No Access to Email Address</h6>
                            <p>There are instances when you no longer use or have access to the email address used to register your account. We suggest first trying to sign in with a secondary email address that's associated with your account. We allow you to sign in</p>
                            <h6 className="mt-3">Password Reset Basics</h6>
                            <p>You can reset your password on the Sign in page. Click the Forgot password?. Enter an email address or phone number you have on your account. If you entered an email address We’llsend a PIN number to your email.</p>
                            <h6 className="mt-3">Safety Center</h6>
                            <p>Concerned about safety? Learn more about staying safe and protecting your account</p>
                            <h6 className="mt-3">Help Forum</h6>
                            <p>Have questions? Knowledge you want to share? Head over to the  Help Forum.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
