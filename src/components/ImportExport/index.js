import React, {Component} from 'react';
import {Button, Form, message, Select, Spin, Tabs, Upload} from "antd";
import {exportXML, getEntity, importXML} from "../../controller/api/entityApi";
import {handleError} from "../../controller/global";

const {TabPane} = Tabs;

class ImportExport extends Component {
    state = {
        data: [],
        importLoading: false,
        exportLoading: false,
        fetching: false
    }

    importRef = React.createRef()
    exportRef = React.createRef()

    getAllEntity = (params = {}) => {
        this.setState({fetching: true})
        getEntity(params).then(res => {
            this.setState({data: res.data.results.filter(i => i.is_table_generated), fetching: false})
        }).catch(err => {
            handleError(err)
            this.setState({fetching: false})
        })
    }


    importData = (values) => {
        this.setState({importLoading: true})
        let form_data = new FormData()
        form_data.append('file', values.file)
        form_data.append('entity', values.entity)
        if (this.state.data.find(i => i.id === values.entity).is_table_generated) {
            form_data.append('restructured', true)
        }
        importXML(form_data).then(res => {
            message.success("Data Imported successfully!")
            this.setState({importLoading: false})
            this.importRef.current.resetFields()
        }).catch(err => {
            handleError(err)
            this.setState({importLoading: false})
        })
    }

    exportData = (values) => {
        this.setState({exportLoading: true})
        exportXML(values).then(res => {
            let downloadLink = document.createElement("a");
            let blob = new Blob([res.data]);
            let url = URL.createObjectURL(blob);
            downloadLink.href = url;
            let name = this.state.data.find(i => i.id === values.entity).name
            downloadLink.download = `${name}.${values.file_type.toLowerCase()}`;
            downloadLink.click();
            this.setState({exportLoading: false})
            this.exportRef.current.resetFields()
        }).catch(err => {
            handleError(err)
            this.setState({exportLoading: false})
        })
    }

    render() {
        return (
            <>
                <div className="container-fluid exp-main-fluid">
                    <div className="row mx-0">
                        <div className="dashboard-container right-container">
                            <Tabs type='card' size='large'>
                                <TabPane tab="Import" key="1">
                                    <Form ref={this.importRef} onFinish={this.importData}>
                                        <div className="row">
                                            <div className="col-2">
                                                <Form.Item rules={[{required: true, message: "This Field is Required"}]}
                                                           className="w-100" name="entity">
                                                    <Select notFoundContent={this.state.fetching ?
                                                        <Spin size="small"/> : null}
                                                            filterOption={false}
                                                            showSearch
                                                            onFocus={() => this.getAllEntity()}
                                                            onSearch={(e) => this.getAllEntity({search: e})}
                                                            placeholder="Entity">
                                                        {this.state.data.map(item => (
                                                            <Select.Option key={item.id}
                                                                           value={item.id}>{item.name}</Select.Option>
                                                        ))}
                                                    </Select>
                                                </Form.Item>
                                            </div>
                                            <div className='col-2'>
                                                <Form.Item name="file" rules={[{
                                                    required: true,
                                                    message: "This Field is Required"
                                                }]}>
                                                    <Button type='primary'>
                                                        <Upload showUploadList={false}
                                                                customRequest={(f) => this.importRef.current.setFieldsValue({
                                                                    file: f.file
                                                                })}
                                                                accept=".csv,.xml">Upload File</Upload>
                                                    </Button>
                                                </Form.Item>
                                            </div>
                                            <div className="col-2">
                                                <Form.Item>
                                                    <Button loading={this.state.importLoading} type='primary'
                                                            htmlType='submit'>Import</Button>
                                                </Form.Item>
                                            </div>
                                        </div>
                                    </Form>
                                </TabPane>
                                <TabPane tab="Export" key="2">
                                    <Form ref={this.exportRef} onFinish={this.exportData}>
                                        <div className="row">
                                            <div className="col-2">
                                                <Form.Item rules={[{required: true, message: "This Field is Required"}]}
                                                           className="w-100" name="entity">
                                                    <Select notFoundContent={this.state.fetching ?
                                                        <Spin size="small"/> : null}
                                                            filterOption={false}
                                                            showSearch
                                                            onFocus={() => this.getAllEntity()}
                                                            onSearch={(e) => this.getAllEntity({search: e})}
                                                            placeholder="Entity">
                                                        {this.state.data.map(item => (
                                                            <Select.Option key={item.id}
                                                                           value={item.id}>{item.name}</Select.Option>
                                                        ))}
                                                    </Select>
                                                </Form.Item>
                                            </div>
                                            <div className="col-2">
                                                <Form.Item rules={[{required: true, message: "This Field is Required"}]}
                                                           className="w-100" name="file_type">
                                                    <Select placeholder="Format">
                                                        <Select.Option value="XML">XML</Select.Option>
                                                        <Select.Option value="CSV">CSV</Select.Option>
                                                    </Select>
                                                </Form.Item>
                                            </div>
                                            <div className="col-2">
                                                <Form.Item>
                                                    <Button loading={this.state.exportLoading} type='primary'
                                                            htmlType='submit'>Export</Button>
                                                </Form.Item>
                                            </div>
                                        </div>
                                    </Form>
                                </TabPane>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default ImportExport;