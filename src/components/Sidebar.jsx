import React, {Component} from 'react';
import {Button, Collapse, Menu} from 'antd';
import {withRouter} from 'react-router-dom';
import {MenuFoldOutlined, MenuUnfoldOutlined} from '@ant-design/icons';
import Routes from '../controller/routes';
// import SubMenu from 'antd/lib/menu/SubMenu';
import {
    AppstoreOutlined,
    QuestionCircleOutlined,
    TagOutlined,
    UserOutlined,
    SettingOutlined,
    CalculatorOutlined,
    ScheduleOutlined,
    SyncOutlined,
    FileDoneOutlined,
    ImportOutlined
} from '@ant-design/icons';

const {Panel} = Collapse;

//Contains Left Sider of application
class Sidebar extends Component {
    state = {
        collapse: document.getElementsByTagName("BODY")[0].classList.contains('small-sidebar')
    }
    toggleSidebar = () => {
        let ele = document.getElementsByTagName("BODY")[0];
        if (ele.classList.contains('small-sidebar')) {
            ele.classList.remove('small-sidebar')
            this.setState({collapse: false})
        } else {
            ele.classList.add('small-sidebar')
            this.setState({collapse: true})
        }
    };

    render() {
        let {collapse} = this.state
        return (
            <div className="sidebar-main-fixed position-fixed">
                <div className="row logo-row-main mx-0">
                    <div className="col-12">
                        <a className="text-uppercase font-weight-bold"
                           onClick={() => this.props.history.push(Routes.dashboard.self)}> Equation Builder</a>
                        <Button onClick={this.toggleSidebar}
                                className='bg-transparent border-0 p-0 shadow-none toggle-btn position-absolute'>{
                            collapse ? <MenuUnfoldOutlined/> : <MenuFoldOutlined/>}</Button>
                    </div>
                </div>
                <div className="row nav-links-row-main mx-0">
                    <div className="col-12 p-0">
                        <Menu mode="inline">
                            <Menu.Item title="Attribute" key="1" icon={<TagOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.attribute.self)}> Attribute</Menu.Item>
                            <Menu.Item title="Entity" key="2" icon={<AppstoreOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.entity.self)}>Entity</Menu.Item>
                            {/* <Menu.Item title="Iteration" key="4" icon={<SyncOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.iteration.self)}>Iteration</Menu.Item> */}
                            <Menu.Item title="Calculator" key="5" icon={<CalculatorOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.calculation.self)}>Calculation</Menu.Item>
                            <Menu.Item title="Schedule" key="6" icon={<ScheduleOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.schedule.self)}>Schedule</Menu.Item>
                            <Menu.Item title="Log" key="7" icon={<FileDoneOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.log.self)}>Log</Menu.Item>
                            <Menu.Item title="Profile" key="8" icon={<UserOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.profile)}>Profile</Menu.Item>
                            <Menu.Item title="Settings" key="9" icon={<SettingOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.setting)}>Settings</Menu.Item>
                            <Menu.Item title="Help" key="10" icon={<QuestionCircleOutlined/>}
                                       onClick={() => this.props.history.push(Routes.dashboard.help)}>Help</Menu.Item>

                            {/* <Menu.Item title="Variable" key="11" 
                                       onClick={() => this.props.history.push(Routes.dashboard.variable.self)}>Variable</Menu.Item>   */}

                            <Menu.Item title="Import/Export"
                                       onClick={() => this.props.history.push(Routes.dashboard.import_export)} key="11"
                                       icon={<ImportOutlined/>}>Import/Export</Menu.Item>
                            {/* <SubMenu key="sub3" title="Lorem Ipsum">
                                <Menu.Item key="10">Dolare</Menu.Item>
                                <Menu.Item key="11">Amet dolar</Menu.Item>
                            </SubMenu> */}
                        </Menu>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Sidebar);