import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image as Images } from "../../Images";
import { Collapse, Button, Modal, List, message } from 'antd';
import { getVariable } from '../../../controller/api/variableApi';
import { handleError } from "../../../controller/global";



const { Panel } = Collapse;
const PanelHeader = ({ image, heading }) => {
  //Dropdown Heading. Get name and image .
  return (

    <div className="exp-header-accordion  d-flex align-items-center">

      <div className="exp-icon-img">
        <img alt="icons" className="img-fluid"
          src={Images[image]} />
      </div>
      <span>{heading}</span>
    </div>
  )
}

function warningPop() {
  Modal.warning({
    title: 'Dimensions warning',
    content: "You can't select more than one dimensions",
  });
}

class AddVariable extends Component {
  state = {
    data: [],
    local: [],
    global: [],
    MathEqEditor: this.props.mathmlReducer,
    visible: false,
    type: null,
    variableName: null,
    dimensions: [],
  }

  componentDidMount() {
    this.getfunctions()
  }

  getfunctions = (params = { }) => {
    getVariable(params)
      .then(res => {
        let local = this.handlelocal(res.data.results);
        let global = this.handleglobal(res.data.results);

        this.setState({
          data: res.data.results, local, global
        })
      }).catch(err => {
        handleError(err)
      })
  };

  handlelocal = (data) => {
    if (!data) {
      return;
    }
    let newData = []
    data.forEach((item) => {
      if (item.entity && item.attribute) {
        newData.push(item)
      }
    });
    return newData
  };

  handleglobal = (data) => {
    if (!data) {
      return;
    }
    let newData = []
    data.forEach((item) => {
      if (!item.entity) {
        newData.push(item)
      }
    });
    return newData
  };

  showPopup = (type, variable) => {
    this.setState({ type, visible: true, variableName: variable })
  };

  ChildFunc = (type, data) => {
    return (
      <div>
        {data.map((item, index) => {
          return (<Button className="d-flex align-items-center shadow-none border-0"
            onClick={() => this.showPopup(type, item.name)} key={index}>
            <img className="img-fluid mr-1" src={Images.fields_icon} alt="icon" /> {item.name}</Button>)

          // return (<Button className="d-flex align-items-center shadow-none border-0"
          //   onClick={() => this.state.MathEqEditor.editor.onVariableSelection(type, item.name)} key={index}>
          //   <img className="img-fluid mr-1" src={Images.fields_icon} alt="icon" /> {item.name}</Button>)

        })}
      </div>)
  };

  globalVar = (type, data) => {
    return (
      <div>
        {data.map((item, index) => {

          return (<Button className="d-flex align-items-center shadow-none border-0"
            onClick={() => this.state.MathEqEditor.editor.onVariableSelection(type, item.name)} key={index}>
            <img className="img-fluid mr-1" src={Images.fields_icon} alt="icon" /> {item.name}</Button>)

        })}
      </div>)
  };

  setSubmit = () => {

    this.state.MathEqEditor.editor.onVariableSelection(this.state.type, this.state.variableName, this.state.dimensions)
    this.setState({ visible: false, dimensions: [] })
  };

  setVisible = () => {
    this.setState({ visible: false, dimensions: [] })
  };

  dimensionData = (data) => {
    // this.state.dimensions.push(data)
    if (this.state.dimensions.length < 1){
      this.state.dimensions.push(data)
      message.success(` You have add ${data} successfully`)
    }else{
      warningPop()
    }
  };

  render() {
    const { local, global, visible } = this.state;

    const dataSource = [
      {
        name: "firstname",
      },
      {
        name: "lastname",
      },
      {
        name: "name",
      },
      {
        name: "salary",
      },
      {
        name: "company",
      },
      {
        name: "city",
      },
      {
        name: "dob",
      },
      {
        name: "country",
      },
      {
        name: "rateofexchange",
      },
    ]

    return (
      <div className="row">
        <div className="col-12">
          <ul className="list-inline mb-0">
            <div >
              <Collapse accordion >
                <Panel header={<PanelHeader image="fields_icon" heading='Local Variable' />} key="1" >
                  {/* {functionChild.map(func =>

                  <li key={func.id}><Button className="d-flex align-items-center shadow-none border-0" onClick={()=>this.funcChild()}>
                  <img className="img-fluid" src={Images.function_icon} alt="icon" />{func.name}</Button><li></li></li>

                  )} */}
                  {this.ChildFunc("local_variable", local)}
                </Panel>
                <Panel header={<PanelHeader image="fields_icon" heading="Global Variable" />} key="2" >
                  {this.globalVar("global_variable", global)}
                </Panel>
              </Collapse>
            </div>
          </ul>

        </div>
        <Modal
          title="Add Dimensions"
          centered
          visible={visible}
          onOk={() => this.setSubmit()}
          onCancel={() => this.setSubmit()}
          width={800}>

          <List
            itemLayout="horizontal"
            dataSource={dataSource}
            renderItem={(item, index) => (
              <List.Item onClick={() => this.dimensionData(item.name)} style={{ cursor: 'pointer' }}>
                <div >{item.name}
                </div>
              </List.Item>
            )}
          />
        </Modal>
        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { ...state };
}
const actionCreators = {
};

export default connect(
  mapStateToProps, actionCreators
)(AddVariable);

