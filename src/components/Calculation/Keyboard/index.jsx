import React, { Component } from 'react';
import { Button, Form, Input } from "antd";
import { buttons } from "./buttons";
import { connect } from "react-redux";
import Modal from 'antd/lib/modal/Modal';
import Numbers from '../Keyboard/Custom/Numbers';
import Texts from '../Keyboard/Custom/Texts';
import { MathEqEditor } from "../EquationBuilder/math/MathEqEditor";

//Define Special Keys
var specialKeys = {
    right: "Right",
    left: "Left",
    bksp: "Backspace",
};

// function position(){
//     var editor = new MathEqEditor({
//         matheqId: 'mathml-equation',
//         activate: true,
//       }) 
//       this.setState({visible: editor['positionis']})
// }
class Keyboard extends Component {
    state = {
        visible: false,
        IntervalId: null,

    }

    // componentDidMount(){
    //     var editor = new MathEqEditor({
    //         matheqId: 'mathml-equation',
    //         activate: true,
    //       }) 
    //       this.setState({visible: editor['positionis']})

    // }

    // componentWillUnmount(){
    //     clearInterval(this.state.IntervId);
    //   // release our intervalID from the variable
    //   this.state.IntervalId = null; 
    // }

    // checkposition=()=>{

    //     if(!this.state.IntervalId){
    //         window.setInterval(position,1000);
    //     }
    // }

    //Function execute when Some key/Button is pressed
    onKeyboardButtonClick = (value) => {
        const { mathquill } = this.props;
        if (specialKeys[value]) {
            mathquill.keystroke(specialKeys[value]);
        } else {
            mathquill.cmd(value);
        }
    };

  
    render() {
        const visible = this.state;
        const { editor } = this.props.mathmlReducer;
        return (
            <div className="col-12 exp-all-btn-calc">
                <div className="row justify-content-md-center">
                    <div className="col-12 col-md-9">
                        <div className="row mx-0">
                            {/* All Buttons for Equation input */}
                            {buttons.map((item, index) => {
                                return <Button key={index} title={item.name}
                                    onClick={() => {
                                        console.log("Sacnhit");
                                        editor.onButtonSelection(item.name)}}
                                    className="d-flex align-items-center justify-content-center">{item.title}</Button>
                            })}
                            <Numbers />
                            <Texts/>       
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { ...state };
}

const actionCreators = {};

export default connect(mapStateToProps, actionCreators)(Keyboard);