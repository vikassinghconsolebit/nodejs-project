import React, { Component } from 'react';
import { connect } from "react-redux";
import Modal from 'antd/lib/modal/Modal';
import { Button, Table } from 'antd';

const dataSource = [
  {
    key: '1',
    name: 'a',
    dimensionOne: 'x',
    dimensionTwo: 'y',
    dimensionThree: 'z'
  },
  {
    key: '2',
    name: 'b',
    dimensionOne: 'a',
    dimensionTwo: 'b',
    dimensionThree: 'c'
  },
];

const columns = [
  {
    title: 'Variable Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Dimension 1',
    dataIndex: 'dimensionOne',
    key: 'dimensionOne',
  },
  {
    title: 'Dimension 2',
    dataIndex: 'dimensionTwo',
    key: 'dimensionTwo',
  },
  {
    title: 'Dimension 3',
    dataIndex: 'dimensionThree',
    key: 'dimensionThree',
  }
];

class Variable extends Component {
  //Find the cursor position
  getCursorOffset = (mathquill) => {
    mathquill.focus();
    let offset = mathquill.__controller.cursor.offset();
    if (!offset) {
      offset = { 'top': 0, 'left': 0 }
    }
    return offset
  };
  // Use for adding Variable
  setAddVariable = (name, dimension) => {
    const { mathquill } = this.props;
    const offset = this.getCursorOffset(mathquill);
    mathquill.dropEmbedded(offset.left, offset.top, {
      htmlString: `<span class="custom-embed mq-non-leaf"><var>${name}</var><span class="mq-supsub mq-non-leaf" ><span class="mq-sup"><var>${dimension}</var></span></span></span>`,
      text: function () {
        return 'variable';
      },
      latex: function () {
        return dimension !== "" ? `\\variable{${name}_{\\variable{${dimension}}}` : `\\variable{${name}}`
      }
    })
  };
  //Prompt For add Variables and dimensions
  handleAddVariable = () => {
    const name = prompt("Enter Variable Name");
    const dimension = prompt("Enter Dimension with comma seprator");
    console.log("name", name, "dim", dimension)
    this.setAddVariable(name, dimension)
  };
  //Call When user click on Table Row For Adding Variable
  rowClick = (e) => {
    let key = e.currentTarget.dataset.rowKey
    let data = dataSource.find((_) => _.key === key)
    let d1 = data.dimensionOne, d2 = data.dimensionTwo, d3 = data.dimensionThree, dimension = [d1, d2, d3]
    dimension = d1 + "," + d2 + "," + d3
    this.setAddVariable(data.name, dimension)
  }
  //Close Add Variable Modal 
  handleClose = () => {
    this.props.toggleAddVariable(false)
  }
  render() {
    return (
      <Modal
        title="Add Variable"
        visible={true}
        // visible={this.props.visible}
        onOk={this.handleClose}
        onCancel={this.handleClose}
        className="main-modal-div variable-modal"
        okText="Submit"
        width="50%"
        style={{ top: 90 }}
        okCancel={'Cancel'}>
        <div className="row">
          <div className="col-12">
            <div className="row mx-0">
              <Button onClick={this.handleAddVariable}
                className="variables-btn-main m-0 text-center">Add Variable</Button>
            </div>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-12">
            <Table pagination={false} dataSource={dataSource} columns={columns} style={{ width: "100%" }} onRow={() => ({ onClick: (e) => this.rowClick(e) })} />
          </div>
        </div>
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return { ...state };
}
const actionCreators = {
};

export default connect(mapStateToProps, actionCreators)(Variable);