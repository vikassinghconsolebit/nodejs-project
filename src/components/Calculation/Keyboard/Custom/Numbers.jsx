import React, { Component } from 'react';
import { Button, Form, Input } from "antd";
import { connect } from "react-redux";
import Modal from 'antd/lib/modal/Modal';

// Define Special Keys
var specialKeys = {
    right: "Right",
    left: "Left",
    bksp: "Backspace"
};

class Numbers extends Component {

    state = {
        visible: false,
        number: [0,1,2,3,4,5,6,7,8,9],
        inputNumber: '',
    }

    toggleComponent = () => {
        this.setState({ visible: true });
        document.addEventListener('keydown', this.handleKeyDown)

    }
    handleClose = () => {
        this.setState({ visible: false, inputNumber: '' });
    }

    SubmitNumber = () => {
        if(this.state.inputNumber != ''){
            this.props.mathmlReducer.editor.onVariableSelection("number", parseFloat(this.state.inputNumber))
        }
        this.setState({ visible: false, inputNumber: '' });
    }


    handleKeyDown = (event) => {
        
        let { key } = event

        if (key.length === 1 && key == "-" || key == "." || key == "Backspace" || (/\d/).test(key)) {
            event.preventDefault()
            this.addnumbers(key)
        }
    };


    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleKeyDown)
    }

    addnumbers = (value) => {

        switch (value) {
            case 'Clear':
                {
                    this.setState({ inputNumber: '' });
                    break;
                }
            case 'Delete':
                {
                    var str = this.state.inputNumber;
                    str = str.slice(0, str.length - 1);
                    this.setState({ inputNumber: str });
                    break;
                }
            case 'Backspace':
                {
                    var str = this.state.inputNumber;
                    str = str.slice(0, str.length - 1);
                    this.setState({ inputNumber: str });
                    break;
                }
            case '.':
                {
                    if (value === '.' && this.state.inputNumber.includes('.')) {
                        return this.state.inputNumber;
                    } else {
                        this.setState({
                            inputNumber: this.state.inputNumber + value
                        })
                    }
                    break;
                }
            case '-':
                {
                    if (value === '-' && this.state.inputNumber.includes('.') || this.state.inputNumber.includes('-')) {
                        return this.state.inputNumber;
                    } else {
                        this.setState({
                            inputNumber: this.state.inputNumber + value
                        })
                    }
                    break;
                }

            default:
                {
                    this.setState({
                        inputNumber: this.state.inputNumber + value
                    })
                    break;
                }
        }
    }

    render() {
        const { visible, number, alphabet, conVisible } = this.state;
        const { editor } = this.props.mathmlReducer;
        return (
            <div className="row mx-0">
                <Button onClick={ this.toggleComponent }
                    className="d-flex align-items-center justify-content-center">0 - 9</Button>
                <Modal destroyOnClose={true}
                    title="Add Numbers"
                    visible={visible}
                    onOk={this.SubmitNumber }
                    onCancel={ this.handleClose }
                    className="main-modal-div variable-modal"
                    okText="Submit"
                    width="50%"
                    style={ { top: 200 } }
                    okCancel={'Cancel'}>
                    <div className="row mt-2">
                        <div className="col-12">
                            <div>{ this.state.inputNumber }</div>
                            <div className="col-12 exp-all-btn-calc">
                                <div className="row justify-content-md-center">
                                    <div className="col-12 col-md-9">
                                        <div className="row mx-0">
                                            {
                                                number.map((numb, index) => (
                                                    <Button key={numb}
                                                        title={index}
                                                        onClick={
                                                            () => this.addnumbers(numb)
                                                        }
                                                        className="d-flex align-items-center justify-content-center">
                                                        {numb}</Button>
                                                ))
                                            }
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={ () => this.addnumbers('.') }>.</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={ () => this.addnumbers('-') }>-</Button>

                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={ () => this.addnumbers('Clear') }>Clear</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={ () => this.addnumbers('Delete') }>Delete</Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state
    };
}

const actionCreators = {};

export default connect(mapStateToProps, actionCreators)(Numbers);  
