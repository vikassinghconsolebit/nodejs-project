import React, { Component } from 'react';
import { Alert, Button, message, Spin } from "antd";
import { connect } from "react-redux";
import Modal from 'antd/lib/modal/Modal';
import $ from 'jquery'

// Define Special Keys
var specialKeys = {
    right: "Right",
    left: "Left",
    bksp: "Backspace"
};

class Texts extends Component {

    state = {
        visible: false,
        inputText: '',
        smallAlphabet: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', "m", 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        bigAlphabet: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
        alphabet: [],
        upperCase: false
    }

    toggleComponent = () => {

        // $(document).off('.calcMatheqEditor')
        // $(document).on('keyup.calcMatheqEditor', ()=>null);
        this.setState({ visible: true, alphabet: this.state.smallAlphabet });
        document.addEventListener('keyup', this.handleKeyDown)
    }

    handleClose = () => {

        this.setState({ visible: false, inputText: '' });
        // $(document).on('click.calcMatheqEditor', ()=>alert('success'))

    }

    submitText = () => {
        if(this.state.inputText != ''){
            this.props.mathmlReducer.editor.onVariableSelection("constant", this.state.inputText)

        }
        this.setState({ visible: false, inputText: '' });
    }


    handleKeyDown = (event) => {
        
        if (!this.state.visible) {
            return
        }
        let { key } = event

        if (key.length === 1 && key.match(/[a-z]/i) || key == " " || key == "-" || key == "," || key == "." || key == "Backspace" || (/\d/).test(key)) {
            event.preventDefault()
            this.addTexts(key)
        }

    };

    componentWillUnmount() {
        document.removeEventListener('keyup', this.handleKeyDown)
    }

    alphabetResize = () => {
        if (this.state.upperCase === false) {
            this.setState({ alphabet: [...this.state.bigAlphabet], upperCase: true })
        } else {
            this.setState({ alphabet: [...this.state.smallAlphabet], upperCase: false })
        }
    }


    addTexts = (value) => {

        switch (value) {
            case 'Clear':
                {
                    this.setState({ inputText: '' });
                    break;
                }
            case 'Delete':
                {
                    var str = this.state.inputText;
                    str = str.slice(0, str.length - 1);
                    this.setState({ inputText: str });
                    break;
                }
            case 'Backspace':
                {
                    var str = this.state.inputText;
                    str = str.slice(0, str.length - 1);
                    this.setState({ inputText: str });
                    break;
                }
            // case '.':
            //     {
            // if (value === '.' && this.state.inputText.includes('.')) {
            //     return this.state.inputText;
            // } else {
            // this.setState({
            //     inputText: this.state.inputText + value
            // })
            // }
            // break;
            // }
            default:
                {
                    this.setState({
                        inputText: this.state.inputText + value
                    })
                    break;
                }
        }
    }


    render() {
        const { visible, alphabet, upperCase } = this.state;
        return (
            <div className="row mx-0">
                <Button onClick={() => this.toggleComponent()}
                    className="d-flex align-items-center justify-content-center">A - Z</Button>

                <Modal destroyOnClose={true}
                    title="Add Text"
                    visible={visible}
                    onOk={this.submitText}
                    onCancel={this.handleClose}
                    className="main-modal-div variable-modal"
                    okText="Submit"
                    width="50%"
                    style={{ top: 200 }}
                    okCancel={'Cancel'}>
                    <div className="row mt-2">
                        <div className="col-12">
                            <div> {this.state.inputText}</div>
                            <div className="col-12 exp-all-btn-calc">
                                <div className="row justify-content-md-center">
                                    <div className="col-12 col-md-9">
                                        <div className="row mx-0">
                                            {
                                                alphabet.map((alpha, index) => (
                                                    <Button key={index}
                                                        title={index}
                                                        onClick={
                                                            () => this.addTexts(alpha)
                                                        }
                                                        className="d-flex align-items-center justify-content-center">
                                                        {alpha}</Button>
                                                ))
                                            }
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts(' ')}>Space</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts('.')}>.</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts(',')}>,</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts('-')}>-</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts('Clear')}>Clear</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.addTexts('Delete')}>Delete</Button>
                                            <Button className="d-flex align-items-center justify-content-center"
                                                onClick={() => this.alphabetResize()}>{upperCase ? 'A-Z' : 'a-z'}</Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state
    };
}

const actionCreators = {};

export default connect(mapStateToProps, actionCreators)(Texts);
