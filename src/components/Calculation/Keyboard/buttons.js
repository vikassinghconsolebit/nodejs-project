import Sider from 'antd/lib/layout/Sider';
import {getKeyThenIncreaseKey} from 'antd/lib/message';
import React from 'react';

// Buttons for Input Equation
export const buttons = [
{
  "title": "÷",
  "name": "insert_operator_divide"
},
// {
// "title": "^",
// "name": "insert_power",
// },
{
  "title": "*",
  "name": "insert_operator_multiply"
},
{
  "title": "-",
  "name": "insert_operator_minus"
},
{
  "title": "+",
  "name": "insert_operator_plus"
},
// {
// "title": "e",
// "name": "insert_exponentiale",
// },
// {
// "title": "ln",
// "name": "insert_ln",
// },
{
  "title": "(",
  "name": "insert_parantheses"
}, 
{
  "title": "{",
  "name": "insert_curlybracket"
},
// {
// "title": "[",
// "name": "insert_squarebracket",
// },
{
  "title": "<",
  "name": "insert_operator_lt"
}, 
{
  "title": ">",
  "name": "insert_operator_gt"
},
// {
// "title": "<=",
// "name": "insert_operator_ltequals",
// },
// {
// "title": ">=",
// "name": "insert_operator_gtequals",
// },
{
  "title": "=",
  "name": "insert_assignment"
}, 
{
  "title": "&&",
  "name": "insert_operator_and"
}, 
{
  "title": "||",
  "name": "insert_operator_or"
}, 
{
  "title": "!=",
  "name": "insert_operator_notequals"
},
// {
// "title": "!",
// "name": "insert_operator_not",
// },
// {
// "title": "∪",
// "name": "insert_operator_union",
// },
// {
// "title": "∩",
// "name": "insert_operator_intersect",
// },
// {
// "title": "Σ",
// "name": "insert_sum",
// },
{
  "title": "∈",
  "name": "insert_set"
}, 
{
  "title": "|",
  "name": "filterset"
}, 
{
  "title": ',',
  "name": "comma"
},
// ///////////////////////////////////////////////////
// {
// "title": "~",
// "name": "insert_operator_objequals"
// },
// {
// "title": "⊂",
// "name": "insert_operator_prsubset"
// },
// {
// "title": "⊆",
// "name": "insert_operator_subset"
// },
// {
// "title": "⊃",
// "name": "insert_operator_prsuperset"
// },
// {
// "title": "⊇",
// "name": "insert_operator_superset"
// },
// {
// "title": "mod",
// "name": "insert_mod"
// },
// {
// "title": "log",
// "name": "insert_log"
// },
// {
// "title": "|  |",
// "name": "insert_set_size"
// },
// {
// "title": "SetDiff",
// "name":"insert_operator_setdiff",
// },
// {
// "title": "addDays",
// "name": "add_days",
// },
// {
// "title":"Isvalid",
// "name":"insert_isvalid",
// },
// {
// "title": "lookup",
// "name": "insert_lookup",
// },
// {
// "title": "Attribute",
// "name": "attribute",
// },
// {
// "title": "ObjCount",
// "name":"insert_object_count_function",
// },
// {
// "title": "isZero",
// "name": "insert_iszero",
// },
// ////////////////////////////////////////////////////
{
  "title": '==',
  "name": "insert_operator_fullequals"
}, 
{
  "title": "Cut",
  "name": "cut"
},
{
  "title": "Copy",
  "name": "copy"
}, 
{
  "title": "Paste",
  "name": "paste"
}, 
{
  "title": "Redo",
  "name": "redo"
},
{
  "title": "Undo",
  "name": "undo"
}, 
{
  "title": "Delete",
  "name": "delete_item"
}, 
{
  "title": "Clear",
  "name": "clear"
},

]
