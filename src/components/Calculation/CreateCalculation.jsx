import React, { Component } from 'react'
import { Button, DatePicker, Form, Input, message } from 'antd'
import { addCalculation } from '../../controller/api/calculationApi';
import Routes from "../../controller/routes";
import { handleError } from "../../controller/global";

export default class CreateCalculation extends Component {
    state={
        loading:false,
    }

    formRef = React.createRef()

    onSubmit = (value) => {
        this.setState({ loading: true })
        addCalculation(value)
            .then(response => {
                message.success('Added Successfully')
                this.props.history.push(Routes.dashboard.calculation.self)
                this.setState({ loading: false })
            }).catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }
    render() {
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Calculation</h5>
                        <Form onFinish={this.onSubmit} ref={this.formRef}>
                            <Form.Item label="Name" name="name">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Day Time" name="day_time">
                                <DatePicker />
                            </Form.Item>
                            <Form.Item label="Start Date" name="start_date">
                                <DatePicker />
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker />
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit" className="mr-3">Submit</Button>
                                <Button onClick={(e) => { this.formRef.current.resetFields()}} type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
