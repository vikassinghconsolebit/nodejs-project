import React, { Component } from 'react';
import { Button, Table, Tabs, Modal, Popconfirm, Input, Space, message } from 'antd';
import Routes from '../../controller/routes';
import { reverse } from 'named-urls';
import { getCalculation, fetchRunCalculation, deleteCalculation } from "../../controller/api/calculationApi";
import { formatDate, handleError } from "../../controller/global";
import { QuestionCircleOutlined } from '@ant-design/icons';
import pagination from "../../controller/Pagination";
import RecordStatus from "./RecordStatus";
import { SearchOutlined } from '@ant-design/icons';

const { TabPane } = Tabs;

export default class Calculation extends Component {
    state = {
        loading: false,
        data: [],
        id: " ",
        selectedRowKeys: [],
        pagination: pagination,
        recordData: [],
    }

    componentDidMount() {
        this.getCalculationData()
    }

    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys });
    };

    getCalculationData = (params = {}) => {
        this.setState({ loading: true })
        getCalculation(params)
            .then(res => {
                const pagination = { ...this.state.pagination }
                pagination.total = res.data.count
                this.setState({
                    loading: false,
                    data: res.data.results,
                    pagination,
                })
            }).catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }

    deleteItem = (rowKeys) => {
        deleteCalculation({
            id_list: rowKeys,
        })
            .then(res => {
                message.success('Calculation Deleted Successfully ')
                this.setState({ selectedRowKeys: [] })
                this.getCalculationData()
            })
    }

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend')
            symbol = '-';
        let params = {
            page: pagination.current,
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.getCalculationData(params);
    };

    getColumnSearchProps = dataIndex => ({

        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (

            <div style={{ padding: 8 }}>

                <Input ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}

                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <Space>
                    <Button type="primary" onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />} size="small" style={{ width: 90 }} >
                        Search </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>Reset</Button>
                </Space>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        this.getCalculationData({ search: selectedKeys[0], page: 1 })
    };

    handleReset = clearFilters => {
        clearFilters();
        this.getCalculationData({ page: 1 })

    };


    render() {
        const { data, pagination, loading, selectedRowKeys, recordData } = this.state;
        const hasSelected = selectedRowKeys.length > 0;

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [

            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                sorter: true,
                ...this.getColumnSearchProps('name'),
            },
            {
                title: 'Description',
                dataIndex: 'description',
                key: 'description',
                sorter: true,
                ...this.getColumnSearchProps('description'),
            },
            // {
            //     title: 'Day Time',
            //     dataIndex: 'day_time',
            //     key: 'day_time',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            // },
            // {
            //     title: 'Start Date',
            //     dataIndex: 'start_date',
            //     key: 'start_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            // },
            // {
            //     title: 'End Date',
            //     dataIndex: 'end_date',
            //     key: 'end_date',
            //     render: (data) => <div>{data && formatDate(data)}</div>,
            // },
            // {
            //     title: 'Created By',
            //     dataIndex: 'created_by',
            //     key: 'created_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,

            // },
            // {
            //     title: 'Updated By',
            //     dataIndex: 'updated_by',
            //     key: 'updated_by',
            //     render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,

            // },
            // {
            //     title: 'Company',
            //     dataIndex: 'company',
            //     key: 'company',
            //     render: (data) => <div>{data && data.name}</div>,

            // },
        ];
        return (

            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <Button type="primary" className="ml-2"
                            onClick={() => this.props.history.push(Routes.dashboard.calculation.create)}>Create New
                            Calculation</Button>

                        <Popconfirm
                            title="Are you sure to delete this Calculation?"
                            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                            onConfirm={() => this.deleteItem(selectedRowKeys)}
                            onCancel={() => this.getCalculationData()}
                            okText="Yes"
                            cancelText="No"
                            placement="right"
                            disabled={!hasSelected}>

                            <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

                        </Popconfirm>
                        <div className="table-container mt-4" >
                            <Table loading={loading} dataSource={data} bordered
                                onChange={this.handleTableChange}
                                columns={columns}
                                rowSelection={rowSelection}
                                pagination={pagination}
                                className="table-cursor"
                                onRow={(record) => {
                                    return {
                                        onClick: (e) => {
                                            this.props.history.push(reverse(Routes.dashboard.calculation.equationBuilder, { id: record.id }),)
                                        }
                                    }
                                }} rowKey={record => record.id}
                            />
                            <div>
                                <Tabs type="card">
                                    <TabPane tab="Record Status" key="1"><RecordStatus /></TabPane>
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
