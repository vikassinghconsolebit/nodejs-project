import React from 'react';
import { Collapse, Modal } from 'antd';
import Keyboard from "../Keyboard";
import { Image as Images } from "../../Images";
import Function from "../Function";
import MathDataList from "../../MathDataList";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import Fields from './Fields';
import Operators from './Operators';
import Constants from './Constants';
import EquationInputRender from './EquationInputRender';
import Routes from '../../../controller/routes';
import { reverse } from 'named-urls';
import { updateEquation } from '../../../controller/api/euationApi';
import AddVariable from "../AddVariable";
import { MathEq, MathEqReg } from "../EquationBuilder/math/MathEq";
import { Renderer } from "../EquationBuilder/math/Renderer";
import { mathMLActions } from "../../../actions/mathml.action";
import { ASSIGNMENT, CONDITION, ITERATION } from '../../../constants/mathml.constant';

const { Panel } = Collapse;

const PanelHeader = ({ image, heading }) => {
  //Dropdown Heading. Get name and image .

  return (

    <div className="exp-header-accordion  d-flex align-items-center">

      <div className="exp-icon-img">
        <img alt="icons" className="img-fluid"
          src={Images[image]} />
      </div>
      <span>{heading}</span>
    </div>
  )
}

class EquationBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalvisible: false,
    };
    this.inputElement = React.createRef();
  }

  handleClose = (data) => {
    if (data){
      this.props.onClose(data)
    }else{
      this.props.onClose()
    }
    
  }

  refreshPage = () => {

    window.location.reload(false);
  }

  onSubmit = () => {
    if (!this.props.colname) {
// Create Equation

      this.props.inputdatacall(this.inputElement.value, this.props)
      // this.props.mathmlReducer['equation'].equation.visualElements
    } else {

// Update Equation

      if (this.props.colname === ITERATION) {
        updateEquation(this.props.rowId, {
          calculation: this.props.match.params.id,
          iteration: "'" + this.inputElement.value + "'"
        }).then(res => {
        })
      }
      else if (this.props.colname === CONDITION) {
        updateEquation(this.props.rowId, {
          calculation: this.props.match.params.id,
          condition: "'" + this.inputElement.value + "'"
         }).then(res => {
        })
      } else if (this.props.colname === ASSIGNMENT) {
        updateEquation(this.props.rowId, {
          calculation: this.props.match.params.id,
          assignment: "'" + this.inputElement.value + "'"
        }).then(res => {
        })
      }

    }
    this.props.removeMathMLExpressionAction()
    this.handleClose('update')

  }


  render() {
    const { visible } = this.props;

    return (
      <>
        <Modal title="Equation Builder" visible={visible} onOk={this.onSubmit} onCancel={()=>this.handleClose()} destroyOnClose={true}
          className="main-modal-div" okText="Submit" style={{ top: 2 }} okCancel={'Cancel'}>
          <div className="row exp-row-main mx-0">
            <div className="col-12">
              <div className="row">
                <div className="col-sm-12 col-12">
                  <div className="row exp-build-calc-row">
                    <div className="col-12 exp-text-area-row">
                      <div className="row">
                        <div className="col-12">
                          <EquationInputRender inputRef={el => this.inputElement = el} EqType={this.props.equationType} />
                        </div>
                        <div className="col-12">
                          <div placeholder=""></div>
                        </div>
                      </div>
                    </div>
                    <Keyboard /> 
                  </div>
                </div>
                <div className="col-sm-12 col-12">
                  <div className="row exp-function-row mx-0">
                    <div className="col-12 col-sm-6 px-0">
                      <div className="row exp-accordion-row mx-0">
                        <div className="col-12 p-0">
                          <Collapse accordion >
                            <Panel header={<PanelHeader image="function_icon" heading="Functions" />} key="1">
                              <Function />
                            </Panel>
                            <Panel header={<PanelHeader image="fields_icon" heading="Add Variables" />} key="2">
                              <AddVariable />
                            </Panel>
                            {/* <Panel header={<PanelHeader image="operators_icon" heading="Operators" />} key="3"> */}
                            {/* <Operators /> */}
                            {/* </Panel> */}
                            {/* <Panel header={<PanelHeader image="constant_icon" heading="Constants" />} key="4"> */}
                            {/* <Constants /> */}
                            {/* </Panel> */}
                            {/* <Panel header={<PanelHeader image="fields_icon" heading="Fields" />} key="5"> */}
                            {/* <Fields /> */}
                            {/* </Panel> */}
                            {/* <Panel header={<PanelHeader image="fields_icon" heading="Selectors" />} key="6"> */}
                            {/* <Fields /> */}
                            {/* </Panel> */}
                            <Panel header={<PanelHeader image="fields_icon" heading="Set Elements" />} key="7">
                              <Operators />
                            </Panel>
                          </Collapse>
                        </div>
                      </div>
                    </div>
                    <MathDataList />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </>
    );
  }
}

function mapStateToProps(state) {
  return { ...state };
}

const actionCreators = {
  removeMathMLExpressionAction: mathMLActions.removeMathMLExpressionAction,

};
export default connect(mapStateToProps, actionCreators)(withRouter(EquationBuilder));
