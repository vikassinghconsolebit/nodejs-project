import React, { Component } from 'react'
import { Button } from 'antd';
import { Image as Images } from "../../Images";
import { connect } from 'react-redux';

import { getVariable } from '../../../controller/api/variableApi';
import { handleError } from "../../../controller/global";

//Dropdown for Fields
class Operators extends Component {
    state = {
        MathEqEditor: this.props.mathmlReducer,
        sets: [],
    }

    componentDidMount() {
        this.getfunctions()
    }

    getfunctions = (params = {}) => {
        getVariable(params)
            .then(res => {
                let sets = this.handlelocal(res.data.results);

                this.setState({ sets })
            }).catch(err => {
                handleError(err)
            })
    };

    handlelocal = (data) => {
        if (!data) {
            return;
        }
        let newData = []
        data.forEach((item) => {
            if (item.entity && !item.attribute) {
                newData.push(item)
            }
        });
        return newData
    };


    render() {
        const { sets } = this.state;

        return (
            <div className="row">
                <div className="col-12">
                    <ul className="list-inline mb-0">
                        <div>
                            {sets.map((item, index) => {
                                return (<Button className="d-flex align-items-center shadow-none border-0"
                                    onClick={() => this.state.MathEqEditor.editor.onVariableSelection('set', item.name)} key={index}>
                                    <img className="img-fluid mr-1" src={Images.fields_icon} alt="icon" /> {item.name}</Button>)

                            })}
                        </div>

                        {/* <li> <Button className="d-flex align-items-center shadow-none border-0"
                        onClick={() => this.state.MathEqEditor.editor.onButtonSelection('AllEmployees')}>
                        <img className="img-fluid" src={Images.fields_icon} alt="icon" /> AllEmployees </Button>
                    </li>
                    <li> <Button className="d-flex align-items-center shadow-none border-0"
                        onClick={() => this.state.MathEqEditor.editor.onButtonSelection('AllCompanies')}>
                        <img className="img-fluid" src={Images.fields_icon} alt="icon" /> AllCompanies </Button>
                    </li> */}
                    </ul>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return { ...state };
}
const actionCreators = {
};

export default connect(mapStateToProps, actionCreators)(Operators);

