import React, { Component }  from 'react'
import { Button } from 'antd';
import { Image as Images } from "../../Images";
import { connect } from 'react-redux';

//Dropdown for Fields
class Fields extends Component {
    state={
        MathEqEditor: this.props.mathmlReducer,
    }

  render() {
    return (
        <div className="row">
            <div className="col-12">
                <ul className="list-inline mb-0">
                    {/* <li> <Button className="d-flex align-items-center shadow-none border-0"
                        onClick={() => this.state.MathEqEditor.editor.onButtonSelection('insert_info')}>
                        <img className="img-fluid" src={Images.fields_icon} alt="icon" /> Info </Button>
                    </li> */}
                </ul>
            </div>
        </div>
    )
}
}
function mapStateToProps(state) {
    return { ...state };
  }
  const actionCreators = {
  };
  
  export default connect( mapStateToProps, actionCreators)(Fields);
  
  