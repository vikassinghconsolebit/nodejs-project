import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image as Images } from "../../Images";
import { Button } from "antd";


class Constants extends Component {
    state={
        MathEqEditor: this.props.mathmlReducer,
    }

  render() {

    return (
        <div className="row">
            <div className="col-12">
                <ul className="list-inline mb-0">
                {/* <li><Button className="d-flex align-items-center shadow-none border-0" 
                    onClick={() => this.state.MathEqEditor.editor.onButtonSelection('Sandra')}>
                        <img className="img-fluid" src={Images.constant_icon} alt="icon" />Sandra Newman</Button> </li> */}
                </ul>
            </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return { ...state };
}
const actionCreators = {
};

export default connect( mapStateToProps, actionCreators)(Constants);


