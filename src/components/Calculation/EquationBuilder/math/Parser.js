/**
 * Parses mathml xml.
 */

import { Elements } from "./Elements";
import { Common } from "./Commons";
import { Equation } from "./Equation";
import { ElementFactory } from "./Module/GenerateElement"
/**
 * Parses a mathml string into an equation element tree.
 * If an equation instance is provided the mathml is inserted into that equation at the equation's current element.
 *
 * @param mathml: The mathml as a plain text string.
 * @param equation: An equation to insert elements into. Optional. If not provided a new equation is created.
 * @returns An equation tree or null if unparsable.
 */
const parse = function (mathml, equation) {
  //debugger
  if (!mathml) {
    return null;
  }
  var doc = null
  try {
    // Use plain DOM api for traversal since jQuery doesn't support xml namespaces.
    // doc = $.parseXML(mathml);
    const parser = new DOMParser();
    doc = parser.parseFromString(mathml, "text/xml");
  } catch (e) {
    return null;
  }

  var resultEquation = equation ? processFragment(doc, equation) : process(doc);

  return resultEquation;
};

/**
 * Private.
 * Map of element type processing functions.
 */
var NodeProcessors = {
  apply: processApplyNode,
  forall: processForallNode,
  ci: processIdentifierNode,
  cn: processNumberNode,
  condition: processConditionNode,
  selector: processSelectorNode,
  eq: processOperatorNode,
  geq: processOperatorNode,
  gt: processOperatorNode,
  lt: processOperatorNode,
  leq: processOperatorNode,
  neq: processOperatorNode,
  plus: processOperatorNode,
  minus: processMinusNode,
  times: processOperatorNode,
  divide: processDivideNode,
  power: processPowerNode,
  log: processLogarithmNode,
  ln: processNaturalLogarithmNode,
  exponentiale: processExponentialNode,
  rem: processModuloNode,
  arctan: processArctanNode,
  sum: processSumNode,
  in: processOperatorNode,
  notin: processOperatorNode,
  and: processOperatorNode,
  or: processOperatorNode,
  set: processSetNode,
  setdiff: processOperatorNode,
  prsubset: processOperatorNode,
  subset: processOperatorNode,
  not: processNotNode,
  union: processUnionNode,
  intersect: processIntersectNode,
  card: processCardinalityNode,
  min: processMinNode,
  max: processMaxNode,
  abs: processAbsoluteNode,
  floor: processFloorNode,
  interval: processIntervalNode,
  csymbol: processCSymbolNode
};

/**
 * Private.
 * Factory functions for the various element types.
 */
// var ElementFactory = {
//   fence: function (node) {
//     //debugger
//     var fence = node.getAttributeNS(Common.Editor.NamespaceUri, Common.Editor.Attribute.Fence);


//     if (fence && fence.length > 0) {
//       return Elements.FenceElement.createElement(fence);
//     }

//     return null;
//   },

//   apply: function (node) {
//     return ElementFactory.fence(node);
//   },

//   fen: function (node) {
//     return ElementFactory.fence(node);
//   },

//   ci: function (node) {
//     var innerText = node.textContent;
//     var type = node.getAttribute('type');
//     var element = null;
//     if ((innerText.length > 0 && innerText !== Elements.EmptyElement.EmptySymbol) ||
//       (innerText.length === 0 && type === 'constant')) {
//       element = new Elements.IdentifierElement(innerText, type);
//     } else {
//       element = new Elements.EmptyElement();
//     }

//     var fence = ElementFactory.fence(node);

//     if (fence) {
//       fence.getContainer().children.add(element);
//       element = fence;
//     }

//     return element;
//   },

//   cn: function (node) {
//     var innerText = node.textContent;
//     var element = null
//     if (innerText.length > 0) {
//       element = new Elements.NumberElement(innerText);
//     } else {
//       element = new Elements.EmptyElement();
//     }

//     var fence = ElementFactory.fence(node);

//     if (fence) {
//       fence.getContainer().children.add(element);
//       element = fence;
//     }

//     return element;
//   },

//   eq: function () {
//     return new Elements.EqualElement();
//   },

//   geq: function () {
//     return new Elements.GreaterEqualThanElement();
//   },

//   gt: function () {
//     return new Elements.GreaterThanElement();
//   },

//   lt: function () {
//     return new Elements.LessThanElement();
//   },

//   leq: function () {
//     return new Elements.LessEqualThanElement();
//   },

//   neq: function () {
//     return new Elements.NotEqualElement();
//   },

//   plus: function () {
//     return new Elements.PlusElement();
//   },

//   minus: function () {
//     return new Elements.MinusElement();
//   },

//   times: function () {
//     return new Elements.TimesElement();
//   },

//   divide: function () {
//     return new Elements.DivideElement();
//   },

//   power: function () {
//     return new Elements.PowerElement();
//   },

//   log: function () {
//     return new Elements.LogarithmElement();
//   },

//   ln: function () {
//     return new Elements.NaturalLogarithmElement();
//   },

//   exponentiale: function () {
//     return new Elements.ExponentialElement();
//   },

//   rem: function () {
//     return new Elements.ModuloElement();
//   },

//   arctan: function () {
//     return new Elements.ArctanElement();
//   },

//   sum: function () {
//     return new Elements.SumElement();
//   },

//   in: function (node) {
//     switch (node.getAttributeNS(Common.NamespaceUri, Common.Editor.Attribute.KeyType)) {
//       case 'versions':
//         return new Elements.InVersionsElement();
//       case 'objects':
//         return new Elements.InObjectsElement();
//       default:
//         return new Elements.InElement();
//     }
//   },

//   notin: function () {
//     return new Elements.NotInElement();
//   },

//   and: function () {
//     return new Elements.AndElement();
//   },

//   or: function () {
//     return new Elements.OrElement();
//   },

//   set: function () {
//     return new Elements.SetElement();
//   },

//   setdiff: function () {
//     return new Elements.SetDiffElement();
//   },

//   prsubset: function () {
//     return new Elements.ProperSubsetElement();
//   },

//   subset: function () {
//     return new Elements.SubsetElement();
//   },

//   not: function () {
//     return new Elements.NotElement();
//   },

//   union: function (node) {
//     if (node.nextSibling && getNodeType(node.nextSibling) === 'condition') {
//       return new Elements.ArbitraryUnionElement();
//     } else {
//       return new Elements.UnionElement();
//     }
//   },

//   intersect: function (node) {
//     if (node.nextSibling && getNodeType(node.nextSibling) === 'condition') {
//       return new Elements.ArbitraryIntersectElement();
//     } else {
//       return new Elements.IntersectElement();
//     }
//   },

//   card: function () {
//     return new Elements.CardinalityElement();
//   },

//   min: function () {
//     return new Elements.MinElement();
//   },

//   max: function () {
//     return new Elements.MaxElement();
//   },

//   abs: function () {
//     return new Elements.AbsoluteElement();
//   },

//   floor: function () {
//     return new Elements.FloorElement();
//   },

//   interval: function () {
//     return new Elements.IntervalElement();
//   },

//   csymbol: function (node) {
//     switch (node.textContent.toLowerCase()) {
//       case 'fulleq':
//         return new Elements.FullEqualsElement();
//       case 'classeq':
//         return new Elements.ObjectEqualsElement();
//       case 'prsuperset':
//         return new Elements.ProperSupersetElement();
//       case 'superset':
//         return new Elements.SupersetElement();
//       case 'overlap':
//         return new Elements.SetOverlapElement();
//       default:
//         return null;
//     }
//   },
// };

/**
 * Private.
 * Checks whether a node is an empty node, i.e. if it is null, is an empty non-constant identifier node,
 * or an empty number node. If so it will create a new DOM identifier node and return this, else it returns
 * the input node.
 *
 * @param node The node to check.
 * @returns An empty identifier node or the input node.
 */
function filterEmptyNode(node) {
  var nodeType = getNodeType(node);


  if (!node || (((nodeType === 'ci' && node.getAttribute('type') !== 'constant') || nodeType === 'cn') &&
    node.textContent.length === 0)) {
    var ciElement = node.ownerDocument.createElementNS(Elements.NamespaceUri, Elements.NamespacePrefix + ':ci');
    ciElement.setAttribute('type', Elements.EmptyElement.EmptyType);
    ciElement.appendChild(node.ownerDocument.createTextNode(Elements.EmptyElement.EmptySymbol));
    return ciElement;
  }

  return node;
}

/**
 * Private.
 * Gets the node type of a mathml node.
 *
 * @param node: A mathml DOM node.
 */
export function getNodeType(node) {
  // //debugger
  return node.localName.toLowerCase();

}

/**
 * Private.
 * Searches through the DOM subtree of a node looking for the first and topmost node of a given type.
 * Traversal is breadth-first including the node itself. Type comparison is case-insensitive.
 *
 * @param type: The node type to look for without namespace, i.e. the local node name.
 * @param base: The base DOM node to search from, inclusive.
 * @returns The first matching node, or null if none was found.
 */
function getTopmostNode(type, base) {

  type = type.toLowerCase();

  var nodes = [base];

  while (nodes.length > 0) {
    var node = nodes.shift();

    // if(node.nodeName === '#text'){
    //   // nodes = nodes.concat(Array.prototype.slice.call(node.childNodes));
    //   var node = nodes.shift();
    //   if(node === undefined){
    //     return null;
    //   }
    // }

    if (getNodeType(node) === type) {
      return node;
    }

    if (node.hasChildNodes()) {
      nodes = nodes.concat(Array.prototype.slice.call(node.childNodes));
    }
  }

  return null;
}

/**
 * Private.
 * Processes the mathml and generates the equation element tree.
 *
 * @param doc: The mathml xml as a DOM document.
 * @returns An equation tree or null if unprocessable.
 */
function process(doc) {
  //debugger
  var node = getTopmostNode('math', doc.documentElement);

  if (node) {
    // Must start processing from the first child of the math node.
    node = node.firstChild;
  } else {
    // No math node found. Start from the topmost apply node instead.
    node = getTopmostNode('apply', doc.documentElement);
  }

  if (!node) {
    // Either empty or invalid mathml document.
    return null;
  }

  var equation = new Equation();

  console.log(equation);

  equation.isParsingInProgress = true;
  processNode(node, equation);
  equation.isParsingInProgress = false;
  return equation;
}

/**
 * Processes the mathml fragment and inserts new elements into the given equation.
 *
 * @param doc: The mathml xml as a DOM document. on a cross-origin.
      }
    } // Don't pass the name explicitly.
    // It will be inferred from DOM tag and Fiber owner.
 * @param equation: The equation to insert processed elements into.
 * @returns The input equation or null if unprocessable.
 */
function processFragment(doc, equation) {
  // Start from the topmost apply node.
  var node = getTopmostNode('apply', doc.documentElement);

  if (!node) {
    // Either empty or invalid mathml fragment.
    return null;
  }

  equation.isParsingInProgress = true;
  processNode(node, equation);
  equation.isParsingInProgress = false;

  return equation;
}

/**
 * Private.
 * Processes a mathml node by forwarding the node to the appropriate processing function.
 *
 * @param node: A mathml node as a DOM node.
 * @param equation: The equation to add mathml elements to.
 */
export function processNode(node, equation) {
  //debugger
  console.log(equation);
  console.log(node);
  var nodeType = getNodeType(node);
  var processor = NodeProcessors[nodeType];

  if (processor) {
    processor(node, equation);
  }
  else {
  }
}

function processApplyNode(applyNode, equation) {
  //debugger
  var fence = ElementFactory.apply(applyNode);

  if (fence) {
    equation.insertElement(fence);
    equation.setCurrentElement(fence.getContainer());
  }

  // Insert elements that may or may not be inside a fence/parenthesis.
  processNode(applyNode.firstChild, equation);

  if (fence) {
    equation.setCurrentElement(fence);
  }
}


function processForallNode(forallNode, equation) {
  var forall = new Elements.ForallElement();
  equation.insertElement(forall);
  equation.setCurrentElement(forall.getOperand());

  // Forall operator is always unary.
  var conditions = forallNode.nextSibling.childNodes;

  for (var i = 0; i < conditions.length; ++i) {
    if (i > 0) {
      processNode(conditions[i], equation);

      if (i + 1 < conditions.length) {
        // Insert separator element between each condition.
        equation.insertElement(new Elements.SeparatorElement());
      }
    }
  }

  equation.setCurrentElement(forall.getLeftmostChild());
}

function processIdentifierNode(ciNode, equation) {
  var identifier = ElementFactory.ci(ciNode);
  equation.insertElement(identifier);
}

function processNumberNode(cnNode, equation) {
  equation.insertElement(ElementFactory.cn(cnNode));
}

function processConditionNode(conditionNode, equation) {
  // Condition node should always contain one child, which should be an apply node.
  var childNode = filterEmptyNode(conditionNode.firstChild);
  processNode(childNode, equation);
}

function processSelectorNode(selectorNode, equation) {
  // Selector is used to indicate subscript. The first argument after the selector is the base, the next
  // arguments are subscripts. Subscripts will be rendered as a comma separated list. A simple "," is
  // represented as a selector tag with a sep=true attribute. There is no support in mathml for a ",",
  // but we need to represent it as mathml to be able to cut, copy and paste a single ",".
  var sep = selectorNode.getAttributeNS(Common.NamespaceUri, Common.Editor.Attribute.Separator);
  var newElement = null;

  if (sep && sep.toLowerCase() === 'true') {
    equation.insertElement(new Elements.SeparatorElement());
    return;
  }

  var baseNode = selectorNode.nextSibling;

  if (!baseNode) {
    baseNode = filterEmptyNode(baseNode);
  }

  if (getNodeType(baseNode) === 'interval') {
    var intervalElement = new Elements.IntervalSubElement(1);
    equation.insertElement(intervalElement);
    equation.setCurrentElement(intervalElement.getContainer());

    // Process interval start node.
    processNode(baseNode.firstChild, equation);
    equation.insertElement(new Elements.SeparatorElement());

    // Process interval end node.
    processNode(baseNode.lastChild, equation);
    equation.setCurrentElement(intervalElement.getSubscript());

    newElement = intervalElement;
  } else {
    var type = baseNode.getAttribute('type');

    if (type) {
      type = type.toLowerCase();
    }

    var identifierElement = new Elements.IdentifierSubElement(baseNode.textContent, type);

    if (type === 'attribute') {
      identifierElement.addAdditionalMathmlAttribute('object-type', baseNode.getAttribute('object-type'));

      // identifierElement.addAdditionalElementsAttribute('object-type', baseNode.getAttribute('object-type'));
    }

    equation.insertElement(identifierElement);
    equation.setCurrentElement(identifierElement.getSubscript());

    newElement = identifierElement;
  }

  // Process subscript nodes.
  var subscriptNode = baseNode.nextSibling;

  while (subscriptNode) {
    processNode(subscriptNode, equation);
    subscriptNode = subscriptNode.nextSibling;

    if (subscriptNode) {
      equation.insertElement(new Elements.SeparatorElement());
    }
  }

  equation.setCurrentElement(newElement);
}
///////////////////////////////////////////////////////////////////////////////////////////////////
function processOperatorNode(operatorNode, equation) {
  var nodeType = getNodeType(operatorNode);

  // Insert operator element.
  var operatorElement = ElementFactory[nodeType](operatorNode);
  equation.insertElement(operatorElement);
  equation.setCurrentElement(operatorElement.getLeftOperand());

  // Insert left operand.
  var leftOperandNode = filterEmptyNode(operatorNode.nextSibling);
  processNode(leftOperandNode, equation);

  // Insert right operand.
  equation.setCurrentElement(operatorElement.getRightOperand());
  var rightOperandNode = filterEmptyNode(leftOperandNode.nextSibling);

  while (rightOperandNode) {
    processNode(rightOperandNode, equation);
    rightOperandNode = rightOperandNode.nextSibling;

    if (rightOperandNode) {
      equation.insertElement(ElementFactory[nodeType](operatorNode));
    }
  }
}

function processMinusNode(minusNode, equation) {
  // Minus may be either a unary operator (-x) or a binary operator (x - y).
  // A unary operator has only one sibling.
  var isUnary = false;
  var firstOperand = minusNode.nextSibling;

  if (!firstOperand) {
    isUnary = true;
    firstOperand = filterEmptyNode(firstOperand);
  } else if (!firstOperand.nextSibling) {
    isUnary = true;
  }

  if (isUnary) {
    var unaryMinusElement = new Elements.UnaryMinusElement();

    equation.insertElement(unaryMinusElement);
    equation.setCurrentElement(unaryMinusElement.getOperand());

    processNode(firstOperand, equation);

    equation.setCurrentElement(unaryMinusElement);
  } else {
    processOperatorNode(minusNode, equation);
  }
}

function processDivideNode(divideNode, equation) {
  // Insert divide element.
  var divideElement = new Elements.DivideElement();
  equation.insertElement(divideElement);

  // Insert numerator.
  equation.setCurrentElement(divideElement.getNumerator());
  var numeratorNode = filterEmptyNode(divideNode.nextSibling);
  processNode(numeratorNode, equation);

  // Insert denumerator.
  equation.setCurrentElement(divideElement.getDenumerator());
  var denumeratorNode = filterEmptyNode(numeratorNode.nextSibling);
  processNode(denumeratorNode, equation);

  equation.setCurrentElement(divideElement);
}

function processPowerNode(powerNode, equation) {
  // Insert power element.
  var powerElement = new Elements.PowerElement();
  equation.insertElement(powerElement);
  equation.setCurrentElement(powerElement.getOperand());

  // Insert operand.
  var firstOperandNode = filterEmptyNode(powerNode.nextSibling);
  processNode(firstOperandNode, equation);

  // Insert power.
  equation.setCurrentElement(powerElement.getPower());
  var secondOperandNode = filterEmptyNode(firstOperandNode.nextSibling);
  processNode(secondOperandNode, equation);

  equation.setCurrentElement(powerElement);
}

function processLogarithmNode(logNode, equation) {
  var logElement = new Elements.LogarithmElement();
  equation.insertElement(logElement);
  equation.setCurrentElement(logElement.getContainer());

  var argNode = filterEmptyNode(logNode.nextSibling);
  processNode(argNode, equation);

  equation.setCurrentElement(logElement);
}

function processNaturalLogarithmNode(lnNode, equation) {
  var lnElement = new Elements.NaturalLogarithmElement();
  equation.insertElement(lnElement);
  equation.setCurrentElement(lnElement.getContainer());

  var argNode = filterEmptyNode(lnNode.nextSibling);
  processNode(argNode, equation);

  equation.setCurrentElement(lnElement);
}

function processExponentialNode(exponentialeNode, equation) {
  equation.insertElement(new Elements.ExponentialElement());
}

function processModuloNode(remNode, equation) {
  // Insert modulo element.
  var moduloElement = new Elements.ModuloElement();
  equation.insertElement(moduloElement);

  // Insert numerator.
  equation.setCurrentElement(moduloElement.getNumerator());
  var numeratorNode = filterEmptyNode(remNode.nextSibling);
  processNode(numeratorNode, equation);

  // Insert denumerator.
  equation.setCurrentElement(moduloElement.getDenumerator());
  var denumeratorNode = filterEmptyNode(numeratorNode.nextSibling);
  processNode(denumeratorNode, equation);

  equation.setCurrentElement(moduloElement);
}

function processArctanNode(arctanNode, equation) {
  var arctanElement = new Elements.ArctanElement();
  equation.insertElement(arctanElement);
  equation.setCurrentElement(arctanElement.getContainer());

  var argNode = filterEmptyNode(arctanNode.nextSibling);
  processNode(argNode, equation);

  equation.setCurrentElement(arctanElement);
}

function processIterationNode(iterationNode, iterationElement, equation) {
  equation.insertElement(iterationElement);

  // Insert subscript.
  equation.setCurrentElement(iterationElement.getSubscript());
  var subscriptNode = filterEmptyNode(iterationNode.nextSibling);
  processNode(subscriptNode, equation);

  // Insert operand.
  equation.setCurrentElement(iterationElement.getOperand());
  var operandNode = filterEmptyNode(subscriptNode.nextSibling);
  processNode(operandNode, equation);

  equation.setCurrentElement(iterationElement);
}

function processSumNode(sumNode, equation) {
  processIterationNode(sumNode, new Elements.SumElement(), equation);
}

function processSetNode(setNode, equation) {
  var setListAttribute = setNode.getAttributeNS(Common.NamespaceUri, Common.Editor.Attribute.SetList);

  if (setListAttribute && setListAttribute.toLowerCase() === 'true') {
    processSetListNode(setNode, equation);
  } else {
    // var nodeType = getNodeType(setNode);
    // var setElement = ElementFactory[nodeType](setNode);
    // equation.insertElement(setElement);

    var setElement = new Elements.FilterSetElement();
    equation.insertElement(setElement);

    // Insert filter.
    equation.setCurrentElement(setElement.getFilter());
    var filterNode = filterEmptyNode(setNode.firstChild);
    processNode(filterNode, equation);

    // Insert set.
    equation.setCurrentElement(setElement.getSet());
    var memberNode = filterEmptyNode(filterNode.nextSibling);
    processNode(memberNode, equation);

    equation.setCurrentElement(setElement);
  }
}

function processSetListNode(setListNode, equation) {
  var conditionNode = setListNode.firstChild;
  var applyNode = null
  if (getNodeType(conditionNode.firstChild.firstChild) === 'or') {
    applyNode = conditionNode.firstChild;
  } else {
    applyNode = conditionNode;
  }

  var applyNodes = [];

  [].forEach.call(applyNode.childNodes, function (node) {
    if (getNodeType(node) === 'apply') {
      applyNodes.push(node);
    }
  });

  applyNode = applyNodes[0];

  var setListElement = new Elements.SetListElement();
  setListElement.attributeName = applyNode.childNodes[1].childNodes[1].textContent;
  setListElement.attributeOwner = setListNode.childNodes[1].childNodes[1].textContent;
  setListElement.objectType = setListNode.childNodes[1].childNodes[2].childNodes[2].textContent;
  equation.insertElement(setListElement);

  // Insert members.
  equation.setCurrentElement(setListElement.getContainer());

  applyNodes.forEach(function (node, index) {
    var memberNode = filterEmptyNode(node.childNodes[2]);
    equation.insertElement(ElementFactory[getNodeType(memberNode)](memberNode));

    if (index < applyNodes.length - 1) {
      equation.insertElement(new Elements.SeparatorElement());
    }
  });

  equation.setCurrentElement(setListElement);
}

function processNotNode(notNode, equation) {
  // Not operator is always unary.
  var notElement = new Elements.NotElement();
  equation.insertElement(notElement);
  equation.setCurrentElement(notElement.getOperand());

  processNode(notNode.nextSibling, equation);

  equation.setCurrentElement(notElement);
}

function processUnionNode(unionNode, equation) {
  if (!unionNode.nextSibling || getNodeType(unionNode.nextSibling) !== 'condition') {
    processOperatorNode(unionNode, equation);
  } else {
    processIterationNode(unionNode, new Elements.ArbitraryUnionElement(), equation);
  }
}

function processIntersectNode(intersectNode, equation) {
  if (!intersectNode.nextSibling || getNodeType(intersectNode.nextSibling) !== 'condition') {
    processOperatorNode(intersectNode, equation);
  } else {
    processIterationNode(intersectNode, new Elements.ArbitraryIntersectElement(), equation);
  }
}

function processCardinalityNode(cardNode, equation) {
  var cardElement = new Elements.CardinalityElement();
  equation.insertElement(cardElement);
  equation.setCurrentElement(cardElement.getSet());

  var setNode = filterEmptyNode(cardNode.nextSibling);
  processNode(setNode, equation);

  equation.setCurrentElement(cardElement);
}

function processMinNode(minNode, equation) {
  var minElement = new Elements.MinElement();
  equation.insertElement(minElement);
  equation.setCurrentElement(minElement.getContainer());

  var siblingNode = minNode.nextSibling;

  while (siblingNode) {
    processNode(siblingNode, equation);
    siblingNode = siblingNode.nextSibling;

    if (siblingNode) {
      equation.insertElement(new Elements.SeparatorElement());
    }
  }

  equation.setCurrentElement(minElement);
}

function processMaxNode(maxNode, equation) {
  var maxElement = new Elements.MaxElement();
  equation.insertElement(maxElement);
  equation.setCurrentElement(maxElement.getContainer());

  var siblingNode = maxNode.nextSibling;

  while (siblingNode) {
    processNode(siblingNode, equation);
    siblingNode = siblingNode.nextSibling;

    if (siblingNode) {
      equation.insertElement(new Elements.SeparatorElement());
    }
  }

  equation.setCurrentElement(maxElement);
}

function processAbsoluteNode(absNode, equation) {
  var absElement = new Elements.AbsoluteElement();
  equation.insertElement(absElement);
  equation.setCurrentElement(absElement.getContainer());

  var argNode = filterEmptyNode(absNode.nextSibling);
  processNode(argNode, equation);

  equation.setCurrentElement(absElement);
}

function processFloorNode(floorNode, equation) {
  var floorElement = new Elements.FloorElement();
  equation.insertElement(floorElement);
  equation.setCurrentElement(floorElement.getContainer());

  var argNode = filterEmptyNode(floorNode.nextSibling);
  processNode(argNode, equation);

  equation.setCurrentElement(floorElement);
}

function processIntervalNode(intervalNode, equation) {
  // Insert interval element.
  var intervalElement = new Elements.IntervalElement();
  equation.insertElement(intervalElement);
  equation.setCurrentElement(intervalElement.getContainer());

  // Insert first and last argument.
  processNode(intervalNode.firstChild, equation);
  equation.insertElement(new Elements.SeparatorElement());
  processNode(intervalNode.lastChild, equation);

  equation.setCurrentElement(intervalElement);
}

function processCSymbolNode(csymbolNode, equation) {
  var funcName = csymbolNode.textContent;
  var funcEncoding = csymbolNode.getAttribute('encoding');
  var funcUrl = csymbolNode.getAttribute('definitionURL');

  var nextSiblingNode = csymbolNode.nextSibling;

  if (funcName === 'fulleq' || funcName === 'classeq' || funcName === 'prsuperset' ||
    funcName === 'superset' || funcName === 'overlap') {
    processOperatorNode(csymbolNode, equation);
  } else if (getNodeType(nextSiblingNode) !== 'condition') {
    var siblingNodes = [];

    while (nextSiblingNode) {
      siblingNodes.push(nextSiblingNode);
      nextSiblingNode = nextSiblingNode.nextSibling;
    }

    var funcElement = new Elements.FunctionElement(funcName, funcEncoding, funcUrl);
    equation.insertElement(funcElement);
    equation.setCurrentElement(funcElement.getCsymbolArguments());

    siblingNodes.forEach(function (node, index) {
      if (index > 0) {
        equation.insertElement(new Elements.SeparatorElement());
      }

      processNode(node, equation);
    });

    equation.setCurrentElement(funcElement);
  } else {
    var conditionSiblingNodes = [];

    // The condition has at least one apply node, or it would not be a condition.
    // The next sibling element here is the condition.
    var conditionChildNode = nextSiblingNode.firstChild;

    while (conditionChildNode) {
      conditionSiblingNodes.push(conditionChildNode);
      conditionChildNode = conditionChildNode.nextSibling;
    }

    // Find the next sibling after the condition, i.e. the arguments.
    var siblingNodes = [];
    nextSiblingNode = nextSiblingNode.nextSibling;

    while (nextSiblingNode) {
      siblingNodes.push(nextSiblingNode);
      nextSiblingNode = nextSiblingNode.nextSibling;
    }

    var funcElement = new Elements.FunctionElement(funcName, funcEncoding, funcUrl);
    funcElement.hasSubscript = conditionSiblingNodes.length > 0;

    equation.insertElement(funcElement);
    equation.setCurrentElement(funcElement.getSubscript());

    conditionSiblingNodes.forEach(function (node, index) {
      if (index > 0) {
        equation.insertElement(new Elements.SeparatorElement());
      }

      processNode(node, equation);
    });

    equation.setCurrentElement(funcElement.getCsymbolArguments());

    siblingNodes.forEach(function (node, index) {
      if (index > 0) {
        equation.insertElement(new Elements.SeparatorElement());
      }

      processNode(node, equation);
    });

    equation.setCurrentElement(funcElement);
  }
}


export const Parser = {
  parse
}