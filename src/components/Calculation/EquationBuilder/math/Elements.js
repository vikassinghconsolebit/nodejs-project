//-------------------------------------------------------------------------------------------------------------
// Element
//-------------------------------------------------------------------------------------------------------------

import {Common} from "./Commons";
import { OperandElement } from "./Element/OperandElement";
import { DivideElement } from "./Element/DivideElement";
import {ElementCollection} from "./ElementCollection";
import { PowerElement } from "./Element/PowerElement";
import { FunctionElement } from "./Element/FunctionElement";
import { IterationElement } from "./Element/IterationElement";
import { FenceElement } from "./Element/FenceElement";
import { SingleOperandElement } from "./Element/SingleOperandElement";
import { EmptyElement } from "./Element/EmptyElement";
import { CardinalityElement } from "./Element/CardinalityElement.js";
import { ExponentialElement } from "./Element/ExponentialElement";
import { OperatorElement } from "./Element/OperatorElement";
import { SeparatorElement } from "./Element/SeparatorElement";
import { ForallElement } from "./Element/ForallElement";
import { IntervalSubElement } from "./Element/IntervalSubElement";
import { LogarithmElement } from "./Element/LogarithmElement";
import { NaturalLogarithmElement } from "./Element/NaturalLogarithmElement";
import { ModuloElement } from "./Element/ModuloElement";
import { ArctanElement } from "./Element/ArctanElement";
import { AbsoluteElement } from "./Element/AbsoluteElement";
import { FloorElement } from "./Element/FloorElement";
import { IntervalElement } from "./Element/IntervalElement";
import { FilterSetElement } from "./Element/FilterSetElement";
import { MaxMinElement } from "./Element/MaxMinElement";
import { IdentifierSubElement } from "./Element/IdentifierSubElement";
import { SetListElement } from "./Element/SetListElement";
import { UnaryMinusElement } from "./Element/UnaryMinusElement";
import { NotElement } from "./Element/NotElement";
import { MaxElement } from "./Element/MaxElement";
import { MinusElement } from "./Element/MinusElement";
import { SumElement } from "./Element/SumElement";
import { ArbitraryUnionElement } from "./Element/ArbitraryUnionElement";
import { ArbitraryIntersectElement } from "./Element/ArbitraryIntersectElement";
import { NumberElement } from "./Element/NumberElement";
import { IdentifierElement } from "./Element/IdentifierElement";
import { TimesElement } from "./Element/TimesElement";
import { PlusElement } from "./Element/PlusElement";
import { AndElement } from "./Element/AndElement";
import { OrElement } from "./Element/OrElement";
import { RelationElement } from "./Element/RelationElement";
import { EqualElement } from "./Element/EqualElement";
import { GreaterEqualThanElement } from "./Element/GreaterEqualThanElement";
import { LessEqualThanElement } from "./Element/LessEqualThanElement";
import { GreaterThanElement } from "./Element/GreaterThanElement";
import { LessThanElement } from "./Element/LessThanElement";
import { NotEqualElement } from "./Element/NotEqualElement";
import { FullEqualsElement } from "./Element/FullEqualsElement";
import { ObjectEqualsElement } from "./Element/ObjectEqualsElement";
import { ProperSupersetElement } from "./Element/ProperSupersetElement";
import { SupersetElement } from "./Element/SupersetElement";
import { ProperSubsetElement } from "./Element/ProperSubsetElement";
import { SubsetElement } from "./Element/SubsetElement";
import { SetOverlapElement } from "./Element/SetOverlapElement";
import { SetRelationElement } from "./Element/SetRelationElement";
import { InElement } from "./Element/InElement";
import { InVersionsElement } from "./Element/InVersionsElement";
import { InObjectsElement } from "./Element/InObjectsElement";
import { NotInElement } from "./Element/NotInElement";
import { UnionElement } from "./Element/UnionElement";
import { IntersectElement } from "./Element/IntersectElement";
import { SetDiffElement } from "./Element/SetDiffElement";

export function Element(parent) {
  this.parent = parent || null;
  this.children = new ElementCollection(this);
  this.precedence = 50;
  this.isSelectable = true;
  this.caretPosition = Common.CaretPosition.None;
  this.visualParent = null;
  this.location = null; // Set by renderer.
  this.size = null; // Set by renderer.
}

Element.prototype.className = 'Element';

Element.prototype.getFlattenedElementTree = function () {
  var result = [];

  (function traverse(element, array) {
    if (element instanceof OperatorElement) {
      traverse(element.getLeftOperand(), array);
      array.push(element);
      traverse(element.getRightOperand(), array);
    } else if (element) {
      array.push(element);
    }
  }(this, result));

  return result;
};

Element.prototype.getAbsoluteLocation = function () {
  var absoluteLocation = {x: 0, y: 0};
  var element = this;

  while (element.parent) {
    absoluteLocation.x += element.location.x;
    absoluteLocation.y += element.location.y;
    element = element.parent;
  }

  return absoluteLocation;
};

Element.prototype.containsPosition = function (position) {
  // Element location is relative to parent, so must calculate absolute position.
  var absolute = this.getAbsoluteLocation();

  return position.x >= absolute.x && position.x < (absolute.x + this.size.width) &&
    position.y >= absolute.y && position.y < (absolute.y + this.size.height);
};

/**
 * Gets the element visually to the left of this element.
 */
Element.prototype.getLeftVisualElement = function () {
  return this.visualParent ? this.visualParent.getLeftVisualElementForChild(this) : null;
};

/**
 * Gets the element visually to the left of a child of this element.
 */
Element.prototype.getLeftVisualElementForChild = function (child) {
  return null;
};

/**
 * Gets the element visually to the right of this element.
 */
Element.prototype.getRightVisualElement = function () {
  return this.visualParent ? this.visualParent.getRightVisualElementForChild(this) : null;
};

/**
 * Gets the element visually to the right of a child of this element.
 */
Element.prototype.getRightVisualElementForChild = function (child) {
  return null;
};

/**
 * Gets the element visually above or inside this element.
 */
Element.prototype.getUpVisualElement = function () {
  var result = this.getInsideVisualElement();

  if (!result) {
    result = this.visualParent ? this.visualParent.getAboveVisualElementForChild(this) : null;
  }

  return result;
};

/**
 * Gets the element visually inside this element.
 */
Element.prototype.getInsideVisualElement = function () {
  return null;
};

/**
 * Gets the element visually above a child of this element.
 */
Element.prototype.getAboveVisualElementForChild = function (child) {
  return {
    element: this,
    caretPosition: child.caretPosition !== Common.CaretPosition.None ?
      child.caretPosition : Common.CaretPosition.Right
  };
};

/**
 * Gets the element visually below or subscripted to this element.
 */
Element.prototype.getDownVisualElement = function () {
  var result = this.getBelowVisualElement();

  if (!result) {
    result = this.visualParent ? this.visualParent.getBelowVisualElementForChild(this) : null;
  }

  return result;
};

/**
 * Gets the element visually below this element.
 */
Element.prototype.getBelowVisualElement = function () {
  return null;
};

/**
 * Gets the element visually below a child of this element.
 */
Element.prototype.getBelowVisualElementForChild = function (child) {
  return this.visualParent ? this.visualParent.getBelowVisualElementForChild(this) : null;
};

/**
 * Gets the element visually to the left of a child of this element from a list of visual elements.
 */
Element.prototype.getLeftVisualElementInList = function (child, visualElements) {
  var result = null;
  var childIndex = visualElements.indexOf(child);

  if (childIndex > 0) {
    result = {
      element: visualElements[childIndex - 1],
      caretPosition: Common.CaretPosition.Right
    };
  } else if (childIndex === 0) {
    result = {
      element: this,
      caretPosition: Common.CaretPosition.Left
    };
  }

  return result;
};

/**
 * Gets the element visually to the right of a child of this element from a list of visual elements.
 */
Element.prototype.getRightVisualElementInList = function (child, visualElements) {
  var result = null;
  var childIndex = visualElements.indexOf(child);

  if (childIndex >= 0 && childIndex < visualElements.length - 1) {
    result = {
      element: visualElements[childIndex + 1],
      caretPosition: Common.CaretPosition.Left
    };
  } else if (childIndex === visualElements.length - 1) {
    result = {
      element: this,
      caretPosition: Common.CaretPosition.Right
    };
  }

  return result;
};

/**
 * Gets the element visually inside this element from a list of visual elements.
 */
Element.prototype.getInsideVisualElementInList = function (visualElements) {
  var result = null;

  if (this.caretPosition === Common.CaretPosition.Right) {
    result = {
      element: visualElements[visualElements.length - 1],
      caretPosition: Common.CaretPosition.Right
    };
  } else {
    result = {
      element: visualElements[0],
      caretPosition: Common.CaretPosition.Left
    };
  }

  return result;
};

/**
 * Gets the element visually below this element from a list of visual elements.
 */
Element.prototype.getBelowVisualElementInList = function (visualElements) {
  return {
    element: this.caretPosition === Common.CaretPosition.Right ?
      visualElements[visualElements.length - 1] : visualElements[0],
    caretPosition: this.caretPosition
  };
};


//-------------------------------------------------------------------------------------------------------------
// Public constructors
//-------------------------------------------------------------------------------------------------------------
export const Elements = {
  Element : Element,
  OperandElement : OperandElement, // Done
  FenceElement : FenceElement, // Done
  SetListElement : SetListElement,
  ForallElement : ForallElement,
  IntervalSubElement : IntervalSubElement,
  UnaryMinusElement : UnaryMinusElement,
  DivideElement : DivideElement, //Done
  PowerElement : PowerElement, //Done
  LogarithmElement : LogarithmElement,
  NaturalLogarithmElement : NaturalLogarithmElement,
  ModuloElement : ModuloElement,
  ArctanElement : ArctanElement,
  NotElement : NotElement,
  CardinalityElement : CardinalityElement,
  AbsoluteElement : AbsoluteElement,
  FloorElement : FloorElement,
  IntervalElement : IntervalElement,
  FunctionElement : FunctionElement,
  FilterSetElement : FilterSetElement,
  MaxMinElement : MaxMinElement,
  MaxElement : MaxElement,
  MinElement : MinusElement,
  IterationElement : IterationElement,
  SumElement : SumElement,
  ArbitraryUnionElement : ArbitraryUnionElement,
  ArbitraryIntersectElement : ArbitraryIntersectElement,
  SingleOperandElement : SingleOperandElement,
  NumberElement : NumberElement,
  EmptyElement : EmptyElement,
  IdentifierElement : IdentifierElement,
  IdentifierSubElement : IdentifierSubElement,
  ExponentialElement : ExponentialElement,
  OperatorElement : OperatorElement,
  SeparatorElement : SeparatorElement,
  TimesElement : TimesElement,
  PlusElement : PlusElement,
  MinusElement : MinusElement,
  AndElement : AndElement,
  OrElement : OrElement,
  RelationElement : RelationElement,
  EqualElement : EqualElement,
  GreaterEqualThanElement : GreaterEqualThanElement,
  LessEqualThanElement : LessEqualThanElement,
  GreaterThanElement : GreaterThanElement,
  LessThanElement : LessThanElement,
  NotEqualElement : NotEqualElement,
  FullEqualsElement : FullEqualsElement,
  ObjectEqualsElement : ObjectEqualsElement,
  ProperSupersetElement : ProperSupersetElement,
  SupersetElement : SupersetElement,
  ProperSubsetElement : ProperSubsetElement,
  SubsetElement : SubsetElement,
  SetOverlapElement : SetOverlapElement,
  SetRelationElement : SetRelationElement,
  InElement : InElement,
  InVersionsElement : InVersionsElement,
  InObjectsElement : InObjectsElement,
  NotInElement : NotInElement,
  UnionElement : UnionElement,
  IntersectElement : IntersectElement,
  SetDiffElement : SetDiffElement,
}
