import { Elements } from '../Elements'
import { Common } from '../Commons';
import { getNodeType } from '../Parser';

export var ElementFactory = {
    fence: function (node) {
        // debugger
        var fence = node.getAttributeNS(Common.Editor.NamespaceUri, Common.Editor.Attribute.Fence);


        if (fence && fence.length > 0) {
            return Elements.FenceElement.createElement(fence);
        }

        return null;
    },

    apply: function (node) {
        return ElementFactory.fence(node);
    },

    fen: function (node) {
        return ElementFactory.fence(node);
    },

    ci: function (node) {
        var innerText = node.textContent;
        var type = node.getAttribute('type');
        var element = null;
        if ((innerText.length > 0 && innerText !== Elements.EmptyElement.EmptySymbol) ||
            (innerText.length === 0 && type === 'constant')) {
            element = new Elements.IdentifierElement(innerText, type);
        } else {
            element = new Elements.EmptyElement();
        }

        var fence = ElementFactory.fence(node);

        if (fence) {
            fence.getContainer().children.add(element);
            element = fence;
        }

        return element;
    },

    cn: function (node) {
        var innerText = node.textContent;
        var element = null
        if (innerText.length > 0) {
            element = new Elements.NumberElement(innerText);
        } else {
            element = new Elements.EmptyElement();
        }

        var fence = ElementFactory.fence(node);

        if (fence) {
            fence.getContainer().children.add(element);
            element = fence;
        }

        return element;
    },

    eq: function () {
        return new Elements.EqualElement();
    },

    geq: function () {
        return new Elements.GreaterEqualThanElement();
    },

    gt: function () {
        return new Elements.GreaterThanElement();
    },

    lt: function () {
        return new Elements.LessThanElement();
    },

    leq: function () {
        return new Elements.LessEqualThanElement();
    },

    neq: function () {
        return new Elements.NotEqualElement();
    },

    plus: function () {
        return new Elements.PlusElement();
    },

    minus: function () {
        return new Elements.MinusElement();
    },

    times: function () {
        return new Elements.TimesElement();
    },

    divide: function () {
        return new Elements.DivideElement();
    },

    power: function () {
        return new Elements.PowerElement();
    },

    log: function () {
        return new Elements.LogarithmElement();
    },

    ln: function () {
        return new Elements.NaturalLogarithmElement();
    },

    exponentiale: function () {
        return new Elements.ExponentialElement();
    },

    rem: function () {
        return new Elements.ModuloElement();
    },

    arctan: function () {
        return new Elements.ArctanElement();
    },

    sum: function () {
        return new Elements.SumElement();
    },

    in: function (node) {
        switch (node.getAttributeNS(Common.NamespaceUri, Common.Editor.Attribute.KeyType)) {
            case 'versions':
                return new Elements.InVersionsElement();
            case 'objects':
                return new Elements.InObjectsElement();
            default:
                return new Elements.InElement();
        }
    },

    notin: function () {
        return new Elements.NotInElement();
    },

    and: function () {
        return new Elements.AndElement();
    },

    or: function () {
        return new Elements.OrElement();
    },

    set: function () {
        return new Elements.SetElement();
    },

    setdiff: function () {
        return new Elements.SetDiffElement();
    },

    prsubset: function () {
        return new Elements.ProperSubsetElement();
    },

    subset: function () {
        return new Elements.SubsetElement();
    },

    not: function () {
        return new Elements.NotElement();
    },

    union: function (node) {
        if (node.nextSibling && getNodeType(node.nextSibling) === 'condition') {
            return new Elements.ArbitraryUnionElement();
        } else {
            return new Elements.UnionElement();
        }
    },

    intersect: function (node) {
        if (node.nextSibling && getNodeType(node.nextSibling) === 'condition') {
            return new Elements.ArbitraryIntersectElement();
        } else {
            return new Elements.IntersectElement();
        }
    },

    card: function () {
        return new Elements.CardinalityElement();
    },

    min: function () {
        return new Elements.MinElement();
    },

    max: function () {
        return new Elements.MaxElement();
    },

    abs: function () {
        return new Elements.AbsoluteElement();
    },

    floor: function () {
        return new Elements.FloorElement();
    },

    interval: function () {
        return new Elements.IntervalElement();
    },

    csymbol: function (node) {
        switch (node.textContent.toLowerCase()) {
            case 'fulleq':
                return new Elements.FullEqualsElement();
            case 'classeq':
                return new Elements.ObjectEqualsElement();
            case 'prsuperset':
                return new Elements.ProperSupersetElement();
            case 'superset':
                return new Elements.SupersetElement();
            case 'overlap':
                return new Elements.SetOverlapElement();
            default:
                return null;
        }
    },
};