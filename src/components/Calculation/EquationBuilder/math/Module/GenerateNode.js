import { setEditorAttribute } from "../Builder";
import { Common } from "../Commons";


function createXmlElement(name) {
    return document.createElementNS(Common.NamespaceUri, name);

}

/**
 * Private.
 * Factory functions for the various xml node types.
 */

export var NodeFactory = {

    createApplyNode: function () {
        return createXmlElement('apply');
    },

    createEqualNode: function () {
        return createXmlElement('eq');
    },

    createConditionNode: function () {
        return createXmlElement('condition');
    },

    createOrNode: function () {
        return createXmlElement('or');
    },

    createAndNode: function () {
        return createXmlElement('and');
    },

    createSetNode: function () {
        return createXmlElement('set');
    },

    createSetListNode: function () {
        var setNode = NodeFactory.createSetNode();
        setEditorAttribute(Common.Editor.Attribute.SetList, 'true', setNode);
        return setNode;
    },

    createForallNode: function () {
        return createXmlElement('forall');
    },

    createIntervalNode: function () {
        var intervalNode = createXmlElement('interval');
        intervalNode.setAttribute('closure', 'closed-open');
        return intervalNode;
    },

    createSelectorNode: function () {
        return createXmlElement('selector');
    },

    createMinusNode: function () {
        return createXmlElement('minus');
    },

    createDivideNode: function () {
        return createXmlElement('divide');
    },

    createPowerNode: function () {
        return createXmlElement('power');
    },

    createLogarithmNode: function () {
        return createXmlElement('log');
    },

    createNaturalLogarithmNode: function () {
        return createXmlElement('ln');
    },

    createModuloNode: function () {
        return createXmlElement('rem');
    },

    createArctanNode: function () {
        return createXmlElement('arctan');
    },

    createNotNode: function () {
        return createXmlElement('not');
    },

    createCardinalityNode: function () {
        return createXmlElement('card');
    },

    createAbsoluteNode: function () {
        return createXmlElement('abs');
    },

    createFloorNode: function () {
        return createXmlElement('floor');
    },

    createMaxNode: function () {
        return createXmlElement('max');
    },

    createMinNode: function () {
        return createXmlElement('min');
    },

    createSumNode: function () {
        return createXmlElement('sum');
    },

    createUnionNode: function () {
        return createXmlElement('union');
    },

    createIntersectNode: function () {
        return createXmlElement('intersect');
    },

    createNumberNode: function (value) {
        var numberNode = createXmlElement('cn');

        if (!value) {
            value = '?';
        }

        numberNode.appendChild(document.createTextNode(value));
        return numberNode;
    },

    createIdentifierNode: function (value, type) {
        var identifierNode = createXmlElement('ci');
        identifierNode.setAttribute('type', type);

        if (!value && type !== 'constant') {
            value = '?';
        }

        identifierNode.appendChild(document.createTextNode(value));
        return identifierNode;
    },

    createExponentialNode: function () {
        return createXmlElement('exponentiale');
    },

    createSeparatorNode: function () {
        var selectorNode = NodeFactory.createSelectorNode();
        setEditorAttribute(Common.Editor.Attribute.Separator, 'true', selectorNode);
        return selectorNode;
    },

    createTimesNode: function () {
        return createXmlElement('times');
    },

    createPlusNode: function () {
        return createXmlElement('plus');
    },

    createGreaterEqualThanNode: function () {
        return createXmlElement('geq');
    },

    createLessEqualThanNode: function () {
        return createXmlElement('leq');
    },

    createGreaterThanNode: function () {
        return createXmlElement('gt');
    },

    createLessThanNode: function () {
        return createXmlElement('lt');
    },

    createNotEqualNode: function () {
        return createXmlElement('neq');
    },

    createCsymbolNode: function (functionName, encoding, definitionUrl) {
        var csymbolNode = createXmlElement('csymbol');
        csymbolNode.setAttribute('encoding', encoding);
        csymbolNode.setAttribute('definitionURL', definitionUrl);
        csymbolNode.appendChild(document.createTextNode(functionName));
        return csymbolNode;
    },

    createFullEqualsNode: function () {
        return NodeFactory.createCsymbolNode('fulleq', 'text', 'http:#');
    },

    createObjectEqualsNode: function () {
        return NodeFactory.createCsymbolNode('classeq', 'text', 'http:#');
    },

    createProperSupersetNode: function () {
        return NodeFactory.createCsymbolNode('prsuperset', 'text', 'http:#');
    },

    createSupersetNode: function () {
        return NodeFactory.createCsymbolNode('superset', 'text', 'http:#');
    },

    createProperSubsetNode: function () {
        return createXmlElement('prsubset');
    },

    createSubsetNode: function () {
        return createXmlElement('subset');
    },

    createSetOverlapNode: function () {
        return NodeFactory.createCsymbolNode('overlap', 'text', 'http:#');
    },

    createInNode: function () {
        return createXmlElement('in');
    },

    createInVersionsNode: function () {
        var inNode = NodeFactory.createInNode();
        setEditorAttribute(Common.Editor.Attribute.KeyType, 'versions', inNode);
        return inNode;
    },

    createInObjectsNode: function () {
        var inNode = NodeFactory.createInNode();
        setEditorAttribute(Common.Editor.Attribute.KeyType, 'objects', inNode);
        return inNode;
    },

    createNotInNode: function () {
        return createXmlElement('notin');
    },

    createSetDiffNode: function () {
        return createXmlElement('setdiff');
    }
};