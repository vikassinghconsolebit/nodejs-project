export var FontFamily = {
    Arial: 'Arial',
    LucidaSansUnicode: 'Lucida Sans Unicode',
    TimesNewRoman: 'Times New Roman'
};

export var Styles = {
    BackgroundColor: '#FFF',    // White
    BackgroundColorRemark: '#90EE90', // Light green
    TextColor: '#000',    // Black
    CaretColor: '#00F',    // Blue
    DefaultFontFamily: FontFamily.Arial,
    DefaultFontSize: 14,
    DefaultFontUnit: 'px',
    DefaultFontLineHeight: 1.15,
    OperatorFontFamily: FontFamily.LucidaSansUnicode,
    SumFontFamily: FontFamily.TimesNewRoman,
    SumFontSize: 20,
    ArbUnIntFontFamily: FontFamily.LucidaSansUnicode,
    ArbUnIntFontSize: 28,
    CanvasPadding: 5,
    ElementPadding: 2,
    VerticalBarWidth: 1,
    HorizontalBarWidth: 1,
    ScaledParenWidth: 1,
    SubscriptScale: 0.9,
    SuperscriptScale: 0.9,
    FilterFenceScale: 0.8,
    ParenWidthHeightRatio: 0.25,
    ParenWidthScale: 0.8,
    IntervalFenceWidth: 5,
    IntervalLineWidth: 1,
    FloorFenceWidth: 4,
    FloorLineWidth: 1,
    SumHeightScale: 0.85,
    ArbUnIntHeightScale: 0.62,
    CaretWidth: 3,
    CurlyBraceLineWidth: 1,
    CurlyBraceWidthRatio: 0.25,
    MaxCurlyBraceWidth: 9,
    MinCurlyBraceWidth: 3,

    // Font adjustments to accommodate differences in browser text rendering.
    ArialFirefoxVerticalAdjust: 2,
    LucidaSansUnicodeFirefoxVerticalAdjust: 1,
    LucidaSansUnicodeChromeIEVerticalAdjust: 1
    // console.log
};
