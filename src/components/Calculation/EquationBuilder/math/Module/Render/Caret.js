import { Common } from "../../Commons";
import { Elements } from "../../Elements";
import { Styles } from "../Styles";


function fillRect(x, y, width, height, gfx) {
  x = Math.floor(x);
  y = Math.floor(y);
  width = Math.floor(width);
  height = Math.floor(height);
  gfx.fillRect(x, y, width, height);
}


export const renderCaret = function (ctx, visible) {
    var element = ctx.equation.currentElement;
  
    if (!element) {
      return;
    }
  
    var gfx = ctx.canvas.getContext('2d');
    var location = element.getAbsoluteLocation();
    var size = (element instanceof Elements.OperatorElement) ? element.operatorSize : element.size;
  
    gfx.save();
  
    // Use fillRect for drawing lines without antialiasing on line ends, because
    // antialiased ends don't get properly cleared when redrawing the line with a lighter color...
    gfx.fillStyle = visible ? Styles.CaretColor : Styles.BackgroundColor;
  
    // Draw line under element.
    fillRect(location.x, location.y + size.height, size.width + Styles.CaretWidth, Styles.CaretWidth, gfx);
  
    if (element.caretPosition === Common.CaretPosition.Left) {
      // Draw line on left side of element.
      fillRect(location.x, location.y, Styles.CaretWidth, size.height + Styles.CaretWidth, gfx);
    } else if (element.caretPosition === Common.CaretPosition.Right) {
      // Draw line on right side of element.
      fillRect(location.x + size.width, location.y, Styles.CaretWidth, size.height + Styles.CaretWidth, gfx);
    }
  
    gfx.restore();
  };