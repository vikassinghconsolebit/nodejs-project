import { BrowserList } from "./DetectBrowser";

/**
 * Private.
 * Sets the width of the matheq canvas.
 *
 * @param canvas: The canvas DOM object.
 * @param $canvas: The canvas jQuery object.
 * @param width: The new width in pixels.
 */

export function setCanvasWidth(canvas, width) {
    canvas.width = width;

    // Also set css width to prevent blurry text in Chrome and Firefox.
    // However, this makes the text blurry in IE, so must have browser check.
    if (BrowserList.chrome || BrowserList.firefox) {
        canvas.style.minWidth = width

        reflowCanvas(canvas);
    }
}

/**
 * Reflows the canvas, i.e. makes sure the browser updates its layout after resizing the canvas.
 *
 * @param $canvas: The canvas jQuery object.
 */

export function reflowCanvas(canvas) {
    // Fix stupid Chrome browser.
    if (BrowserList.chrome) {
        // Force a browser reflow to update the canvas html element after resizing it.
        // $canvas.hide().show(0);
        canvas.style.display = "block";
    }

}

/**
 * Private.
 * Sets the height of the matheq canvas.
 *
 * @param canvas: The canvas DOM object.
 * @param $canvas: The canvas jQuery object.
 * @param height: The new height in pixels.
 */

export function setCanvasHeight(canvas, height) {
    canvas.height = height;
  
    // Also set css height to prevent blurry text in Chrome and Firefox.
    // However, this makes the text blurry in IE, so must have browser check.
    if (BrowserList.chrome || BrowserList.firefox) {
      // $canvas.css('min-height', height);
      canvas.style.minHeight = height
      reflowCanvas(canvas);
    }
    // canvas.style.minHeight = height
    // reflowCanvas(canvas);
  }