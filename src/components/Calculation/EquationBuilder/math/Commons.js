export const Common = {
  NamespaceUri: 'http://www.w3.org/1998/Math/MathML',
  NamespacePrefix: '',
  Editor: {
    NamespaceUri: 'http://www.tietoenator.com/ec/alloc/MathMLEditor',
    NamespacePrefix: 'ecm',
    Attribute: {
      Fence: 'fence',
      Separator: 'sep',
      SetList: 'IsSetList',
      KeyType: 'keytype',
      ReadOnly: 'readOnly'
    }
  },
  CaretPosition: {
    Left: 0,
    Right: 1,
    Under: 2,
    None: 3
  }
}


