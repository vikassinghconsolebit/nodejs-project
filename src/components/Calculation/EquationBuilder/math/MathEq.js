/**
 * The mathml equation component. Wraps the mathml and its canvas with methods for setting/getting mathml,
 * rendering, resizing, etc.
 */
import { Builder } from "./Builder";
import { Parser } from "./Parser";
import { Equation } from "./Equation";
import { Renderer } from "./Renderer";
import $ from 'jquery';
import { setCanvasHeight, setCanvasWidth } from "./Module/Canvas";
//-----------------------------------------------------------------------------------------------------------------
// MathEqReg:
// Registry for MathEq instances.
//-----------------------------------------------------------------------------------------------------------------

/**
 * Private.
 * Creates an equation instance from a mathml string.
 * If the provided mathml is not proper mathml, an empty equation is returned.
 *
 * @param: The mathml to parse.
 * @returns An equation instance.
 */

function parse(mathml) {
  var equation = Parser.parse(mathml);

  // For dev testing only.
  // Tests that parsed and rebuild mathml is identical to input mathml.
  if (equation) {
    var buildXml = Builder.build(equation);

    //  debugger
    // Get input xml with same formatting as build xml.
    const parser = new DOMParser();
    const doc = parser.parseFromString(mathml, "text/xml");

    //  var origXml = new XMLSerializer().serializeToString($.parseXML(mathml));
    var origXml = new XMLSerializer().serializeToString(doc);


    if (origXml !== buildXml) {
    }
  }

  // Return empty equation if mathml was unparsable.
  equation = equation || new Equation();
  equation.setCurrentElement(null);

  return equation;
}




export const MathEqReg = {
  reg: {},

  /**
   * Registers a MathEq instance in the registry. This prevents the MathEq instance from being garbage
   * collected until it is either unregistered or a new instance with the same id is registered.
   */
  register: function (id, matheq) {
    if (matheq instanceof MathEq) {
      MathEqReg.reg[id] = matheq;
    }
  },

  /**
   * Unregisters a MathEq instance from the internal registry.
   */
  unregister: function (id) {
    delete MathEqReg.reg[id];
  },

  /**
   * Fetches a MathEq instance from the registry.
   */
  get: function (id) {
    return MathEqReg.reg[id];
  }
};

//-----------------------------------------------------------------------------------------------------------------
// Window resize handling.
//-----------------------------------------------------------------------------------------------------------------

/**
 * Resizes the last equation table column upon window resize event.
 */
// $(window).on('resize ecScreenResize', () => {
//   var matheqsToResize = [];

//   Object.keys(MathEqReg.reg).forEach(function (key) {
//     var matheq = MathEqReg.reg[key];

//     if (matheq.isInCell()) {
//       var $canvasInLastColumn = matheq.$canvas.parentsUntil('tr').last().parent().find('td:last-child canvas');

//       if ($canvasInLastColumn.attr('id') === matheq.canvasId) {
//         // Set canvas width to 0 to support making the row smaller.
//         setCanvasWidth(matheq.canvas, matheq.$canvas, 0);
//         matheqsToResize.push(matheq);
//       }
//     }
//   });

//   matheqsToResize.forEach(function (matheq) {
//     if (matheq.resize()) {
//       matheq.render();
//     }
//   });
// });

window.addEventListener('resize ecScreenResize', () => {
  var matheqsToResize = [];

  Object.keys(MathEqReg.reg).forEach(function (key) {
    var matheq = MathEqReg.reg[key];

    if (matheq.isInCell()) {
      var $canvasInLastColumn = matheq.$canvas.parentsUntil('tr').last().parent().find('td:last-child canvas');

      if ($canvasInLastColumn.attr('id') === matheq.canvasId) {
        // Set canvas width to 0 to support making the row smaller.
        setCanvasWidth(matheq.canvas, matheq.$canvas, 0);
        matheqsToResize.push(matheq);
      }
    }
  });

  matheqsToResize.forEach(function (matheq) {
    if (matheq.resize()) {
      matheq.render();
    }
  });
});

//-----------------------------------------------------------------------------------------------------------------
// MathEq:
//-----------------------------------------------------------------------------------------------------------------

/**
 * Constructs a new MathEq instance.
 *
 * @param args: An object holding initialization values.
 *   args = {
 *     canvasId: DOM id of the canvas element to render on. Mandatory.
 *     mathmlId: DOM id of the input element holding the mathml value. Optional if not databound.
 *     context : 'cell' if the canvas is inside a table cell,
 *               'dialog' if it is inside a dialog window,
 *               'free' if not limited by parent bounds. Mandatory.
 *     editable: true if the canvas should be interactive, false if it should just render the equation. Optional.
 *               Default is false.
 *     sourceId: JSF component client id. Mandatory when editable is true.
 *     equationType : 'ITERATIONS', 'CONDITION', or 'EQUATION'. Optional.
 *     calculationId: The object id of the calculation this equation belongs to. Mandatory when editable is true.
 *     calcContextId:
￼
The object id of the calculation context of this calculation. Mandatory when editable is true.
 *     calcDaytime  : The daytime of this calculation. Mandatory when editable is true.
 *   }
 */

export class MathEq {
  constructor(args) {
    // debugger
    this.canvasId = args.canvasId;
    this.mathmlId = args.mathmlId;
    this.context = args.context;
    this.editable = !!args.editable;
    this.sourceId = args.sourceId;
    this.equationType = args.equationType;
    this.calculationId = args.calculationId;
    this.calcContextId = args.calcContextId;
    this.calcDaytime = args.calcDaytime;
    //  this.$canvas = $('[id="' + this.canvasId + '"]');
    this.canvas = document.getElementById(`${this.canvasId}`);

    // Parse initial mathml.
    if (this.mathmlId) {
      this.parse(this.getMathml());
    }
  }

  /**
   * Gets the mathml.
   *
   * @returns The current mathml as a string.
   */
  getMathml = () => {
    return document.getElementById(this.mathmlId).value
  };

  /**
   * Sets the mathml. This method does not parse or render the mathml.
   *
   * @param mathml: The new mathml as a string.
   */
  setMathml = (mathml) => {
    // debugger
    //  var $input = $('[id="' + this.mathmlId + '"]');
    // var $input = $('[id="' + this.mathmlId + '"]');
    // localStorage.setItem("mathml",mathml);
    var input = document.getElementById(`${this.mathmlId}`);

    // $input.val(mathml);
    // $input.change();
    input.value = mathml;
    // input.onchange();
  };

  /**
   * Sets the mathml. The mathml is parsed and rendered immediately.
   *
   * @param mathml: The new mathml as a string.
   * @param skipRender: Whether or not to skip rendering. If true only parsing will be performed.
   */
  reset = (mathml, skipRender) => {
    this.parse(mathml);
    this.refresh(skipRender);
  };

  /**
   * Rebuilds the internal mathml from the current equation and renders the equation.
   *
   * @param skipRender: Whether or not to skip rendering.
   */
  refresh = (skipRender) => {
    this.setMathml(Builder.build(this.equation));

    if (!skipRender) {
      this.resize();
      this.render();
    }
  };

  /**
   * Parses a mathml string and replaces the internal equation.
   *
   * @param mathml: The mathml to parse.
   */
  parse = (mathml) => {
    this.equation = parse(mathml);
  };

  /**
   * Parses and inserts mathml at the equation's currently selected element.
   *
   * @param mathml: The mathml string to be inserted.
   */
  insertMathml = (mathml) => {
    parse(mathml, this.equation);
  };

  /**
   * Checks if the matheq component is located inside a table cell.
   *
   * @returns true if in a cell, false if not.
   */
  isInCell = () => {
    return this.context === 'cell';
  };

  /**
   * Checks if the matheq component is located inside a dialog.
   *
   * @returns true if in a dialog, false if not.
   */
  isInDialog = () => {
    return this.context === 'dialog';
  };

  /**
   * Checks if the matheq component is standalone, i.e. not embedded inside limiting parent containers.
   *
   * @returns true if free, false if not.
   */
  isStandalone = () => {
    return this.context === 'free';
  };

  /**
   * Resizes the matheq component.
   * The matheq canvas will be resized to fit within its container.
   * If the container is a table cell, other matheq components in the same table row may also be resized.
   *
   * @returns true if the matheq component size was changed, false if not.
   */
  resize = () => {
    var resized = this.equation.resize();

    if (this.isInCell()) {
      resized = this.resizeToFitCell() || resized;
      this.alignRowCanvases();
    } else if (this.isInDialog()) {
      resized = this.resizeToFitDialog() || resized;
    } else if (this.isStandalone()) {
      resized = this.resizeToFitEquation() || resized;
    }
    return resized;
  };

  /**
   * Resizes the matheq component to fit within its parent table cell.
   *
   * @returns true if the matheq component size was changed, false if not.
   */
  resizeToFitCell = () => {
    debugger
    var width = this.$canvas.parent().width() - (this.$canvas.outerWidth(true) - this.$canvas.width());
    var height = this.equation.getEquationSize().height;

    if (width !== this.canvas.width || height !== this.canvas.height) {
      setCanvasWidth(this.canvas, width);
      setCanvasHeight(this.canvas, height);

      return true;
    }

    return false;
  };

  /**
   * Adjusts the height of all canvases in the same table row as this matheq component
   * to be equal to the height of the highest canvas in the row.
   */
  alignRowCanvases = () => {
    var $canvases = this.$canvas.parentsUntil('tr').last().parent().find('canvas');
    var highest = 0;

    // Find highest equation.
    $canvases.each(() => {
      var $canvas = $(this);
      var matheq = MathEqReg.get($canvas.data('matheqid'));
      var height = null
      if (matheq) {
        height = matheq.equation.getEquationSize().height;
      } else {
        // Use plain canvas height if matheq has not yet been created.
        var canvas = $canvas.get(0);
        height = canvas.height;
      }

      if (height > highest) {
        highest = height;
      }
    });

    // Adjust canvas heights.
    var $this = this;
    $canvases.each(() => {
      var $canvas = $(this);
      var canvas = $canvas.get(0);

      if (canvas.height !== highest) {
        setCanvasHeight(canvas, $canvas, highest);

        // Canvas is automatically cleared when resizing, so must also rerender.
        var matheq = MathEqReg.get($canvas.data('matheqid'));

        if (matheq && matheq !== $this) {
          // Only rerender other components. We will rerender ourself later.
          matheq.render();
        }
      }
    });
  };

  /**
   * Resizes the matheq component to fit within its parent dialog.
   *
   * @returns true if the matheq component size was changed, false if not.
   */
  resizeToFitDialog = () => {
    var $parent = this.$canvas.parent();
    var width = this.equation.getEquationSize().width;
    var height = $parent.height();

    if (width !== this.canvas.width || height !== this.canvas.height) {
      setCanvasWidth(this.canvas, width);
      setCanvasHeight(this.canvas, height);

      return true;
    }

    return false;
  };

  /**
   * Resizes the matheq component to the size of its equation.
   *
   * @returns true if the matheq component size was changed, false if not.
   */
  resizeToFitEquation = () => {
    var width = this.equation.getEquationSize().width;
    var height = this.equation.getEquationSize().height;

    if (width !== this.canvas.width || height !== this.canvas.height) {
      setCanvasWidth(this.canvas, width);
      setCanvasHeight(this.canvas, height);

      return true;
    }

    return false;
  };

  /**
   * Gets a render context for rendering this matheq component.
   *
   * @returns A render context object.
   */
  getRenderContext = () => {
    return {
      equation: this.equation,
      canvas: this.canvas,
      editable: this.editable
    };
  };

  /**
   * Renders the matheq component.
   */
  render = () => {
    Renderer.render(this.getRenderContext());
  };

}
