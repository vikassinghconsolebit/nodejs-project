import {Common} from "./Commons";
import {Elements} from "./Elements";

export class ElementCollection {
  constructor(owner, elements) {
    this.owner = owner || null;
    this.elements = elements || [];

    this.elements.forEach(function (element) {
      element.parent = this.owner;
    }, this);
  }

  /**
   * Removes all elements in the collection and optionally adds new ones.
   *
   * @param args: Each argument is an element to be added.
   */
  reset = (...args) => {
    this.elements = [];

    for (var i = 0; i < args.length; ++i) {
      var element = args[i];
      element.parent = this.owner;
      this.elements.push(element);
    }
  };

  /**
   * Gets the element at a specific index.
   *
   * @param index: The index of the element to get.
   */
  at = (index) => {
    return this.elements[index];
  };

  /**
   * Replaces an element in the collection with a new one.
   *
   * @param oldElement: The element to be removed.
   * @param newElement: The element to be added.
   */
  replace = (oldElement, newElement) => {
    var index = this.elements.indexOf(oldElement);

    if (index >= 0) {
      newElement.parent = this.owner;
      this.elements[index] = newElement;
    }
  };

  /**
   * Adds an element to the collection.
   *
   * @param newElement: The element to be added.
   */
  add = (newElement) => {
    if (this.elements.length === 0) {
      // Add element to parent, since this element cannot contain elements.
      this.owner.parent.children.insert(this.owner, newElement, Common.CaretPosition.Right);
    } else {
      var currentElement;

      // if (this.owner && this.owner instanceof Common.OperatorElement) {
        if (this.owner) {

        var rightOperand = this.owner.getRightOperand();

        if (rightOperand instanceof Elements.EmptyElement) {
          var leftOperand = this.owner.getLeftOperand();

          if (leftOperand instanceof Elements.EmptyElement) {
            currentElement = leftOperand;
          } else {
            currentElement = rightOperand;
          }
        }
      }

      if (!currentElement) {
        // Get the rightmost child element.
        var elements = this.elements[0].getFlattenedElementTree();
        currentElement = elements[elements.length - 1];
      }

      this.insert(currentElement, newElement, Common.CaretPosition.Right);
    }
  };

  /**
   * Inserts an element into the collection next to a given current element.
   *
   * @param currentElement: The element to insert next to.
   * @param newElement: The element to be inserted.
   * @param caretPosition: Which side of currentElement to insert newElement at.
   */
  insert = (currentElement, newElement, caretPosition) => {
    if (currentElement instanceof Elements.EmptyElement) {
      currentElement.parent.children.replace(currentElement, newElement);
    } else if (newElement instanceof Elements.OperatorElement) {
      insertOperator(currentElement, newElement, caretPosition);
      restructureContentTree(currentElement);
    } else {
      insertOperand(currentElement, newElement, caretPosition);
    }
  };
}


/**
 * Private.
 * Inserts an operator element into the collection next to a given current element.
 *
 * @param currentElement: The element to insert next to.
 * @param newElement: The element to be inserted.
 * @param caretPosition: Which side of currentElement to insert newElement at.
 */
function insertOperator(currentElement, newElement, caretPosition) {
  if (currentElement instanceof Elements.OperatorElement) {
    if (caretPosition === Common.CaretPosition.Left) {
      newElement.setLeftOperand(currentElement.getLeftOperand());
      currentElement.setLeftOperand(newElement);
    } else {
      newElement.setRightOperand(currentElement.getRightOperand());
      currentElement.setRightOperand(newElement);
    }
  } else {
    var parentElement = currentElement.parent;

    if (caretPosition === Common.CaretPosition.Left) {
      newElement.setRightOperand(currentElement);
    } else {
      newElement.setLeftOperand(currentElement);
    }

    parentElement.children.replace(currentElement, newElement);
  }
}

/**
 * Private.
 * Inserts an operand element into the collection next to a given current element.
 *
 * @param currentElement: The element to insert next to.
 * @param newElement: The element to be inserted.
 * @param caretPosition: Which side of currentElement to insert newElement at.
 */
function insertOperand(currentElement, newElement, caretPosition) {
  if (currentElement instanceof Elements.OperatorElement) {
    if (caretPosition === Common.CaretPosition.Left) {
      if (currentElement.getLeftOperand() instanceof Elements.EmptyElement) {
        currentElement.setLeftOperand(newElement);
      } else {
        insertOperand(currentElement.getLeftOperand(), newElement, Common.CaretPosition.Right);
      }
    } else {
      if (currentElement.getRightOperand() instanceof Elements.EmptyElement) {
        currentElement.setRightOperand(newElement);
      } else {
        insertOperand(currentElement.getRightOperand(), newElement, Common.CaretPosition.Left);
      }
    }
  } else if (currentElement instanceof Elements.EmptyElement) {
    currentElement.parent.children.replace(currentElement, newElement);
  } else {
    var parentElement = currentElement.parent;

    // Need to insert times element between two operands.
    // var times = new Elements.EmptyElement();
    var times = new Elements.TimesElement();

    if (caretPosition === Common.CaretPosition.Left) {
      times.setLeftOperand(newElement);
      times.setRightOperand(currentElement);
    } else {
      times.setLeftOperand(currentElement);
      times.setRightOperand(newElement);
    }

    parentElement.children.replace(currentElement, times);
  }
}

/**
 * Private.
 * Restructures the operator content tree of an element based on operator precedence.
 *
 * @param element: The element to be restructured.
 */
function restructureContentTree(element) {
  var rootElement = null
  if (element instanceof Elements.OperatorElement) {
    rootElement = element;
  } else {
    rootElement = element.parent;
  }

  // Find operator ancestor.
  while (rootElement instanceof Elements.OperatorElement && rootElement.parent instanceof Elements.OperatorElement) {
    rootElement = rootElement.parent;
  }

  var elements = rootElement.getFlattenedElementTree();
  var rootParentElement = rootElement.parent;

  // Find the new operator root node.
  var newRootElement = restructureContentTreeAux(elements, 0, elements.length - 1);

  if (rootElement !== newRootElement) {
    rootParentElement.children.replace(rootElement, newRootElement);
  }
}

/**
 * Private.
 * Restructures the operator content tree of a list of elements based on operator precedence.
 *
 * @param elements: Elements to be restructured.
 * @param startIndex: Start index of subtree.
 * @param endIndex: End index of subtree.
 * @returns The root element.
 */
function restructureContentTreeAux(elements, startIndex, endIndex) {
  var precedence = 1000;
  var rootIndex = -1;
  var rootOperatorElement = null
  var element = null

  // Find operator with lowest precedence.
  // This operator will be the root element.
  for (var i = startIndex; i <= endIndex; ++i) {
    element = elements[i];

    // Must use <= rather than < to make sure that a - b - c becomes
    // (a - b) - c rather than a - (b - c) = a - b + c
    if (element instanceof Elements.OperatorElement && element.precedence <= precedence) {
      precedence = element.precedence;
      rootOperatorElement = element;
      rootIndex = i;
    }
  }

  if (rootOperatorElement) {
    rootOperatorElement.setLeftOperand(restructureContentTreeAux(elements, startIndex, rootIndex - 1));
    rootOperatorElement.setRightOperand(restructureContentTreeAux(elements, rootIndex + 1, endIndex));
    return rootOperatorElement;
  } else {
    return element;
  }
}


