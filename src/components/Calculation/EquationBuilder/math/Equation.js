/**
 * Equation inherits from Element.
 */
import {Elements} from "./Elements";
import {Common} from "./Commons";


export class Equation extends Elements.Element {
  constructor(props) {
    super(props)
    this.children.reset(new Elements.EmptyElement());
    this.currentElement = this.getRootElement();
    this.isParsingInProgress = false;
    this.visualElements = [];
    this.needsResize = true;
  }

  /**
   * Sets current element, i.e. the element the caret is at.
   *
   * @param element: The mathml element that should become current element.
   */
  setCurrentElement = (element) => {
    if (element && (element.isSelectable || !this.isParsingInProgress)) {
      if (this.currentElement) {
        this.currentElement.caretPosition = Common.CaretPosition.None;
      }

      this.currentElement = element;

      if (this.isParsingInProgress || this.currentElement.caretPosition === Common.CaretPosition.None) {
        // When parsing mathml caret position is to the right to support "running" insertion of elements.
        this.currentElement.caretPosition = Common.CaretPosition.Right;
      }
    } else if (!element) {
      this.currentElement.caretPosition = Common.CaretPosition.None;
      this.currentElement = null;
    }
  };

  /**
   * Inserts an element into the equation at current position.
   *
   * @param element: The mathml element to insert.
   */
  insertElement = (element) => {
    if (element && this.currentElement && this.currentElement.parent) {
      this.currentElement.parent.children.insert(this.currentElement, element, this.currentElement.caretPosition);

      if (element instanceof Elements.OperatorElement && !this.isParsingInProgress &&
        element.caretPosition === Common.CaretPosition.None) {
        if (element.getRightOperand() instanceof Elements.EmptyElement) {
          this.setCurrentElement(element.getRightOperand());
          this.currentElement.caretPosition = Common.CaretPosition.Left;
        } else if (element.getLeftOperand() instanceof Elements.EmptyElement) {
          this.setCurrentElement(element.getLeftOperand());
          this.currentElement.caretPosition = Common.CaretPosition.Right;
        } else {
          this.setCurrentElement(element);
          this.currentElement.caretPosition = Common.CaretPosition.Right;
        }
      } else if (!element.isSelectable && element.children.length > 0) {
        this.setCurrentElement(element.children.at(0));
      } else {
        this.setCurrentElement(element);
      }

      this.needsResize = true;
    }
  };

  /**
   * Removes an element from the equation.
   * Operand elements are replaced with an empty element.
   * Operator elements are replaced with a times operator element, unless both of its children
   * are empty elements, in which case it is replaced with an empty element.
   *
   * @param element: The mathml element to delete.
   */
  deleteElement = (element) => {
    if (element) {
      var currentElement = this.currentElement;

      if (element instanceof Elements.OperandElement) {
        var newElement = new Elements.EmptyElement();
      } else if (element instanceof Elements.OperatorElement) {
        var leftOperand = element.getLeftOperand();
        var rightOperand = element.getRightOperand();

        if (leftOperand instanceof Elements.EmptyElement && rightOperand instanceof Elements.EmptyElement) {
          var newElement = new Elements.EmptyElement();
        } else if (rightOperand instanceof Elements.EmptyElement) {
          var newElement = leftOperand;
        } else if (leftOperand instanceof Elements.EmptyElement) {
          var newElement = rightOperand;
        } else {
          var newElement = new Elements.TimesElement();
          newElement.setLeftOperand(leftOperand);
          newElement.setRightOperand(rightOperand);
        }
      }

      element.parent.children.replace(element, newElement);

      if (element === currentElement) {
        newElement.caretPosition = element.caretPosition;
        this.setCurrentElement(newElement);
      }

      this.needsResize = true;
    }
  };

  /**
   * Replaces the element at current position with a new element.
   *
   * @param newElement: The replacement element.
   */
  replaceCurrentElement = (newElement) => {
    if (newElement instanceof Elements.OperatorElement || this.currentElement instanceof Elements.OperatorElement) {
      newElement.setLeftOperand(this.currentElement.getLeftOperand());
      newElement.setRightOperand(this.currentElement.getRightOperand());
    }

    this.currentElement.parent.children.replace(this.currentElement, newElement);
    newElement.caretPosition = this.currentElement.caretPosition;
    this.setCurrentElement(newElement);

    this.needsResize = true;
  };

  /**
   * Checks if the equation is empty, i.e. contains no elements.
   *
   * @returns true if empty, false if not.
   */
  isEmpty = () => {
    return this.getRootElement() instanceof Elements.EmptyElement;
  };

  /**
   * Gets the equation root element.
   *
   * @returns The root mathml element.
   */
  getRootElement = () => {
    // debugger
    return this.children.at(0);
  };

  /**
   * Sets the equation root element.
   *
   * @param element: The new root mathml element.
   */
  setRootElement = (element) => {
    if (element) {
      this.children.replace(this.getRootElement(), element);
    }
  };

  /**
   * Gets the innermost element underlying a specific position, optionally starting from a given root element.
   */
  getElementByPosition = (position, rootElement) => {
    rootElement = rootElement || this.getRootElement();
    var targetElement = null;

    // Post-order traversal, breaking out as soon as an element is found.
    rootElement.children.elements.some(function (element) {
      targetElement = this.getElementByPosition(position, element);
      
      return !!targetElement;
    }, this);

    if (!targetElement && rootElement.isSelectable && rootElement.containsPosition(position)) {
      targetElement = rootElement;
    }

    return targetElement;
  };

  /**
   * Builds a list of elements that visually lie on the same line.
   */
  buildVisualElementList = () => {
    this.visualElements = this.getRootElement().getFlattenedElementTree();

    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };

  /**
   * Searches through the equation elements looking for an identifier element of type logstmt
   * containing the text REMARK.
   *
   * @returns true if the equation is a remark, false if not.
   */
  isRemark = () => {
    var elements = [this.getRootElement()];

    while (elements.length > 0) {
      var element = elements.shift();

      if (element instanceof Elements.IdentifierElement && element.type === 'logstmt' && element.text === 'REMARK') {
        return true;
      }

      if (element.children.elements.length > 0) {
        elements = elements.concat(element.children.elements);
      }
    }

    return false;
  };

  /**
   * Gets the visual element to the left of a given visual child element.
   *
   * @param child: The visual element.
   * @returns An object holding the visual element and the new caret position, or null.
   */
  getLeftVisualElementForChild = (child) => {
    var result = this.getLeftVisualElementInList(child, this.visualElements);

    if (result.element === this) {
      // Prevent selection of entire equation.
      result = null;
    }

    return result;
  };

  /**
   * Gets the visual element to the right of a given visual child element.
   *
   * @param child: The visual element.
   * @returns An object holding the visual element and the new caret position, or null.
   */
  getRightVisualElementForChild = (child) => {
    var result = this.getRightVisualElementInList(child, this.visualElements);

    if (result.element === this) {
      // Prevent selection of entire equation.
      result = null;
    }

    return result;
  };

  /**
   * Gets the visual element above a given visual child element.
   *
   * @param child: The visual element.
   * @returns null.
   */
  getAboveVisualElementForChild = (child) => {
    return null;
  };

  /**
   * Gets the visual element below a given visual child element.
   *
   * @param child: The visual element.
   * @returns null.
   */
  getBelowVisualElementForChild = function (child) {
    return null;
  };

  /**
   * Gets the first, i.e. leftmost, visual element in the equation.
   */
  getFirstVisualElement = () => {
    var result = {};
    var rootElement = this.getRootElement();

    if (rootElement instanceof Elements.ForallElement) {
      result.element = rootElement.visualElements[0];
    } else {
      result.element = this.visualElements[0];
    }

    result.caretPosition = Common.CaretPosition.Left;

    return result;
  };

  /**
   * Gets the last, i.e. rightmost, visual element in the equation.
   */
  getLastVisualElement = () => {
    var result = {};
    var rootElement = this.getRootElement();

    if (rootElement instanceof Elements.ForallElement) {
      result.element = rootElement.visualElements[rootElement.visualElements.length - 1];
    } else {
      result.element = this.visualElements[this.visualElements.length - 1];
    }

    result.caretPosition = Common.CaretPosition.Right;

    return result;
  };


}
