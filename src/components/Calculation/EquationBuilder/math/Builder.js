import { Common } from "./Commons";
import { Elements } from "./Elements";
import { NodeFactory } from "./Module/GenerateNode";
import { processNode } from "./Parser";

function build(equation) {
// debugger
  if (!equation) {
    return null;
  }

  // Create a new document with a math node as root.
  // var doc = document.implementation.createDocument(Common.NamespaceUri, 'math', null);
  var doc = document.implementation.createDocument(Common.NamespaceUri, 'math', null );


  // Traverse the equation tree and build xml nodes for all elements.
  var childNode = processElement(equation.getRootElement());
  doc.firstChild.appendChild(childNode);


  // Return mathml document as string.
  return new XMLSerializer().serializeToString(doc);
}

/**
 * Builds mathml xml from a mathml element.
 *
 * @param element: The mathml element to process.
 * @param excludeOperands: If the element is an operator, only the mathml for the operator itself is generated.
 *                         Operator operands are replaced with empty elements.
 * @returns The element as a mathml string.
 */
function buildElement(element, excludeOperands) {
  var elementNode = processElement(element);

  if (excludeOperands && element instanceof Elements.OperatorElement) {
    var applyNode = NodeFactory.createApplyNode();
    applyNode.appendChild(elementNode.firstChild);
    applyNode.appendChild(NodeFactory.createIdentifierNode(Elements.EmptyElement.EmptySymbol, Elements.EmptyElement.EmptyType));
    applyNode.appendChild(NodeFactory.createIdentifierNode(Elements.EmptyElement.EmptySymbol, Elements.EmptyElement.EmptyType));
    elementNode = applyNode;
  }
  return new XMLSerializer().serializeToString(elementNode);
};

/**
 * Private.
 * Map of element class processing functions.
 */
var ElementProcessors = {
  ArctanElement: processArctanElement,
  AbsoluteElement: processAbsoluteElement,
  ArbitraryUnionElement: processArbitraryUnionElement,
  ArbitraryIntersectElement: processArbitraryIntersectElement,
  AndElement: processAndElement,
  CardinalityElement: processCardinalityElement,
  DivideElement: processDivideElement,
  EqualElement: processEqualElement,
  ExponentialElement: processExponentialElement,
  EmptyElement: processEmptyElement,
  FenceElement: processFenceElement,
  ForallElement: processForallElement,
  FloorElement: processFloorElement,
  FunctionElement: processFunctionElement,
  FilterSetElement: processFilterSetElement,
  FullEqualsElement: processFullEqualsElement,
  GreaterEqualThanElement: processGreaterEqualThanElement,
  GreaterThanElement: processGreaterThanElement,
  IntersectElement: processIntersectElement,
  IntervalSubElement: processIntervalSubElement,
  IntervalElement: processIntervalElement,
  IdentifierElement: processIdentifierElement,
  IdentifierSubElement: processIdentifierSubElement,
  InElement: processInElement,
  InVersionsElement: processInVersionsElement,
  InObjectsElement: processInObjectsElement,
  LogarithmElement: processLogarithmElement,
  LessEqualThanElement: processLessEqualThanElement,
  LessThanElement: processLessThanElement,
  MaxElement: processMaxElement,
  MinElement: processMinElement,
  ModuloElement: processModuloElement,
  MinusElement: processMinusElement,
  NaturalLogarithmElement: processNaturalLogarithmElement,
  NotElement: processNotElement,
  NumberElement: processNumberElement,
  NotEqualElement: processNotEqualElement,
  NotInElement: processNotInElement,
  OrElement: processOrElement,
  ObjectEqualsElement: processObjectEqualsElement,
  PowerElement: processPowerElement,
  PlusElement: processPlusElement,
  ProperSupersetElement: processProperSupersetElement,
  ProperSubsetElement: processProperSubsetElement,
  SetListElement: processSetListElement,
  SumElement: processSumElement,
  SupersetElement: processSupersetElement,
  SubsetElement: processSubsetElement,
  SetOverlapElement: processSetOverlapElement,
  SeparatorElement: processSeparatorElement,
  SetDiffElement: processSetDiffElement,
  TimesElement: processTimesElement,
  UnaryMinusElement: processUnaryMinusElement,
  UnionElement: processUnionElement,
};



/**
 * Private.
 * Creates an xml DOM element in the mathml namespace.
 *
 * @param name: Element name without namespace prefix.
 * @returns The new xml element.
 */
// function createXmlElement(name) {
//   return document.createElementNS(Common.NamespaceUri, name);

// }

/**
 * Private.
 * Sets a DOM node attribute in the Tieto specific math editor namespace.
 *
 * @param name: The name of the attribute without namespace prefix.
 * @param value: The value of the attribute.
 * @param node: The DOM node to add the attribute to.
 */

export function setEditorAttribute(name, value, node) {
  node.setAttributeNS(Common.Editor.NamespaceUri, Common.Editor.NamespacePrefix + ':' + name, value);
}

/**
 * Private.
 * Processes a mathml element by forwarding the element to the appropriate processing function.
 *
 * @param element: The mathml element to process.
 * @returns An xml node tree representing the element and all its children.
 */
function processElement(element) {
  return ElementProcessors[element.className](element);

}

/**
 * Private.
 * Gets a list of elements from a comma separated list/set.
 *
 * @param element: A separator mathml element, or the only element in the list if no separator.
 * @returns A list of mathml elements.
 */
function getListElements(element) {
  var elements = [];

  if (element instanceof Elements.SeparatorElement) {
    if (element.getLeftOperand() instanceof Elements.SeparatorElement) {
      elements = elements.concat(getListElements(element.getLeftOperand()));
    } else {
      elements.push(element.getLeftOperand());
    }

    if (element.getRightOperand() instanceof Elements.SeparatorElement) {
      elements = elements.concat(getListElements(element.getRightOperand()));
    } else {
      elements.push(element.getRightOperand());
    }
  } else if (element) {
    elements.push(element);
  }

  return elements;
}

/**
 * Private.
 * Gets a list of DOM nodes from a comma separated list/set.
 *
 * @param element: A separator mathml element.
 * @returns A list of DOM nodes.
 */
function getListNodes(element) {
  var nodes = [];

  if (element instanceof Elements.SeparatorElement) {
    if (element.getLeftOperand() instanceof Elements.SeparatorElement) {
      nodes = nodes.concat(getListNodes(element.getLeftOperand()));
    } else {
      nodes.push(processElement(element.getLeftOperand()));
    }

    if (element.getRightOperand() instanceof Elements.SeparatorElement) {
      nodes = nodes.concat(getListNodes(element.getRightOperand()));
    } else {
      nodes.push(processElement(element.getRightOperand()));
    }
  }

  return nodes;
}

function processFenceElement(fenceElement) {
  var containerNode = processElement(fenceElement.getContainer());
  setEditorAttribute(Common.Editor.Attribute.Fence, fenceElement.type, containerNode);

  return containerNode;

}




function processSetListElement(setListElement) {
  if (setListElement.attributeName && setListElement.attributeOwner && setListElement.objectType) {
    // Get condition node.
    var setElements = getListElements(setListElement.getContainer());
    var applyNodes = [];
    var applyNode;

    setElements.forEach(function (setElement) {
      applyNode = NodeFactory.createApplyNode();

      var idElement = new Elements.IdentifierSubElement(setListElement.attributeName, 'attribute');
      idElement.getSubscript().children.add(new Elements.IdentifierElement(setListElement.attributeOwner, 'iterator'));

      applyNode.appendChild(NodeFactory.createEqualNode());
      applyNode.appendChild(processElement(idElement));
      applyNode.appendChild(processElement(setElement));

      applyNodes.push(applyNode);
    });

    if (applyNodes.length > 0) {
      var conditionNode = NodeFactory.createConditionNode();

      if (applyNodes.length > 1) {
        // Include or-element since we have more than one element in the list.
        applyNode = NodeFactory.createApplyNode();
        applyNode.appendChild(NodeFactory.createOrNode());
        conditionNode.appendChild(applyNode);

        applyNodes.forEach(function (applyChildNode) {
          applyNode.appendChild(applyChildNode);
        });
      } else {
        // Just a single element, no need for or-element.
        applyNodes.forEach(function (applyChildNode) {
          conditionNode.appendChild(applyChildNode);
        });
      }
    }
  }

  if (conditionNode) {
    var rootNode = NodeFactory.createSetListNode();
    rootNode.appendChild(conditionNode);

    var inElement = new Elements.InElement();
    inElement.setLeftOperand(new Elements.IdentifierElement(setListElement.attributeOwner, 'iterator'));

    var lookupElement = new Elements.FunctionElement('lookup', 'text');
    var objTypeElement = new Elements.IdentifierElement('ObjectType', 'constant');
    var separatorElement = new Elements.SeparatorElement();
    var objTypeValueElement = new Elements.IdentifierElement(setListElement.objectType, 'constant');

    lookupElement.children.insert(lookupElement.getCsymbolArguments(), objTypeElement, Common.CaretPosition.Right);
    lookupElement.children.insert(objTypeElement, separatorElement, Common.CaretPosition.Right);
    lookupElement.children.insert(separatorElement, objTypeValueElement, Common.CaretPosition.Right);

    inElement.setRightOperand(lookupElement);
    rootNode.appendChild(processNode(inElement));
  } else {
    // No condition, just a simple set elements list.
    var rootNode = NodeFactory.createSetNode();

    if (setListElement.getContainer() instanceof Elements.SeparatorElement) {
      if (!setElements) {
        var setElements = getListElements(setListElement.getContainer());
      }

      setElements.forEach(function (setElement) {
        rootNode.appendChild(processElement(setElement));
      });
    } else {
      rootNode.appendChild(processElement(setListElement.getContainer()));
    }
  }

  return rootNode;
}

function processForallElement(forallElement) {
  if (forallElement.getOperand() instanceof Elements.EmptyElement) {
    return processElement(forallElement.getOperand());
  } else {
    var applyNode = NodeFactory.createApplyNode();
    var forallNode = NodeFactory.createForallNode();
    var conditionNode = NodeFactory.createConditionNode();
    var andNode = NodeFactory.createAndNode();

    applyNode.appendChild(forallNode);
    applyNode.appendChild(conditionNode);
    conditionNode.appendChild(andNode);

    var operandNode = processElement(forallElement.getOperand());

    if (forallElement.getOperand() instanceof Elements.SeparatorElement) {
      var childNodes = getListNodes(forallElement.getOperand());

      childNodes.forEach(function (childNode) {
        conditionNode.appendChild(childNode);
      });
    } else {
      conditionNode.appendChild(operandNode);
    }

    return applyNode;
  }
}

function processIntervalSubElement(intervalSubElement) {
  var intervalNode = NodeFactory.createIntervalNode();
  var selectorNode = NodeFactory.createSelectorNode();
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(selectorNode);
  applyNode.appendChild(intervalNode);

  var elements = getListElements(intervalSubElement.getContainer());

  elements.forEach(function (element) {
    intervalNode.appendChild(processElement(element));
  });

  if (intervalSubElement.hasSubscript) {
    var subElements = getListElements(intervalSubElement.getSubscript());

    subElements.forEach(function (element) {
      applyNode.appendChild(processElement(element));
    });
  }

  return applyNode;
}

function processUnaryMinusElement(unaryMinusElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createMinusNode());
  applyNode.appendChild(processElement(unaryMinusElement.getOperand()));

  return applyNode;
}

function processDivideElement(divideElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createDivideNode());
  applyNode.appendChild(processElement(divideElement.getNumerator()));
  applyNode.appendChild(processElement(divideElement.getDenumerator()));

  return applyNode;
}

function processPowerElement(powerElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createPowerNode());
  applyNode.appendChild(processElement(powerElement.getOperand()));
  applyNode.appendChild(processElement(powerElement.getPower()));

  return applyNode;
}

function processLogarithmElement(logarithmElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createLogarithmNode());
  applyNode.appendChild(processElement(logarithmElement.getContainer()));

  return applyNode;
}

function processNaturalLogarithmElement(naturalLogarithmElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createNaturalLogarithmNode());
  applyNode.appendChild(processElement(naturalLogarithmElement.getContainer()));

  return applyNode;
}

function processModuloElement(moduloElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createModuloNode());
  applyNode.appendChild(processElement(moduloElement.getNumerator()));
  applyNode.appendChild(processElement(moduloElement.getDenumerator()));

  return applyNode;
}

function processArctanElement(arctanElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createArctanNode());
  applyNode.appendChild(processElement(arctanElement.getContainer()));

  return applyNode;
}

function processNotElement(notElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createNotNode());
  applyNode.appendChild(processElement(notElement.getOperand()));

  return applyNode;
}

function processCardinalityElement(cardinalityElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createCardinalityNode());
  applyNode.appendChild(processElement(cardinalityElement.getSet()));

  return applyNode;
}

function processAbsoluteElement(absoluteElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createAbsoluteNode());
  applyNode.appendChild(processElement(absoluteElement.getContainer()));

  return applyNode;
}

function processFloorElement(floorElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createFloorNode());
  applyNode.appendChild(processElement(floorElement.getContainer()));

  return applyNode;
}

function processIntervalElement(intervalElement) {
  var intervalNode = NodeFactory.createIntervalNode();
  var elements = getListElements(intervalElement.getContainer());

  elements.forEach(function (element) {
    intervalNode.appendChild(processElement(element));
  });

  return intervalNode;
}

function processFunctionElement(functionElement) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createCsymbolNode(functionElement.name, functionElement.encoding, functionElement.url));

  if (functionElement.hasSubscript) {
    var conditionNode = NodeFactory.createConditionNode();
    var conditionElements = getListElements(functionElement.getSubscript());

    conditionElements.forEach(function (element) {
      conditionNode.appendChild(processElement(element));
    });

    applyNode.appendChild(conditionNode);
  }

  var argElements = getListElements(functionElement.getCsymbolArguments());

  argElements.forEach(function (element) {
    applyNode.appendChild(processElement(element));
  });

  return applyNode;
}

function processFilterSetElement(filterSetElement) {
  var rootNode = NodeFactory.createSetNode();
  var conditionNode = NodeFactory.createConditionNode();

  rootNode.appendChild(conditionNode);
  rootNode.appendChild(processElement(filterSetElement.getSet()));
  conditionNode.appendChild(processElement(filterSetElement.getFilter()));

  return rootNode;
}

function processMaxMinElement(maxMinElement, operatorNode) {
  var applyNode = NodeFactory.createApplyNode();
  var elements = getListElements(maxMinElement.getContainer());

  applyNode.appendChild(operatorNode);

  elements.forEach(function (element) {
    applyNode.appendChild(processElement(element));
  });

  return applyNode;
}

function processMaxElement(maxElement) {
  return processMaxMinElement(maxElement, NodeFactory.createMaxNode());
}

function processMinElement(minElement) {
  return processMaxMinElement(minElement, NodeFactory.createMinNode());
}

function processIterationElement(iterationElement, iterationNode) {
  var applyNode = NodeFactory.createApplyNode();
  var conditionNode = NodeFactory.createConditionNode();
  var conditionApplyNode = NodeFactory.createApplyNode();

  conditionNode.appendChild(conditionApplyNode);
  conditionApplyNode.appendChild(processElement(iterationElement.getSubscript()));

  applyNode.appendChild(iterationNode);
  applyNode.appendChild(conditionNode);
  applyNode.appendChild(processElement(iterationElement.getOperand()));

  return applyNode;
}

function processSumElement(sumElement) {
  return processIterationElement(sumElement, NodeFactory.createSumNode());
}

function processArbitraryUnionElement(arbitraryUnionElement) {
  return processIterationElement(arbitraryUnionElement, NodeFactory.createUnionNode());
}

function processArbitraryIntersectElement(arbitraryIntersectElement) {
  return processIterationElement(arbitraryIntersectElement, NodeFactory.createIntersectNode());
}

function processNumberElement(numberElement) {
  return NodeFactory.createNumberNode(numberElement.text);
}

function processEmptyElement(emptyElement) {
  return NodeFactory.createIdentifierNode(Elements.EmptyElement.EmptySymbol, Elements.EmptyElement.EmptyType);
}

function processIdentifierElement(identifierElement) {
  return NodeFactory.createIdentifierNode(identifierElement.identifier, identifierElement.type);
}

function processIdentifierSubElement(identifierSubElement) {
  var identifierNode = null
  if (identifierSubElement.type === 'placeholder') {
    // Must remove the $ on both sides of the element if it is a placeholder,
    // as these should only be visual and not be saved to the xml.
    var identifierNode = NodeFactory.createIdentifierNode(
      identifierSubElement.text.substring(1, identifierSubElement.text.lastIndexOf('$') - 1),
      identifierSubElement.type);
  } else {
    identifierNode = NodeFactory.createIdentifierNode(identifierSubElement.text, identifierSubElement.type);
  }

  for (var attribute in identifierSubElement.additionalAttributes) {
    var value = identifierSubElement.getAdditionalMathmlAttribute(attribute);
    // var value = identifierSubElement.getAdditionalElementsAttribute(attribute);

    if (value) {
      identifierNode.setAttribute(attribute, value);
    }
  }

  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(NodeFactory.createSelectorNode());
  applyNode.appendChild(identifierNode);


  if (identifierSubElement.getSubscript() instanceof Elements.SeparatorElement) {
    var subElements = getListNodes(identifierSubElement.getSubscript());

    subElements.forEach(function (element) {
      applyNode.appendChild(element);
    });
  } else {
    applyNode.appendChild(processElement(identifierSubElement.getSubscript()));
  }

  return applyNode;
}

function processExponentialElement(exponentialElement) {
  return NodeFactory.createExponentialNode();
}

function processSeparatorElement(separatorElement) {
  return NodeFactory.createSeparatorNode();
}

function processOperatorElement(operatorElement, operatorNode) {
  var applyNode = NodeFactory.createApplyNode();

  applyNode.appendChild(operatorNode);

  applyNode.appendChild(processElement(operatorElement.getLeftOperand()));
  applyNode.appendChild(processElement(operatorElement.getRightOperand()));


  return applyNode;
}


function processTimesElement(timesElement) {
  return processOperatorElement(timesElement, NodeFactory.createTimesNode());
}

function processPlusElement(plusElement) {
  return processOperatorElement(plusElement, NodeFactory.createPlusNode());
}

function processMinusElement(minusElement) {
  return processOperatorElement(minusElement, NodeFactory.createMinusNode());
}

function processAndElement(andElement) {
  return processOperatorElement(andElement, NodeFactory.createAndNode());
}

function processOrElement(orElement) {
  return processOperatorElement(orElement, NodeFactory.createOrNode());
}

function processEqualElement(equalElement) {
  return processOperatorElement(equalElement, NodeFactory.createEqualNode());
}

function processGreaterEqualThanElement(greaterEqualThanElement) {
  return processOperatorElement(greaterEqualThanElement, NodeFactory.createGreaterEqualThanNode());
}

function processLessEqualThanElement(lessEqualThanElement) {
  return processOperatorElement(lessEqualThanElement, NodeFactory.createLessEqualThanNode());
}

function processGreaterThanElement(greaterThanElement) {
  return processOperatorElement(greaterThanElement, NodeFactory.createGreaterThanNode());
}

function processLessThanElement(lessThanElement) {
  return processOperatorElement(lessThanElement, NodeFactory.createLessThanNode());
}

function processNotEqualElement(notEqualElement) {
  return processOperatorElement(notEqualElement, NodeFactory.createNotEqualNode());
}

function processFullEqualsElement(fullEqualsElement) {
  return processOperatorElement(fullEqualsElement, NodeFactory.createFullEqualsNode());
}

function processObjectEqualsElement(objectEqualsElement) {
  return processOperatorElement(objectEqualsElement, NodeFactory.createObjectEqualsNode());
}

function processProperSupersetElement(properSupersetElement) {
  return processOperatorElement(properSupersetElement, NodeFactory.createProperSupersetNode());
}

function processSupersetElement(supersetElement) {
  return processOperatorElement(supersetElement, NodeFactory.createSupersetNode());
}

function processProperSubsetElement(properSubsetElement) {
  return processOperatorElement(properSubsetElement, NodeFactory.createProperSubsetNode());
}

function processSubsetElement(subsetElement) {
  return processOperatorElement(subsetElement, NodeFactory.createSubsetNode());
}

function processSetOverlapElement(setOverlapElement) {
  return processOperatorElement(setOverlapElement, NodeFactory.createSetOverlapNode());
}

function processInElement(inElement) {
  return processOperatorElement(inElement, NodeFactory.createInNode());
}

function processInVersionsElement(inVersionsElement) {
  return processOperatorElement(inVersionsElement, NodeFactory.createInVersionsNode());
}

function processInObjectsElement(inObjectsElement) {
  return processOperatorElement(inObjectsElement, NodeFactory.createInObjectsNode());
}

function processNotInElement(notInElement) {
  return processOperatorElement(notInElement, NodeFactory.createNotInNode());
}

function processUnionElement(unionElement) {
  return processOperatorElement(unionElement, NodeFactory.createUnionNode());
}

function processIntersectElement(intersectElement) {
  return processOperatorElement(intersectElement, NodeFactory.createIntersectNode());
}

function processSetDiffElement(setDiffElement) {
  return processOperatorElement(setDiffElement, NodeFactory.createSetDiffNode());
}


export const Builder = {
  build,
  buildElement
}