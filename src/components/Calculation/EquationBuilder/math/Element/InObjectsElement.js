import { SetRelationElement } from "./SetRelationElement";

export function InObjectsElement() {
    SetRelationElement.call(this, '\u2208 [obj]');
  }
  
  InObjectsElement.prototype = Object.create(SetRelationElement.prototype);
  InObjectsElement.prototype.constructor = InObjectsElement;
  InObjectsElement.prototype.className = 'InObjectsElement';
  