import { SetRelationElement } from "./SetRelationElement";

export function NotInElement() {
    SetRelationElement.call(this, '\u2209');
  }
  
  NotInElement.prototype = Object.create(SetRelationElement.prototype);
  NotInElement.prototype.constructor = NotInElement;
  NotInElement.prototype.className = 'NotInElement';
  
  