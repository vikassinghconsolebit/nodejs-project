import { SetRelationElement } from "./SetRelationElement";

export function UnionElement() {
    SetRelationElement.call(this, '\u222A');
  }
  
  UnionElement.prototype = Object.create(SetRelationElement.prototype);
  UnionElement.prototype.constructor = UnionElement;
  UnionElement.prototype.className = 'UnionElement';
  