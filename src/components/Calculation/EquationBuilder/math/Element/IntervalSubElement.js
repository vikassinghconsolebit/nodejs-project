import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";
import { SeparatorElement } from "./SeparatorElement";

export function IntervalSubElement(numSubscripts) {
    OperandElement.call(this);
  
    this.hasSubscript = numSubscripts > 0;
    this.visualElements = []; // Contains elements that visually lie on the same line.
    this.visualSubElements = []; // Contains sub elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  
    if (this.hasSubscript) {
      for (var i = 1; i < numSubscripts; ++i) {
        this.getSubscript().children.add(new SeparatorElement());
      }
    }
  }
  
  IntervalSubElement.prototype = Object.create(OperandElement.prototype);
  IntervalSubElement.prototype.constructor = IntervalSubElement;
  IntervalSubElement.prototype.className = 'IntervalSubElement';
  
  IntervalSubElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  IntervalSubElement.prototype.getSubscript = function () {
    return this.children.at(1);
  };
  
  IntervalSubElement.prototype.setSubscript = function (element) {
    return this.children.replace(this.getSubscript(), element);
  };
  
  IntervalSubElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualSubElements = this.getSubscript().getFlattenedElementTree();
  
    this.visualSubElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  IntervalSubElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualElements);
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualSubElements);
    }
  
    return result;
  };
  
  IntervalSubElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualElements);
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualSubElements);
    }
  
    return result;
  };
  
  IntervalSubElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  
  IntervalSubElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualSubElements);
  };
  