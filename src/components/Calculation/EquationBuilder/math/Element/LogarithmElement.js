import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function LogarithmElement() {
    OperandElement.call(this);
  
    this.text = 'log';
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  LogarithmElement.prototype = Object.create(OperandElement.prototype);
  LogarithmElement.prototype.constructor = LogarithmElement;
  LogarithmElement.prototype.className = 'LogarithmElement';
  
  LogarithmElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  LogarithmElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  LogarithmElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  LogarithmElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  LogarithmElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  