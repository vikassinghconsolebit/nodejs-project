import { RelationElement } from "./RelationElement";

export function ObjectEqualsElement() {
    RelationElement.call(this, '~');
  
    this.precedence = 30;
  }
  
  ObjectEqualsElement.prototype = Object.create(RelationElement.prototype);
  ObjectEqualsElement.prototype.constructor = ObjectEqualsElement;
  ObjectEqualsElement.prototype.className = 'ObjectEqualsElement';
  