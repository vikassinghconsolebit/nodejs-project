import { OperatorElement } from "./OperatorElement";

export function OrElement() {
    OperatorElement.call(this, '\u2228');
  
    this.precedence = 20;
  }
  
  OrElement.prototype = Object.create(OperatorElement.prototype);
  OrElement.prototype.constructor = OrElement;
  OrElement.prototype.className = 'OrElement';
  
  