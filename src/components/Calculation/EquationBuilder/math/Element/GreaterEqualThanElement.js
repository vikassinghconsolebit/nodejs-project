import { RelationElement } from "./RelationElement";

export function GreaterEqualThanElement() {
    RelationElement.call(this, '\u2265');
  }
  
  GreaterEqualThanElement.prototype = Object.create(RelationElement.prototype);
  GreaterEqualThanElement.prototype.constructor = GreaterEqualThanElement;
  GreaterEqualThanElement.prototype.className = 'GreaterEqualThanElement';
  