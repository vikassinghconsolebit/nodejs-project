import { SingleOperandElement } from "./SingleOperandElement";

export function EmptyElement(text) {
    SingleOperandElement.call(this, typeof text === 'string' ? text : EmptyElement.EmptySymbol);
  }
  
  EmptyElement.prototype = Object.create(SingleOperandElement.prototype);
  EmptyElement.prototype.constructor = EmptyElement;
  EmptyElement.prototype.className = 'EmptyElement';
  
  EmptyElement.EmptySymbol = '?';
  EmptyElement.EmptyType = 'empty';
  
  