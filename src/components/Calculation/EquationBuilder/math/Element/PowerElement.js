import { Common } from "../Commons";
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function PowerElement() {
    OperandElement.call(this);
  
    this.visualOperandElements = []; // Contains operand elements that visually lie on the same line.
    this.visualPowerElements = []; // Contains power elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  }
  
  PowerElement.prototype = Object.create(OperandElement.prototype);
  PowerElement.prototype.constructor = PowerElement;
  PowerElement.prototype.className = 'PowerElement';
  
  PowerElement.prototype.getOperand = function () {
    return this.children.at(0);
  };
  
  PowerElement.prototype.getPower = function () {
    return this.children.at(1);
  };
  
  PowerElement.prototype.buildVisualElementList = function () {
    this.visualOperandElements = this.getOperand().getFlattenedElementTree();
  
    this.visualOperandElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualPowerElements = this.getPower().getFlattenedElementTree();
  
    this.visualPowerElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  PowerElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualOperandElements);
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualPowerElements);
    }
  
    return result;
  };
  
  PowerElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualOperandElements);
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualPowerElements);
    }
  
    return result;
  };
  
  PowerElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualPowerElements);
  };
  
  PowerElement.prototype.getAboveVisualElementForChild = function (child) {
    if (this.visualPowerElements.indexOf(child) >= 0) {
      return this.visualParent ? this.visualParent.getAboveVisualElementForChild(this) : null;
    }
  
    return {
      element: this,
      caretPosition: child.caretPosition !== Common.CaretPosition.None ?
        child.caretPosition : Common.CaretPosition.Right
    };
  };
  
  PowerElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualOperandElements);
  };
  
  PowerElement.prototype.getBelowVisualElementForChild = function (child) {
    if (this.visualPowerElements.indexOf(child) >= 0) {
      return {
        element: this,
        caretPosition: child.caretPosition !== Common.CaretPosition.None ?
          child.caretPosition : Common.CaretPosition.Right
      };
    }
  
    return this.visualParent ? this.visualParent.getBelowVisualElementForChild(this) : null;
  };
  
  