import { SetRelationElement } from "./SetRelationElement";

export function InVersionsElement() {
    SetRelationElement.call(this, '\u2208 [ver]');
  }
  
  InVersionsElement.prototype = Object.create(SetRelationElement.prototype);
  InVersionsElement.prototype.constructor = InVersionsElement;
  InVersionsElement.prototype.className = 'InVersionsElement';
  