import { MaxMinElement } from "./MaxMinElement";

export function MaxElement() {
    MaxMinElement.call(this, 'max');
  }
  
  MaxElement.prototype = Object.create(MaxMinElement.prototype);
  MaxElement.prototype.constructor = MaxElement;
  MaxElement.prototype.className = 'MaxElement';
  