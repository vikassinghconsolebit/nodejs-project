import { OperatorElement } from "./OperatorElement";

export function PlusElement() {
    OperatorElement.call(this, '+');
  }
  
  PlusElement.prototype = Object.create(OperatorElement.prototype);
  PlusElement.prototype.constructor = PlusElement;
  PlusElement.prototype.className = 'PlusElement';
  
  