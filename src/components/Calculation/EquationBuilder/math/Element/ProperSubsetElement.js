import { RelationElement } from "./RelationElement";

export function ProperSubsetElement() {
    RelationElement.call(this, '\u2282');
  }
  
  ProperSubsetElement.prototype = Object.create(RelationElement.prototype);
  ProperSubsetElement.prototype.constructor = ProperSubsetElement;
  ProperSubsetElement.prototype.className = 'ProperSubsetElement';
  
  