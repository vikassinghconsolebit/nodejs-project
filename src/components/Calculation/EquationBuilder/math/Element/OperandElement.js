import { Element}from "../Elements";

export function OperandElement() {
    Element.call(this);
}

OperandElement.prototype = Object.create(Element.prototype);
OperandElement.prototype.constructor = OperandElement;
OperandElement.prototype.className = 'OperandElement';
