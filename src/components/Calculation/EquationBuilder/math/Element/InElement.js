import { SetRelationElement } from "./SetRelationElement";

export function InElement() {
    SetRelationElement.call(this, '\u2208');
  }
  
  InElement.prototype = Object.create(SetRelationElement.prototype);
  InElement.prototype.constructor = InElement;
  InElement.prototype.className = 'InElement';
  