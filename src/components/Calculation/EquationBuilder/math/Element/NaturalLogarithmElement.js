import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function NaturalLogarithmElement() {
    OperandElement.call(this);
  
    this.text = 'ln';
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  NaturalLogarithmElement.prototype = Object.create(OperandElement.prototype);
  NaturalLogarithmElement.prototype.constructor = NaturalLogarithmElement;
  NaturalLogarithmElement.prototype.className = 'NaturalLogarithmElement';
  
  NaturalLogarithmElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  NaturalLogarithmElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  NaturalLogarithmElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  NaturalLogarithmElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  NaturalLogarithmElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  