import { EmptyElement } from "./EmptyElement";
import { SingleOperandElement } from "./SingleOperandElement";

export function IdentifierSubElement(identifier, type) {
    var text = type === 'placeholder' ? '$' + identifier + '$' : identifier;
  
    SingleOperandElement.call(this, text, type);
  
    this.visualElements = []; // Contains elements that visually lie on the same line.
    this.additionalAttributes = {};
  
    this.children.reset(new EmptyElement());
  }
  
  IdentifierSubElement.prototype = Object.create(SingleOperandElement.prototype);
  IdentifierSubElement.prototype.constructor = IdentifierSubElement;
  IdentifierSubElement.prototype.className = 'IdentifierSubElement';
  
  IdentifierSubElement.prototype.addAdditionalMathmlAttribute = function (name, value) {
    this.additionalAttributes[name] = value;
  };
  
  IdentifierSubElement.prototype.getAdditionalMathmlAttribute = function (name) {
    return this.additionalAttributes[name];
  };
  
  IdentifierSubElement.prototype.getSubscript = function () {
    return this.children.at(0);
  };
  
  IdentifierSubElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getSubscript().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  IdentifierSubElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  IdentifierSubElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  IdentifierSubElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualElements);
  };