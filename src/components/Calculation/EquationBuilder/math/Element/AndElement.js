import { OperatorElement } from "./OperatorElement";

export function AndElement() {
    OperatorElement.call(this, '\u2227');
  
    this.precedence = 20;
  }
  
  AndElement.prototype = Object.create(OperatorElement.prototype);
  AndElement.prototype.constructor = AndElement;
  AndElement.prototype.className = 'AndElement';
  