import { MaxMinElement } from "./MaxMinElement";

export function MinElement() {
    MaxMinElement.call(this, 'min');
  }
  
  MinElement.prototype = Object.create(MaxMinElement.prototype);
  MinElement.prototype.constructor = MinElement;
  MinElement.prototype.className = 'MinElement';
  