import { SingleOperandElement } from "./SingleOperandElement";

export function NumberElement(number) {
    SingleOperandElement.call(this, number);
  }
  
  NumberElement.prototype = Object.create(SingleOperandElement.prototype);
  NumberElement.prototype.constructor = NumberElement;
  NumberElement.prototype.className = 'NumberElement';
  