import { OperatorElement } from "./OperatorElement";

export function TimesElement() {
    OperatorElement.call(this, '•');
  
    this.precedence = 60;
  }
  
  TimesElement.prototype = Object.create(OperatorElement.prototype);
  TimesElement.prototype.constructor = TimesElement;
  TimesElement.prototype.className = 'TimesElement';
  
  