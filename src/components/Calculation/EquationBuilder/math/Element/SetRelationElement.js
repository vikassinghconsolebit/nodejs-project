import { RelationElement } from "./RelationElement";

export function SetRelationElement(text) {
    RelationElement.call(this, text);
  }
  
  SetRelationElement.prototype = Object.create(RelationElement.prototype);
  SetRelationElement.prototype.constructor = SetRelationElement;
  SetRelationElement.prototype.className = 'SetRelationElement';
  
  