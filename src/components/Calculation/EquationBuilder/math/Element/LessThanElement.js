import { RelationElement } from "./RelationElement";

export function LessThanElement() {
    RelationElement.call(this, '<');
  }
  
  LessThanElement.prototype = Object.create(RelationElement.prototype);
  LessThanElement.prototype.constructor = LessThanElement;
  LessThanElement.prototype.className = 'LessThanElement';
  