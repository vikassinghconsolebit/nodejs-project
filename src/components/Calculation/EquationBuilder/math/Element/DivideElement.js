import { Common } from "../Commons";
import { PowerElement } from "./PowerElement";
import { OperandElement } from "./OperandElement";
import { EmptyElement } from "./EmptyElement";

export function DivideElement() {
    OperandElement.call(this);

    this.visualNumeratorElements = []; // Contains numerator elements that visually lie on the same line.
    this.visualDenumeratorElements = []; // Contains denumerator elements that visually lie on the same line.

    this.children.reset(new EmptyElement(), new EmptyElement());
}

DivideElement.prototype = Object.create(OperandElement.prototype);
DivideElement.prototype.constructor = DivideElement;
DivideElement.prototype.className = 'DivideElement';

DivideElement.prototype.getNumerator = function () {
    return this.children.at(0);
};

DivideElement.prototype.setNumerator = function (element) {
    if (element) {
        this.children.replace(this.getNumerator(), element);
    }
};

DivideElement.prototype.getDenumerator = function () {
    return this.children.at(1);
};

DivideElement.prototype.setDenumerator = function (element) {
    if (element) {
        this.children.replace(this.getDenumerator(), element);
    }
};

DivideElement.prototype.buildVisualElementList = function () {
    this.visualNumeratorElements = this.getNumerator().getFlattenedElementTree();

    this.visualNumeratorElements.forEach(function (element) {
        element.visualParent = this;
    }, this);

    this.visualDenumeratorElements = this.getDenumerator().getFlattenedElementTree();

    this.visualDenumeratorElements.forEach(function (element) {
        element.visualParent = this;
    }, this);
};

DivideElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualNumeratorElements);

    if (!result) {
        result = this.getLeftVisualElementInList(child, this.visualDenumeratorElements);
    }

    return result;
};

DivideElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualNumeratorElements);

    if (!result) {
        result = this.getRightVisualElementInList(child, this.visualDenumeratorElements);
    }

    return result;
};

DivideElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualNumeratorElements);
};

PowerElement.prototype.getAboveVisualElementForChild = function (child) {
    if (this.visualDenumeratorElements.indexOf(child) >= 0) {
        return this.visualParent ? this.visualParent.getAboveVisualElementForChild(this) : null;
    }

    return {
        element: this,
        caretPosition: child.caretPosition !== Common.CaretPosition.None ?
            child.caretPosition : Common.CaretPosition.Right
    };
};

DivideElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualDenumeratorElements);
};

DivideElement.prototype.getBelowVisualElementForChild = function (child) {
    if (this.visualNumeratorElements.indexOf(child) >= 0) {
        return {
            element: this,
            caretPosition: child.caretPosition !== Common.CaretPosition.None ?
                child.caretPosition : Common.CaretPosition.Right
        };
    }
    return null;
};
