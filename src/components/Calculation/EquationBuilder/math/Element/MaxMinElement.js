import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function MaxMinElement(text) {
    OperandElement.call(this);
  
    this.text = text;
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  MaxMinElement.prototype = Object.create(OperandElement.prototype);
  MaxMinElement.prototype.constructor = MaxMinElement;
  MaxMinElement.prototype.className = 'MaxMinElement';
  
  MaxMinElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  MaxMinElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  MaxMinElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  MaxMinElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  MaxMinElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  