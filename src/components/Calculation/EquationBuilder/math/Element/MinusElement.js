import { OperatorElement } from "./OperatorElement";

export function MinusElement() {
    OperatorElement.call(this, '-');
  }
  
  MinusElement.prototype = Object.create(OperatorElement.prototype);
  MinusElement.prototype.constructor = MinusElement;
  MinusElement.prototype.className = 'MinusElement';
  