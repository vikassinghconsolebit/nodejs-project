import { RelationElement } from "./RelationElement";

export function NotEqualElement() {
    RelationElement.call(this, '\u2260');
  }
  
  NotEqualElement.prototype = Object.create(RelationElement.prototype);
  NotEqualElement.prototype.constructor = NotEqualElement;
  NotEqualElement.prototype.className = 'NotEqualElement';
  