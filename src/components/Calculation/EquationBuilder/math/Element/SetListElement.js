import { FenceElement } from "./FenceElement";

export function SetListElement(type) {
    FenceElement.call(this, FenceElement.FenceType.CurlyBracket);
  
    this.attributeName = null;
    this.attributeOwner = null;
    this.objectType = null;
  }
  
  SetListElement.prototype = Object.create(FenceElement.prototype);
  SetListElement.prototype.constructor = SetListElement;
  SetListElement.prototype.className = 'SetListElement';
  
  SetListElement.prototype.getSet = function () {
    return this.getContainer();
  };
  