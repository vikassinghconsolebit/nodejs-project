import { RelationElement } from "./RelationElement";

export function SetOverlapElement() {
    RelationElement.call(this, '\u222A?');
  }
  
  SetOverlapElement.prototype = Object.create(RelationElement.prototype);
  SetOverlapElement.prototype.constructor = SetOverlapElement;
  SetOverlapElement.prototype.className = 'SetOverlapElement';
  
  