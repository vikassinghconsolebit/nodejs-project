
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function FenceElement(type) {
    // debugger
    OperandElement.call(this);
  
    this.type = type;
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  FenceElement.prototype = Object.create(OperandElement.prototype);
  FenceElement.prototype.constructor = FenceElement;
  FenceElement.prototype.className = 'FenceElement';
  
  FenceElement.FenceType = {
    Parenthesis: '()',
    SquareBracket: '[]',
    CurlyBracket: '{}'
  };
  
  FenceElement.createElement = function (type) {
    var element = null;
  
    type = type.trim();
  
    if (type === FenceElement.FenceType.Parenthesis ||
      type === FenceElement.FenceType.SquareBracket ||
      type === FenceElement.FenceType.CurlyBracket) {
      element = new FenceElement(type);
    }
  
    return element;
  };
  
  FenceElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  FenceElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  FenceElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  FenceElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  FenceElement.prototype.getUpVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  
  