import { OperatorElement } from "./OperatorElement";

export function RelationElement(text) {
    OperatorElement.call(this, text);
  
    this.precedence = 40;
  }
  
  RelationElement.prototype = Object.create(OperatorElement.prototype);
  RelationElement.prototype.constructor = RelationElement;
  RelationElement.prototype.className = 'RelationElement';
  
  