import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function ArctanElement() {
    OperandElement.call(this);
  
    this.text = 'arctan';
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  ArctanElement.prototype = Object.create(OperandElement.prototype);
  ArctanElement.prototype.constructor = ArctanElement;
  ArctanElement.prototype.className = 'ArctanElement';
  
  ArctanElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  ArctanElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  ArctanElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  ArctanElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  ArctanElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  