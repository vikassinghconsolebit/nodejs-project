import { RelationElement } from "./RelationElement";

export function FullEqualsElement() {
    RelationElement.call(this, '\u2261');
  
    this.precedence = 30;
  }
  
  FullEqualsElement.prototype = Object.create(RelationElement.prototype);
  FullEqualsElement.prototype.constructor = FullEqualsElement;
  FullEqualsElement.prototype.className = 'FullEqualsElement';
  