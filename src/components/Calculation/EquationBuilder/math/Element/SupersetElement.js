import { RelationElement } from "./RelationElement";

export function SupersetElement() {
    RelationElement.call(this, '\u2287');
  }
  
  SupersetElement.prototype = Object.create(RelationElement.prototype);
  SupersetElement.prototype.constructor = SupersetElement;
  SupersetElement.prototype.className = 'SupersetElement';
  