import { RelationElement } from "./RelationElement";

export function ProperSupersetElement() {
    RelationElement.call(this, '\u2283');
  }
  
  ProperSupersetElement.prototype = Object.create(RelationElement.prototype);
  ProperSupersetElement.prototype.constructor = ProperSupersetElement;
  ProperSupersetElement.prototype.className = 'ProperSupersetElement';
  