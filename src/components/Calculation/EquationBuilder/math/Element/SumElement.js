import { IterationElement } from "./IterationElement";

export function SumElement() {
    IterationElement.call(this, '\u2211');
  }
  
  SumElement.prototype = Object.create(IterationElement.prototype);
  SumElement.prototype.constructor = SumElement;
  SumElement.prototype.className = 'SumElement';
  