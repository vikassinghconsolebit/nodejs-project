import { SetRelationElement } from "./SetRelationElement";

export function SetDiffElement() {
    SetRelationElement.call(this, '\\');
  }
  
  SetDiffElement.prototype = Object.create(SetRelationElement.prototype);
  SetDiffElement.prototype.constructor = SetDiffElement;
  SetDiffElement.prototype.className = 'SetDiffElement';
  
  