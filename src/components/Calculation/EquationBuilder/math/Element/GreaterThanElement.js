import { RelationElement } from "./RelationElement";

export function GreaterThanElement() {
    RelationElement.call(this, '>');
  }
  
  GreaterThanElement.prototype = Object.create(RelationElement.prototype);
  GreaterThanElement.prototype.constructor = GreaterThanElement;
  GreaterThanElement.prototype.className = 'GreaterThanElement';
  
  