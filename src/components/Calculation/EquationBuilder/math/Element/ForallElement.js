import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function ForallElement() {
    OperandElement.call(this);
  
    this.isSelectable = false;
    this.precedence = 5;
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  ForallElement.prototype = Object.create(OperandElement.prototype);
  ForallElement.prototype.constructor = ForallElement;
  ForallElement.prototype.className = 'ForallElement';
  
  ForallElement.prototype.getOperand = function () {
    return this.children.at(0);
  };
  
  ForallElement.prototype.getLeftmostChild = function () {
    this.buildVisualElementList();
    return this.visualElements[0];
  };
  
  ForallElement.prototype.getRightmostChild = function () {
    this.buildVisualElementList();
    return this.visualElements[this.visualElements.length - 1];
  };
  
  ForallElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getOperand().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  ForallElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualElements);
  
    if (result && result.element === this) {
      result = null;
    }
  
    return result;
  };
  
  ForallElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualElements);
  
    if (result && result.element === this) {
      result = null;
    }
  
    return result;
  };
  
  ForallElement.prototype.getAboveVisualElementForChild = function (child) {
    return null;
  };
  
  