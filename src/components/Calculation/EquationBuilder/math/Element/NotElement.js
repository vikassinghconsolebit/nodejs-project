import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function NotElement() {
    OperandElement.call(this);
  
    this.text = '¬';
  
    this.children.reset(new EmptyElement());
  }
  
  NotElement.prototype = Object.create(OperandElement.prototype);
  NotElement.prototype.constructor = NotElement;
  NotElement.prototype.className = 'NotElement';
  
  NotElement.prototype.getOperand = function () {
    return this.children.at(0);
  };
  