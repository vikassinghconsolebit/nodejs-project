import { SingleOperandElement } from "./SingleOperandElement";

export function ExponentialElement(text) {
    SingleOperandElement.call(this, 'e');
  }
  
  ExponentialElement.prototype = Object.create(SingleOperandElement.prototype);
  ExponentialElement.prototype.constructor = ExponentialElement;
  ExponentialElement.prototype.className = 'ExponentialElement';
  
  