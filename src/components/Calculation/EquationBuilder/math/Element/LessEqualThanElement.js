import { RelationElement } from "./RelationElement";

export function LessEqualThanElement() {
    RelationElement.call(this, '\u2264');
  }
  
  LessEqualThanElement.prototype = Object.create(RelationElement.prototype);
  LessEqualThanElement.prototype.constructor = LessEqualThanElement;
  LessEqualThanElement.prototype.className = 'LessEqualThanElement';
  