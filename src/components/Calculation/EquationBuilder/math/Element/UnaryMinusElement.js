import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function UnaryMinusElement() {
    OperandElement.call(this);
  
    this.text = '-';
  
    this.children.reset(new EmptyElement());
  }
  
  UnaryMinusElement.prototype = Object.create(OperandElement.prototype);
  UnaryMinusElement.prototype.constructor = UnaryMinusElement;
  UnaryMinusElement.prototype.className = 'UnaryMinusElement';
  
  UnaryMinusElement.prototype.getOperand = function () {
    return this.children.at(0);
  };
  