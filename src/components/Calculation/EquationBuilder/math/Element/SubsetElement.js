import { RelationElement } from "./RelationElement";

export function SubsetElement() {
    RelationElement.call(this, '\u2286');
  }
  
  SubsetElement.prototype = Object.create(RelationElement.prototype);
  SubsetElement.prototype.constructor = SubsetElement;
  SubsetElement.prototype.className = 'SubsetElement';
  
  