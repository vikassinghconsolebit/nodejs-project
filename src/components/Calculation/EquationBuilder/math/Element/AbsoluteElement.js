import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function AbsoluteElement() {
    OperandElement.call(this);
  
    this.text = 'abs';
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  AbsoluteElement.prototype = Object.create(OperandElement.prototype);
  AbsoluteElement.prototype.constructor = AbsoluteElement;
  AbsoluteElement.prototype.className = 'AbsoluteElement';
  
  AbsoluteElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  AbsoluteElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  AbsoluteElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  AbsoluteElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  AbsoluteElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  