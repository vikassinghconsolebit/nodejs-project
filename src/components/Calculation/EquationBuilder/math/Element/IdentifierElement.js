import { SingleOperandElement } from "./SingleOperandElement";

export function IdentifierElement(identifier, type) {
    var text = type === 'constant' ? "'" + identifier + "'" :
      type === 'placeholder' ? '$' + identifier + '$' :
        identifier;
  
    SingleOperandElement.call(this, text, type);
  
    this.identifier = identifier;
  }
  
  IdentifierElement.prototype = Object.create(SingleOperandElement.prototype);
  IdentifierElement.prototype.constructor = IdentifierElement;
  IdentifierElement.prototype.className = 'IdentifierElement';
  