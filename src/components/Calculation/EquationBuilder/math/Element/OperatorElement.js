import { Element } from "../Elements";
import { EmptyElement } from "./EmptyElement";

export function OperatorElement(operatorText) {
    Element.call(this);
  
    this.operatorText = operatorText;
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  }
  
  OperatorElement.prototype = Object.create(Element.prototype);
  OperatorElement.prototype.constructor = OperatorElement;
  OperatorElement.prototype.className = 'OperatorElement';
  
  OperatorElement.prototype.getLeftOperand = function () {
    return this.children.at(0);
  };
  
  OperatorElement.prototype.setLeftOperand = function (element) {
    if (element) {
      this.children.replace(this.getLeftOperand(), element);
    }
  };
  
  OperatorElement.prototype.getRightOperand = function () {
    return this.children.at(1);
  };
  
  OperatorElement.prototype.setRightOperand = function (element) {
    if (element) {
      this.children.replace(this.getRightOperand(), element);
    }
  };
  
  OperatorElement.prototype.getAbsoluteLocation = function () {
    var absoluteLocation = Element.prototype.getAbsoluteLocation.call(this);
  
    absoluteLocation.x += this.operatorLocation.x;
    absoluteLocation.y += this.operatorLocation.y;
  
    return absoluteLocation;
  };
  
  OperatorElement.prototype.containsPosition = function (position) {
    // Element location is relative to parent, so must calculate absolute position.
    var absolute = this.getAbsoluteLocation();
  
    return position.x >= absolute.x && position.x < (absolute.x + this.operatorSize.width) &&
      position.y >= absolute.y && position.y < (absolute.y + this.operatorSize.height);
  };
  