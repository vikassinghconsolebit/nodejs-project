
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function IterationElement(symbol) {
    OperandElement.call(this);
  
    this.symbol = symbol;
    this.visualSubscriptElements = []; // Contains subscript elements that visually lie on the same line.
    this.visualOperandElements = []; // Contains operand elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  }
  
  IterationElement.prototype = Object.create(OperandElement.prototype);
  IterationElement.prototype.constructor = IterationElement;
  IterationElement.prototype.className = 'IterationElement';
  
  IterationElement.prototype.getSubscript = function () {
    return this.children.at(0);
  };
  
  IterationElement.prototype.setSubscript = function (element) {
    if (element) {
      this.children.replace(this.getSubscript(), element);
    }
  };
  
  IterationElement.prototype.getOperand = function () {
    return this.children.at(1);
  };
  
  IterationElement.prototype.setOperand = function (element) {
    if (element) {
      this.children.replace(this.getOperand(), element);
    }
  };
  
  IterationElement.prototype.buildVisualElementList = function () {
    this.visualSubscriptElements = this.getSubscript().getFlattenedElementTree();
  
    this.visualSubscriptElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualOperandElements = this.getOperand().getFlattenedElementTree();
  
    this.visualOperandElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  IterationElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualOperandElements);
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualSubscriptElements);
    }
  
    return result;
  };
  
  IterationElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualOperandElements);
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualSubscriptElements);
    }
  
    return result;
  };
  
  IterationElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualOperandElements);
  };
  
  IterationElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualSubscriptElements);
  };
  