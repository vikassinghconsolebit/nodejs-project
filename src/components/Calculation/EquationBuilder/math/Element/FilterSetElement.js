import { Common } from "../Commons";
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function FilterSetElement() {
    OperandElement.call(this);
  
    this.visualSetElements = []; // Contains set elements that visually lie on the same line.
    this.visualFilterElements = []; // Contains filter elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  }
  
  FilterSetElement.prototype = Object.create(OperandElement.prototype);
  FilterSetElement.prototype.constructor = FilterSetElement;
  FilterSetElement.prototype.className = 'FilterSetElement';
  
  FilterSetElement.prototype.getSet = function () {
    return this.children.at(0);
  };
  
  FilterSetElement.prototype.getFilter = function () {
    return this.children.at(1);
  };
  
  FilterSetElement.prototype.buildVisualElementList = function () {
    this.visualSetElements = this.getSet().getFlattenedElementTree();
  
    this.visualSetElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualFilterElements = this.getFilter().getFlattenedElementTree();
  
    this.visualFilterElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  FilterSetElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualFilterElements);
  
    if (result && result.element === this) {
      result = {
        element: this.visualSetElements[this.visualSetElements.length - 1],
        caretPosition: Common.CaretPosition.Right
      };
    }
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualSetElements);
    }
  
    return result;
  };
  
  FilterSetElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualSetElements);
  
    if (result && result.element === this) {
      result = {
        element: this.visualFilterElements[0],
        caretPosition: Common.CaretPosition.Left
      };
    }
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualFilterElements);
    }
  
    return result;
  };
  
  FilterSetElement.prototype.getInsideVisualElement = function () {
    return {
      element: (this.caretPosition === Common.CaretPosition.Right) ?
        this.visualFilterElements[this.visualFilterElements.length - 1] : this.visualSetElements[0],
      caretPosition: this.caretPosition
    };
  };
  