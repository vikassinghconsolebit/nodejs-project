import { IterationElement } from "./IterationElement";

export function ArbitraryUnionElement() {
    IterationElement.call(this, '\u222A');
  }
  
  ArbitraryUnionElement.prototype = Object.create(IterationElement.prototype);
  ArbitraryUnionElement.prototype.constructor = ArbitraryUnionElement;
  ArbitraryUnionElement.prototype.className = 'ArbitraryUnionElement';
  