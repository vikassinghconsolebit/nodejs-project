import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function IntervalElement() {
    OperandElement.call(this);
  
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  IntervalElement.prototype = Object.create(OperandElement.prototype);
  IntervalElement.prototype.constructor = IntervalElement;
  IntervalElement.prototype.className = 'IntervalElement';
  
  IntervalElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  IntervalElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  IntervalElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  IntervalElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  IntervalElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  
  