import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function FloorElement() {
    OperandElement.call(this);
  
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  FloorElement.prototype = Object.create(OperandElement.prototype);
  FloorElement.prototype.constructor = FloorElement;
  FloorElement.prototype.className = 'FloorElement';
  
  FloorElement.prototype.getContainer = function () {
    return this.children.at(0);
  };
  
  FloorElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getContainer().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  FloorElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  FloorElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  FloorElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  
  