
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function CardinalityElement() {
    OperandElement.call(this);
  
    this.visualElements = []; // Contains elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement());
  }
  
  CardinalityElement.prototype = Object.create(OperandElement.prototype);
  CardinalityElement.prototype.constructor = CardinalityElement;
  CardinalityElement.prototype.className = 'CardinalityElement';
  
  CardinalityElement.prototype.getSet = function () {
    return this.children.at(0);
  };
  
  CardinalityElement.prototype.buildVisualElementList = function () {
    this.visualElements = this.getSet().getFlattenedElementTree();
  
    this.visualElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  CardinalityElement.prototype.getLeftVisualElementForChild = function (child) {
    return this.getLeftVisualElementInList(child, this.visualElements);
  };
  
  CardinalityElement.prototype.getRightVisualElementForChild = function (child) {
    return this.getRightVisualElementInList(child, this.visualElements);
  };
  
  CardinalityElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualElements);
  };
  
  