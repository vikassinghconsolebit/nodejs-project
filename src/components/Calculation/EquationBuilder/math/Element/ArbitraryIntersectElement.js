import { IterationElement } from "./IterationElement";

export function ArbitraryIntersectElement() {
    IterationElement.call(this, '\u2229');
  }
  
  ArbitraryIntersectElement.prototype = Object.create(IterationElement.prototype);
  ArbitraryIntersectElement.prototype.constructor = ArbitraryIntersectElement;
  ArbitraryIntersectElement.prototype.className = 'ArbitraryIntersectElement';
  