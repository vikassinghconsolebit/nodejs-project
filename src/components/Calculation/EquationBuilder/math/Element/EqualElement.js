import { RelationElement } from "./RelationElement";

export function EqualElement() {
    RelationElement.call(this, '=');
  
    this.precedence = 30;
  }
  
  EqualElement.prototype = Object.create(RelationElement.prototype);
  EqualElement.prototype.constructor = EqualElement;
  EqualElement.prototype.className = 'EqualElement';
  
  