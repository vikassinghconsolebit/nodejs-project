import { SetRelationElement } from "./SetRelationElement";

export function IntersectElement() {
    SetRelationElement.call(this, '\u2229');
  }
  
  IntersectElement.prototype = Object.create(SetRelationElement.prototype);
  IntersectElement.prototype.constructor = IntersectElement;
  IntersectElement.prototype.className = 'IntersectElement';
  
  