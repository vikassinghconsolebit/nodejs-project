import { OperatorElement } from "./OperatorElement";

export function SeparatorElement() {
    OperatorElement.call(this, ',');
  
    this.precedence = 10;
  }
  
  SeparatorElement.prototype = Object.create(OperatorElement.prototype);
  SeparatorElement.prototype.constructor = SeparatorElement;
  SeparatorElement.prototype.className = 'SeparatorElement';
  
  