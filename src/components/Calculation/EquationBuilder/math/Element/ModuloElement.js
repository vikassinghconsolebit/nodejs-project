import { Common } from "../Commons";
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function ModuloElement() {
    OperandElement.call(this);
  
    this.text = 'mod';
    this.visualNumeratorElements = []; // Contains elements that visually lie on the left side of the "mod".
    this.visualDenumeratorElements = []; // Contains elements that visually lie on the right side of the "mod".
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  }
  
  ModuloElement.prototype = Object.create(OperandElement.prototype);
  ModuloElement.prototype.constructor = ModuloElement;
  ModuloElement.prototype.className = 'ModuloElement';
  
  ModuloElement.prototype.getNumerator = function () {
    return this.children.at(0);
  };
  
  ModuloElement.prototype.setNumerator = function (element) {
    if (element) {
      this.children.replace(this.getNumerator(), element);
    }
  };
  
  ModuloElement.prototype.getDenumerator = function () {
    return this.children.at(1);
  };
  
  ModuloElement.prototype.setDenumerator = function (element) {
    if (element) {
      this.children.replace(this.getDenumerator(), element);
    }
  };
  
  ModuloElement.prototype.buildVisualElementList = function () {
    this.visualNumeratorElements = this.getNumerator().getFlattenedElementTree();
  
    this.visualNumeratorElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualDenumeratorElements = this.getDenumerator().getFlattenedElementTree();
  
    this.visualDenumeratorElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  ModuloElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualDenumeratorElements);
  
    if (result && result.element === this) {
      result = {
        element: this.visualNumeratorElements[this.visualNumeratorElements.length - 1],
        caretPosition: Common.CaretPosition.Right
      };
    }
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualNumeratorElements);
    }
  
    return result;
  };
  
  ModuloElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualNumeratorElements);
  
    if (result && result.element === this) {
      result = {
        element: this.visualDenumeratorElements[0],
        caretPosition: Common.CaretPosition.Left
      };
    }
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualDenumeratorElements);
    }
  
    return result;
  };
  
  ModuloElement.prototype.getInsideVisualElement = function () {
    return {
      element: (this.caretPosition === Common.CaretPosition.Right) ?
        this.visualDenumeratorElements[this.visualDenumeratorElements.length - 1] :
        this.visualNumeratorElements[0],
      caretPosition: this.caretPosition
    };
  };
  
  