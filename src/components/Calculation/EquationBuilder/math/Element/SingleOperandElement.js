import { OperandElement } from "./OperandElement";

export function SingleOperandElement(text, type) {
    OperandElement.call(this);
  
    this.text = text;
    this.type = type || null;
  }
  
  SingleOperandElement.prototype = Object.create(OperandElement.prototype);
  SingleOperandElement.prototype.constructor = SingleOperandElement;
  SingleOperandElement.prototype.className = 'SingleOperandElement';
  
  /**
   * Returns the name part of the element signature if the element is a variable.
   * This is used to get the element text for display purposes only. Variables are held
   * as signatures in the XML document (i.e. <name>[<OBJECT-TYPE>,<OBJECT-TYPE>...]),
   * but are displayed using names.
   *
   * E.g. <m:ci type="variable">NStdVol[STREAM,DAY]</m:ci> is displayed as NStdVol.
   */
  SingleOperandElement.prototype.getDisplayText = function () {
    var text = this.text;
  
    if (this.text && this.type) {
      if (this.type === 'variable') {
        var pos = text.indexOf('[');
  
        if (pos !== -1) {
          text = text.substring(0, pos);
        }
      } else if (this.type === 'logstmt') {
        if (this.text === 'NONFATALERR') {
          text = 'ERROR';
        } else if (this.text === 'ERROR') {
          text = 'FATAL';
        }
      }
    }
  
    return text;
  };
  