import { SeparatorElement } from "./SeparatorElement";
import { EmptyElement } from "./EmptyElement";
import { OperandElement } from "./OperandElement";

export function FunctionElement(name, encoding, url, numSubscripts, numArgs) {
    OperandElement.call(this);
  
    this.name = name;
    this.encoding = encoding || 'text';
    this.url = url || 'http:#';
    this.hasSubscript = numSubscripts > 0;
    this.argCount = numArgs;
  
    this.visualArgsElements = []; // Contains csymbol argument elements that visually lie on the same line.
    this.visualSubscriptElements = []; // Contains subscript elements that visually lie on the same line.
  
    this.children.reset(new EmptyElement(), new EmptyElement());
  
    if (this.hasSubscript) {
      for (var i = 1; i < numSubscripts; ++i) {
        this.getSubscript().children.add(new SeparatorElement());
      }
    }
  
    for (var i = 1; i < numArgs; ++i) {
      this.getCsymbolArguments().children.add(new SeparatorElement());
    }
  }
  
  FunctionElement.prototype = Object.create(OperandElement.prototype);
  FunctionElement.prototype.constructor = FunctionElement;
  FunctionElement.prototype.className = 'FunctionElement';
  
  FunctionElement.prototype.getSubscript = function () {
    return this.children.at(0);
  };
  
  FunctionElement.prototype.setSubscript = function (element) {
    if (element) {
      this.children.replace(this.getSubscript(), element);
    }
  };
  
  FunctionElement.prototype.getCsymbolArguments = function () {
    return this.children.at(1);
  };
  
  FunctionElement.prototype.setCsymbolArguments = function (element) {
    if (element) {
      this.children.replace(this.getCsymbolArguments(), element);
    }
  };
  
  FunctionElement.prototype.buildVisualElementList = function () {
    this.visualArgsElements = this.getCsymbolArguments().getFlattenedElementTree();
  
    this.visualArgsElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  
    this.visualSubscriptElements = this.getSubscript().getFlattenedElementTree();
  
    this.visualSubscriptElements.forEach(function (element) {
      element.visualParent = this;
    }, this);
  };
  
  FunctionElement.prototype.getLeftVisualElementForChild = function (child) {
    var result = this.getLeftVisualElementInList(child, this.visualArgsElements);
  
    if (!result) {
      result = this.getLeftVisualElementInList(child, this.visualSubscriptElements);
    }
  
    return result;
  };
  
  FunctionElement.prototype.getRightVisualElementForChild = function (child) {
    var result = this.getRightVisualElementInList(child, this.visualArgsElements);
  
    if (!result) {
      result = this.getRightVisualElementInList(child, this.visualSubscriptElements);
    }
  
    return result;
  };
  
  FunctionElement.prototype.getInsideVisualElement = function () {
    return this.getInsideVisualElementInList(this.visualArgsElements);
  };
  
  FunctionElement.prototype.getBelowVisualElement = function () {
    return this.getBelowVisualElementInList(this.visualSubscriptElements);
  };
  
  FunctionElement.prototype.getActualArgs = function () {
    if (this.argCount === 1 || this.getCsymbolArguments() instanceof EmptyElement) {
      return [this.getCsymbolArguments()];
    }
  
    return this.getCsymbolArguments().children.elements;
  };
  