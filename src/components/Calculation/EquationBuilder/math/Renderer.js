/**
 * Renders equations on html canvases.
 */


import { Equation } from "./Equation";
import { Elements } from "./Elements";
import { Common } from "./Commons";
import $ from 'jquery'
import { FontFamily, Styles } from "./Module/Styles";

function Size(width, height) {
  this.width = width || 0;
  this.height = height || 0;
}

function Location(x, y) {
  this.x = Math.floor(x) || 0;
  this.y = Math.floor(y) || 0;
}


//-------------------------------------------------------------------------------------------------------------
// Fonts and Styles
//-------------------------------------------------------------------------------------------------------------


/**
 * Simple font class used internally for storing element font info.
 */
function Font(family, size, unit, style) {
  this.family = family || Styles.DefaultFontFamily;
  this.size = size || Styles.DefaultFontSize;
  this.unit = unit || Styles.DefaultFontUnit;
  this.style = style;
  this.lineHeight = Styles.DefaultFontLineHeight;
}

/**
 * Gets font properties as CSS value, e.g. 'italic 11px/1.15 Arial'.
 *
 * @return CSS font property value.
 */
Font.prototype.css = function () {
  return (this.style ? this.style + ' ' : '') + this.size + this.unit + '/' + this.lineHeight + ' ' + this.family;
};

/**
 * Gets the value of a named style used by the equation renderer.
 *
 * @param name: The name of the style to fetch.
 */
const getStyle = function (name) {
  return Styles[name];
};

/**
 * Sets the value of a named style used by the equation renderer.
 *
 * @param name: The name of the style to alter.
 * @param value: The new value of the style.
 */
const setStyle = function (name, value) {
  if (Styles[name]) {
    Styles[name] = value;
  }
};

//-------------------------------------------------------------------------------------------------------------
// Text Metrics
//-------------------------------------------------------------------------------------------------------------

/**
 * Cache for text metrics to avoid having to recalculate them every time an equation is rendered.
 */
var TextMetricCache = {};

/**
 * Measures the width and height of a string rendered with a specific font.
 *
 * @param str: The text string to be measured.
 * @param font: The font to use for measurement.
 */
function measureText(str, font) {
  var cacheKey = font.css() + ' ' + str;
  // debugger
  // console.log(str);

  console.log(TextMetricCache);
  if (!TextMetricCache[cacheKey]) {
    // We could have used canvas context.measureText() to get the text width, but we
    // also need the height. Thus we might as well get both using the following technique.
    // if (!TextMetricCache.$div) {
    if (!TextMetricCache.div) { TextMetricCache = document.createElement('div');
      TextMetricCache.style.position = "absolute";
      TextMetricCache.style.top = "-9999px";
      TextMetricCache.style.left = "-9999px";
      TextMetricCache.style.whiteSpace = "nowrap";
      document.body.appendChild(TextMetricCache);
    }
    // debugger
    TextMetricCache.style.font = font.css()
    TextMetricCache.innerHTML = str
    var rect = TextMetricCache.getBoundingClientRect();

    console.log(TextMetricCache); TextMetricCache[cacheKey] = new Size(rect.width, rect.height);
  }

  var size = TextMetricCache[cacheKey];

  return new Size(size.width, size.height);
}


//-------------------------------------------------------------------------------------------------------------
// Rendering
//-------------------------------------------------------------------------------------------------------------

/**
 * Renders an equation on a canvas.
 *
 * @param ctx: An object containing the render context.
 *   ctx = {
 *     equation: The equation to render.
 *     canvas:   The canvas to render on.
 *     editable: Whether or not the canvas is in edit mode.
 *   }
 */
const render = function (ctx) {
  ctx.gfx = ctx.canvas.getContext('2d');
  renderBackground(ctx);
  renderEquation(ctx);
};

function renderBackground(ctx) {
  if (!ctx.editable && ctx.equation.isRemark()) {
    ctx.gfx.fillStyle = Styles.BackgroundColorRemark;
  } else {
    ctx.gfx.fillStyle = Styles.BackgroundColor;
  }

  ctx.gfx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function renderEquation(ctx) {
  // debugger
  console.log(ctx.equation);
  // Apply global rendering styles to the canvas context.
  ctx.gfx.save();
  ctx.gfx.fillStyle = Styles.TextColor;
  ctx.gfx.strokeStyle = Styles.TextColor;
  ctx.gfx.textBaseline = 'top';
  ctx.gfx.translate(Styles.CanvasPadding, Styles.CanvasPadding);

  // Calculate element sizes and render.
  ctx.equation.resize();
  ctx.equation.render(ctx.gfx);
  ctx.gfx.restore();

  console.log(ctx);
}

function drawLine(fromX, fromY, toX, toY, width, gfx) {
  fromX = Math.floor(fromX);
  fromY = Math.floor(fromY);
  toX = Math.floor(toX);
  toY = Math.floor(toY);

  if (width % 2 !== 0) {
    // Prevent lines of odd numbered width from becoming wide and blurry.
    fromX += 0.5;
    fromY += 0.5;
    toX += 0.5;
    toY += 0.5;
  }

  gfx.lineWidth = width;
  gfx.beginPath();
  gfx.moveTo(fromX, fromY);
  gfx.lineTo(toX, toY);
  gfx.stroke();
}

function drawText(str, x, y, font, gfx) {
  // Adjust text position to compensate for differences in browser font rendering.
  // if ($.browser.mozilla && font.family === FontFamily.Arial) {
  y += Styles.ArialFirefoxVerticalAdjust;
  // }

  if (font.family === FontFamily.LucidaSansUnicode) {
    // if ($.browser.mozilla) {
    //   y += Styles.LucidaSansUnicodeFirefoxVerticalAdjust;
    // } else {
    y += Styles.LucidaSansUnicodeChromeIEVerticalAdjust;
    // }
  }

  x = Math.floor(x);
  y = Math.floor(y);

  gfx.font = font.css();
  gfx.fillText(str, x, y);
}

function drawArc(x, y, width, height, lineWidth, gfx) {
  gfx.lineWidth = lineWidth;
  gfx.beginPath();
  gfx.moveTo(x, y);
  gfx.quadraticCurveTo(x + width, y + height / 2, x, y + height);
  gfx.stroke();
}

function getCurlyBraceWidth(height) {
  var width = Math.round(height * Styles.CurlyBraceWidthRatio);

  if (width > Styles.MaxCurlyBraceWidth) {
    return Styles.MaxCurlyBraceWidth;
  } else if (width < Styles.MinCurlyBraceWidth) {
    return Styles.MinCurlyBraceWidth;
  }

  return width;
}

function drawCurlyBrace(x, y, height, lineWidth, gfx, leftPointing) {
  x = Math.floor(x);
  y = Math.floor(y);

  var width = getCurlyBraceWidth(height);
  var halfWidth = width / 2;
  var quarterWidth = width / 4;
  var halfHeight = height / 2;

  gfx.lineWidth = lineWidth;
  gfx.beginPath();

  if (leftPointing) {
    gfx.moveTo(x + width, y);
    gfx.quadraticCurveTo(x + halfWidth, y, x + halfWidth, y + halfWidth + quarterWidth);
    gfx.lineTo(x + halfWidth, y + halfHeight - halfWidth - quarterWidth);
    gfx.quadraticCurveTo(x + halfWidth, y + halfHeight - quarterWidth, x, y + halfHeight);
    gfx.quadraticCurveTo(x + halfWidth, y + halfHeight + quarterWidth, x + halfWidth, y + halfHeight + halfWidth + quarterWidth);
    gfx.lineTo(x + halfWidth, y + height - halfWidth - quarterWidth);
    gfx.quadraticCurveTo(x + halfWidth, y + height, x + width, y + height);
  } else {
    gfx.moveTo(x, y);
    gfx.quadraticCurveTo(x + halfWidth, y, x + halfWidth, y + halfWidth + quarterWidth);
    gfx.lineTo(x + halfWidth, y + halfHeight - halfWidth - quarterWidth);
    gfx.quadraticCurveTo(x + halfWidth, y + halfHeight - quarterWidth, x + width, y + halfHeight);
    gfx.quadraticCurveTo(x + halfWidth, y + halfHeight + quarterWidth, x + halfWidth, y + halfHeight + halfWidth + quarterWidth);
    gfx.lineTo(x + halfWidth, y + height - halfWidth - quarterWidth);
    gfx.quadraticCurveTo(x + halfWidth, y + height, x, y + height);
  }

  gfx.stroke();
}

// function fillRect(x, y, width, height, gfx) {
//   x = Math.floor(x);
//   y = Math.floor(y);
//   width = Math.floor(width);
//   height = Math.floor(height);
//   gfx.fillRect(x, y, width, height);
// }

function translate(x, y, gfx) {
  x = Math.floor(x);
  y = Math.floor(y);
  gfx.translate(x, y);
}

function addElementPadding(size) {
  size.width += Styles.ElementPadding * 2;
  return size;
}


//-------------------------------------------------------------------------------------------------------------
// Caret
//-------------------------------------------------------------------------------------------------------------

// const renderCaret = function (ctx, visible) {
//   var element = ctx.equation.currentElement;

//   if (!element) {
//     return;
//   }

//   var gfx = ctx.canvas.getContext('2d');
//   var location = element.getAbsoluteLocation();
//   var size = (element instanceof Elements.OperatorElement) ? element.operatorSize : element.size;

//   gfx.save();

//   // Use fillRect for drawing lines without antialiasing on line ends, because
//   // antialiased ends don't get properly cleared when redrawing the line with a lighter color...
//   gfx.fillStyle = visible ? Styles.CaretColor : Styles.BackgroundColor;

//   // Draw line under element.
//   fillRect(location.x, location.y + size.height, size.width + Styles.CaretWidth, Styles.CaretWidth, gfx);

//   if (element.caretPosition === Common.CaretPosition.Left) {
//     // Draw line on left side of element.
//     fillRect(location.x, location.y, Styles.CaretWidth, size.height + Styles.CaretWidth, gfx);
//   } else if (element.caretPosition === Common.CaretPosition.Right) {
//     // Draw line on right side of element.
//     fillRect(location.x + size.width, location.y, Styles.CaretWidth, size.height + Styles.CaretWidth, gfx);
//   }

//   gfx.restore();
// };


//-------------------------------------------------------------------------------------------------------------
// Element
//-------------------------------------------------------------------------------------------------------------

Elements.Element.prototype.calculateSize = function () {
  this.size = new Size();
};

Elements.Element.prototype.calculateLocation = function () {
  this.location = new Location();
};

Elements.Element.prototype.getLineCenterOffset = function () {
  return this.size.height / 2;
};

Elements.Element.prototype.render = function (gfx) {
};


//-------------------------------------------------------------------------------------------------------------
// Equation
//-------------------------------------------------------------------------------------------------------------

const EquationRenderer = {
  resize() {
    if (this.needsResize) {
      this.calculateSize();
      this.calculateLocation();
      this.needsResize = false;
      this.equationSize = null;

      return true;
    }

    return false;
  },

  calculateSize() {
    if (!this.font) {
      this.font = new Font();
    }

    var rootElement = this.getRootElement();

    rootElement.calculateSize();

    this.size = rootElement.size;
    this.location = rootElement.location;
    this.equationSize = null;
  },
  calculateLocation() {
    var rootElement = this.getRootElement();
    rootElement.calculateLocation();
    this.location = rootElement.location;
    this.location.x += Styles.CanvasPadding;
    this.location.y += Styles.CanvasPadding;
    this.buildVisualElementList();
    this.equationSize = null;
  },

  getEquationSize() {
    if (!this.equationSize) {
      var rootElement = this.getRootElement();
      if (rootElement instanceof Elements.OperatorElement) {
        // Need to adjust root element according to its children.
        var elements = rootElement.getFlattenedElementTree();
        var center = rootElement.getLineCenterOffset();
        var height = 0;

        elements.forEach(function (element) {
          height = Math.max(height, center - element.getLineCenterOffset() + element.size.height);
        });

        this.equationSize = new Size(Math.ceil(rootElement.size.width + Styles.CanvasPadding * 2),
          Math.ceil(height + Styles.CanvasPadding * 2));
      } else {
        this.equationSize = new Size(Math.ceil(this.size.width + Styles.CanvasPadding * 2),
          Math.ceil(this.size.height + Styles.CanvasPadding * 2));
      }
    }

    return this.equationSize;
  },

  render(gfx) {
    this.getRootElement().render(gfx);
  }


}
Object.setPrototypeOf(Equation.prototype, EquationRenderer);


//-------------------------------------------------------------------------------------------------------------
// OperatorElement
//-------------------------------------------------------------------------------------------------------------

Elements.OperatorElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var leftOperand = this.getLeftOperand();
  var rightOperand = this.getRightOperand();

  leftOperand.calculateSize();
  rightOperand.calculateSize();

  if (!this.operatorFont || this.operatorFont.size !== this.font.size) {
    this.operatorFont = new Font(Styles.OperatorFontFamily, this.font.size, this.font.unit);
  }

  this.operatorLocation = new Location();
  this.operatorSize = addElementPadding(measureText(this.operatorText, this.operatorFont));

  this.size = new Size(leftOperand.size.width + this.operatorSize.width + rightOperand.size.width,
    Math.max(leftOperand.size.height, this.operatorSize.height, rightOperand.size.height));

  // May need to adjust the size if operands must be relocated.
  this.calculateLocation();
};

Elements.OperatorElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var leftOperand = this.getLeftOperand();
  var rightOperand = this.getRightOperand();

  leftOperand.calculateLocation();
  rightOperand.calculateLocation();

  var center = this.getLineCenterOffset();

  // Adjust all elements to lay on the same line.
  leftOperand.location = new Location(0, center - leftOperand.getLineCenterOffset());
  this.operatorLocation = new Location(leftOperand.size.width, center - this.operatorSize.height / 2);
  rightOperand.location = new Location(leftOperand.size.width + this.operatorSize.width,
    center - rightOperand.getLineCenterOffset());

  // Determine whether we need to adjust our size since we have relocated elements.
  var heightLeft = leftOperand.location.y + leftOperand.size.height;
  var heightRight = rightOperand.location.y + rightOperand.size.height;
  var heightOperator = this.operatorLocation.y + this.operatorSize.height;
  var maxHeight = Math.max(heightLeft, heightRight, heightOperator);

  if (maxHeight > this.size.height) {
    this.size.height = maxHeight;
  }
};

Elements.OperatorElement.prototype.getLineCenterOffset = function () {
  return Math.max(this.getLeftOperand().getLineCenterOffset(),
    this.getRightOperand().getLineCenterOffset(),
    this.operatorSize.height / 2);
};

Elements.OperatorElement.prototype.render = function (gfx) {
  // Draw operator symbol.
  drawText(this.operatorText,
    this.operatorLocation.x + Styles.ElementPadding, this.operatorLocation.y,
    this.operatorFont, gfx);

  // Draw left side.
  var leftOperand = this.getLeftOperand();
  gfx.save();
  translate(leftOperand.location.x, leftOperand.location.y, gfx);
  leftOperand.render(gfx);
  gfx.restore();

  // Draw right side.
  var rightOperand = this.getRightOperand();
  gfx.save();
  translate(rightOperand.location.x, rightOperand.location.y, gfx);
  rightOperand.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// SingleOperandElement
//-------------------------------------------------------------------------------------------------------------

Elements.SingleOperandElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  this.size = addElementPadding(measureText(this.getDisplayText(), this.font));
};

Elements.SingleOperandElement.prototype.render = function (gfx) {
  drawText(this.getDisplayText(), Styles.ElementPadding, 0, this.font, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// IdentifierSubElement
//-------------------------------------------------------------------------------------------------------------

Elements.IdentifierSubElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }
  var fontStyle = null
  if (this.font.style === 'italic') {
    fontStyle = 'normal';
  } else {
    fontStyle = 'italic';
  }

  var subscript = this.getSubscript();

  // Subscript is drawn with font size smaller than parent (identifier).
  if (!subscript.font || subscript.font.size === this.font.size) {
    subscript.font = new Font(this.font.family, this.font.size * Styles.SubscriptScale, this.font.unit, fontStyle);
  }

  Elements.SingleOperandElement.prototype.calculateSize.call(this);
  subscript.calculateSize();
  this.identifierSize = this.size;

  this.size = new Size(this.identifierSize.width + subscript.size.width,
    this.identifierSize.height + subscript.size.height - this.getLineCenterOffset());
};

Elements.IdentifierSubElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var subscript = this.getSubscript();

  Elements.SingleOperandElement.prototype.calculateLocation.call(this);
  subscript.calculateLocation();

  subscript.location = new Location(this.identifierSize.width,
    this.identifierSize.height - this.getLineCenterOffset());
  this.buildVisualElementList();
};

Elements.IdentifierSubElement.prototype.getLineCenterOffset = function () {
  return this.identifierSize.height / 2;
};

Elements.IdentifierSubElement.prototype.render = function (gfx) {
  // Draw identifier.
  Elements.SingleOperandElement.prototype.render.call(this, gfx);

  // Draw subscript.
  var subscript = this.getSubscript();
  gfx.save();
  translate(subscript.location.x, subscript.location.y, gfx);
  subscript.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// CardinalityElement
//-------------------------------------------------------------------------------------------------------------

Elements.CardinalityElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var set = this.getSet();
  set.calculateSize();

  this.size = addElementPadding(new Size(set.size.width + Styles.VerticalBarWidth * 2, set.size.height));
};

Elements.CardinalityElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var set = this.getSet();

  set.calculateLocation();
  set.location = new Location(Styles.VerticalBarWidth + Styles.ElementPadding, 0);

  this.buildVisualElementList();
};

Elements.CardinalityElement.prototype.render = function (gfx) {
  // Draw left vertical bar.
  drawLine(Styles.ElementPadding, 0, Styles.ElementPadding, this.size.height, Styles.VerticalBarWidth, gfx);

  // Draw right vertical bar.
  drawLine(this.size.width - Styles.ElementPadding, 0,
    this.size.width - Styles.ElementPadding, this.size.height,
    Styles.VerticalBarWidth, gfx);

  // Draw set.
  var set = this.getSet();
  gfx.save();
  translate(set.location.x, set.location.y, gfx);
  set.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// FilterSetElement
//-------------------------------------------------------------------------------------------------------------

Elements.FilterSetElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var set = this.getSet();
  var filter = this.getFilter();

  set.calculateSize();
  filter.calculateSize();

  this.barSize = addElementPadding(new Size(Styles.VerticalBarWidth, filter.size.height));
  this.size = new Size(set.size.width + this.barSize.width + filter.size.width,
    Math.max(set.size.height, filter.size.height));
  this.fenceSize = addElementPadding(new Size(getCurlyBraceWidth(this.size.height), this.size.height));
  this.size.width += this.fenceSize.width * 2;
};

Elements.FilterSetElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var set = this.getSet();
  var filter = this.getFilter();

  set.calculateLocation();
  filter.calculateLocation();

  set.location = new Location(this.fenceSize.width, 0);
  filter.location = new Location(set.location.x + set.size.width + this.barSize.width, 0);

  this.buildVisualElementList();
};

Elements.FilterSetElement.prototype.render = function (gfx) {
  var set = this.getSet();
  var filter = this.getFilter();

  // Draw curly brackets.
  drawCurlyBrace(Styles.ElementPadding, 0, this.fenceSize.height, Styles.CurlyBraceLineWidth, gfx, true);
  drawCurlyBrace(this.size.width - this.fenceSize.width + Styles.ElementPadding, 0, this.fenceSize.height,
    Styles.CurlyBraceLineWidth, gfx, false);

  // Draw vertical bar.
  drawLine(filter.location.x - this.barSize.width + Styles.ElementPadding, 0,
    filter.location.x - this.barSize.width + Styles.ElementPadding, this.size.height,
    Styles.VerticalBarWidth, gfx);

  // Draw set and filter.
  gfx.save();
  translate(set.location.x, set.location.y, gfx);
  set.render(gfx);
  translate(filter.location.x - set.location.x, filter.location.y - set.location.y, gfx);
  filter.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// ForallElement
//-------------------------------------------------------------------------------------------------------------

Elements.ForallElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var operand = this.getOperand();
  operand.calculateSize();

  this.size = new Size(operand.size.width, operand.size.height);
};

Elements.ForallElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var operand = this.getOperand();
  operand.calculateLocation();

  this.buildVisualElementList();
};

Elements.ForallElement.prototype.render = function (gfx) {
  var operand = this.getOperand();
  gfx.save();
  translate(operand.location.x, operand.location.y, gfx);
  operand.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// DivideElement
//-------------------------------------------------------------------------------------------------------------

Elements.DivideElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();

  numerator.calculateSize();
  denumerator.calculateSize();

  this.size = addElementPadding(new Size(
    Math.max(numerator.size.width, denumerator.size.width),
    numerator.size.height + denumerator.size.height + Styles.HorizontalBarWidth + Styles.ElementPadding * 2));
};

Elements.DivideElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();

  numerator.calculateLocation();
  denumerator.calculateLocation();

  // Center elements.
  numerator.location = new Location(this.size.width / 2 - numerator.size.width / 2, 0);
  denumerator.location = new Location(this.size.width / 2 - denumerator.size.width / 2,
    numerator.size.height + Styles.HorizontalBarWidth + Styles.ElementPadding * 2);
  this.buildVisualElementList();
};

Elements.DivideElement.prototype.render = function (gfx) {
  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();
  // Draw divider line.
  drawLine(Styles.ElementPadding, numerator.size.height + Styles.ElementPadding,
    this.size.width - Styles.ElementPadding, numerator.size.height + Styles.ElementPadding,
    Styles.HorizontalBarWidth, gfx);

  // Draw numerator and denumerator.
  gfx.save();
  translate(numerator.location.x, numerator.location.y, gfx);
  numerator.render(gfx);
  translate(denumerator.location.x - numerator.location.x, denumerator.location.y - numerator.location.y, gfx);
  denumerator.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// UnaryMinusElement
//-------------------------------------------------------------------------------------------------------------

Elements.UnaryMinusElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  if (!this.operatorFont || this.operatorFont.size !== this.font.size) {
    this.operatorFont = new Font(Styles.OperatorFontFamily, this.font.size, this.font.unit);
  }

  var operand = this.getOperand();

  this.minusSize = measureText(this.text, this.operatorFont);
  this.minusSize.width += Styles.ElementPadding; // Only padding on left side.

  operand.calculateSize();

  this.size = new Size(this.minusSize.width + operand.size.width,
    Math.max(this.minusSize.height, operand.size.height));
};

Elements.UnaryMinusElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var operand = this.getOperand();

  operand.calculateLocation();
  operand.location = new Location(this.minusSize.width, 0);

  // Position minus vertically at center of operand.
  var y = operand.getLineCenterOffset() - this.minusSize.height / 2;

  if (y < 0) {
    y = 0;
  }

  this.minusLocation = new Location(0, y);
  operand.visualParent = this;
};

Elements.UnaryMinusElement.prototype.render = function (gfx) {
  // Draw unary operator.
  drawText(this.text, this.minusLocation.x + Styles.ElementPadding, this.minusLocation.y, this.operatorFont, gfx);

  // Draw operand.
  var operand = this.getOperand();
  gfx.save();
  translate(operand.location.x, operand.location.y, gfx);
  operand.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// NotElement
//-------------------------------------------------------------------------------------------------------------

Elements.NotElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  if (!this.operatorFont || this.operatorFont.size !== this.font.size) {
    this.operatorFont = new Font(Styles.OperatorFontFamily, this.font.size, this.font.unit);
  }

  var operand = this.getOperand();

  this.notSize = measureText(this.text, this.operatorFont);
  this.notSize.width += Styles.ElementPadding; // Only padding on left side.

  operand.calculateSize();

  this.size = new Size(this.notSize.width + operand.size.width,
    Math.max(this.notSize.height, operand.size.height));
};

Elements.NotElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var operand = this.getOperand();

  operand.calculateLocation();
  operand.location = new Location(this.notSize.width, 0);

  // Position not operator vertically at center of operand.
  var y = operand.getLineCenterOffset() - this.notSize.height / 2;

  if (y < 0) {
    y = 0;
  }

  this.notLocation = new Location(0, y);
  operand.visualParent = this;
};

Elements.NotElement.prototype.render = function (gfx) {
  // Draw not operator.
  drawText(this.text, this.notLocation.x + Styles.ElementPadding, this.notLocation.y, this.operatorFont, gfx);

  // Draw operand.
  var operand = this.getOperand();
  gfx.save();
  translate(operand.location.x, operand.location.y, gfx);
  operand.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// FenceElement
//-------------------------------------------------------------------------------------------------------------

Elements.FenceElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var container = this.getContainer();
  container.calculateSize();

  switch (this.type) {
    case Elements.FenceElement.FenceType.Parenthesis:
      this.fenceLeftSize = addElementPadding(new Size(
        container.size.height * Styles.ParenWidthHeightRatio * Styles.ParenWidthScale,
        container.size.height));
      this.fenceRightSize = this.fenceLeftSize;
      break;
    case Elements.FenceElement.FenceType.SquareBracket:
      this.fenceLeftSize = addElementPadding(measureText('[', this.font));
      this.fenceRightSize = addElementPadding(measureText(']', this.font));
      break;
    case Elements.FenceElement.FenceType.CurlyBracket:
      this.fenceLeftSize = addElementPadding(measureText('{', this.font));
      this.fenceRightSize = addElementPadding(measureText('}', this.font));
      break;
  }

  this.size = new Size(this.fenceLeftSize.width + container.size.width + this.fenceRightSize.width,
    Math.max(container.size.height, this.fenceLeftSize.height, this.fenceRightSize.height));
};

Elements.FenceElement.prototype.calculateLocation = function () {
  this.location = new Location();

  this.getContainer().calculateLocation();
  this.getContainer().location = new Location(this.fenceLeftSize.width, 0);

  this.buildVisualElementList();
};

Elements.FenceElement.prototype.render = function (gfx) {
  // Draw fence.
  switch (this.type) {
    case Elements.FenceElement.FenceType.Parenthesis:
      // Draw left parenthesis.
      drawArc(this.fenceLeftSize.width - Styles.ElementPadding, 0,
        -this.size.height * Styles.ParenWidthHeightRatio, this.size.height,
        Styles.ScaledParenWidth, gfx);

      // Draw right parenthesis.
      drawArc(this.size.width - this.fenceRightSize.width + Styles.ElementPadding, 0,
        this.size.height * Styles.ParenWidthHeightRatio, this.size.height,
        Styles.ScaledParenWidth, gfx);
      break;
    case Elements.FenceElement.FenceType.SquareBracket:
      drawText('[', Styles.ElementPadding, 0, this.font, gfx);
      drawText(']', this.size.width - this.fenceRightSize.width + Styles.ElementPadding, 0, this.font, gfx);
      break;
    case Elements.FenceElement.FenceType.CurlyBracket:
      drawText('{', Styles.ElementPadding, 0, this.font, gfx);
      drawText('}', this.size.width - this.fenceRightSize.width + Styles.ElementPadding, 0, this.font, gfx);
      break;
  }

  // Draw contents.
  var container = this.getContainer();
  gfx.save();
  translate(container.location.x, container.location.y, gfx);
  container.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// IntervalElement
//-------------------------------------------------------------------------------------------------------------

Elements.IntervalElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var container = this.getContainer();
  container.calculateSize();

  this.intervalSize = new Size(Styles.IntervalFenceWidth + Styles.ElementPadding,
    container.size.height + Styles.IntervalLineWidth * 2);
  this.size = addElementPadding(new Size(this.intervalSize.width * 2 + container.size.width,
    this.intervalSize.height));
};

Elements.IntervalElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var container = this.getContainer();
  container.calculateLocation();
  container.location = new Location(this.intervalSize.width + Styles.ElementPadding, Styles.IntervalLineWidth);

  this.buildVisualElementList();
};

Elements.IntervalElement.prototype.render = function (gfx) {
  // Draw left interval sign:
  drawLine(this.intervalSize.width, 0, Styles.ElementPadding, 0,
    Styles.IntervalLineWidth, gfx);
  drawLine(Styles.ElementPadding, 0, Styles.ElementPadding, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);
  drawLine(Styles.ElementPadding, this.intervalSize.height - Styles.IntervalLineWidth,
    this.intervalSize.width, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);

  // Draw right interval sign:
  drawLine(this.size.width - this.intervalSize.width, 0,
    this.size.width - Styles.ElementPadding, (this.intervalSize.height - Styles.IntervalLineWidth) / 2,
    Styles.IntervalLineWidth, gfx);
  drawLine(this.size.width - Styles.ElementPadding, (this.intervalSize.height - Styles.IntervalLineWidth) / 2,
    this.size.width - this.intervalSize.width, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);

  // Draw contents.
  var container = this.getContainer();
  gfx.save();
  translate(container.location.x, container.location.y, gfx);
  container.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// IntervalSubElement
//-------------------------------------------------------------------------------------------------------------

Elements.IntervalSubElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }
  var fontStyle = null
  if (this.font.style === 'italic') {
    fontStyle = 'normal';
  } else {
    fontStyle = 'italic';
  }

  var subscript = this.getSubscript();
  var container = this.getContainer();

  // Subscript is drawn with font size smaller than parent (identifier).
  if (!subscript.font || subscript.font.size === this.font.size) {
    subscript.font = new Font(this.font.family, this.font.size * Styles.SubscriptScale, this.font.unit, fontStyle);
  }

  container.calculateSize();
  subscript.calculateSize();

  this.intervalSize = new Size(Styles.IntervalFenceWidth + Styles.ElementPadding,
    container.size.height + Styles.IntervalLineWidth * 2);
  this.size = addElementPadding(new Size(this.intervalSize.width * 2 + container.size.width + subscript.size.width,
    this.intervalSize.height));
};

Elements.IntervalSubElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var container = this.getContainer();
  container.calculateLocation();
  container.location = new Location(this.intervalSize.width + Styles.ElementPadding, Styles.IntervalLineWidth);

  var subscript = this.getSubscript();
  subscript.calculateLocation();
  subscript.location = new Location(this.size.width - subscript.size.width, this.size.height / 2);

  // Adjust height of interval element.
  this.size.height = Math.max(subscript.location.y + subscript.size.height, container.size.height);

  this.buildVisualElementList();
};

Elements.IntervalSubElement.prototype.getLineCenterOffset = function () {
  return this.intervalSize.height / 2;
};

Elements.IntervalSubElement.prototype.render = function (gfx) {
  // Draw left interval sign:
  drawLine(this.intervalSize.width, 0, Styles.ElementPadding, 0,
    Styles.IntervalLineWidth, gfx);
  drawLine(Styles.ElementPadding, 0, Styles.ElementPadding, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);
  drawLine(Styles.ElementPadding, this.intervalSize.height - Styles.IntervalLineWidth,
    this.intervalSize.width, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);

  // Draw right interval sign:
  var subscript = this.getSubscript();
  var rightIntervalEdge = this.size.width - subscript.size.width;

  drawLine(rightIntervalEdge - this.intervalSize.width, 0,
    rightIntervalEdge - Styles.ElementPadding, (this.intervalSize.height - Styles.IntervalLineWidth) / 2,
    Styles.IntervalLineWidth, gfx);
  drawLine(rightIntervalEdge - Styles.ElementPadding, (this.intervalSize.height - Styles.IntervalLineWidth) / 2,
    rightIntervalEdge - this.intervalSize.width, this.intervalSize.height - Styles.IntervalLineWidth,
    Styles.IntervalLineWidth, gfx);

  // Draw contents.
  var container = this.getContainer();
  gfx.save();
  translate(container.location.x, container.location.y, gfx);
  container.render(gfx);
  gfx.restore();

  // Draw subscript.
  if (this.hasSubscript) {
    gfx.save();
    translate(subscript.location.x, subscript.location.y, gfx);
    subscript.render(gfx);
    gfx.restore();
  }
};


//-------------------------------------------------------------------------------------------------------------
// ModuloElement
//-------------------------------------------------------------------------------------------------------------

Elements.ModuloElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();

  numerator.calculateSize();
  denumerator.calculateSize();

  this.textSize = addElementPadding(measureText(this.text, this.font));

  this.size = new Size(numerator.size.width + this.textSize.width + denumerator.size.width,
    Math.max(numerator.size.height, this.textSize.height, denumerator.size.height));
};

Elements.ModuloElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();

  numerator.calculateLocation();
  denumerator.calculateLocation();

  var center = this.getLineCenterOffset();

  // Adjust all elements to lay on the same line.
  numerator.location = new Location(0, center - numerator.getLineCenterOffset());
  this.textLocation = new Location(numerator.size.width, center - this.textSize.height / 2);
  denumerator.location = new Location(numerator.size.width + this.textSize.width,
    center - denumerator.getLineCenterOffset());

  // Determine whether we need to adjust our size since we have relocated elements.
  var heightLeft = numerator.location.y + numerator.size.height;
  var heightRight = denumerator.location.y + denumerator.size.height;
  var heightText = this.textLocation.y + this.textSize.height;
  var maxHeight = Math.max(heightLeft, heightRight, heightText);

  if (maxHeight > this.size.height) {
    this.size.height = maxHeight;
  }

  this.buildVisualElementList();
};

Elements.ModuloElement.prototype.getLineCenterOffset = function () {
  return Math.max(this.getNumerator().getLineCenterOffset(),
    this.getDenumerator().getLineCenterOffset(),
    this.textSize.height / 2);
};

Elements.ModuloElement.prototype.render = function (gfx) {
  drawText(this.text, this.textLocation.x + Styles.ElementPadding, this.textLocation.y, this.font, gfx);

  // Draw numerator and denumerator.
  var numerator = this.getNumerator();
  var denumerator = this.getDenumerator();

  gfx.save();
  translate(numerator.location.x, numerator.location.y, gfx);
  numerator.render(gfx);
  translate(denumerator.location.x - numerator.location.x, denumerator.location.y - numerator.location.y, gfx);
  denumerator.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// PowerElement
//-------------------------------------------------------------------------------------------------------------

Elements.PowerElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var operand = this.getOperand();
  var power = this.getPower();

  // Power is drawn with font size smaller than parent (operand).
  if (!power.font || power.font.size === this.font.size) {
    power.font = new Font(this.font.family, this.font.size * Styles.SuperscriptScale, this.font.unit);
  }

  operand.calculateSize();
  power.calculateSize();

  this.size = new Size(operand.size.width + power.size.width, operand.size.height + power.size.height / 2);
};

Elements.PowerElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var operand = this.getOperand();
  var power = this.getPower();

  operand.calculateLocation();
  power.calculateLocation();

  power.location = new Location(operand.size.width, 0);
  operand.location = new Location(0, power.size.height - power.getLineCenterOffset());

  // Adjust our height.
  this.size.height = operand.size.height + power.size.height - power.getLineCenterOffset();

  this.buildVisualElementList();
};

Elements.PowerElement.prototype.getLineCenterOffset = function () {
  return this.getOperand().location.y + this.getOperand().getLineCenterOffset();
};

Elements.PowerElement.prototype.render = function (gfx) {
  // Draw operand.
  var operand = this.getOperand();
  gfx.save();
  translate(operand.location.x, operand.location.y, gfx);
  operand.render(gfx);
  gfx.restore();

  // Draw power.
  var power = this.getPower();
  gfx.save();
  translate(power.location.x, power.location.y, gfx);
  power.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// FunctionElement
//-------------------------------------------------------------------------------------------------------------

Elements.FunctionElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }
  var fontStyle = null
  if (this.font.style === 'italic') {
    fontStyle = 'normal';
  } else {
    fontStyle = 'italic';
  }

  var subscript = this.getSubscript();
  var args = this.getCsymbolArguments();

  // Subscript is drawn with font size smaller than parent (function).
  if (!subscript.font || subscript.font.size === this.font.size) {
    subscript.font = new Font(this.font.family, this.font.size * Styles.SubscriptScale, this.font.unit, fontStyle);
  }

  subscript.calculateSize();
  args.calculateSize();

  if (!this.hasSubscript) {
    subscript.size = new Size();
  }

  this.textSize = measureText(this.name, this.font);

  this.fenceLeftSize = addElementPadding(new Size(
    args.size.height * Styles.ParenWidthHeightRatio * Styles.ParenWidthScale, args.size.height));
  this.fenceRightSize = new Size(this.fenceLeftSize.width - Styles.ElementPadding, this.fenceLeftSize.height);

  this.size = addElementPadding(new Size(
    this.textSize.width + subscript.size.width + args.size.width + this.fenceLeftSize.width + this.fenceRightSize.width,
    Math.max(subscript.size.height + this.textSize.height / 2, args.size.height)));
};

Elements.FunctionElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var subscript = this.getSubscript();
  var args = this.getCsymbolArguments();

  subscript.calculateLocation();
  args.calculateLocation();

  if (!this.hasSubscript) {
    subscript.size = new Size();
  }

  var center = this.getLineCenterOffset();

  this.textLocation = new Location(Styles.ElementPadding, center - this.textSize.height / 2);
  subscript.location = new Location(this.textLocation.x + this.textSize.width,
    this.textLocation.y + this.textSize.height / 2);
  args.location = new Location(this.textLocation.x + this.textSize.width + subscript.size.width +
    this.fenceLeftSize.width, center - args.size.height / 2);

  // Determine whether we need to adjust our size since we have relocated elements.
  var heightFunc = this.textLocation.y + this.textSize.height / 2 + subscript.size.height;
  var heightArgs = args.location.y + args.size.height;
  var maxHeight = Math.max(heightFunc, heightArgs);

  if (maxHeight > this.size.height) {
    this.size.height = maxHeight;
  }

  this.buildVisualElementList();
};

Elements.FunctionElement.prototype.getLineCenterOffset = function () {
  return this.getCsymbolArguments().size.height / 2;
};

Elements.FunctionElement.prototype.render = function (gfx) {
  drawText(this.name, this.textLocation.x, this.textLocation.y, this.font, gfx);

  // Draw subscript.
  var subscript = this.getSubscript();

  if (this.hasSubscript) {
    gfx.save();
    translate(subscript.location.x, subscript.location.y, gfx);
    subscript.render(gfx);
    gfx.restore();
  }

  // Draw arguments.
  var args = this.getCsymbolArguments();
  gfx.save();
  translate(args.location.x, args.location.y, gfx);
  args.render(gfx);
  gfx.restore();

  // Draw left parenthesis.
  drawArc(this.textLocation.x + this.textSize.width + subscript.size.width + this.fenceLeftSize.width -
    Styles.ElementPadding, 0, -args.size.height * Styles.ParenWidthHeightRatio,
    args.size.height, Styles.ScaledParenWidth, gfx);

  // Draw right parenthesis.
  drawArc(this.size.width - this.fenceRightSize.width, 0, args.size.height * Styles.ParenWidthHeightRatio,
    args.size.height, Styles.ScaledParenWidth, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// FloorElement
//-------------------------------------------------------------------------------------------------------------

Elements.FloorElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var container = this.getContainer();
  container.calculateSize();

  this.floorSize = new Size(Styles.FloorFenceWidth + Styles.ElementPadding, container.size.height + Styles.FloorLineWidth);
  this.size = new Size(this.floorSize.width * 2 + container.size.width, this.floorSize.height + Styles.FloorLineWidth);
};

Elements.FloorElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var container = this.getContainer();
  container.calculateLocation();

  container.location = new Location(this.floorSize.width, 0);

  this.buildVisualElementList();
};

Elements.FloorElement.prototype.render = function (gfx) {
  // Draw left floor sign:
  drawLine(Styles.ElementPadding, 0, Styles.ElementPadding, this.floorSize.height, Styles.FloorLineWidth, gfx);
  drawLine(Styles.ElementPadding, this.floorSize.height, this.floorSize.width, this.floorSize.height,
    Styles.FloorLineWidth, gfx);

  // Draw right floor sign:
  drawLine(this.size.width - Styles.ElementPadding - Styles.FloorLineWidth, 0,
    this.size.width - Styles.ElementPadding - Styles.FloorLineWidth, this.floorSize.height,
    Styles.FloorLineWidth, gfx);
  drawLine(this.size.width - Styles.ElementPadding - Styles.FloorLineWidth, this.floorSize.height,
    this.size.width - Styles.FloorLineWidth - this.floorSize.width, this.floorSize.height,
    Styles.FloorLineWidth, gfx);

  // Draw contents.
  var container = this.getContainer();
  gfx.save();
  translate(container.location.x, container.location.y, gfx);
  container.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// IterationElement
//-------------------------------------------------------------------------------------------------------------

Elements.IterationElement.prototype.calculateSize = function () {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var subscript = this.getSubscript();
  var operand = this.getOperand();

  // Subscript is drawn with font size smaller than parent (iteration).
  if (!subscript.font || subscript.font.size === this.font.size) {
    subscript.font = new Font(this.font.family, this.font.size * Styles.SubscriptScale, this.font.unit);
  }

  subscript.calculateSize();
  operand.calculateSize();

  this.symbolSize = addElementPadding(measureText(this.symbol, this.symbolFont));
  this.symbolHeightAdjust = this.symbolSize.height - this.symbolSize.height * this.symbolHeightScale;
  this.symbolSize.height -= this.symbolHeightAdjust;

  this.size = new Size(Math.max(this.symbolSize.width, subscript.size.width) + operand.size.width,
    Math.max(this.symbolSize.height + subscript.size.height, operand.size.height));
};

Elements.IterationElement.prototype.calculateLocation = function () {
  this.location = new Location();

  var subscript = this.getSubscript();
  var operand = this.getOperand();

  subscript.calculateLocation();
  operand.calculateLocation();

  this.symbolLocation = new Location(
    Math.max(this.symbolSize.width, subscript.size.width) / 2 - this.symbolSize.width / 2,
    (this.symbolSize.height + subscript.size.height) > operand.size.height ?
      0 : operand.getLineCenterOffset() - this.symbolSize.height / 2);

  subscript.location = new Location(
    Math.max(this.symbolSize.width, subscript.size.width) / 2 - subscript.size.width / 2,
    this.symbolLocation.y + this.symbolSize.height);

  operand.location = new Location(
    Math.max(this.symbolSize.width, subscript.size.width),
    (this.symbolSize.height + subscript.size.height) > operand.size.height ?
      this.symbolSize.height / 2 - operand.getLineCenterOffset() : 0);

  this.size.height = Math.max(this.symbolLocation.y + this.symbolSize.height + subscript.size.height,
    operand.location.y + operand.size.height);

  this.buildVisualElementList();
};

Elements.IterationElement.prototype.getLineCenterOffset = function () {
  return this.symbolLocation.y + this.symbolSize.height / 2;
};

Elements.IterationElement.prototype.render = function (gfx) {
  drawText(this.symbol, this.symbolLocation.x + Styles.ElementPadding,
    this.symbolLocation.y - this.symbolHeightAdjust, this.symbolFont, gfx);

  // Draw subscript.
  var subscript = this.getSubscript();
  gfx.save();
  translate(subscript.location.x, subscript.location.y, gfx);
  subscript.render(gfx);
  gfx.restore();

  // Draw operand.
  var operand = this.getOperand();
  gfx.save();
  translate(operand.location.x, operand.location.y, gfx);
  operand.render(gfx);
  gfx.restore();
};


//-------------------------------------------------------------------------------------------------------------
// SumElement
//-------------------------------------------------------------------------------------------------------------

Elements.SumElement.prototype.calculateSize = function () {
  if (!this.symbolFont) {
    this.symbolFont = new Font(Styles.SumFontFamily, Styles.SumFontSize);
  }

  this.symbolHeightScale = Styles.SumHeightScale;

  Elements.IterationElement.prototype.calculateSize.call(this);
};


//-------------------------------------------------------------------------------------------------------------
// ArbitraryUnionElement
//-------------------------------------------------------------------------------------------------------------

Elements.ArbitraryUnionElement.prototype.calculateSize = function () {
  if (!this.symbolFont) {
    this.symbolFont = new Font(Styles.ArbUnIntFontFamily, Styles.ArbUnIntFontSize);
  }

  this.symbolHeightScale = Styles.ArbUnIntHeightScale;

  Elements.IterationElement.prototype.calculateSize.call(this);
};


//-------------------------------------------------------------------------------------------------------------
// ArbitraryIntersectElement
//-------------------------------------------------------------------------------------------------------------

Elements.ArbitraryIntersectElement.prototype.calculateSize = function () {
  if (!this.symbolFont) {
    this.symbolFont = new Font(Styles.ArbUnIntFontFamily, Styles.ArbUnIntFontSize);
  }

  this.symbolHeightScale = Styles.ArbUnIntHeightScale;

  Elements.IterationElement.prototype.calculateSize.call(this);
};


//-------------------------------------------------------------------------------------------------------------
// Shared rendering methods for function-like elements.
//-------------------------------------------------------------------------------------------------------------

function functionCalculateSize() {
  if (!this.font) {
    this.font = this.parent.font;
  }

  var container = this.getContainer();
  container.calculateSize();

  this.textSize = measureText(this.text, this.font);
  this.fenceLeftSize = addElementPadding(new Size(
    container.size.height * Styles.ParenWidthHeightRatio * Styles.ParenWidthScale, container.size.height));
  this.fenceRightSize = new Size(this.fenceLeftSize.width - Styles.ElementPadding, this.fenceLeftSize.height);

  this.size = addElementPadding(new Size(
    this.textSize.width + container.size.width + this.fenceLeftSize.width + this.fenceRightSize.width,
    Math.max(this.textSize.height, container.size.height, this.fenceLeftSize.height, this.fenceRightSize.height)));
}

function functionCalculateLocation() {
  this.location = new Location();
  this.textLocation = new Location(Styles.ElementPadding);

  var container = this.getContainer();
  container.calculateLocation();
  container.location = new Location(this.textLocation.x + this.textSize.width + this.fenceLeftSize.width, 0);

  // Adjust text according to contents.
  if (container.size.height > this.textSize.height) {
    this.textLocation = new Location(this.textLocation.x, this.getLineCenterOffset() - this.textSize.height / 2);
  }

  this.buildVisualElementList();
}

function functionRender(gfx) {
  drawText(this.text, this.textLocation.x, this.textLocation.y, this.font, gfx);

  // Draw left parenthesis.
  drawArc(this.textLocation.x + this.textSize.width + this.fenceLeftSize.width - Styles.ElementPadding, 0,
    -this.size.height * Styles.ParenWidthHeightRatio, this.size.height, Styles.ScaledParenWidth, gfx);

  // Draw right parenthesis.
  drawArc(this.size.width - this.fenceRightSize.width, 0,
    this.size.height * Styles.ParenWidthHeightRatio, this.size.height, Styles.ScaledParenWidth, gfx);

  // Draw contents.
  var container = this.getContainer();
  gfx.save();
  translate(container.location.x, container.location.y, gfx);
  container.render(gfx);
  gfx.restore();
}


//-------------------------------------------------------------------------------------------------------------
// MaxMinElement
//-------------------------------------------------------------------------------------------------------------

Elements.MaxMinElement.prototype.calculateSize = function () {
  functionCalculateSize.call(this);
};

Elements.MaxMinElement.prototype.calculateLocation = function () {
  functionCalculateLocation.call(this);
};

Elements.MaxMinElement.prototype.render = function (gfx) {
  functionRender.call(this, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// AbsoluteElement
//-------------------------------------------------------------------------------------------------------------

Elements.AbsoluteElement.prototype.calculateSize = function () {
  functionCalculateSize.call(this);
};

Elements.AbsoluteElement.prototype.calculateLocation = function () {
  functionCalculateLocation.call(this);
};

Elements.AbsoluteElement.prototype.render = function (gfx) {
  functionRender.call(this, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// LogarithmElement
//-------------------------------------------------------------------------------------------------------------

Elements.LogarithmElement.prototype.calculateSize = function () {
  functionCalculateSize.call(this);
};

Elements.LogarithmElement.prototype.calculateLocation = function () {
  functionCalculateLocation.call(this);
};

Elements.LogarithmElement.prototype.render = function (gfx) {
  functionRender.call(this, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// NaturalLogarithmElement
//-------------------------------------------------------------------------------------------------------------

Elements.NaturalLogarithmElement.prototype.calculateSize = function () {
  functionCalculateSize.call(this);
};

Elements.NaturalLogarithmElement.prototype.calculateLocation = function () {
  functionCalculateLocation.call(this);
};

Elements.NaturalLogarithmElement.prototype.render = function (gfx) {
  functionRender.call(this, gfx);
};


//-------------------------------------------------------------------------------------------------------------
// ArctanElement
//-------------------------------------------------------------------------------------------------------------

Elements.ArctanElement.prototype.calculateSize = function () {
  functionCalculateSize.call(this);
};

Elements.ArctanElement.prototype.calculateLocation = function () {
  functionCalculateLocation.call(this);
};

Elements.ArctanElement.prototype.render = function (gfx) {
  functionRender.call(this, gfx);
};

export const Renderer = {
  render,
  // renderCaret,
  setStyle,
  getStyle
}
