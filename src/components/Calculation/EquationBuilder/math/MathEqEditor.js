import { Elements } from "./Elements";
import { Builder } from "./Builder";
import { Common } from "./Commons";
import { MathEqReg } from "./MathEq";
import { Renderer } from "./Renderer";
import { renderCaret } from "./Module/Render/Caret";
// import $ from 'jquery'

var CaretBlinkInterval = 700; // Milliseconds between caret blinks.

var EquationType = {
  Iterations: 'ITERATIONS',
  Condition: 'CONDITION',
  Equation: 'EQUATION'
};


export class MathEqEditor {

  constructor(args) {
    this.matheqId = args.matheqId;
    this.screenletId = args.screenletId;
    this.matheq = MathEqReg.get(this.matheqId);
    this.undoStack = [];
    this.redoStack = [];

    // used for check mouse position
    this.positionis = false;
    // used for copy and paste equation
    this.copyEquation = [];

    if (args.activate) {
      this.activate();
    }
  }

  editMatheq = (matheq) => {
    this.setMathml(matheq.getMathml(), true);
    this.matheq.equationType = matheq.equationType;
    this.matheq.calcContextId = matheq.calcContextId;
    this.matheq.calculationId = matheq.calculationId;
    this.matheq.calcDaytime = matheq.calcDaytime;
  }

  setMathml = (mathml, skipRender) => {
    this.matheq.reset(mathml, skipRender);
    this.undoStack = [];
    this.redoStack = [];
  }

  activate = () => {
    // this.matheq.$canvas.on('mousedown.calcMatheqEditor', { editor: this }, onMouseDownInCanvas);
    // debugger

    this.matheq.canvas.addEventListener('mousedown', onMouseDownInCanvas)
    this.matheq.canvas.editor = this
    // $(document).on('keyup.calcMatheqEditor', '#math-equation', {
    //   editor: this
    // }, onKeyUp);
    // debugger
    document.addEventListener("keyup", onKeyUp);
    document.editor = this



    this.startCaretTimer();

  }

  deactivate = () => {
    // this.matheq.$canvas.off('.calcMatheqEditor');
    // $(document).off('.calcMatheqEditor');
    this.matheq.canvas.removeEventListener("mousedown");
    document.removeEventListener("keyup");
    this.stopCaretTimer();

  }

  startCaretTimer = () => {
    this.stopCaretTimer();
    this.caretVisible = true;
    renderCaret(this.matheq.getRenderContext(), this.caretVisible)

    var thisValue = this;
    this.caretTimerId = setTimeout(function () {
      onCaretBlinkTimer(thisValue);
    }, CaretBlinkInterval);
  }
  stopCaretTimer = () => {
    if (this.caretTimerId) {
      clearTimeout(this.caretTimerId);
      this.caretTimerId = null;

      this.caretVisible = false;
      renderCaret(this.matheq.getRenderContext(), this.caretVisible);
    }
  };


  openPopup = (url, requestArgs, popupArgs, okCallback, cancelCallback) => { // Register callbacks.
    this.popupOkCallback = okCallback;
    this.popupCancelCallback = cancelCallback;

    // Build popup request url.
    var popupRequestUrl = url + '?statusArea=false&toolbar=false';

    for (var arg in requestArgs) {
      popupRequestUrl += '&' + arg + '=' + encodeURIComponent(requestArgs[arg]);
    }

    popupArgs.screenletId = this.screenletId;

    // Disable context menu while popup is open or else keyboard shortcuts will still be active.
    this.popupVisible = true;

    // showEcPopup.call(this, popupRequestUrl, popupArgs);
  }

  onPopupClosed = (popupReturnValue, popupRowData, canceled) => {
    if (this.popupOkCallback) {
      if (!canceled) {
        var rowData = {};

        if (popupRowData) {
          popupRowData.split('|').forEach(function (entry) {
            var equalsIndex = entry.indexOf('=');
            var key = entry.substring(0, equalsIndex);
            var value = entry.substring(equalsIndex + 1);
            rowData[key] = value;
          });
        }

        this.popupOkCallback.call(this, popupReturnValue, rowData);
      }

      this.popupOkCallback = null;
    }

    if (this.popupCancelCallback) {
      if (canceled) {
        this.popupCancelCallback.call(this);
      }

      this.popupCancelCallback = null;
    }
    debugger
    detachPopupCloseHook.call(this);
    this.popupVisible = false;
    // this.focus();
  };

  /**
 * Sets focus to this editor instance.
 */
  // focus = () => {
  //   debugger
  //   this.matheq.$canvas.attr('tabindex', 1000);
  //   this.matheq.$canvas.focus();
  // };

  /**
 * Pushes the current mathml onto the undo stack.
 */
  pushUndo = () => {
    var mathml = this.matheq.getMathml();

    if (this.undoStack.length === 0 || mathml !== this.undoStack[this.undoStack.length - 1]) {
      this.undoStack.push(mathml);
      this.redoStack = [];
    }
  };

  /**
 * Executes an action that manipulates the editor equation.
 * This method performs standard undo stack, caret timer and context menu update handling so that
 * the action  need not worry about that.
 *
 * @param action: A function to execute that will perform the edit action.
 *                The function will be called with the editor instance as this-reference.
 * @param noRefresh: Do not refresh the matheq component after the edit action has been run. Default is false.
 */
  edit = (action, noRefresh) => {

    this.pushUndo();
    this.stopCaretTimer();

    action.call(this);

    injectIterationsRootElement.call(this);

    if (!noRefresh) {
      this.matheq.refresh();
    }

    this.startCaretTimer();
  };

  /**
 * Callback that handles context menu item selection.
 *
 * @param id: The id of the selected menu item.
 */
  onButtonSelection = (id) => {
    switch (id) {
      case 'undo':
        this.undo();
        break;
      case 'redo':
        this.redo();
        break;
      case 'clear':
        this.clear();
        break;
      case 'delete_item':
        this.deleteItem();
        break;
      case 'copy':
        this.copy();
        break;
      case 'copy_all':
        this.copyAll();
        break;
      case 'cut':
        this.cut();
        break;
      case 'paste':
        this.paste();
        break;
      case 'insert_number':
        this.insertNumber();
        break;
      case 'insert_text':
        this.insertText();
        break;
      case 'insert_iterator':
        this.insertIterator();
        break;
      case 'insert_parantheses':
        this.insertParentheses();
        break;

      // ////////////////////////////////////////////////

      case 'insert_squarebracket':
        // this.insertCardinality();
        this.insertSquareBracket();
        break;
      case 'insert_curlybracket':
        this.insertCurlyBracket();
        break;

      // case 'insert_variable':
      //   this.insertVariable();
      //   break;
      case 'replace_variable':
        this.replaceVariable();
        break;

      case 'insert_set':
        this.insertSetElement();
        // this.insertSet();
        break;
      case 'setvariable':
        this.insertSetVariable();
        break;
      case 'comma':
        this.insertcomma();
        break;
      case 'insertSTR':
        this.insertSTR();
        break;
      case 'filterset':
        this.insertFilterSet();
        break;
      case 'ADDMONTHS':
        this.insertAddMonths();
        break;
      case 'DAYSINMONTH':
        this.insertdaysinMonth();
        break;
      case 'COUNT':
        this.insertCount();
        break;
      // Date Function
      case 'DATEDIF':
        this.insertDatediff();
        break;
      case 'ROUND':
        this.insertBinaryRound();
        break;
      case 'FOYEAR':
        this.insertFoYear();
        break;
      case 'FOMONTH':
        this.insertFoMonth();
        break;
      case 'WEEKDAY':
        this.insertWeekDay();
        break;
      case 'WEEKNUM':
        this.insertNum();
        break;

      case 'MONTHFULL':
        this.insertMonthFull();
        break;
      case 'MONTHSHORT':
        this.insertMonthShort();
        break;
      case 'Sin':
        this.insertIsZero();
        break;
      case 'ADDDAYS':
        this.insertAddDays();
        break;
      case 'month':
        this.insertFirstOfMonth();
        break;
      case 'attribute':
        this.insertAttribute();
        break;
      case 'add_days':
        this.insertAddDays();
        break;

      // LOGICAL FUNCTION
      case 'AND':
        this.insertAndFunc()
        break;
      case 'IF':
        this.insertIfFunc()
        break;
      case 'TFUNC':
        this.insertTFUNC()
        break;
      // /////////////////////////////////////////////////
      case 'insert_set_attribute':
        this.insertObjectAttribute(false);
        break;
      case 'replace_set_attribute':
        this.replaceObjectAttribute(false);
        break;
      case 'insert_set_attribute_objkey':
        this.insertObjectAttribute(true);
        break;
      case 'replace_set_attribute_objkey':
        this.replaceObjectAttribute(true);
        break;
      case 'insert_set_attr_global':
        this.insertGlobalAttribute();
        break;
      case 'insert_engineparam_function':
        this.insertEngineParamFunction();
        break;
      case 'insert_iteration':
        this.insertIteration();
        break;
      case 'insert_versions_iteration':
        this.insertVersionsIteration();
        break;
      case 'insert_objects_iteration':
        this.insertObjectsIteration();
        break;
      case 'replace_iteration':
        this.replaceIteration();
        break;
      case 'replace_versions_iteration':
        this.replaceVersionsIteration();
        break;
      case 'replace_objects_iteration':
        this.replaceObjectsIteration();
        break;
      case 'insert_iteration_divider':
        this.insertIterationDivider();
        break;
      case 'insert_assignment':
        this.insertEqual();
        break;
      case 'insert_operator_plus':
        this.insertPlus();
        break;
      case 'insert_operator_minus':
        this.insertMinus();
        break;
      case 'insert_operator_multiply':
        this.insertMultiply();
        break;
      case 'insert_operator_divide':
        this.insertDivide();
        break;
      case 'insert_sum':
        this.insertSum();
        break;
      case 'insert_set_size':
        this.insertCardinality();
        break;
      case 'insert_version_count_function':
        this.insertVersionCount();
        break;
      case 'insert_object_count_function':
        this.insertObjectCount();
        break;
      case 'replace_set_size':
        this.replaceCardinality();
        break;
      case 'replace_version_count_function':
        this.replaceVersionCount();
        break;
      case 'replace_object_count_function':
        this.replaceObjectCount();
        break;
      case 'insert_min':
        this.insertMin();
        break;
      case 'insert_max':
        this.insertMax();
        break;
      case 'insert_mod':
        this.insertModulo();
        break;
      case 'insert_floor':
        this.insertFloor();
        break;
      case 'insert_abs':
        this.insertAbsolute();
        break;
      case 'insert_arctan':
        this.insertArctan();
        break;
      case 'insert_unary_round_function':
        this.insertUnaryRound();
        break;
      case 'insert_binary_round_function':
        this.insertBinaryRound();
        break;
      case 'insert_trinary_round_function':
        this.insertTrinaryRound();
        break;
      case 'insert_power':
        this.insertPower();
        break;
      case 'insert_exponentiale':
        this.insertExponential();
        break;
      case 'insert_log':
        this.insertLogarithm();
        break;
      case 'insert_ln':
        this.insertNaturalLogarithm();
        break;
      case 'insert_operator_union':
        this.insertUnion();
        break;
      case 'insert_operator_intersect':
        this.insertIntersect();
        break;
      case 'insert_operator_setdiff':
        this.insertSetDiff();
        break;
      case 'insert_arbitrary_union':
        this.insertArbitraryUnion();
        break;
      case 'insert_arbitrary_intersect':
        this.insertArbitraryIntersect();
        break;
      case 'insert_operator_filterset':
        this.insertFilterSet();
        break;
      case 'insert_interval':
        this.insertInterval(0);
        break;
      case 'insert_interval_sub1':
        this.insertInterval(1);
        break;
      case 'insert_interval_sub2':
        this.insertInterval(2);
        break;
      case 'insert_lookup':
        this.insertLookup();
        break;
      case 'insert_snapshot_function':
        this.insertSnapshot();
        break;
      case 'insert_expand_function':
        this.insertExpand();
        break;
      // case 'insert_argmin_function':
      //   this.insertArgMin();
      //   break;
      // case 'insert_argmax_function':
      //   this.insertArgMax();
      //   break;
      case 'insert_sortasc_function':
        this.insertSortAsc();
        break;
      case 'insert_sortdesc_function':
        this.insertSortDesc();
        break;
      case 'insert_args':
        this.insertArgs();
        break;
      case 'insert_set_*':
        this.insertSetStar();
        break;
      case 'insert_elementat_function':
        this.insertElementAt();
        break;
      case 'insert_operator_equals':
        this.insertEqual();
        break;
      case 'insert_operator_notequals':
        this.insertNotEqual();
        break;
      case 'insert_operator_lt':
        this.insertLessThan();
        break;
      case 'insert_operator_gt':
        this.insertGreaterThan();
        break;
      case 'insert_operator_ltequals':
        this.insertLessThanOrEqual();
        break;
      case 'insert_operator_gtequals':
        this.insertGreaterThanOrEqual();
        break;
      case 'insert_operator_fullequals':
        this.insertFullEquals();
        break;
      case 'insert_operator_objequals':
        this.insertObjectEquals();
        break;
      case 'insert_operator_prsubset':
        this.insertProperSubset();
        break;
      case 'insert_operator_subset':
        this.insertSubset();
        break;
      case 'insert_operator_prsuperset':
        this.insertProperSuperset();
        break;
      case 'insert_operator_superset':
        this.insertSuperset();
        break;
      case 'insert_operator_setoverlap':
        this.insertSetOverlap();
        break;
      case 'replace_operator_fullequals':
        this.replaceFullEquals();
        break;
      case 'replace_operator_objequals':
        this.replaceObjectEquals();
        break;
      case 'replace_operator_prsubset':
        this.replaceProperSubset();
        break;
      case 'replace_operator_subset':
        this.replaceSubset();
        break;
      case 'replace_operator_prsuperset':
        this.replaceProperSuperset();
        break;
      case 'replace_operator_superset':
        this.replaceSuperset();
        break;
      case 'replace_operator_setoverlap':
        this.replaceSetOverlap();
        break;
      case 'insert_operator_or':
        this.insertOr();
        break;
      case 'insert_operator_and':
        this.insertAnd();
        break;
      case 'insert_operator_not':
        this.insertNot();
        break;
      case 'insert_isvalid':
        this.insertIsValid();
        break;
      case 'insert_iszero':
        this.insertIsZero();
        break;
      case 'insert_addsubdays_function':
        this.insertAddSubDays();
        break;
      case 'insert_adddays_function':
        this.insertAddDays();
        break;


      case 'insert_midnight_function':
        this.insertMidnight();
        break;
      case 'insert_firstofmonth_function':
        this.insertFirstOfMonth();
        break;
      case 'insert_firstofquarter_function':
        this.insertFirstOfQuarter();
        break;
      case 'insert_firstofyear_function':
        this.insertFirstOfYear();
        break;
      case 'insert_tosubday_function':
        this.insertToSubDay();
        break;
      case 'insert_today_function':
        this.insertToDay();
        break;
      case 'insert_tomonth_function':
        this.insertToMonth();
        break;
      case 'insert_toyear_function':
        this.insertToYear();
        break;
      case 'insert_label':
        this.insertLabel();
        break;
      case 'insert_goto':
        this.insertGoto();
        break;
      case 'insert_decisionvalue':
        this.insertDecisionValue();
        break;
      case 'insert_info':
        this.insertInfo();
        break;
      case 'insert_info_sub':
        this.insertInfoSub();
        break;
      case 'insert_warning':
        this.insertWarning();
        break;
      case 'insert_warning_sub':
        this.insertWarningSub();
        break;
      case 'insert_error':
        this.insertError();
        break;
      case 'insert_error_sub':
        this.insertErrorSub();
        break;
      case 'insert_fatal':
        this.insertFatal();
        break;
      case 'insert_fatal_sub':
        this.insertFatalSub();
        break;
      case 'insert_error_count':
        this.insertErrorCount();
        break;
      case 'insert_extfn':
        this.insertExternalFunction();
        break;
      case 'insert_remark':
        this.insertRemark();
        break;
      case 'insert_response_param':
        this.insertResponseParam();
        break;
      case 'show_variable_info':
        this.showVariableInfo();
        break;
      case 'show_attribute_info':
        this.showAttributeInfo();
        break;
    }
  };


  onVariableSelection = (id, data, dimension = null) => {

    switch (id) {
      case 'local_variable':
        this.insertLocalVariable(data, dimension);
        break;
      case 'global_variable':
        this.insertGlobalVariable(data);
        break;
      case 'number':
        this.insertNumb(data);
        break;
      case 'constant':
        this.insertConstants(data);
        break;
      case 'set':
        this.insertSetVariable(data)

    }
  };

  /**
 * Pushes the current mathml onto the redo stack and replaces it with mathml from the undo stack.
 */
  undo = () => {
    this.startCaretTimer();
    if (this.undoStack.length > 0) {
      var oldMathml = this.matheq.getMathml();
      var newMathml = this.undoStack.pop();

      if (oldMathml === newMathml) {

        newMathml = this.undoStack.pop();
      }

      this.redoStack.push(oldMathml);
      this.matheq.reset(newMathml);
    }
  };

  /**
 * Pushes the current mathml onto the undo stack and replaces it with mathml from the redo stack.
 */
  redo = () => {
    if (this.redoStack.length > 0) {
      var oldMathml = this.matheq.getMathml();
      var newMathml = this.redoStack.pop();

      this.undoStack.push(oldMathml);
      this.matheq.reset(newMathml);
    }
  };

  /**
 * Clears the equation, i.e. replaces the current equation with a new empty equation.
 */
  clear = () => { // Only clear if not already empty.
    if (!this.matheq.equation.isEmpty()) {
      this.edit(function () {
        this.matheq.reset(null);
        // Select the first and only element.
        var selection = this.matheq.equation.getFirstVisualElement();
        this.matheq.equation.setCurrentElement(selection.element);

      }, true);
    }
  };

  /**
 * Deletes the currently selected element.
 */
  deleteItem = () => {
    var selectedElement = this.matheq.equation.currentElement;

    if (selectedElement) {
      this.edit(function () {
        this.matheq.equation.deleteElement(selectedElement);
      });
    }
  };

  /**
 * Copies the mathml of the currently selected element to the clipboard.
  */
  // function myFunction() {
  // /* Get the text field */
  // var copyText = document.getElementById("myInput");

  // /* Select the text field */
  // copyText.select();
  // copyText.setSelectionRange(0, 99999); /* For mobile devices */

  // /* Copy the text inside the text field */
  // navigator.clipboard.writeText(copyText.value);

  // /* Alert the copied text */
  // alert("Copied the text: " + copyText.value);
  // }
  copy = () => {
    var selectedElement = this.matheq.equation.currentElement;

    if (selectedElement) {

      var mathml = Builder.buildElement(selectedElement, true);

      // Operator elements are already wrapped in a namespaced apply node, but other elements may not be.
      // Backwards compatibility requires that all elements that are copy/pasted are wrapped in unnamespaced
      // apply nodes, so we must wrap them all again.
      mathml = '<apply>' + mathml + '</apply>';

      // mathml.select();

      this.copyEquation.push(selectedElement)
      // this.mathEqObject['data'] = selectedElement
      // this.mathEqObject['type'] = 'copy'

      // var i = navigator.clipboard.writeText(mathml);
      // EC.clipboard.copyToClipboard({text: mathml});
    }
  };

  /**
 * Copies the entire equation mathml to the clipboard.
 */
  copyAll = () => {
    var mathml = Builder.buildElement(this.matheq.equation.getRootElement());

    // The mathml is already wrapped in a namespaced apply node, but for backwards compatibility
    // we need to wrap it in another unnamespaced apply node.
    mathml = '<apply>' + mathml + '</apply>';


    // this.clipboard.copyToClipboard({text: mathml});


  };


  /**
 * Copies the mathml of the currently selected element to the clipboard and deletes the element.
 */
  cut = () => {
    this.copy();
    this.deleteItem();
  };

  /**
 * Parses mathml from the clipboard and inserts it into the equation at current position.
 */
  paste = () => {

    if (this.matheq.equation.isEmpty()) { // Allow pasting into an empty equation without having to select the empty element first.
      this.matheq.equation.currentElement = this.matheq.equation.getRootElement();
    }
    if (this.matheq.equation.currentElement) {

      var len = this.copyEquation.length

      var mathmlObj = this.copyEquation[len - 1]

      var thisValue = this;
      thisValue.edit(function () {
        this.matheq.equation.insertElement(mathmlObj)

      });
      thisValue.edit(function () {

        var mathml1 = this.matheq.getMathml();
        this.matheq.reset(mathml1)
        // this.matheq.render()
      });
    }


    // if (this.matheq.equation.currentElement) {
    //     var editor = this;
    //     this.clipboard.readFromClipboard(function (mathml) {
    //         editor.edit(function () {
    //             this.matheq.insertMathml(mathml);
    //             // this.matheq.insertMathml('<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">i</ci><ci type="set">conversionfactors</ci></apply></condition></apply></math>');

    //         });
    //     });
    // }
  };


  /**
 * Opens a simple number input popup,
 * then inserts or replaces the number at current caret position.
 */
  insertNumber = () => {
    debugger
    var initialNumber = '';
    var replace = false;
    if (this.matheq.equation.currentElement instanceof Elements.NumberElement) {

      initialNumber = this.matheq.equation.currentElement.text;
      replace = true;
    }

    var thisValue = this;
    insertConstant('Insert Number', 'NUMBER', 'New Value', true, initialNumber, function (popupReturnValue) {
      if (isNaN(popupReturnValue)) {
        thisValue.edit(function () {
          if (replace) {
            this.matheq.equation.deleteElement(this.matheq.equation.currentElement);
          }

          this.matheq.equation.insertElement(new Elements.NumberElement(popupReturnValue))
        });
      }
    });
  };

  /**
 * Opens a simple text input popup,
 * then inserts or replaces the text at current caret position.
 */
  insertText = () => {
    var initialText = '';
    var replace = false;

    if (identifyElement(this.matheq.equation.currentElement) === 'constant') {
      initialText = this.matheq.equation.currentElement.text;
      // Strip off leading and trailing ' signs.
      initialText = initialText.substring(1, initialText.length - 1);
      replace = true;
    }

    var thisValue = this;
    insertConstant('Insert Text', 'STRING', 'New Value', false, initialText, function (popupReturnValue) {
      thisValue.edit(function () {
        if (replace) {
          this.matheq.equation.deleteElement(this.matheq.equation.currentElement);
        }

        this.matheq.equation.insertElement(new Elements.IdentifierElement(popupReturnValue, 'constant'));
      });
    });
  };

  /**
 * Opens a simple iterator name input popup,
 * then inserts or replaces the iterator at current caret position.
 */
  insertIterator = () => {
    var initialText = '';
    var replace = false;

    if (identifyElement(this.matheq.equation.currentElement) === 'iterator') {
      initialText = this.matheq.equation.currentElement.text;
      replace = true;
    }

    var thisValue = this;
    insertConstant('Insert Iterator', 'STRING', 'Iterator Name', true, initialText, function (popupReturnValue) {
      if (popupReturnValue) {
        thisValue.edit(function () {
          if (replace) {
            this.matheq.equation.deleteElement(thisValue.matheq.equation.currentElement);
          }

          this.matheq.equation.insertElement(new Elements.IdentifierElement(popupReturnValue, 'iterator'));
        });
      }
    });
  };

  /**
 * Inserts parentheses at the current caret position.
 */
  insertParentheses = () => {
    this.edit(function () {

      this.matheq.equation.insertElement(new Elements.FenceElement(Elements.FenceElement.FenceType.Parenthesis));
    });
  };

  insertSquareBracket = () => {
    this.edit(function () { // this.matheq.equation.insertElement(new Elements.IntervalElement);

      this.matheq.equation.insertElement(new Elements.FenceElement(Elements.FenceElement.FenceType.SquareBracket));
    });
  };
  insertCurlyBracket = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FenceElement(Elements.FenceElement.FenceType.CurlyBracket));
    });
  };

  /**
 * Opens a popup with a list of variables that can be inserted,
 * then inserts the new variable at current caret position.
 */
  insertVariable = () => {
    debugger
    var thisValue = this;
    var callback = function (popupReturnValue, popupRowData) {
      if (popupReturnValue) {
        var numDimensions = 0;
        var signature = popupReturnValue;

        for (var i = 0; i < 5; ++i) {
          var value = popupRowData['DIM' + (
            i + 1
          ) + '_OBJECT_TYPE_CODE'];

          if (value && value.length > 0) {
            signature += (numDimensions === 0 ? '[' : ',') + value;
            ++numDimensions;
          }
        }

        if (numDimensions > 0) {
          signature += ']';
        }

        if (numDimensions === 0) {
          thisValue.edit(function () {
            this.matheq.equation.insertElement(new Elements.IdentifierElement(signature, 'variable'));
          });
        } else {
          thisValue.edit(function () {
            var element = new Elements.IdentifierSubElement(signature, 'variable');

            for (i = 1; i < numDimensions; ++i) {
              element.getSubscript().children.add(new Elements.SeparatorElement());
            }

            this.matheq.equation.insertElement(element);
          });
        }
      }
    };

    this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_insert_var_popup', {
      CALC_CONTEXT_ID: this.matheq.calcContextId,
      CALCULATION_ID: this.matheq.calculationId,
      DAYTIME: this.matheq.calcDaytime,
      PopupReturnColumn: 'NAME'
    }, {
      width: 1070,
      height: 500,
      title: 'Insert variable'
    }, callback);
  };

  /**
 * Opens a popup with a list of variables that can replace the current variable,
 * then replaces current variable with selected variable.
 */
  replaceVariable = () => {
    var selectedElement = this.matheq.equation.currentElement;

    if (identifyElement(selectedElement) !== 'variable') {
      return;
    }

    var thisValue = this;
    var callback = function (popupReturnValue, popupRowData) {
      if (popupReturnValue) {
        var numDimensions = 0;
        var signature = popupReturnValue;

        for (var i = 0; i < 5; ++i) {
          var value = popupRowData['DIM' + (
            i + 1
          ) + '_OBJECT_TYPE_CODE'];

          if (value && value.length > 0) {
            signature += (numDimensions === 0 ? '[' : ',') + value;
            ++numDimensions;
          }
        }

        if (numDimensions > 0) {
          signature += ']';
        }

        thisValue.edit(function () {
          selectedElement.text = signature;
          this.matheq.equation.needsResize = true;
        });
      }
    };

    var requestArgs = {
      CALC_CONTEXT_ID: this.matheq.calcContextId,
      CALCULATION_ID: this.matheq.calculationId,
      DAYTIME: this.matheq.calcDaytime,
      PopupReturnColumn: 'NAME'
    };

    // Build dimension args.
    var signature = selectedElement.text.trim();
    var dimensions = signature.substring(signature.indexOf('[') + 1, signature.lastIndexOf(']')).split(',');

    for (var i = 0; i < 5; ++i) {
      if (i < dimensions.length && dimensions[i] && dimensions[i].length > 0) {
        var dimensionClause = 'AND DIM' + (
          i + 1
        ) + "_OBJECT_TYPE_CODE = '" + dimensions[i] + "'";
      } else {
        var dimensionClause = 'AND DIM' + (
          i + 1
        ) + '_OBJECT_TYPE_CODE IS NULL';
      } requestArgs['DIM' + (
        i + 1
      ) + '_CLAUSE'] = dimensionClause;
    }

    this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_replace_var_popup', requestArgs, {
      width: 1070,
      height: 500,
      title: 'Replace variable'
    }, callback);
  };

  /**
 * Opens a popup with a list of sets to insert,
 * then inserts the new set at current caret position.
 */
  insertSet = () => {

    var thisValue = this;
    this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_insert_set_popup', {
      CALCULATION_ID: this.matheq.calculationId,
      DAYTIME: this.matheq.calcDaytime,
      PopupReturnColumn: 'CALC_SET_NAME'
    }, {
      width: 940,
      height: 500,
      title: 'Insert set'
    }, function (popupReturnValue) {
      if (popupReturnValue) {
        thisValue.edit(function () {
          this.matheq.equation.insertElement(new Elements.IdentifierElement(popupReturnValue, 'set'));
        });
      }
    });
  };

  /**
 * Opens a popup listing attributes to insert,
 * then inserts new attribute at current caret position.
 *
 * @param objectKey: Whether the attribute should have a ref date subscript or not.
 */
  insertObjectAttribute = (objectKey) => {
    var thisValue = this;
    this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_insert_obj_attr_popup', {
      CALC_CONTEXT_ID: this.matheq.calcContextId,
      PopupReturnColumn: 'NAME'
    }, {
      width: 535,
      height: 500,
      title: 'Insert object attribute'
    }, function (popupReturnValue, popupRowData) {
      if (popupReturnValue) {
        var element = new Elements.IdentifierSubElement(popupReturnValue, 'attribute');
        element.addAdditionalMathmlAttribute('object-type', popupRowData.OBJECT_TYPE_CODE);

        if (objectKey) {
          element.getSubscript().children.add(new Elements.SeparatorElement());
        }

        thisValue.edit(function () {
          this.matheq.equation.insertElement(element);
        });
      }
    });
  };

  /**
 * Replaces current attribute with object attribute.
 *
 * @param objectKey: Whether the attribute should have a ref date subscript or not.
 */
  replaceObjectAttribute = (objectKey) => {
    var selectedElement = this.matheq.equation.currentElement;

    if (selectedElement instanceof Elements.IdentifierSubElement) {
      var objectType = selectedElement.getAdditionalMathmlAttribute('object-type');
      var subscript = objectKey ? selectedElement.getSubscript() : selectedElement.getSubscript().children.at(0);

      var newElement = new Elements.IdentifierSubElement(selectedElement.text, 'attribute');
      newElement.addAdditionalMathmlAttribute('object-type', objectType);
      newElement.getSubscript().children.add(subscript);

      if (objectKey) {
        newElement.getSubscript().children.add(new Elements.SeparatorElement());
      }

      this.edit(function () {
        this.matheq.equation.deleteElement(selectedElement);
        this.matheq.equation.insertElement(newElement);
      });
    }
  };

  /**
 * Opens a popup listing global attributes,
 * then inserts the new attribute at the current caret position.
 */
  insertGlobalAttribute = () => {
    var thisValue = this;
    this.openPopup('/com.ec.frmw.bs.calc.screens/popup_default_dynamic', {
      CALC_CONTEXT_ID: this.matheq.calcContextId,
      PopupQueryURL: '/com.ec.frmw.bs.calc.screens/query/eqnedit_insert_global_attr.xml',
      PopupLayout: '/com.ec.frmw.bs.calc.screens/layout/popup_default_layout.xml',
      PopupReturnColumn: 'ATTRIBUTE_NAME'
    }, {
      width: 650,
      height: 300,
      title: 'Insert global attribute'
    }, function (popupReturnValue, popupRowData) {
      if (popupReturnValue) {
        var globalAttribute = new Elements.IdentifierSubElement(popupReturnValue, 'attribute');
        var globalChild = new Elements.IdentifierElement('global', 'set');
        globalAttribute.children.replace(globalAttribute.children.at(0), globalChild);

        this.edit(function () {
          this.matheq.equation.insertElement(globalAttribute);
        });
      }
    });
  };

  /**
 * Inserts an engineParameter function at the current caret position.
 */
  insertEngineParamFunction = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('engineParameter', null, null, 0, 1));
    });
  };

  /**
 * Opens an iterator popup and a set popup,
 * then inserts a new iteration at current caret position.
 *
 * @param iterationElement: The iteration mathml element to insert. If not provided a normal InElement is inserted.
 */
  insertIteration = (iterationElement) => {
    var initialText = '';
    var replace = false;

    if (identifyElement(this.matheq.equation.currentElement) === 'iterator') {
      initialText = this.matheq.equation.currentElement.text;
      replace = true;
    }

    var thisValue = this;

    insertConstant('Insert Iterator', 'STRING', 'Iterator Name', true, initialText, function (iteratorPopupReturnValue) {
      if (iteratorPopupReturnValue) {
        var iterator = iteratorPopupReturnValue;

        // Must use setTimeout to allow first jsf popup to finish properly before opening next popup.
        // Timeout is set to 1 ms just to postpone the execution of this code to the next js engine "tick".
        setTimeout(function () {
          var insertIterationElements = function (setElement) {
            thisValue.edit(function () {
              if (replace) {
                this.matheq.equation.deleteElement(this.matheq.equation.currentElement);
              }

              this.matheq.equation.insertElement(new Elements.IdentifierElement(iterator, 'iterator'));
              this.matheq.equation.insertElement(iterationElement ? iterationElement : new Elements.InElement());
              this.matheq.equation.insertElement(setElement);
            });
          };

          thisValue.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_insert_set_popup', {
            CALCULATION_ID: thisValue.matheq.calculationId,
            DAYTIME: thisValue.matheq.calcDaytime,
            PopupReturnColumn: 'CALC_SET_NAME'
          }, {
            width: 930,
            height: 500,
            title: 'Insert set'
          }, function (setPopupReturnValue) {
            insertIterationElements(new Elements.IdentifierElement(setPopupReturnValue, 'set'));
          }, function () {
            insertIterationElements(new Elements.EmptyElement());
          });
        }, 1);
      }
    });
  };

  /**
 * Opens an iterator popup and a set popup,
 * then inserts a new versions iteration at current caret position.
 */
  insertVersionsIteration = () => {
    this.insertIteration(new Elements.InVersionsElement());
  };

  /**
 * Opens an iterator popup and a set popup,
 * then inserts a new objects iteration at current caret position.
 */
  insertObjectsIteration = () => {
    this.insertIteration(new Elements.InObjectsElement());
  };

  /**
 * Replaces current iteration with a plain iteration.
 */
  replaceIteration = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.InElement());
    });
  };

  /**
 * Replaces current iteration with a versions iteration.
 */
  replaceVersionsIteration = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.InVersionsElement());
    });
  };

  /**
 * Replaces current iteration with an objects iteration.
 */
  replaceObjectsIteration = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.InObjectsElement());
    });
  };

  /**
 * Inserts an iteration separator at the current caret position.
 */
  insertIterationDivider = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SeparatorElement());
    });
  };

  /**
 * Inserts an assignment/equals operator at the current caret position.
 */
  insertEqual = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.EqualElement());
    });
  };

  /**
 * Inserts a plus operator at the current caret position.
 */
  insertPlus = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.PlusElement());

    });
  };


  /**
 * Inserts a minus operator at the current caret position.
 */
  insertMinus = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.MinusElement());
    });
  };

  /**
 * Inserts a multiply operator at the current caret position.
 */
  insertMultiply = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.TimesElement());
    });
  };

  /**
 * Inserts a divide operator at the current caret position.
 */
  insertDivide = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.DivideElement());
    });
  };

  /**
 * Inserts a sum element at the current caret position.
 */
  insertSum = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SumElement());
    });
  };

  /**
 * Inserts a set cardinality element at the current caret position.
 */
  insertCardinality = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.CardinalityElement());
    });
  };

  /**
 * Inserts a versionCount function at the current caret position.
 */
  insertVersionCount = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('versionCount', null, null, 0, 1));
    });
  };

  /**
 * Inserts an objectCount function at the current caret position.
 */
  insertObjectCount = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('objectCount', null, null, 0, 1));
    });
  };

  /**
 * Replaces current function element with cardinality element.
 */
  replaceCardinality = () => {
    var selectedElement = this.matheq.equation.currentElement;
    var cardinalitySet = selectedElement.getCsymbolArguments();
    var cardinalityElement = new Elements.CardinalityElement();

    this.edit(function () {
      this.matheq.equation.deleteElement(selectedElement);
      this.matheq.equation.insertElement(cardinalityElement);
      this.matheq.equation.setCurrentElement(cardinalityElement.getSet());
      this.matheq.equation.insertElement(cardinalitySet);
      this.matheq.equation.setCurrentElement(cardinalityElement);
    });
  };
  /**
 * Inserts an argmin function at the current caret position.
 */
  // insertArgMin = () => {
  //   getNumberOfSubscripts.call(this, function (numSubscripts) {
  //     this.edit(function () {
  //       this.matheq.equation.insertElement(new Elements.FunctionElement('argmin', null, null, numSubscripts, 2));
  //     });
  //   });
  // };

  /**
 * Inserts an argmax function at the current caret position.
 */
  // insertArgMax = () => {
  //   getNumberOfSubscripts.call(this, function (numSubscripts) {
  //     this.edit(function () {
  //       this.matheq.equation.insertElement(new Elements.FunctionElement('argmax', null, null, numSubscripts, 2));
  //     });
  //   });
  // };

  /**
 * Inserts a sort asc function at the current caret position.
 */
  insertSortAsc = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('sortAsc', null, null, 1, 1));
    });
  };

  /**
 * Inserts a sort desc function at the current caret position.
 */
  insertSortDesc = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('sortDesc', null, null, 1, 1));
    });
  };

  /**
 * Inserts an args function at the current caret position.
 */
  insertArgs = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('args', null, null, 0, 2));
    });
  };

  /**
 * Inserts a set * element at the current caret position.
 */
  insertSetStar = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('*', 'set'));
    });
  };

  /**
 * Inserts an element at function at the current caret position.
 */
  insertElementAt = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('elementAt', null, null, 0, 2));
    });
  };

  /**
 * Inserts a not equal operator at the current caret position.
 */
  insertNotEqual = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.NotEqualElement());
    });
  };

  /**
 * Inserts a less than operator at the current caret position.
 */
  insertLessThan = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.LessThanElement());
    });
  };

  /**
 * Inserts a greater than operator at the current caret position.
 */
  insertGreaterThan = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.GreaterThanElement());
    });
  };

  /**
 * Inserts a less than or equal operator at the current caret position.
 */
  insertLessThanOrEqual = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.LessEqualThanElement());
    });
  };

  /**
 * Inserts a greater than or equal operator at the current caret position.
 */
  insertGreaterThanOrEqual = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.GreaterEqualThanElement());
    });
  };

  /**
 * Inserts a full equals operator at the current caret position.
 */
  insertFullEquals = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FullEqualsElement());
    });
  };

  /**
 * Inserts an object equals operator at the current caret position.
 */
  insertObjectEquals = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ObjectEqualsElement());
    });
  };

  /**
 * Inserts a proper subset operator at the current caret position.
 */
  insertProperSubset = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ProperSubsetElement());
    });
  };

  /**
 * Inserts a subset operator at the current caret position.
 */
  insertSubset = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SubsetElement());
    });
  };

  /**
 * Inserts a proper superset operator at the current caret position.
 */
  insertProperSuperset = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ProperSupersetElement());
    });
  };

  /**
 * Inserts a superset operator at the current caret position.
 */
  insertSuperset = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SupersetElement());
    });
  };

  /**
 * Inserts a set overlap operator at the current caret position.
 */
  insertSetOverlap = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SetOverlapElement());
    });
  };

  /**
 * Replaces current element with a full equals operator.
 */
  replaceFullEquals = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.FullEqualsElement());
    });
  };

  /**
 * Replaces current element with an object equals operator.
 */
  replaceObjectEquals = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.ObjectEqualsElement());
    });
  };

  /**
 * Replaces current element with a proper subset operator.
 */
  replaceProperSubset = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.ProperSubsetElement());
    });
  };

  /**
 * Replaces current element with a subset operator.
 */
  replaceSubset = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.SubsetElement());
    });
  };

  /**
 * Replaces current element with a proper superset operator.
 */
  replaceProperSuperset = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.ProperSupersetElement());
    });
  };

  /**
 * Replaces current element with a superset operator.
 */
  replaceSuperset = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.SupersetElement());
    });
  };

  /**
 * Replaces current element with a set overlap operator.
 */
  replaceSetOverlap = () => {
    this.edit(function () {
      this.matheq.equation.replaceCurrentElement(new Elements.SetOverlapElement());
    });
  };

  /**
 * Inserts an or operator at the current caret position.
 */
  insertOr = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.OrElement());
    });
  };

  /**
 * Inserts an and operator at the current caret position.
 */
  insertAnd = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.AndElement());
    });
  };

  /**
 * Inserts a not operator at the current caret position.
 */
  insertNot = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.NotElement());
    });
  };

  /**
 * Inserts an isValid function at the current caret position.
 */
  insertIsValid = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('isValid', null, null, 0, 1));
    });
  };

  /**
 * Inserts an isZero function at the current caret position.
 */
  insertIsZero = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('isZero', null, null, 0, 1));
    });
  };

  /**
 * Inserts an addSubDays function at the current caret position.
 */
  insertAddSubDays = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('addSubDays', null, null, 0, 3));
    });
  };

  /**
 * Inserts an addDays function at the current caret position.
 */
  insertAddDays = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('ADDDAYS', null, null, 0, 2));
    });
  };

  /**
 * Inserts an addMonths function at the current caret position.
 */
  insertAddMonths = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('ADDMONTHS', null, null, 0, 2));
    });
  };

  insertFoYear = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('FOYEAR', null, null, 0, 1));
    });
  };

  insertFoMonth = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('FOMONTH', null, null, 0, 1));
    });
  };

  insertDatediff = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('DATEDIF', null, null, 0, 3));
    });
  };

  insertWeekDay = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('WEEKDAY', null, null, 0, 3));
    });
  };
  insertNum = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('WEEKNUM', null, null, 0, 3));
    });
  };
  insertMonthFull = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('MONTHFULL', null, null, 0, 1));
    });
  };

  insertMonthShort = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('MONTHSHORT', null, null, 0, 1));
    });
  };

  insertAndFunc = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('AND', null, null, 0, 2));
    });
  }

  insertIfFunc = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('IF', null, null, 0, 3));
    });
  }
  insertTFUNC = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('TFUNC', null, null, 0, 1));
    });
  }

  insertAttribute = () => {
    // var element = new Elements.IdentifierSubElement("Code", 'attribute');
    // element.addAdditionalMathmlAttribute('object-type', "ALLOC_NODE");

    // this.edit(function () {
    // this.matheq.equation.insertElement(element);
    // });
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierSubElement('Code', 'attribute'))
    });
  };
  /**
 * Inserts a Numbers at the current caret position.
 */
  insertNumb = (num) => {
    var numbers = num.toString();
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.NumberElement(numbers));

    });
  };


  insertSetVariable = (data) => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement(data, 'set'));

    });

  };

  insertcomma = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SeparatorElement());

    });

  };

  insertConstants = (data) => {

    if (this.matheq.equation.currentElement) {

      this.edit(function () {
        this.matheq.equation.insertElement(new Elements.IdentifierElement(data, 'constant'));
      });
    }
    // this.edit(function () {
    //     this.matheq.equation.insertElement(new Elements.IdentifierElement(data, 'constant'));
    // });

  };

  insertSTR = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('STR', 'constant'));
    });

  };

  /**
 * Inserts a Set element operator at the current caret position.
 */
  insertSetElement = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.InElement());

    });
  };

  // checkSubIdentifier =(equation)=>{
  //     if (equation.currentElement.text.indexOf('[') === -1 && equation.currentElement.text.indexOf(']') === -1){
  //         if(equation.currentElement.parent.text){
  //             if(equation.currentElement.parent.text.indexOf('[')== -1 && equation.currentElement.parent.text.indexOf(']')== -1){
  //                 return "complete"
  //             }
  //             return "not complete"

  //         }

  //         }
  //         return 'half'
  // }
  /**
* Inserts a Local variable at the current caret position.
*/
  insertLocalVariable = (variableName, dimension) => {

    // if (this.matheq.equation.currentElement.parent.text.indexOf('[') > -1){
    //     const a = typeof(this.matheq.equation.currentElement.parent)
    // }
    // const a =this.checkSubIdentifier(this.matheq.equation)

    var popupRowData = [...dimension]
    var numDimensions = 0;
    var signature = variableName;

    for (var i = 0; i < popupRowData.length; ++i) { // var value = popupRowData['DIM' + (i + 1) + '_OBJECT_TYPE_CODE'];
      var value = popupRowData[(i)];

      if (value && value.length > 0) {
        signature += (numDimensions === 0 ? '[' : ',') + value;
        ++numDimensions;
      }
    }

    if (numDimensions > 0) {
      signature += ']';

    }

    if (numDimensions === 0) {
      this.edit(function () {
        this.matheq.equation.insertElement(new Elements.IdentifierElement(signature, 'variable'));
      });
    } else {
      this.edit(function () {
        var element = new Elements.IdentifierSubElement(signature, 'variable');

        for (i = 1; i < numDimensions; ++i) {
          element.getSubscript().children.add(new Elements.SeparatorElement());
        }

        this.matheq.equation.insertElement(element);

      });

    }


  };

  /**
* Inserts a Global variable at the current caret position.
*/
  insertGlobalVariable = (variableName) => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement(variableName, 'iterator'));

    });
  };
  /**
 * Inserts a midnight function at the current caret position.
 */
  insertMidnight = () => {
    this.edit(function () {

      this.matheq.equation.insertElement(new Elements.FunctionElement('midnight', null, null, 0, 1));
    });
  };

  /**
 * Inserts a firstOfMonth function at the current caret position.
 */
  insertFirstOfMonth = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('firstOfMonth', null, null, 0, 1));
    });
  };

  /**
 * Inserts a firstOfQuarter function at the current caret position.
 */
  insertFirstOfQuarter = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('firstOfQuarter', null, null, 0, 1));
    });
  };

  /**
 * Inserts a firstOfYear function at the current caret position.
 */
  insertFirstOfYear = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('firstOfYear', null, null, 0, 1));
    });
  };

  /**
 * Inserts a toSubDay function at the current caret position.
 */
  insertToSubDay = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('toSubDay', null, null, 0, 1));
    });
  };

  /**
 * Inserts a toDay function at the current caret position.
 */
  insertToDay = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('toDay', null, null, 0, 1));
    });
  };

  /**
 * Inserts a toMonth function at the current caret position.
 */
  insertToMonth = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('toMonth', null, null, 0, 1));
    });
  };

  /**
 * Inserts a toYear function at the current caret position.
 */
  insertToYear = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('toYear', null, null, 0, 1));
    });
  };

  /**
 * Inserts a label element at the current caret position.
 */
  insertLabel = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('LABEL', 'special-identifier'));
    });
  };

  /**
 * Inserts a goto element at the current caret position.
 */
  insertGoto = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('GOTO', 'special-identifier'));
    });
  };

  /**
 * Inserts a decision value element at the current caret position.
 */
  insertDecisionValue = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('DecisionValue', 'variable'));
    });
  };

  /**
 * Inserts an info log element at the current caret position.
 */
  insertInfo = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('INFO', 'logstmt'));
    });
  };

  /**
 * Inserts a warning log element at the current caret position.
 */
  insertWarning = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('WARNING', 'logstmt'));
    });
  };

  /**
 * Inserts a warning log element with subscript at the current caret position.
 */
  insertWarningSub = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierSubElement('WARNING', 'logstmt'));
    });
  };

  /**
 * Inserts an error log element at the current caret position.
 */
  insertError = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('NONFATALERR', 'logstmt'));
    });
  };

  /**
 * Inserts an error log element with subscript at the current caret position.
 */
  insertErrorSub = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierSubElement('NONFATALERR', 'logstmt'));
    });
  };

  /**
 * Inserts a fatal log element at the current caret position.
 */
  insertFatal = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('ERROR', 'logstmt'));
    });
  };

  /**
 * Inserts a fatal log element with subscript at the current caret position.
 */
  insertFatalSub = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierSubElement('ERROR', 'logstmt'));
    });
  };

  /**
 * Inserts an error count element at the current caret position.
 */
  insertErrorCount = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('ErrorCount', 'variable'));
    });
  };

  /**
 * Inserts an external function at the current caret position.
 */
  insertExternalFunction = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('extfn', null, null, 0, 2));
    });
  };

  /**
 * Inserts a remark log element at the current caret position.
 */
  insertRemark = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('REMARK', 'logstmt'));
    });
  };

  /**
 * Inserts a response param log element at the current caret position.
 */
  insertResponseParam = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IdentifierElement('RESPONSE', 'logstmt'));
    });
  };

  /**
 * Opens a popup showing various info about the current variable.
 */
  showVariableInfo = () => {
    var selectedElement = this.matheq.equation.currentElement;

    if (selectedElement instanceof Elements.IdentifierSubElement && selectedElement.type === 'variable') {
      this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_show_var_info_popup', {
        CALCULATION_ID: this.matheq.calculationId,
        DAYTIME: this.matheq.calcDaytime,
        CALC_VAR_SIGNATURE: selectedElement.text
      }, {
        width: 315,
        height: 360,
        title: 'Variable Information'
      });
    }
  };

  /**
 * Opens a popup showing various info about the current attribute.
 */
  showAttributeInfo = () => {
    var selectedElement = this.matheq.equation.currentElement;

    if (selectedElement instanceof Elements.IdentifierSubElement && selectedElement.type === 'attribute') {
      var objectTypeCode = selectedElement.getAdditionalMathmlAttribute('object-type');

      if (objectTypeCode) {
        this.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_show_attr_info_popup', {
          CALC_CONTEXT_ID: this.matheq.calcContextId,
          CALC_ATTRIBUTE_NAME: selectedElement.text,
          CALC_OBJECT_TYPE_CODE: objectTypeCode
        }, {
          width: 360,
          height: 215,
          title: 'Attribute Information'
        });
      } else {
        alert('Attribute info is only available for attributes with object-type defined. ' + 'Reinsert the attribute into the equation to have object-type added.');
      }
    }
  };

  /**
 * Callback that handles context menu item state.
 *
 * Must return one of the following values:
 * - 'hidden': The menu item should not be visible in the menu at all.
 * - 'disabled': The menu item should be visible but disabled, e.g. grayed out.
 * - any other value: The menu item should be visible and enabled.
 *
 * @param id: The id of the menu item to evaluate.
 */
  onContextMenuUpdateState = (id) => {
    var equation = this.matheq.equation;
    var equationType = this.matheq.equationType;
    var selectedElement = equation.currentElement;
    // var enabled = Calc.ContextMenu.ItemState.Enabled;
    // var disabled = Calc.ContextMenu.ItemState.Disabled;
    // var hidden = Calc.ContextMenu.ItemState.Hidden;
    var enabled = 'a';
    var disabled = 'b';
    var hidden = 'c';

    // Items that do not require an element selection.
    switch (id) {
      case 'undo':
        return (this.undoStack.length > 0) ? enabled : disabled;
      case 'redo':
        return (this.redoStack.length > 0) ? enabled : disabled;
      case 'copy_all':
        return enabled;
      case 'clear':
        return enabled;
      case 'paste':
        return enabled;
    }

    // All other items require an element selection.
    if (!selectedElement || selectedElement.caretPosition === Common.CaretPosition.None) {
      return hidden;
    }

    // First param of an args and the param of a round(?) must be a variable.
    if (isVisualChildOf(selectedElement, 'args', 1, true) || isVisualChildOf(selectedElement, 'round_1', 1, true)) {
      switch (id) {
        case 'cut':
        case 'delete_item':
        case 'show_variable_info':
        case 'show_attribute_info':
        case 'sub_insert_operand':
        // case 'insert_variable':
        case 'replace_variable':
          break;
        default:
          return hidden;
      }
    }

    // Items only available in specific equation types.
    switch (id) {
      case 'insert_assignment':
      case 'sub_flow_control':
      case 'insert_info':
      case 'insert_info_sub':
      case 'insert_warning':
      case 'insert_warning_sub':
      case 'insert_error':
      case 'insert_error_sub':
      case 'insert_fatal':
      case 'insert_fatal_sub':
      case 'insert_response_param':
        if (equationType !== EquationType.Equation) {
          return hidden;
        }
        break;
      case 'sub_log_messages':
      case 'insert_error_count':
        if (equationType === EquationType.Iterations) {
          return hidden;
        }
        break;
      case 'insert_iteration_divider':
        if (equationType !== EquationType.Iterations) {
          return hidden;
        }
        break;
    }

    // Items only available for some selections.
    switch (id) {
      case 'cut':
      case 'delete_item':
        if (selectedElement instanceof Elements.EmptyElement) {
          return hidden;
        }
        break;
      case 'replace_variable':
      case 'show_variable_info':
        if (identifyElement(selectedElement) !== 'variable') {
          return hidden;
        }
        break;
      case 'show_attribute_info':
        if (identifyElement(selectedElement) !== 'attribute') {
          return hidden;
        }
        break;
      case 'insert_assignment':
        if (equation.getRootElement() instanceof Elements.EqualElement) {
          return hidden;
        }
        break;
      case 'insert_set_*':
        // Must be in a child of the first param of an args.
        if (!isVisualChildOf(selectedElement, 'args', 1, false)) {
          return hidden;
        }
        break;
      case 'sub_flow_control':
      case 'insert_info':
      case 'insert_info_sub':
      case 'insert_warning':
      case 'insert_warning_sub':
      case 'insert_error':
      case 'insert_error_sub':
      case 'insert_fatal':
      case 'insert_fatal_sub':
      case 'insert_response_param':
        if (!equation.isEmpty() && !isOnLeftHandSide(equation)) {
          return hidden;
        }
        break;
      case 'replace_set_attribute':
        // Current element must be an attribute with two subscripts where the last subscript is empty.
        if (!(selectedElement instanceof Elements.IdentifierSubElement && selectedElement.type === 'attribute' && selectedElement.getSubscript().children.elements.length === 2 && selectedElement.getSubscript().children.at(1) instanceof Elements.EmptyElement)) {
          return hidden;
        }
        break;
      case 'replace_set_attribute_objkey':
        // Current element must be an attribute with only one subscript.
        if (!(selectedElement instanceof Elements.IdentifierSubElement && selectedElement.type === 'attribute' && selectedElement.getSubscript().children.elements.length === 0)) {
          return hidden;
        }
        break;
      case 'replace_iteration':
        if (!(selectedElement instanceof Elements.InVersionsElement) && !(selectedElement instanceof Elements.InObjectsElement)) {
          return hidden;
        }
        break;
      case 'replace_versions_iteration':
        if (!(selectedElement instanceof Elements.InElement) && !(selectedElement instanceof Elements.InObjectsElement)) {
          return hidden;
        }
        break;
      case 'replace_objects_iteration':
        if (!(selectedElement instanceof Elements.InVersionsElement) && !(selectedElement instanceof Elements.InElement)) {
          return hidden;
        }
        break;
      case 'sub_replace_obj_comp_operators':
        if (!(selectedElement instanceof Elements.FullEqualsElement) && !(selectedElement instanceof Elements.ObjectEqualsElement) && !(selectedElement instanceof Elements.ProperSubsetElement) && !(selectedElement instanceof Elements.SubsetElement) && !(selectedElement instanceof Elements.ProperSupersetElement) && !(selectedElement instanceof Elements.SupersetElement) && !(selectedElement instanceof Elements.SetOverlapElement) && !(selectedElement instanceof Elements.EqualElement)) {
          return hidden;
        }
        break;
      case 'replace_set_size':
        if (identifyElement(selectedElement) !== 'versionCount' && identifyElement(selectedElement) !== 'objectCount') {
          return hidden;
        }
        break;
      case 'replace_version_count_function':
        if (!(selectedElement instanceof Elements.CardinalityElement) && identifyElement(selectedElement) !== 'objectCount') {
          return hidden;
        }
        break;
      case 'replace_object_count_function':
        if (!(selectedElement instanceof Elements.CardinalityElement) && identifyElement(selectedElement) !== 'versionCount') {
          return hidden;
        }
        break;
    }

    return enabled;
  };

  /**
 * Replaces current objectCount function with versionCount function.
 */
  replaceVersionCount = () => {
    replaceSetCount.call(this, 'versionCount');
  };

  /**
 * Replaces current versionCount function with objectCount function.
 */
  replaceObjectCount = () => {
    replaceSetCount.call(this, 'objectCount');
  };

  /**
 * Inserts a min element at the current caret position.
 */
  insertMin = () => {
    var newElement = new Elements.MinElement();
    newElement.children.add(new Elements.SeparatorElement());

    this.edit(function () {
      this.matheq.equation.insertElement(newElement);
    });
  };

  /**
 * Inserts a max element at the current caret position.
 */
  insertMax = () => {
    var newElement = new Elements.MaxElement();
    newElement.children.add(new Elements.SeparatorElement());

    this.edit(function () {
      this.matheq.equation.insertElement(newElement);
    });
  };

  /**
 * Inserts a modulo element at the current caret position.
 */
  insertModulo = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ModuloElement());
    });
  };

  /**
 * Inserts a floor element at the current caret position.
 */
  insertFloor = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FloorElement());
    });
  };

  /**
 * Inserts a absolute element at the current caret position.
 */
  insertAbsolute = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.AbsoluteElement());
    });
  };

  /**
 * Inserts an arctan element at the current caret position.
 */
  insertArctan = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ArctanElement());
    });
  };

  /**
 * Inserts a round(?) function at the current caret position.
 */
  insertUnaryRound = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('round', null, null, 0, 1));
    });
  };

  /**
 * Inserts a round(?, ?) function at the current caret position.
 */
  insertBinaryRound = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('round', null, null, 0, 2));
    });
  };

  /**
 * Inserts a round(?, ?, ?) function at the current caret position.
 */
  insertTrinaryRound = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('round', null, null, 0, 3));
    });
  };

  /**
 * Inserts a power element at the current caret position.
 */
  insertPower = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.PowerElement());
    });
  };

  /**
 * Inserts an exponential element at the current caret position.
 */
  insertExponential = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ExponentialElement());
    });
  };

  /**
 * Inserts a logarithm element at the current caret position.
 */
  insertLogarithm = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.LogarithmElement());
    });
  };

  /**
 * Inserts a natural logarithm element at the current caret position.
 */
  insertNaturalLogarithm = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.NaturalLogarithmElement());
    });
  };

  /**
 * Inserts a union operator at the current caret position.
 */
  insertUnion = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.UnionElement());
    });
  };

  /**
 * Inserts a intersect operator at the current caret position.
 */
  insertIntersect = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.IntersectElement());
    });
  };

  /**
 * Inserts a set diff operator at the current caret position.
 */
  insertSetDiff = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.SetDiffElement());
    });
  };

  /**
 * Inserts an arbitrary union element at the current caret position.
 */
  insertArbitraryUnion = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ArbitraryUnionElement());
    });
  };

  /**
 * Inserts an arbitrary intersect element at the current caret position.
 */
  insertArbitraryIntersect = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.ArbitraryIntersectElement());
    });
  };

  /**
 * Inserts a filter set operator at the current caret position.
 */
  insertFilterSet = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FilterSetElement());
    });
  };

  /**
 * Inserts an interval element at the current caret position.
 */
  insertInterval = (numSubscripts) => {
    if (numSubscripts > 0) {
      var newElement = new Elements.IntervalSubElement(numSubscripts);
    } else {
      var newElement = new Elements.IntervalElement();
    }

    newElement.children.add(new Elements.SeparatorElement());

    this.edit(function () {
      this.matheq.equation.insertElement(newElement);
    });
  };

  /**
 * Inserts a lookup function at the current caret position.
 */
  insertLookup = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('lookup', null, null, 0, 2));
    });
  };

  /**
 * Inserts a snapshot function at the current caret position.
 */
  insertSnapshot = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('snapshot', null, null, 0, 2));
    });
  };

  /**
 * Inserts an expand function at the current caret position.
 */
  insertExpand = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('expand', null, null, 0, 1));
    });
  };
  /**
 * Inserts an daysinmonth function at the current caret position.
 */
  insertdaysinMonth = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('DaysInMonth', null, null, 0, 2));

    });
  };

  insertCount = () => {
    this.edit(function () {
      this.matheq.equation.insertElement(new Elements.FunctionElement('Count', null, null, 0, 1));
    });
  };

}


function onCaretBlinkTimer(editor) {
  editor.caretVisible = !editor.caretVisible;
  renderCaret(editor.matheq.getRenderContext(), editor.caretVisible);
  editor.caretTimerId = setTimeout(function () {
    onCaretBlinkTimer(editor);
  }, CaretBlinkInterval);
}

function onMouseDownInCanvas(event) {
  // debugger
  if (event.which !== 1) { // We only care about left mouse button clicks.
    return;
  }

  var editor = event.currentTarget.editor;
  // var editor = event.data.editor;

  // Find element at cursor position.
  var cursorPosition = getMousePosition(event, editor);
  var element = editor.matheq.equation.getElementByPosition(cursorPosition);

  // this is for check the mouse position on canvas-start
  editor.positionis = true;
  // end

  // Select the element.
  if (element) {
    editor.stopCaretTimer();

    editor.matheq.equation.setCurrentElement(element);
    element.caretPosition = getCaretPosition(element, cursorPosition);


    editor.startCaretTimer();
  }

}

function onKeyUp(event) {
  // debugger
  var isNavKey = false;
  var editor = event.currentTarget.editor;
  // var editor = event.data.editor;
  var equation = editor.matheq.equation;
  var currentElement = equation.currentElement;
  var selection;

  if (event.ctrlKey) {
    switch (event.keyCode) {
      case 67: // ctrl + C
        if (currentElement) {
          selection = editor.copy();
        }
        isNavKey = true;
        break;
      case 86: // ctrl + v
        if (currentElement) {
          selection = editor.paste();
        }
        isNavKey = true;
        break;
      case 88: // ctrl + x
        if (currentElement) {
          selection = editor.cut();
        }
        isNavKey = true;
        break;
      case 89: // ctrl + y
        if (currentElement) {
          selection = editor.redo();
        }
        isNavKey = true;
        break;
      case 90: // ctrl + Z
        if (currentElement) {
          selection = editor.undo();
        }
        isNavKey = true;
        break;
    }
  }
  switch (event.which) {
    case 37: // Left arrow.
      if (currentElement) {
        if (currentElement.caretPosition === Common.CaretPosition.Left) {
          selection = currentElement.getLeftVisualElement();
        } else if (currentElement.caretPosition === Common.CaretPosition.Right) {
          selection = {
            element: currentElement,
            caretPosition: Common.CaretPosition.Left
          };
        }
      }
      isNavKey = true;
      break;
    case 39: // Right arrow.
      if (currentElement) {
        if (currentElement.caretPosition === Common.CaretPosition.Left) {
          selection = {
            element: currentElement,
            caretPosition: Common.CaretPosition.Right
          };
        } else if (currentElement.caretPosition === Common.CaretPosition.Right) {
          selection = currentElement.getRightVisualElement();
        }
      }
      isNavKey = true;
      break;
    case 38: // Up arrow.
      if (currentElement) {
        selection = currentElement.getUpVisualElement();
      }
      isNavKey = true;
      break;
    case 40: // Down arrow.
      if (currentElement) {
        selection = currentElement.getDownVisualElement();
      }
      isNavKey = true;
      break;
    case 36: // Home.
      selection = equation.getFirstVisualElement();
      isNavKey = true;
      break;
    case 35: // End.
      selection = equation.getLastVisualElement();
      isNavKey = true;
      break;
    case 8: // Backspace
      if (currentElement) {
        selection = editor.deleteItem();
      }
      isNavKey = true;
      break;
  }

  if (selection) {
    editor.stopCaretTimer();

    if (equation.currentElement !== selection.element) {
      equation.setCurrentElement(selection.element);
    }

    if (equation.currentElement.caretPosition !== selection.caretPosition) {
      equation.currentElement.caretPosition = selection.caretPosition;
    }

    editor.startCaretTimer();
  }

  if (isNavKey) {
    event.preventDefault();
    event.stopPropagation();
  }
}

function onKeyDown(event) {

  var isNavKey = false;
  var editor = event.data.editor;
  var equation = editor.matheq.equation;
  var currentElement = equation.currentElement;
  var selection;

  switch (event.which) {
    case 37: // Left arrow.
      if (currentElement) {
        if (currentElement.caretPosition === Common.CaretPosition.Left) {
          selection = currentElement.getLeftVisualElement();
        } else if (currentElement.caretPosition === Common.CaretPosition.Right) {
          selection = {
            element: currentElement,
            caretPosition: Common.CaretPosition.Left
          };
        }
      }
      isNavKey = true;
      break;
    case 39: // Right arrow.
      if (currentElement) {
        if (currentElement.caretPosition === Common.CaretPosition.Left) {
          selection = {
            element: currentElement,
            caretPosition: Common.CaretPosition.Right
          };
        } else if (currentElement.caretPosition === Common.CaretPosition.Right) {
          selection = currentElement.getRightVisualElement();
        }
      }
      isNavKey = true;
      break;
    case 38: // Up arrow.
      if (currentElement) {
        selection = currentElement.getUpVisualElement();
      }
      isNavKey = true;
      break;
    case 40: // Down arrow.
      if (currentElement) {
        selection = currentElement.getDownVisualElement();
      }
      isNavKey = true;
      break;
    case 36: // Home.
      selection = equation.getFirstVisualElement();
      isNavKey = true;
      break;
    case 35: // End.
      selection = equation.getLastVisualElement();
      isNavKey = true;
      break;
    // case 8: // Backspace
    //     if (currentElement) {
    //         selection = editor.deleteItem();
    //     }
    //     isNavKey = true;
    //     break;
  }


  if (selection) {
    editor.stopCaretTimer();

    if (equation.currentElement !== selection.element) {
      equation.setCurrentElement(selection.element);
    }

    if (equation.currentElement.caretPosition !== selection.caretPosition) {
      equation.currentElement.caretPosition = selection.caretPosition;
    }

    editor.startCaretTimer();
  }

  if (isNavKey) {
    event.preventDefault();
    event.stopPropagation();
  }
}
// function getMousePosition(event, editor) {
// debugger
//   var canvasOffset = editor.matheq.canvas.offset();
//   var canvasScroll = editor.matheq.canvas.scrollLeft();

//   return {
//     x: event.pageX - canvasOffset.left + canvasScroll,
//     y: event.pageY - canvasOffset.top
//   };
// }
function getMousePosition(event, editor) {
  const rect = editor.matheq.canvas.getBoundingClientRect();
  // var canvasOffset = editor.matheq.$canvas.offset();
  var canvasOffset = {
    left: rect.left,
    top: rect.top
  };
  // var canvasScroll = editor.matheq.canvas.scrollLeft();
  var canvasScroll = 0;

  return {
    x: event.pageX - canvasOffset.left + canvasScroll,
    y: event.pageY - canvasOffset.top
  };
}
function getCaretPosition(element, position) {

  var location = element.getAbsoluteLocation();
  var size = (element instanceof Elements.OperatorElement) ? element.operatorSize : element.size;

  var midpoint = location.x + size.width / 2;

  return position.x < midpoint ? Common.CaretPosition.Left : Common.CaretPosition.Right;
}
const xmlEntityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&apos;'
}; /**
 * Escapes characters that might confuse an xml parser.
 */
function escapeXml(str) {
  return String(str).replace(/[&<>"']/g, function (s) {
    return xmlEntityMap[s];
  });
} /**
 * Private.
 * Shows the standard EC popup.
 * Must be called with the editor as this-reference.
 *
 * @param url: The popup request url.
 * @param args: Arguments to configure the popup.
 */
// function showEcPopup(url, args) { // Fix issues with the standard EC popup.
//   debugger
//   if (!this.ecPopupInstrumented) {
//     var thisValue = this;
//     // var pfPopup = PF('popup');
//     var pfPopup = ('popup');

//     // Stop keydown events on the popup itself from bubbling out of the popup.
//     // Otherwise they will trigger undesired actions in parent component.
//     var jqData = $.hasData(document) && $._data(document);

//     if (jqData && jqData.events && jqData.events.keydown) {
//       var eventHandler = function (e) {
//         if (thisValue.popupVisible) {
//           e.stopImmediatePropagation();
//         }

//         originalHandler.call(this, e);
//       };

//       for (var i = 0; i < jqData.events.keydown.length; ++i) {
//         var event = jqData.events.keydown[i];

//         if (event.namespace === 'dialog_' + pfPopup.id) {
//           var originalHandler = event.handler;
//           event.handler = eventHandler;
//           break;
//         }
//       }
//     }

//     this.ecPopupInstrumented = true;
//   }

//   attachPopupCloseHook.call(this);

//   // Set popup attributes.
//   // beforePopupShowJs([{name: 'screenletId', value: args.screenletId}]);

//   $('[id="popupForm:popupUrl"]').val(url);
//   $('[id="popupIFrame"]').attr('src', url + '&isPopup=true');
//   $('[id="popupForm:headerLabel"]').text(args.title);
//   $('[id="popupForm:popup"]').css({
//     width: (args.width - 40) + 'px',
//     height: (args.height - 40) + 'px'
//   });

//   // Finally show the EC popup.
//   if (!pfPopup) { // var pfPopup = PF('popup');
//     var pfPopup = ('popup');
//   }

//   pfPopup.show();
// } 

/**
 * Private.
 * Override original PrimeFaces popup onHide function to always notify us when it is closed.
 * This is to make sure we get a chance to clean up if the popup is canceled.
 * Must be called with the editor as this-reference.
 */
function attachPopupCloseHook() {
  var thisValue = this;
  var pfPopup = ('popup');
  // var pfPopup = PF('popup');
  this.originalPopupOnHide = pfPopup.onHide;

  pfPopup.onHide = function (arg1, arg2) {
    debugger
    thisValue.originalPopupOnHide.call(pfPopup, arg1, arg2);
    thisValue.onPopupClosed(null, null, true);
  };
} /**
 * Private.
 * Sets the original PrimeFaces popup onHide function back to normal.
 * Must be called with the editor as this-reference.
 */
function detachPopupCloseHook() {
  debugger
  var thisValue = this;
  // var pfPopup = PF('popup');
  var pfPopup = ('popup');
  pfPopup.onHide = this.originalPopupOnHide;
} /**
 * Private.
 * ForallElement must be the root element of iteration type equations.
 * This function injects a new forall element at root if one is not already present.
 */
function injectIterationsRootElement() {

  // if (this.matheq.equationType === EquationType.Iterations && !this.matheq.equation.isEmpty() && this.matheq.equation.currentElement.parent instanceof Elements.InElement) {


  if (this.matheq.equationType === EquationType.Iterations && !this.matheq.equation.isEmpty()) {
    var oldRootElement = this.matheq.equation.getRootElement();

    if (!(oldRootElement instanceof Elements.ForallElement)) { // Inject forall element at root of equation.
      var forallElement = new Elements.ForallElement();
      this.matheq.equation.setRootElement(forallElement);
      this.matheq.equation.setCurrentElement(forallElement.getOperand());
      this.matheq.equation.insertElement(oldRootElement);
    }
  }
  console.log(this.matheq.equation)
} /**
 * Private.
 * Opens the constant input popup.
 *
 * @param popupTitle: The popup title text.
 * @param dataType: The data type of the constant, e.g. STRING or NUMBER.
 * @param inputLabel: The label of the input box.
 * @param required: Whether or not the input box is mandatory, i.e. cannot be empty.
 * @param initialValue: The initial value of the input box.
 * @param callback: Callback function that will be handed the popup return value.
 */
function insertConstant(popupTitle, dataType, inputLabel, required, initialValue, callback) { // Calc.MathEqEditor.openPopup(

  $: MathEqEditor = new MathEqEditor({ matheqId: 'mathml-equation', activate: true })

  MathEqEditor.openPopup('/com.ec.frmw.bs.calc.screens/eqnedit_const_popup', {
    DATA_TYPE: dataType,
    DATA_LABEL: inputLabel,
    DATA_REQUIRED: !!required,
    DATA_VIEW_WIDTH: 170,
    DATA_VALUE: escapeXml(initialValue),
    PopupReturnColumn: 'VALUE'
  }, {
    width: 250,
    height: 170,
    title: popupTitle
  }, callback);
} /**
 * Private.
 * Replaces cardinality and version/object count functions.
 * Must be called with an editor instance as this-reference.
 *
 * @param functionName: 'versionCount' or 'objectCount'.
 */
function replaceSetCount(functionName) {
  var selectedElement = this.matheq.equation.currentElement;

  if (selectedElement instanceof Elements.CardinalityElement) {
    var arg = selectedElement.getSet();
  } else {
    var arg = selectedElement.getCsymbolArguments();
  }

  var newElement = new Elements.FunctionElement(functionName, null, null, 0, 1);
  newElement.setCsymbolArguments(arg);

  this.edit(function () {
    this.matheq.equation.deleteElement(selectedElement);
    this.matheq.equation.insertElement(newElement);
  });
} /**
 * Private.
 * Opens a popup for the user to indicate the number of iterations/subscripts in the function element.
 * Must be called with the editor as this-reference.
 *
 * @param callback: A function that will handle the return value of the popup.
 *                  The function is only called if the return value is valid.
 *                  The function will be called with the editor instance as this-reference.
 */
function getNumberOfSubscripts(callback) {
  debugger
  insertConstant('Insert num iterations', 'INTEGER', 'Number of Iterations', true, '', function (popupReturnValue) {
    if (isNaN(popupReturnValue) && popupReturnValue > 0) {
      callback.call(this, popupReturnValue);
    } else {
      alert('The number of iterations must be a positive integer.\n0 iterations is not permitted.');
    }
  });
} /**
 * Private.
 * Finds a textual name for an element based on its class and properties.
 * Only a few mathml element classes are supported, the rest returns '<unknown>'.
 *
 * @param element: The element to identify.
 * @returns An identifier string.
 */
function identifyElement(element) {
  if (element instanceof Elements.EmptyElement) {
    return 'empty';
  } else if (element instanceof Elements.IdentifierElement || element instanceof Elements.IdentifierSubElement) {
    return element.type;
  } else if (element instanceof Elements.OperatorElement) {
    return element.operatorText;
  } else if (element instanceof Elements.FunctionElement) {
    if (element.name === 'round') {
      return 'round_' + element.getActualArgs().length;
    } else {
      return element.name;
    }
  } else if (element instanceof Elements.FenceElement && element.type === Elements.FenceElement.FenceType.CurlyBracket) {
    return 'object_list';
  }

  return '<unknown>';
} /**
 * Private.
 * Scans the element tree to see if the given element has a parent of the given type.
 * It is also possible to only search for parents that have the element at a given parameter index.
 * Typical example: "Am I inside the second argument of an argmin function?"
 *
 * Note: This function doesn't handle nested parents when the paramNo filter is used.
 * Example: args(x, args(y, z))
 * If we ask for IsVisualChildOf(y, 'args', 2, false) then this returns false since y is the first param
 * of the first 'args' it finds (even though it is in the second parameter of the outer 'args').
 *
 * @param element:        The element to check.
 * @param parentName:     The name of the parent to find.
 * @param paramNo:        The parameter index to check for, -1 is ignore, 1 is first index and so on.
 * @param immediateChild: Set to true to limit the search to the immediate parent.
 */
function isVisualChildOf(element, parentName, paramNo, immediateChild) {
  var isChild = false;
  var visualParent = element.visualParent;

  // Scan upwards and see if we can find the requested parent.
  while (visualParent) {
    if (identifyElement(visualParent) === parentName) {
      isChild = true;
      break;
    } else {
      if (immediateChild) {
        break;
      }

      if (visualParent.visualParent === visualParent) {
        break;
      }

      visualParent = visualParent.visualParent;
    }
  }

  // Check that we are in the required argument branch (if requested).
  if (isChild && paramNo > 0) {
    if (visualParent instanceof Elements.FunctionElement) {
      var children = visualParent.getActualArgs();
      --paramNo;
    } else {
      var children = visualParent.children.elements;
    }

    if (!isChildOf(children[paramNo], element)) {
      isChild = false;
    }
  }

  return isChild;
} /**
 * Private.
 * Checks if an element is a child of another element.
 *
 * @param top: The parent element.
 * @param element: The child element to look for.
 */
function isChildOf(parent, element) {
  var found = false;

  if (parent === element) {
    found = true;
  } else if (parent) {
    found = parent.children.elements.some(function (childElement) {
      if (isChildOf(childElement, element)) {
        return true;
      }
    });
  }

  return found;
} /**
 * Checks if the currently selected element is on the left hand side of the equation.
 *
 * @param equation: The equation to check.
 */
function isOnLeftHandSide(equation) {
  var rootElement = equation.getRootElement();

  if (rootElement instanceof Elements.EqualElement) {
    if (rootElement === equation.currentElement) { // The assignment element is the current element, so it's right or left side depending on the caret.
      return rootElement.caretPosition === Common.CaretPosition.Left;
    } else { // Examine the left hand side of the assignment element.
      return isChildOf(rootElement.getLeftOperand(), equation.currentElement);
    }
  }

  return false;
}
