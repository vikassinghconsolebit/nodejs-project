import React, { Component } from 'react';
import { Form, Button } from 'antd'
import { connect } from 'react-redux';
import { mathMLActions } from "../../../actions/mathml.action";
import { MathEq, MathEqReg } from "./math/MathEq";
import { Renderer } from "./math/Renderer"
import { MathEqEditor } from "./math/MathEqEditor";

require('./math/Builder')
require('./math/Commons')
require('./math/ElementCollection')
require('./math/Elements')
require('./math/Equation')
require('./math/MathEq')
require('./math/Parser')
require('./math/Renderer')




class EquationInputRender extends Component {

  state = {
    math: '<math xmlns:m="http://www.w3.org/1998/Math/MathML"><apply><gt/><apply><csymbol encoding="text" definitionURL="http:#">objectCount</csymbol><set><condition><apply><and/><apply><csymbol encoding="text" definitionURL="http:#">isValid</csymbol><apply><selector/><ci type="variable">OldGasRate[ALLOC_NODE,DAY,DAY,LITERAL(FCSTTYPE),LITERAL(FCSTSCEN)]</ci><ci type="iterator">iwell</ci><apply><csymbol encoding="text" definitionURL="http:#">firstOfMonth</csymbol><ci type="set">d</ci></apply><ci type="iterator">idy6</ci><ci type="constant">MTHLY</ci><ci type="constant">NOTIFICATION</ci></apply></apply><apply><leq/><ci type="iterator">idy6</ci><apply><csymbol encoding="text" definitionURL="http:#">firstOfMonth</csymbol><ci type="set">d</ci></apply></apply></apply></condition><apply><in/><ci type="iterator">idy6</ci><apply><csymbol encoding="text" definitionURL="http:#">args</csymbol><apply><selector/><ci type="variable">OldGasRate[ALLOC_NODE,DAY,DAY,LITERAL(FCSTTYPE),LITERAL(FCSTSCEN)]</ci><ci type="iterator">iwell</ci><apply><csymbol encoding="text" definitionURL="http:#">firstOfMonth</csymbol><ci type="set">d</ci></apply><apply><in/><ci type="iterator">idy</ci><ci type="set">*</ci></apply><ci type="constant">MTHLY</ci><ci type="constant">NOTIFICATION</ci></apply><ci type="iterator">idy</ci></apply></apply></set></apply><cn>0</cn></apply></math>',
    mathml: '<math><apply><plus/><ci type=\'variable\'>x</ci><ci type=\'variable\'>y</ci></apply></math>',
    math1: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><in/><ci type="iterator">i</ci><set><condition><apply><and/><apply><and/><apply><eq/><apply><selector/><ci type="logstmt">FName</ci><ci type="iterator">string</ci></apply><ci type="iterator">d</ci></apply><apply><eq/><apply><selector/><ci type="logstmt">Sal</ci><ci type="iterator">string</ci></apply><ci type="constant">BEST</ci></apply></apply><apply><eq/><apply><selector/><ci type="logstmt">Cmpny</ci><ci type="iterator">string</ci></apply><ci type="constant">VANC</ci></apply></apply></condition><apply><in/><ci type="iterator">string</ci><ci type="set">AllEmployees</ci></apply></set></apply></math>',
    math2: '<math xmlns:m="http://www.w3.org/1998/Math/MathML"><apply><forall /><condition><and /><apply><in /><ci type="iterator">s</ci><set><condition><apply><and /><apply><and /><apply><eq /><apply><selector /><ci type="attribute" object-type="ALLOC_STREAM">FromNode</ci><ci type="iterator">fstrm</ci></apply><ci type="iterator">this</ci></apply><apply><eq /><apply><selector /><ci type="attribute" object-type="ALLOC_STREAM">Phase</ci><ci type="iterator">fstrm</ci></apply><ci type="constant">GAS</ci></apply></apply><apply xmlns:ecm="http://www.tietoenator.com/ec/alloc/MathMLEditor" ecfence="()"><eq /><apply><selector /><ci type="attribute" object-type="ALLOC_STREAM">StreamCategory</ci><ci type="iterator">fstrm</ci></apply><ci type="constant">FACILITY_TOTAL_MAIN</ci></apply></apply></condition><apply><in /><ci type="iterator">fstrm</ci><ci type="set">FacilityTotalStreams</ci></apply></set></apply></condition></apply></math>',
    math3: '<math xmlns:m="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">icntr</ci><ci type="set">parameter_set_BUDGET_1</ci></apply><apply><in/><ci type="iterator">ixdp</ci><apply><csymbol encoding="text" definitionURL="http:#">args</csymbol><apply><selector/><ci type="variable">NewBudget[CONTRACT,MTH,DELIVERY_POINT]</ci><ci type="iterator">icntr</ci><apply><csymbol encoding="text" definitionURL="http:#">toMonth</csymbol><ci type="set">d</ci></apply><apply><in/><ci type="iterator">dp</ci><ci type="set">*</ci></apply></apply><ci type="iterator">dp</ci></apply></apply><apply><in eckeytype="objects" xmlns:ecm="http://www.tietoenator.com/ec/alloc/MathMLEditor"/><ci type="iterator">iCompany</ci><set><condition><apply><csymbol encoding="text" definitionURL="http:#">isValid</csymbol><apply><selector/><ci type="variable">ContractParty[CONTRACT,DAY,COMPANY]</ci><ci type="iterator">icntr</ci><ci type="set">d</ci><ci type="iterator">icpy2</ci></apply></apply></condition><apply><in/><ci type="iterator">icpy2</ci><ci type="set">cmpny_pcs</ci></apply></set></apply><apply><in/><ci type="iterator">iPC</ci><set><condition><apply><eq/><apply><selector/><ci type="attribute">Code</ci><ci type="iterator">iCompany</ci></apply><apply><selector/><ci type="attribute">Code</ci><ci type="iterator">ipfc2</ci></apply></apply></condition><apply><in eckeytype="versions" xmlns:ecm="http://www.tietoenator.com/ec/alloc/MathMLEditor"/><ci type="iterator">ipfc2</ci><ci type="set">pfcs_cmpny</ci></apply></set></apply><apply><in/><ci type="iterator">ipfc</ci><set><condition><apply><csymbol encoding="text" definitionURL="http:#">isValid</csymbol><apply><selector/><ci type="variable">RelativePercentage[CONTRACT,DAY,PROFIT_CENTRE]</ci><ci type="iterator">icntr</ci><ci type="set">d</ci><ci type="iterator">ipfc3</ci></apply></apply></condition><apply><in/><ci type="iterator">ipfc3</ci><ci type="set">pcs</ci></apply></set></apply></condition></apply></math>',
    math4: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">i</ci><ci type="set">conversionfactors</ci></apply></condition></apply></math>',
    math5: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">emp</ci><set><condition><apply><lt/><apply><csymbol encoding="text" definitionURL="http:#">DATEDIF</csymbol><apply><eq/><ci type="variable">Dob</ci><ci type="constant">BEST</ci></apply><apply><csymbol encoding="text" definitionURL="http:#">FOYEAR</csymbol><apply><csymbol encoding="text" definitionURL="http:#">ADDMONTHS</csymbol><ci type="iterator">d</ci><cn>12</cn></apply></apply></apply><cn>60</cn></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="set">AllEmployees</ci></apply></set></apply></condition></apply></math>',
    math6: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">emp</ci><set><condition><apply><lt/><apply><csymbol encoding="text" definitionURL="http:#">DATEDIF</csymbol><apply><in/><ci type="variable">Dob</ci><set><condition><apply><eq/><ci type="variable">demo</ci><ci type="constant">BEST</ci></apply></condition><apply><in/><ci type="iterator">icmpny</ci><ci type="set">AllCompanies</ci></apply></set></apply><apply><csymbol encoding="text" definitionURL="http:#">FOYEAR</csymbol><apply><csymbol encoding="text" definitionURL="http:#">ADDMONTHS</csymbol><ci type="iterator">d</ci><cn>12</cn></apply></apply></apply><cn>60</cn></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="set">AllEmployees</ci></apply></set></apply></condition></apply></math>',
    math7: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">emp</ci><set><condition><apply><lt/><apply><csymbol encoding="text" definitionURL="http:#">DATEDIF</csymbol><apply><eq/><ci type="variable">Dob</ci><ci type="variable">demo</ci></apply><apply><csymbol encoding="text" definitionURL="http:#">FOYEAR</csymbol><apply><csymbol encoding="text" definitionURL="http:#">ADDMONTHS</csymbol><ci type="iterator">d</ci><cn>12</cn></apply></apply></apply><cn>60</cn></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="set">AllEmployees</ci></apply></set></apply></condition></apply></math>',
    Iteration: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><forall/><condition><and/><apply><in/><ci type="iterator">emp</ci><set><condition><apply><lt/><apply><csymbol encoding="text" definitionURL="http:#">DATEDIF</csymbol><apply><selector/><ci type="variable">Dob[DAY,COMPANY]</ci><ci type="iterator">i</ci><set><condition><apply><eq/><apply><selector/><ci type="variable">Cmpny[COMPANY]</ci><ci type="iterator">i</ci></apply><ci type="constant">BEST</ci></apply></condition><apply><in/><ci type="iterator">icmpny</ci><ci type="set">AllCompanies</ci></apply></set></apply><apply><csymbol encoding="text" definitionURL="http:#">FOYEAR</csymbol><apply><csymbol encoding="text" definitionURL="http:#">ADDMONTHS</csymbol><ci type="iterator">d</ci><cn>12</cn></apply></apply></apply><cn>60</cn></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="set">AllEmployees</ci></apply></set></apply></condition></apply></math>',
    condition: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><and/><apply><neq/><apply><selector/><ci type="variable">FName[DAY,COMPANY]</ci><set><condition><apply><eq/><apply><selector/><ci type="variable">Cmpny[DAY]</ci><ci type="iterator">j</ci></apply><ci type="constant">VANC</ci></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="iterator">emp</ci></apply></set></apply><ci type="constant">JOHN</ci></apply><apply><neq/><apply><selector/><ci type="variable">LName[DAY,COMPANY]</ci><set><condition><apply><eq/><apply><selector/><ci type="variable">Cmpny[DAY]</ci><ci type="iterator">j</ci></apply><ci type="constant">VANC</ci></apply></condition><apply><in/><ci type="iterator">i</ci><ci type="iterator">emp</ci></apply></set></apply><ci type="constant">SMITH</ci></apply></apply></math>',
    assignment: '<math xmlns="http://www.w3.org/1998/Math/MathML"><apply><eq/><apply><selector/><ci type="logstmt">Sal</ci><ci type="iterator">emp</ci><ci type="iterator">d</ci></apply><apply><times/><apply><selector/><ci type="logstmt">Sal</ci><ci type="iterator">emp</ci><apply><csymbol encoding="text" definitionURL="http:#">ADDMONTHS</csymbol><ci type="iterator">d</ci><cn>-12</cn></apply></apply><cn>1.05</cn></apply></apply></math>',
  }


  componentDidMount() {
    const equation = new MathEq({
      mathmlId: 'mathml-equation',
      canvasId: 'math-equation',
      editable: true,
      context: 'free',
      equationType: this.props.EqType
    })
    MathEqReg.register('mathml-equation', equation)
    const editor = new MathEqEditor({
      matheqId: 'mathml-equation',
      activate: true,
    })
    Renderer.render({
      equation: equation,
      canvas: equation.canvas,
      editable: true
    })
    this.props.setMathEditorAction(editor)
    this.props.setMathEquationAction(equation)

  }

  render() {
    const mathdata = this.props.mathmlReducer.mathml ? this.props.mathmlReducer.mathml : '';
    return (
      <>
        <input id="mathml-equation" type="hidden" value={mathdata} ref={this.props.inputRef} />
        <div>
          <canvas id="math-equation" tabIndex="-1" />
        </div>
      </>
    );
  }
}


function mapStateToProps(state) {
  return { ...state };
}
const actionCreators = {
  setMathEditorAction: mathMLActions.setMathEditorAction,
  setMathEquationAction: mathMLActions.setMathEquationAction,
  setMathMLExpressionAction: mathMLActions.setMathMLExpressionAction,
};
export default connect(mapStateToProps, actionCreators)(EquationInputRender);