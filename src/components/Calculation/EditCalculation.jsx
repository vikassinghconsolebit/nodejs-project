import React, { Component } from 'react'
import { Button, DatePicker, Form, Input, message } from 'antd'
import { addCalculation, updateCalculation, getCalculationById } from '../../controller/api/calculationApi';
import Routes from "../../controller/routes";
import { handleError } from "../../controller/global";
import moment from "moment";
import { reverse } from 'named-urls';

export default class EditCalculation extends Component {
    state = {
        loading: false,
        Caldata: null,
    }
    formRef = React.createRef()

    componentDidMount() {
        getCalculationById(this.props.match.params.id).then(res => {
            this.setState({ Caldata: res.data })
        })

    }
    
    componentDidUpdate() {
        this.formRef.current.setFieldsValue({
            name: this.state.Caldata.name,
            description: this.state.Caldata.description,
            day_time: moment(this.state.Caldata.day_time),
            start_date: moment(this.state.Caldata.start_date),
            end_date: moment(this.state.Caldata.end_date)
        })
    }

    onSubmit = (value) => {
        this.setState({ loading: true })
        updateCalculation(this.props.match.params.id, value).then(res => {
            message.success("Calculation Updated Successfully")
            this.props.history.push(reverse(Routes.dashboard.calculation.equationBuilder, { id: this.props.match.params.id }),)

            // this.props.history.push(Routes.dashboard.calculation.self);
            this.setState({ loading: false })
        }).catch(err => {
            handleError(err)
            this.setState({ loading: false })
        })
    }

    render() {

        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Update Calculation</h5>
                        <Form onFinish={this.onSubmit} ref={this.formRef}>
                            <Form.Item label="Name" name="name">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input />
                            </Form.Item>
                            <Form.Item label="Day Time" name="day_time">
                                <DatePicker />
                            </Form.Item>
                            <Form.Item label="Start Date" name="start_date">
                                <DatePicker />
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker />
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit" className="mr-3">Submit</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}
