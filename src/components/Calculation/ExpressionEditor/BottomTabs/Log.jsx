import React, {Component} from 'react'
import {Button, Table, Modal} from 'antd';
import Routes from '../../../../controller/routes';
import {reverse} from 'named-urls';
import {fetchRunCalculation, fetchCalculationStatus} from "../../../../controller/api/calculationApi";
import {formatDate, handleError} from "../../../../controller/global";
import pagination from "../../../../controller/Pagination"

export default class Log extends Component {
    state = {
        loading: false,
        data: [],
        pagination: pagination

    }

    componentDidMount() {
        this.getCalculationStatus();
    }

    runEquation = (id) => {
        setTimeout(function () {
            this.getCalculationStatus()
        }.bind(this), 800);

        fetchRunCalculation(id).then(res => {
            setTimeout(function () {
                this.getCalculationStatus()
            }.bind(this), 100);
            this.setState({loading: false})

        }).catch(err => {
            // handleError(err)
            this.setState({loading: false})
        })

    }

    getCalculationStatus = (params = {}) => {
        this.setState({loading: true})
        fetchCalculationStatus({
            calculation: this.props.match.params.id,
            ...params
        }).then(res => {
            const pagination = {
                ...this.state.pagination
            }
            pagination.total = res.data.count
            this.setState({data: res.data.results, pagination, loading: false})
        }).catch(err => {
            console.log("error", err)
            // handleError(err)
            this.setState({loading: false})
        })
    }

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend') 
            symbol = '-';
        
        let params = {
            page: pagination.current
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.getCalculationStatus(params);
    };


    render() {
        let {loading, data, pagination} = this.state;
        const columns = [
            {
                title: 'Status',
                dataIndex: 'status',
                key: 'status',
                render: (data) => <div>{
                    data 
                }</div>
            }, {
                title: 'Run Time',
                dataIndex: 'run_time',
                key: 'run_time',
                render: (data) => <div>{
                    data && parseFloat(data).toFixed(2) + " Sec" || 'Calculating...'
                }</div>
            }, {
                title: 'End Time',
                dataIndex: 'end_time',
                key: 'end_time',
                render: (data) => <div>{
                    formatDate(data)
                }</div>
            }, {
                title: 'Start Time',
                dataIndex: 'start_time',
                key: 'start_time',
                render: (data) => <div>{
                    formatDate(data)
                }</div>
            },
        ];

        return (
            <div>
                <Button type="primary" className="mr-2"
                    onClick={
                        () => {
                            this.props.history.push(reverse(Routes.dashboard.calculation.detail, {id: this.props.match.params.id}))
                        }
                }>Edit Calculation</Button>

                <Button type="primary" className="mr-2"
                    style={
                        {backgroundColor: "#32b632"}
                    }
                    onClick={
                        () => this.runEquation(this.props.match.params.id)
                }>Run Calculation</Button>

                <Table bordered className="mt-2"
                    loading={loading}
                    onChange={
                        this.handleTableChange
                    }
                    dataSource={data}
                    columns={columns}
                    pagination={pagination}
                    rowKey={
                        record => record.start_time
                    }/>
            </div>
        )
    }
}
