import React, { Component } from 'react';
import { Input, Spin } from 'antd';
import { getEquation } from '../../../../controller/api/euationApi';
import { handleError } from '../../../../controller/global';
import { connect } from "react-redux";
// import { data } from 'jquery';


class RecordStatus extends Component {
    state = {
        data: [],
        loading: false,
        recordData: [],
    }

    componentDidMount() {
        this.fetchEquation()
    }

    fetchEquation = () => {
        this.setState({ loading: true })

        getEquation({ calculation: this.props.match.params.id })
            .then(res => {
                this.setState({
                    loading: false,
                    data: res.data.results,
                }, () => {
                    this.setState({ recordData: [...this.state.data].shift() })
                })
            })
            .catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }

    render() {
        const { data, recordData } = this.state;
        if (!data) {
            return <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <div className='text-center'><Spin size='large' /></div>
                    </div>
                </div>
            </div>
        }

        return (
            <div>
                <div>
                    <div className="row">
                        <div className="col-md-5">
                            <label>Created by : </label>
                            <span className="wid-30">{recordData.created_by ? `${recordData.created_by.first_name} ${recordData.created_by.last_name}` : '-'}</span>
                            {/* <Input className="wid-30" />
                            <Input className="ml-2 wid-30" /> */}
                        </div>
                        <div className="col-md-4 p-0">
                            <label>Comapany : </label>
                            <span className="wid-30">{recordData.company ? `${recordData.company.name}` : '-'}</span>
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-md-5">
                            <label>Last Updated by : </label>
                            <span className="wid-30">{recordData.updated_by ? `${recordData.updated_by.first_name} ${recordData.updated_by.last_name}` : '-'}</span>

                            {/* <Input className="wid-30" />
                            <Input className="ml-2 wid-30" /> */}
                        </div>
                        {/* <div className="col-md-4 p-0">
                            <label>Comapany</label>
                            <span className="wid-30">: {recordData.company ? `${recordData.company.name}` : '-'}</span>

                            <Input className="wid-60" />
                        </div> */}
                        {/* <div className="col-md-3">
                            <label>Revision Event</label>
                            <Input className="ml-2 w-50" />
                        </div> */}
                    </div>
                </div>

            </div>
        )
    }
}

const actionCreators = {
};
export default connect(null, actionCreators)(RecordStatus);
