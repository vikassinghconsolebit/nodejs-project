import React, { Component } from 'react'
import { Button, DatePicker, Select, Form, Table } from 'antd';
import Routes from '../../../controller/routes';
import { reverse } from 'named-urls';
import { getCalculation, fetchRunCalculation, fetchCalculationStatus } from "../../../controller/api/calculationApi";
import { formatDate, handleError } from "../../../controller/global";
import Log from "./BottomTabs/Log";

const { Option } = Select;
const columns = [

    {
        title: 'Start Time',
        dataIndex: 'start_time',
        key: 'start_time',
    },
    {
        title: 'End TIme',
        dataIndex: 'end_time',
        key: 'end_time',
        render: (data) => <div>{data && formatDate(data)}</div>,

    },
    {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
    },

];
export default class Filters extends Component {
    state = {
        data: null,
    }


    render() {
        let { data } = this.state;
        return (
            <div className="dahboard-filters">
                {/* Filters */}
                {/* <Form>
                <Form.Item label="Date">
                    <DatePicker style={{ width: 200 }} />
                </Form.Item>
                <Form.Item label="Calculation">
                    <Select defaultValue="lucy" style={{ width: 200 }}>
                        <Option value="jack">Calculation</Option>
                        <Option value="lucy">Calculation</Option>
                    </Select>
                </Form.Item>
            </Form>
            <Button type="primary" className="ml-2">Generate Documentation</Button> */}

            </div>
        )
    }
}