import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Form, Select, Button, Modal, message, Input } from "antd";
import Routes from "../../../controller/routes";
import { handleError } from "../../../controller/global";
import { getCalculation } from "../../../controller/api/calculationApi";
import EquationBuilder from '../EquationBuilder/EquationBuilder';
import { addEquation } from '../../../controller/api/euationApi';
import { MathEq, MathEqReg } from "../EquationBuilder/math/MathEq";
import { Renderer } from "../EquationBuilder/math/Renderer";
import { reverse } from 'named-urls';
import MathmlLists from './MathmlLists';
import { mathMLActions } from "../../../actions/mathml.action";
import { connect } from 'react-redux';
import { ITERATION, CONDITION, ASSIGNMENT } from '../../../constants/mathml.constant'

const { Option } = Select;

class CreateEquation extends Component {

    state = {
        allcalculation: [],
        loading: false,
        visible: false,
        inputdata: [],
        iteration: '',
        condition: '',
        assignment: '',
        colKey: null,
        columnType: null,
        modalVisible: false,
        copieddata: [],
        jsObject: null,
        // mathml: localStorage.getItem('Copiedmathml'),
        mathml: this.props.mathmlReducer.copy_equation,
        IterArray: [],
        CondArray: [],
        AssignArray: [],
        ModelContent:'',
    }

    formRef = React.createRef()

    componentDidMount() {
        this.getCalculationData()
        this.renderMathmlEquation()
        // if (this.state.mathml) {

        //     this.state.IterArray = JSON.parse(this.state.mathml).filter((auto) => auto.type.includes(ITERATION))
        //     this.state.CondArray = JSON.parse(this.state.mathml).filter((auto) => auto.type.includes(CONDITION))
        //     this.state.AssignArray = JSON.parse(this.state.mathml).filter((auto) => auto.type.includes(ASSIGNMENT))
        // }
        if (this.state.mathml) {

            this.state.IterArray = this.state.mathml.filter((auto) => auto.type.includes(ITERATION))
            this.state.CondArray = this.state.mathml.filter((auto) => auto.type.includes(CONDITION))
            this.state.AssignArray = this.state.mathml.filter((auto) => auto.type.includes(ASSIGNMENT))
            
        }
    }
    

    getCalculationData = (params = {}) => {
        this.setState({ loading: true })
        getCalculation(params)
            .then(res => {
                this.setState({
                    loading: false,
                    allcalculation: res.data.results,
                })
            }).catch(err => {
                handleError(err)
                this.setState({ loading: false })
            })
    }


    onSubmit = (value) => {
        let data = {
            calculation: this.props.location.state.id,

            iteration: "'" + this.state.iteration + "'",
            condition: "'" + this.state.condition + "'",
            assignment: "'" + this.state.assignment + "'",
        }
        this.setState({ loading: true })
        addEquation(data)
            .then(response => {
                message.success('Added Successfully')
                this.setState({ loading: false })
                this.props.history.push({
                    pathname: reverse(Routes.dashboard.calculation.equationBuilder, { id: this.props.location.state.id }),
                    state: { activetab: '1' }})
            })
            .catch(err => {
                handleError(err)
            })
    }

    handleChange = (colName) => {

        this.setState({ visible: true, colKey: colName })
    }

    handleCallback = (childData, obj) => {
        this.setState({ jsObject: obj })
        var nKey = this.state.colKey;
        if (nKey === ITERATION) {
            this.setState({ iteration: childData })

        } else if (nKey === CONDITION) {
            this.setState({ condition: childData })
        } else if (nKey === ASSIGNMENT) {
            this.setState({ assignment: childData })
        }

        setTimeout(function () {
            this.renderMathmlEquation()
        }.bind(this), 100);
    }

    activateMathmlEquation = (inputId, canvasId, equationType) => {

        const equation = new MathEq({
            mathmlId: inputId,
            canvasId: canvasId,
            editable: true,
            context: 'free',
            equationType: equationType
        })
        MathEqReg.register(inputId, equation)
        Renderer.render({
            equation: equation,
            canvas: equation.canvas,
            editable: true
        })
    }

    renderMathmlEquation = () => {
        this.activateMathmlEquation(`mathml-equation-iteration`, `canvas-equation-iteration`, ITERATION)
        this.activateMathmlEquation(`mathml-equation-condition`, `canvas-equation-condition`, CONDITION)
        this.activateMathmlEquation(`mathml-equation-assignment`, `canvas-equation-assignment`, ASSIGNMENT)
    }

    showEquations = (type = null) => {
        var mathmldata = this.state.mathml;
        // var mathmldata = JSON.parse(this.state.mathml);
        var data;
        if (type === ITERATION) {
            data = mathmldata.filter((auto) => auto.type.includes(ITERATION))
            this.setState({ ModelContent: "Import Iteration" })

        } else if (type === CONDITION) {
            data = mathmldata.filter((auto) => auto.type.includes(CONDITION))
            this.setState({  ModelContent: "Import Condition" })

        } else if (type === ASSIGNMENT) {
            data = mathmldata.filter((auto) => auto.type.includes(ASSIGNMENT))
            this.setState({ ModelContent: "Import Assignment" }) 

        }

        this.setState({ copieddata: data, modalVisible: true })
    }


    modelClosed = () => {
        this.setState({ modalVisible: false })
    }


    setEquation = (Eq) => {
        if (Eq.type === ITERATION) {

            const iterData = Eq.value;
            this.setState({ iteration: iterData })
            this.props.setMathMLExpressionAction(iterData)

        } else if (Eq.type === CONDITION) {

            const condData = Eq.value;
            this.setState({ condition: condData })
            this.props.setMathMLExpressionAction(condData)

        } else if (Eq.type === ASSIGNMENT) {
            const EqData = Eq.value;
            this.setState({ assignment: EqData })
            this.props.setMathMLExpressionAction(EqData)
        }

        setTimeout(function () {
            this.renderMathmlEquation()
        }.bind(this), 100);

    };

    equationBuilderClose = () => {
        this.props.removeMathMLExpressionAction()
        this.setState({ visible: false })
    }

    render() {
        let { modalVisible, visible, iteration, condition, assignment, copieddata } = this.state;

        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Equation</h5>

                        <Form className="entity-form" ref={this.formRef} onFinish={this.onSubmit}>
                            {this.state.IterArray.length > 0 ? <div onClick={() => this.showEquations(ITERATION)} style={{ cursor: "pointer" }}><span style={{ color: "red" }}>*</span> Import Iteration</div> : ''}
                            <Form.Item label="Add Iteration" name="iteration" >

                                <Input id="mathml-equation-iteration" placeholder="Iteration" type="hidden" value={iteration} />
                                <canvas id="canvas-equation-iteration" onClick={() => this.handleChange(ITERATION)} />

                            </Form.Item>
                            <div className="mt-3 mb-3">
                                {this.state.CondArray.length > 0 ? <div onClick={() => this.showEquations(CONDITION)}><span style={{ color: "red" }}>*</span> Import Condition</div> : ''}

                                <Form.Item label="Add Condition" name="condition" >

                                    <Input id="mathml-equation-condition" placeholder="Condition" type="hidden" value={condition} />
                                    <canvas id="canvas-equation-condition" onClick={() => this.handleChange(CONDITION)} />

                                </Form.Item>
                            </div>

                            {this.state.AssignArray.length > 0 ? <div onClick={() => this.showEquations(ASSIGNMENT)}><span style={{ color: "red" }}>*</span> Import Assignment</div> : ''}

                            <Form.Item label="Add Assignment" name="assignment" >

                                <Input id="mathml-equation-assignment" placeholder="Assignment" type="hidden" value={assignment} />
                                <canvas id="canvas-equation-assignment" onClick={() => this.handleChange(ASSIGNMENT)} />

                            </Form.Item>
                            {/* <div>
                                <Form.Item rules={[{ required: true, message: "This Field is required" }]}
                                    label="Selected Calculation" name="calculation" hidden={true}>
                                        <input defaultValue={this.props.location.state.id} />

                                    {/* <Select placeholder="Select Type" style={{ width: "100%" }} >

                                        {allcalculation.map((calculation) => <Option key={calculation.id}
                                            value={calculation.id}>{calculation.name}</Option>)}
                                    </Select> */}
                            {/* </Form.Item> */}
                            {/* </div> */}

                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                    className="mr-3" >Submit</Button>
                                <Button onClick={() => this.formRef.current.resetFields()} type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
                <EquationBuilder visible={visible} inputdatacall={this.handleCallback} equationType={this.state.colKey} onClose={() => this.equationBuilderClose()} />

                <Modal destroyOnClose={true}
                    title={this.state.ModelContent}
                    centered
                    visible={modalVisible}
                    onOk={() => this.modelClosed()}
                    onCancel={() => this.modelClosed()}
                    width={1000}>

                    <MathmlLists data={copieddata} setEquation={this.setEquation} />

                </Modal>

            </div>
        );
    }
}



function mapStateToProps(state) {
    return { ...state };
}
const actionCreators = {
    setMathMLExpressionAction: mathMLActions.setMathMLExpressionAction,
    removeMathMLExpressionAction: mathMLActions.removeMathMLExpressionAction,
    setCopyMathMLExpressionAction: mathMLActions.setCopyMathMLExpressionAction


};
export default connect(mapStateToProps, actionCreators)(CreateEquation);