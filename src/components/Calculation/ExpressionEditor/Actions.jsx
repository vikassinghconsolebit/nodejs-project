import React from 'react';
import { DownloadOutlined, UndoOutlined, PlusSquareFilled, ArrowsAltOutlined, MinusSquareFilled, StarFilled } from '@ant-design/icons';

export const Actions = () => {
    return (
        <React.Fragment>
            {/* Actions */}
            <div className="home-actions-container">
                <ul>
                    <li><DownloadOutlined style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                    <li><UndoOutlined style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                    <li><PlusSquareFilled style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                    <li><ArrowsAltOutlined style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                    <li><MinusSquareFilled style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                    <li><StarFilled style={{ color: "#95ac5c", fontSize: '19px' }} /></li>
                </ul>
                <label>Maintain Calculation</label>
            </div>
        </React.Fragment>
    )
}
