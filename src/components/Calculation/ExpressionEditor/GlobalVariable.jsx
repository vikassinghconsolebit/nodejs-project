import React, { Component } from 'react'
import { Button, Table, message, Popconfirm, Input, Space, Empty } from 'antd';
import Routes from '../../../controller/routes';
import { reverse } from "named-urls/dist/index.es";
import { formatDate, handleError } from "../../../controller/global";
import { getVariable, deleteVariable } from '../../../controller/api/variableApi';
import pagination from "../../../controller/Pagination";
import { connect } from "react-redux";
import { QuestionCircleOutlined } from '@ant-design/icons';
import { SearchOutlined } from '@ant-design/icons';


class GlobalVariable extends Component {
    state = {
        loading: false,
        data: [],
        pagination: pagination,
        selectedRowKeys: [],
        searchText: '',
        searchedColumn: '',

    }

    componentDidMount() {
        this.fetchVariable()
    }

    fetchVariable = (params = {}) => {
        this.setState({ loading: true, params })

        getVariable(params).then(res => {
            const pagination = { ...this.state.pagination }
            pagination.total = res.data.count
            let data = this.handleKey(res.data.results);

            this.setState({
                loading: false,
                pagination,
                data
            })
        }).catch(err => {
            handleError(err)
            this.setState({ loading: false })
        })
    }

    handleKey = (data) => {
        if (!data) {
            return;
        }
        let newData = []
        data.forEach((item) => {
            if (!item.entity) {
                newData.push(item)
            }
        });
        return newData
    };

    onSelectChange = selectedRowKeys => {
        this.setState({ selectedRowKeys });
    };

    deleteItem = (rowKeys) => {
        deleteVariable({ id_list: rowKeys, })
            .then(res => {
                message.success('Calculation Deleted Successfully ')
                this.setState({ selectedRowKeys: [] })
                this.fetchVariable()
            })
    }

    handleTableChange = (pagination, filters, sorter) => {
        let symbol = '';
        if (sorter.order === 'descend')
            symbol = '-';
        let params = {
            page: pagination.current,
        }
        if (sorter.columnKey) {
            params.ordering = `${symbol}${sorter.columnKey}`
        }
        this.fetchVariable({ ...this.state.params, ...params });

    };

    onRowClick = (row) => {
        this.props.history.push(
            {
                pathname: reverse(Routes.dashboard.globalvariable.detail, { id: row.id }),
                state: { variableId: this.props.match.params.id }
            }
        )
    }

    getColumnSearchProps = dataIndex => ({

        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (

            <div style={{ padding: 8 }}>

                <Input ref={node => { this.searchInput = node; }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}

                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />

                <Space>
                    <Button type="primary" onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined />} size="small" style={{ width: 90 }} >
                        Search </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>Reset</Button>
                </Space>
            </div>
        ),

        filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,

    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        this.fetchVariable({ search: selectedKeys[0], page: 1 })
        // this.fetchVariable({ [dataIndex]: selectedKeys[0], page: 1 })
    };

    handleReset = clearFilters => {
        clearFilters();
        this.fetchVariable({ page: 1 })

    };


    render() {
        const { data, pagination, loading, selectedRowKeys } = this.state;
        const hasSelected = selectedRowKeys.length > 0;

        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                sorter: true,
                ...this.getColumnSearchProps('name'),
            },
            {
                title: 'Description',
                dataIndex: 'description',
                key: 'description',
                sorter: true,
                ...this.getColumnSearchProps('description'),
            },
            {
                title: 'Start Date',
                dataIndex: 'start_date',
                key: 'start_date',
                render: (data) => <div>{formatDate(data)}</div>,
                sorter: true,
                ...this.getColumnSearchProps('start_date'),
            },
            {
                title: 'End Date',
                dataIndex: 'end_date',
                key: 'end_date',
                render: (data) => <div>{formatDate(data)}</div>,
                sorter: true,
                ...this.getColumnSearchProps('end_date'),
            },
            {
                title: 'Created By',
                dataIndex: 'created_by',
                key: 'created_by',
                render: (data) => <div>{data ? `${data.first_name} ${data.last_name}` : '-'}</div>,
                sorter: true,
                ...this.getColumnSearchProps('created_by'),
            },
            {
                title: 'Updated By',
                dataIndex: 'updated_by',
                key: 'updated_by',               
                render: (data) => <div>{data ? `${data.first_name} ${data.last_name}` : '-'}</div>,
                // render: (data) => <div>{data ? data : "-"}</div>,
                sorter: true,
                ...this.getColumnSearchProps('updated_by'),

            },
        ];
        return (
            <>
                <Button type="primary" className="ml-2"
                    onClick={() => this.props.history.push(Routes.dashboard.globalvariable.create, { id: this.props.match.params.id })}>Create New Variable</Button>
                <Popconfirm
                    title="Are you sure to delete this Global Variable?"
                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                    onConfirm={() => this.deleteItem(selectedRowKeys)}
                    onCancel={() => this.getCalculationData()}
                    okText="Yes"
                    cancelText="No"
                    placement="right"
                    disabled={!hasSelected}>

                    <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

                </Popconfirm>

                <div className="table-container mt-4">
                    <Table loading={loading} onChange={this.handleTableChange} pagination={pagination}
                        dataSource={data} bordered columns={columns} rowKey={data => data.id} rowSelection={rowSelection}
                        onRow={(record) => {
                            return {
                                onClick: event => {
                                    this.onRowClick(record)
                                }
                            };
                        }}
                        locale={{
                            emptyText:
                                (<div style={{ textAlign: "center" }}>
                                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} >
                                        <p className='text-danger editable-cell-value-wrap' onClick={() => this.fetchVariable()}>Click here to go Back</p>
                                    </Empty>
                                </div>
                                )
                        }} />
                </div>
            </>

        )
    }
}


const actionCreators = {
};
export default connect(null, actionCreators)(GlobalVariable);