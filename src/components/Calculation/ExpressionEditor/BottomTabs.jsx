import React, { Component } from 'react';
import { Input, Tabs } from 'antd'
import { connect } from 'react-redux';
import Log from './BottomTabs/Log';
import RecordStatus from './BottomTabs/RecordStatus';
import { getEquation, deleteEquation } from '../../../controller/api/euationApi';
import { handleError } from '../../../controller/global';

const { TabPane } = Tabs;

const TabData = () => {
    return (<div>
        {/* <div className="row">
            <div className="col-md-4 p-0">
                <label>Record Status</label>
                <Input className="wid-60" />
            </div>
            {/* <div className="col-md-5">
            <label>Created by</label>
            <Input className="wid-30" />
            <Input className="ml-2 wid-30" />
        </div> */}

        {/* </div> */}
        <div className="row mt-2">
            {/* <div className="col-md-5">
                <label>Last Updated by</label>
                <Input className="wid-30" />
                <Input className="ml-2 wid-30" />
            </div> */}
            <div className="col-md-4 p-0">
                <label>Revision Text</label>
                <Input className="wid-60" />
            </div>
            <div className="col-md-3">
                <label>Revision Event</label>
                <Input className="ml-2 w-50" />
            </div>
        </div></div>)
}

class BottomTabs extends Component {
    state = {

    }

    render() {

        return (
            <Tabs type="card">
                <TabPane tab="Log" key="1"><Log {...this.props} /></TabPane>
                <TabPane tab="RECORD STATUS" key="2"><RecordStatus {...this.props} /></TabPane>
                <TabPane tab="REVISION INFO" key="3"><TabData /></TabPane>
                <TabPane tab="APPROVAL STATUS" key="4"><TabData /></TabPane>
                {/* <TabPane tab="HINTS & TIPS" key="5"><TabData /></TabPane> */}
                {/* <TabPane tab="VALIDATION" key="6"> <TabData /> </TabPane> */}
                {/* <TabPane tab="TRENDING" key="7"> <TabData /></TabPane> */}
            </Tabs>
        )
    }
}

function mapStateToProps(state) {
    return { ...state };
}
const actionCreators = {
};

export default connect(mapStateToProps, actionCreators)(BottomTabs);


