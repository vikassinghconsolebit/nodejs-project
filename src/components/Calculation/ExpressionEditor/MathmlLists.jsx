import React, { Component } from 'react'
import { List } from 'antd';
import { MathEq, MathEqReg } from "../EquationBuilder/math/MathEq";
import { Renderer } from "../EquationBuilder/math/Renderer";

export default class MathmlLists extends Component {

    componentDidMount() {
        this.renderMathmlEquation(this.props.data)
    }

    activateMathmlEquation = (inputId, canvasId) => {

        const equation = new MathEq({
            mathmlId: inputId,
            canvasId: canvasId,
            editable: true,
            context: 'free',
        })
        MathEqReg.register(inputId, equation)
        Renderer.render({
            equation: equation,
            canvas: equation.canvas,
            editable: true
        })
    }

    renderMathmlEquation = () => {
        this.props.data.forEach((item, index) => {
            this.activateMathmlEquation(`mathml-equation-iteration-${index}`, `canvas-equation-iteration-${index}`)
        })

    }
    selectedMathml=(data)=>{
        this.props.setEquation(data)
    }

    render() {
        return (
            <>
                <List
                    itemLayout="horizontal"
                    dataSource={this.props.data}
                    renderItem={(item, index) => (
                        <List.Item onClick={()=>this.selectedMathml(item)} style={{ cursor: 'pointer'}}>
                            <div>
                                <input id={`mathml-equation-iteration-${index}`} type="hidden" value={item.value} />
                                <canvas id={`canvas-equation-iteration-${index}`} />
                            </div>
                    </List.Item>
                    )}
                />
            </>
        )
    }
}


