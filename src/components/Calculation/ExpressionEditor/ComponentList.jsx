import React, { Component } from 'react'
import { connect } from 'react-redux';
import Variable from '../Keyboard/Custom/Variable';
import EquationBuilder from '../EquationBuilder/EquationBuilder';

//Contains that Component which can be user everywhere in the application.
class ComponentList extends Component {
    render() {
        return (
            <React.Fragment>
                {/*{componentReducer.addVariable && <Variable visible={componentReducer.addVariable} />}*/}
             <EquationBuilder />
            </React.Fragment>
        )
    }
}
function mapStateToProps(state) {
    return { ...state };
}
export default connect(mapStateToProps)(ComponentList)
