import React, { Component } from 'react';
import {withRouter} from "react-router-dom";
import { Form, Input, message, Select, Button, DatePicker} from "antd";
import { getEntity } from "../../../controller/api/entityApi"
import { getAttribute } from "../../../controller/api/attributeApi";
import Routes from "../../../controller/routes";
import {handleError} from "../../../controller/global";
import {addVariable} from '../../../controller/api/variableApi';
import { reverse } from 'named-urls';

const {Option} = Select;

class CreateLocalVariable extends Component {
    state = {
        allEntity: [],
        allAttribute:[],
        loading: false,
    }

    formRef = React.createRef()

    componentDidMount() {
        this.fetchEntityData()
    }

    fetchEntityData = (params = {}) => {
        getEntity(params)
            .then(response => {
                this.setState({allEntity: response.data.results})
            })
    }
    
    fetchAttributeData = (params = {}) => {
        getAttribute(params)
            .then(response => {
                this.setState({allAttribute: response.data.results})
            })
    }

    onSubmit = (value) => {
        this.setState({loading: true})
        addVariable(value)
            .then(response => {
                message.success('Added Successfully')
                this.props.history.push({
                    pathname: reverse(Routes.dashboard.calculation.equationBuilder, { id: this.props.location.state.id }),
                    state: { activetab: '4' }})
                // this.props.history.push(Routes.dashboard.localvariable.self)
                this.setState({loading: false})
            }).catch(err => {
            handleError(err)
            this.setState({loading: false})
        })
    }
    

    handleEntityChange = (attribute) => {
        this.fetchAttributeData({entity: attribute})
    }


    render() {
        const { allEntity, allAttribute} = this.state
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Create Variable</h5>
                        <Form className="entity-form" ref={this.formRef} onFinish={this.onSubmit}>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                        label="Name" name="name">
                                <Input placeholder="Variable Name"/>
                            </Form.Item>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                         label="Selected Entity" name="entity">
                                <Select onChange={this.handleEntityChange}  placeholder="Select Type" style={{width: "100%"}}  >
                                    {allEntity.length > 0 &&
                                    allEntity.map((entity) => <Option key={entity.id}
                                                                                value={entity.id}>{entity.name}</Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]}
                                         label="Selected Attribute" name="attribute">
                                <Select  placeholder="Select Type" style={{width: "100%"}}>
                                    {allAttribute.length > 0 &&
                                    allAttribute.map((attribute) => <Option key={attribute.id}
                                                                                value={attribute.id}>{attribute.name}</Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input placeholder="Description"/>
                            </Form.Item>
                            <Form.Item rules={[{required: true, message: "This Field is required"}]} label="Start Date"
                                       name="start_date">
                                <DatePicker showTime/>
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker showTime/>
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                    className="mr-3">Submit</Button>
                                <Button onClick={() => this.formRef.current.resetFields()} type="text">Clear</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>


        );
    }
}




export default withRouter(CreateLocalVariable);

