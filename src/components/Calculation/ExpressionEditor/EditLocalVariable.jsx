import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Form, Input, message, Select, Button, DatePicker } from "antd";
import { getEntity } from "../../../controller/api/entityApi"
import { getAttribute } from "../../../controller/api/attributeApi";
import Routes from "../../../controller/routes";
import { handleError } from "../../../controller/global";
import { reverse } from 'named-urls';
import { getVariableById, updateVariable } from '../../../controller/api/variableApi'
import moment from "moment";


const { Option } = Select;

class EditLocalVariable extends Component {
    _isMounted = false;

    state = {
        allEntity: [],
        allAttribute: [],
        loading: false,
        Valdata: null,
        
    }

    formRef = React.createRef()

    componentDidMount() {
        this._isMounted = true;
        getVariableById(this.props.match.params.id).then(res => {
            if(this._isMounted){

            
            this.setState({ Valdata: res.data })}
            
            this.formRef.current.setFieldsValue({
                name: res.data.name,
                entity: res.data.entity.id,
                attribute: (res.data.attribute ? res.data.attribute.name : res.data.attribute),
                description: res.data.description,
                start_date: moment(res.data.start_date),
                end_date: moment(res.data.end_date),
            })
            this.fetchAttributeData({ entity: res.data.entity.id })

        });

        this.fetchEntityData();
    }

    componentWillUnmount() {
        
        this._isMounted = false;

    }


    onSubmit = (value) => {
        if (typeof value.attribute === "string") {
            value.attribute = this.state.Valdata.attribute.id
        }
        this.setState({ loading: true })
        updateVariable(this.props.match.params.id, value).then(res => {
            message.success("Variable Updated Successfully")
            this.props.history.push({
                pathname: reverse(Routes.dashboard.calculation.equationBuilder, { id: this.props.location.state.variableId }),
                state: { activetab: '4' }})
            this.setState({ loading: false })
        }).catch(err => {
            handleError(err)
            this.setState({ loading: false })
        })
    }



    fetchEntityData = (params = {}) => {
        getEntity(params)
            .then(response => {
                this.setState({ allEntity: response.data.results })
            })
    }

    fetchAttributeData = (params = {}) => {
        getAttribute(params)
            .then(response => {                
                this.setState({ allAttribute: response.data.results })
            })
    }


    handleEntityChange = (attribute) => {
        this.fetchAttributeData({ entity: attribute })
    }


    render() {
        const { allEntity, allAttribute } = this.state
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Variable Detail</h5>
                        <Form className="entity-form" ref={this.formRef} onFinish={this.onSubmit}>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]}
                                label="Name" name="name">
                                <Input placeholder="Variable Name" />
                            </Form.Item>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]}
                                label="Selected Entity" name="entity">
                                <Select onChange={this.handleEntityChange} placeholder="Select Type" style={{ width: "100%" }}  >
                                    {allEntity.length > 0 &&
                                        allEntity.map((entity) => <Option key={entity.id}
                                            value={entity.id}>{entity.name}</Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]}
                                label="Selected Attribute" name="attribute">
                                <Select placeholder="Select Type" style={{ width: "100%" }}>
                                    {allAttribute.length > 0 &&
                                        allAttribute.map((attribute) => <Option key={attribute.id}
                                            value={attribute.id}>{attribute.name}</Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input placeholder="Description" />
                            </Form.Item>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]} label="Start Date"
                                name="start_date">
                                <DatePicker showTime />
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker showTime />
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                    className="mr-3">Submit</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>


        );
    }
}

export default withRouter(EditLocalVariable);