import React, { Component } from 'react';
import { connect } from 'react-redux';
import ComponentList from './ComponentList';
import { Actions } from './Actions';
import BottomTabs from './BottomTabs';
import EquationTable from './EquationTable';
import Filters from './Filters';

//Equation Editor Component
class ExpressionsEditor extends Component {
    state = {
        copymathmldata: []
    
      }



      setCopyMathml = (mathml, type) => {
        let data = {
            value: mathml.replace(/'/g, ''),
            type: type
          }
      
          if (this.state.copymathmldata.length > 15) {
            this.state.copymathmldata.push(data);
      
            this.state.copymathmldata.splice(0, 1);
            this.setState({ copymathmldata: this.state.copymathmldata })
          } else {
            this.state.copymathmldata.push(data);
            this.setState({ copymathmldata: this.state.copymathmldata })
          }
      
          localStorage.setItem('Copiedmathml', JSON.stringify(this.state.copymathmldata));
        
    };


    render() {
        return (
            <div className="container-fluid exp-main-fluid">
                <div className="row mx-0">
                    <div className="dashboard-container">
                        {/* <Actions /> */}
                        {/* <Filters {...this.props}/> */}
                        <div className="mt-2">
                        {/* <div className="right-container mt-3"> */}
                            <EquationTable {...this.props} setCopyMathml={this.setCopyMathml} />
                            <div className="home-tabs mt-4">
                                <BottomTabs {...this.props} />
                            </div>
                            {/* <ComponentList {...this.props} /> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { ...state };
}

const actionCreators = {
};
export default connect(mapStateToProps, actionCreators)(ExpressionsEditor);