import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Form, Input, message, Button, DatePicker } from "antd";
import Routes from "../../../controller/routes";
import { handleError } from "../../../controller/global";
import { reverse } from 'named-urls';
import { getVariableById, updateVariable } from '../../../controller/api/variableApi'
import moment from "moment";



class EditGlobalVariable extends Component {
    state = {
        loading: false,
    }

    formRef = React.createRef()

    componentDidMount() {
        getVariableById(this.props.match.params.id).then(res => {
            this.formRef.current.setFieldsValue({
                name: res.data.name,
                description: res.data.description,
                start_date: moment(res.data.start_date),
                end_date: moment(res.data.end_date),
            })
        })
    }


    onSubmit = (value) => {
        this.setState({ loading: true })
        updateVariable(this.props.match.params.id, value).then(res => {
            message.success("Variable Updated Successfully")
            this.props.history.push({
                pathname : reverse(Routes.dashboard.calculation.equationBuilder, { id: this.props.location.state.variableId }),
                state: { activetab: '5' }})
            this.setState({ loading: false })
        }).catch(err => {
            handleError(err)
            this.setState({ loading: false })
        })
    }

    render() {
        return (
            <div className="container-fluid exp-main-fluid create-form">
                <div className="row mx-0">
                    <div className="dashboard-container right-container">
                        <h5>Variable Detail</h5>
                        <Form className="entity-form" ref={this.formRef} onFinish={this.onSubmit}>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]}
                                label="Name" name="name">
                                <Input placeholder="Variable Name" />
                            </Form.Item>
                            <Form.Item label="Description" name="description">
                                <Input placeholder="Description" />
                            </Form.Item>
                            <Form.Item rules={[{ required: true, message: "This Field is required" }]} label="Start Date"
                                name="start_date">
                                <DatePicker showTime />
                            </Form.Item>
                            <Form.Item label="End Date" name="end_date">
                                <DatePicker showTime />
                            </Form.Item>
                            <div className="d-block mt-4">
                                <Button loading={this.state.loading} type="primary" htmlType="submit"
                                    className="mr-3">Submit</Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>

        );
    }
}


export default withRouter(EditGlobalVariable);