import React, { Component } from 'react';
import { Button, Table, Tabs, Popconfirm, message, Dropdown, Menu } from 'antd';
import EquationBuilder from '../EquationBuilder/EquationBuilder';
import { getEquation, deleteEquation } from '../../../controller/api/euationApi';
import { handleError } from '../../../controller/global';
import Routes from "../../../controller/routes";
import { mathMLActions } from "../../../actions/mathml.action";
import { connect } from "react-redux";
import { MathEq, MathEqReg } from "../EquationBuilder/math/MathEq";
import { Renderer } from "../EquationBuilder/math/Renderer";
import LocalVariable from "./LocalVariable";
import GlobalVariable from "./GlobalVariable";
import { QuestionCircleOutlined } from '@ant-design/icons';
import pagination from "../../../controller/Pagination"
import SetVariable from './SetVariable';
import { ITERATION, CONDITION, ASSIGNMENT } from '../../../constants/mathml.constant';

require('../EquationBuilder/math/Builder')
require('../EquationBuilder/math/Commons')
require('../EquationBuilder/math/ElementCollection')
require('../EquationBuilder/math/Elements')
require('../EquationBuilder/math/Equation')
require('../EquationBuilder/math/MathEq')
require('../EquationBuilder/math/Parser')
require('../EquationBuilder/math/Renderer')


const { TabPane } = Tabs;

const dataSource = [
  {
    key: '1',
    iteration: '3',
    condition: '-',
    assignment: '-',
    calculation: "-",
    company: "?",
    created_by: "-",
    updated_by: "-"
  },
  {
    key: '2',
    iteration: '3',
    condition: '-',
    assignment: "-",
    calculation: "-",
    company: "?",
    created_by: "-",
    updated_by: "-"
  },
  {
    key: '3',
    iteration: '3',
    condition: '-',
    assignment: "-",
    calculation: "-",
    company: "?",
    created_by: "-",
    updated_by: "-"
  },
];


export class EquationTable extends Component {
  state = {
    visible: false,
    loading: false,
    data: [],
    column_name: '',
    rowId: '',
    selectedRowKeys: [],
    pagination: pagination,
    copyMathml: [],
    activekey: '1',

  }
  menu = (data, type, eauationType) => (
    <Menu>
      <Menu.Item key="1" onClick={() => this.selectEquation(data, type)}>Copy {eauationType}</Menu.Item>
      {/* <Menu.Item key="1" onClick={() => this.props.setCopyMathml(data, type)}>Copy {eauationType}</Menu.Item> */}


    </Menu>
  );

  selectEquation = (mathml, type) => {
    let data = {
      value: mathml.replace(/'/g, ''),
      type: type
    }

    if (this.state.copyMathml.length > 15) {
      this.state.copyMathml.push(data);

      this.state.copyMathml.splice(0, 1);
      this.setState({ copyMathml: this.state.copyMathml })
    } else {
      this.state.copyMathml.push(data);
      this.setState({ copyMathml: this.state.copyMathml })
    }
    this.props.setCopyMathMLExpressionAction(this.state.copyMathml)

    // localStorage.setItem('Copiedmathml', JSON.stringify(this.state.copyMathml));
  }


  componentDidMount() {
    this.fetchEquation()
    if (this.props.location.state){
      this.setState({activekey:this.props.location.state.activetab}) 
    }

  }

  fetchEquation = (params = {}) => {
    this.setState({ loading: true })
    getEquation({ calculation: this.props.match.params.id }, params)
      .then(res => {
        const pagination = { ...this.state.pagination }
        pagination.total = res.data.count
        this.setState({
          loading: false,
          data: res.data.results,
          pagination,

        })
        this.renderMathmlEquation(res.data.results)
      })
      .catch(err => {
        // handleError(err)
      })
  }

  activateMathmlEquation = (inputId, canvasId, equationType) => {
    const equation = new MathEq({
      mathmlId: inputId,
      canvasId: canvasId,
      editable: true,
      context: 'free',
      equationType: equationType
    })
    MathEqReg.register(inputId, equation)
    Renderer.render({
      equation: equation,
      canvas: equation.canvas,
      editable: true
    })
  }

  renderMathmlEquation = (data) => {
    data.forEach(item => {
      this.activateMathmlEquation(`mathml-equation-iteration-${item.id}`, `canvas-equation-iteration-${item.id}`, ITERATION)
      this.activateMathmlEquation(`mathml-equation-condition-${item.id}`, `canvas-equation-condition-${item.id}`, CONDITION)
      this.activateMathmlEquation(`mathml-equation-assignment-${item.id}`, `canvas-equation-assignment-${item.id}`, ASSIGNMENT)

    })
  }

  onSelectChange = selectedRowKeys => {
    this.setState({ selectedRowKeys });
  };

  onColumnData = (data, rowId, column_name) => {
    const mathmldata = data.replace(/'/g, '')
    this.setState({ visible: true, column_name, rowId })
    this.props.setMathMLExpressionAction(mathmldata)
  }

  deleteItem = (rowKeys) => {
    deleteEquation({
      id_list: rowKeys,
    })
      .then(res => {
        message.success('Equation Deleted Successfully ')
        this.setState({ selectedRowKeys: [] })
        this.fetchEquation()
      })
  }

  columns = [
    {
      title: 'Iteration',
      dataIndex: 'iteration',
      key: 'iteration',
      align: "left",
      render: (data, row) => (<span onDoubleClick={() => { this.onColumnData(data, row.id, ITERATION) }}>
        <Dropdown overlay={this.menu(data, ITERATION, 'Iteration')} trigger={['contextMenu']}>
          <div className="site-dropdown-context-menu">
            <input id={`mathml-equation-iteration-${row.id}`} type="hidden" value={data.replace(/'/g, '')} />
            <canvas id={`canvas-equation-iteration-${row.id}`} />
          </div>
        </Dropdown>

      </span>)

    },
    {
      title: 'Condition',
      dataIndex: 'condition',
      key: 'condition',
      align: "left",
      render: (data, row) => (<span onDoubleClick={() => { this.onColumnData(data, row.id, CONDITION) }} >
        <Dropdown overlay={this.menu(data, CONDITION, 'Condition')} trigger={['contextMenu']}>
          <div className="site-dropdown-context-menu">
            <input id={`mathml-equation-condition-${row.id}`} type="hidden" value={data.replace(/'/g, '')} />
            <canvas id={`canvas-equation-condition-${row.id}`} />
          </div>
        </Dropdown>

      </span>),
    },
    {
      title: 'Assignment',
      dataIndex: 'assignment',
      key: 'assignment',
      align: "left",
      render: (data, row) => (<span onDoubleClick={() => { this.onColumnData(data, row.id, ASSIGNMENT) }} >
        <Dropdown overlay={this.menu(data, ASSIGNMENT, 'Assignment')} trigger={['contextMenu']}>
          <div className="site-dropdown-context-menu">
            <input id={`mathml-equation-assignment-${row.id}`} type="hidden" value={data.replace(/'/g, '')} />
            <canvas id={`canvas-equation-assignment-${row.id}`} />
          </div>
        </Dropdown>

      </span>),
    },
    // {
    //   title: 'Company',
    //   dataIndex: 'company',
    //   key: 'company',
    //   render: (data) => <div>{data && data.name}</div>,
    // },
    // {
    //   title: 'Created By',
    //   dataIndex: 'created_by',
    //   key: 'created_by',
    //   render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,

    // },
    // {
    //   title: 'Updated By',
    //   dataIndex: 'updated_by',
    //   key: 'updated_by',
    //   render: (data) => <div>{data && `${data.first_name} ${data.last_name}`}</div>,

    // },
  ];

  equationBuilderClose = (data) => {
    this.props.removeMathMLExpressionAction()
    this.setState({ visible: false })
    if (data){
      setTimeout(function () {
        this.fetchEquation()
      }.bind(this), 700);
    }
  }

  handleSelect=()=>{
    this.setState({activekey: '1'})
  }

  tabChange = (key) => {
        
    this.setState({activekey: key})
}


  render() {
    const {activekey, visible, data, loading, column_name, rowId, selectedRowKeys, pagination } = this.state;
    const hasSelected = selectedRowKeys.length > 0;

    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    return (
      <>
        <Tabs type="card" activeKey={activekey} id="uncontrolled-tab-example" onChange={this.tabChange} >
          <TabPane tab="EQUATIONS" key="1">

            <Button type="primary" className="ml-2" onClick={() => this.props.history.push(Routes.dashboard.calculation.equation.create, { id: this.props.match.params.id })}>Add Equation</Button>

            <Popconfirm
              title="Are you sure to delete this Entity?"
              icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
              onConfirm={() => this.deleteItem(selectedRowKeys)}
              onCancel={() => this.fetchEquation()}
              okText="Yes"
              cancelText="No"
              placement="right"
              disabled={!hasSelected}>

              <Button type="danger" className="ml-2" disabled={!hasSelected}>Delete Selected Item</Button>

            </Popconfirm>
            <div className="table-container mt-4">
              <Table dataSource={data} bordered columns={this.columns} rowSelection={rowSelection}
                loading={loading} pagination={pagination} className="table-cursor"
                rowKey={record => record.id} />
            </div>
          </TabPane>
          <TabPane tab="LOCAL SETS" key="2">
            <SetVariable {...this.props}/>
          </TabPane>
          <TabPane tab="INHERITED SETS" key="3">
            {/* <Table dataSource={dataSource} bordered columns={this.columns} pagination={false} /> */}    

          </TabPane>
          <TabPane tab="LOCAL VARIABLES" key="4">
            <LocalVariable {...this.props} />
          </TabPane>
          <TabPane tab="GLOBAL VARIABLES" key="5">
            {/* <TabPane tab="INHERITED VARIABLES" key="5"> */}
            <GlobalVariable {...this.props} />
          </TabPane>
        </Tabs>
        <EquationBuilder visible={visible} colname={column_name} equationType={column_name} rowId={rowId} onClose={(data) => this.equationBuilderClose(data)} />
      </>
    )
  }
}

function mapStateToProps(state) {
  return { ...state };
}

const actionCreators = {
  setMathEditorAction: mathMLActions.setMathEditorAction,
  setMathEquationAction: mathMLActions.setMathEquationAction,
  setMathMLExpressionAction: mathMLActions.setMathMLExpressionAction,
  removeMathMLExpressionAction: mathMLActions.removeMathMLExpressionAction,
  setCopyMathMLExpressionAction: mathMLActions.setCopyMathMLExpressionAction

};
export default connect(mapStateToProps, actionCreators)(EquationTable);
