import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image as Images } from "../../Images";
import { Button } from "antd";

class DateTime extends Component {
    data = [
        {
            "title": "DATE DIFF",
            "description": "",
            "latex_code": "\\DATEDIFF"
        },
        {
            "title": "ADD MONTHS",
            "description": "",
            "latex_code": "\\ADDMONTHS"
        },
        {
            "title": "FOYEAR",
            "description": "",
            "latex_code": "\\FOYEAR"
        }
    ]

    onButtonClick = () => {
        this.props.setMathListData(this.data)
    }


    render() {
        return (
            <div className="row">
        <div className="col-12">
          <ul className="list-inline mb-0">
            {/* <li><Trigonometry /></li> */}
            {/* <li>{this.aggregate()}</li>
            <li><DateTime /></li>
            <li>{this.logical()}</li>
            <li>{this.math()}</li>
            <li>{this.other()}</li> */}
          </ul>
        </div>
      </div>
 
        );
    }
}

function mapStateToProps(state) {
    return { ...state };
}

const actionCreators = {
};

export default connect(mapStateToProps, actionCreators)(DateTime);
