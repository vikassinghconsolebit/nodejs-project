import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image as Images } from "../../Images";
import { Button } from "antd";
import { Collapse, Modal } from 'antd';
import { getEntityAllFunction } from '../../../controller/api/entityApi';
import { handleError } from "../../../controller/global";
import { isTemplateLiteral } from '@babel/types';



const { Panel } = Collapse;
const PanelHeader = ({ image, heading }) => {
  //Dropdown Heading. Get name and image .



  return (

    <div className="exp-header-accordion  d-flex align-items-center">

      <div className="exp-icon-img">
        <img alt="icons" className="img-fluid"
          src={Images[image]} />
      </div>
      <span>{heading}</span>
    </div>
  )
}


class Function extends Component {
  state = {
    data: [],
    functionChild: [],
    MathEqEditor: this.props.mathmlReducer,
  }
  componentDidMount() {
    this.getfunctions()

  }


  getfunctions = (params = {}) => {
    getEntityAllFunction(params)
      .then(res => {
        let functionChild = this.handleKey(res.data.results);
        this.setState({
          data: res.data.results, functionChild
        })
      }).catch(err => {
        handleError(err)
      })
  }

  handleKey = (data) => {
    if (!data) {
      return;
    }
    let newData = []
    data.forEach((item) => {
      if (!item.parent) {
        newData.push(item)
      }
    });
    return newData
  };

  // addVariable = () => {
  //   return (<Button onClick={() => this.props.toggleAddVariable(true)} className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" /> Add Variable</Button>)
  // }
  // aggregate = () => {
  //   return (<Button className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" />Aggregate</Button>)
  // }
  // dateTime = () => {
  //   return (<Button className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" />Date - Time</Button>)
  // }
  // logical = () => {
  //   return (<Button className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" />Logical</Button>)
  // }
  // math = () => {
  //   return (<Button className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" />Math</Button>)
  // }
  // other = () => {
  //   return (<Button className="d-flex align-items-center shadow-none border-0">
  //     <img className="img-fluid" src={Images.function_icon} alt="icon" />Other</Button>)
  // }
  ChildFuncGet = (children) => { 
    return (
      <div>
        {children.map((item, index) => {

          return (<Button className="d-flex align-items-center shadow-none border-0" id={index}
            onClick={() => this.state.MathEqEditor.editor.onButtonSelection(item.name)}>
            <img className="img-fluid" src={Images.function_icon} alt="icon" />{item.name}</Button>)

        })}
      </div>)

  }

  render() {
    const { data, functionChild } = this.state


    return (
      <div className="row">
        <div className="col-12">
          <ul className="list-inline mb-0">

            {functionChild.map((func, index) =>

              // <li key={func.id}><Button className="d-flex align-items-center shadow-none border-0" onClick={()=>this.funcChild()}>
              //   <img className="img-fluid" src={Images.function_icon} alt="icon" />{func.name}</Button><li></li></li>
              <div >
                <Collapse accordion >
                  <Panel  header={<PanelHeader image="function_icon" heading={func.name} />} key={index} >
                    {this.ChildFuncGet(func.children)}
                    {/* {buttons.map((item, index) => {
                                return <Button key={index} title={item.name}
                                    onClick={() => editor.onButtonSelection(item.name)}
                                    className="d-flex align-items-center justify-content-center">{item.title}</Button>
                            })} */}

                    {/* {func.children.map(child => <li key={func.children.id}><Button className="d-flex align-items-center shadow-none border-0" >
                  <img className="img-fluid" src={Images.function_icon} alt="icon" />{child.name}</Button></li>)} */}

                  </Panel>
                </Collapse>
              </div>

            )}
          </ul>

          {/* <ul className="list-inline mb-0"> */}

          {/* <li><Trigonometry /></li>
            <li>{this.addVariable()}</li>
            <li>{this.aggregate()}</li>
            <li><DateTime /></li>
            <li>{this.logical()}</li>
            <li>{this.math()}</li>
            <li>{this.other()}</li>
          </ul> */}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { ...state };
}
const actionCreators = {
};

export default connect( mapStateToProps, actionCreators)(Function);


