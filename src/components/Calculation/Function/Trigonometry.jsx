import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Image as Images} from "../../Images";
import {Button} from "antd";
import {mathMLActions} from "../../../actions/mathml.action";

class Trigonometry extends Component {
  data = [
    {
      "title": "Sin",
      "description": "",
      "latex_code": "sin"
    }, {
      "title": "Cos",
      "description": "",
      "latex_code": "cos"
    }, {
      "title": "Tan",
      "description": "",
      "latex_code": "tan"
    }, {
      "title": "Cot",
      "description": "",
      "latex_code": "cot"
    }, {
      "title": "Sec",
      "description": "",
      "latex_code": "sec"
    }, {
      "title": "Cosec",
      "description": "",
      "latex_code": "cosec"
    }
  ]

  onButtonClick = () => {
    this.props.setMathListData(this.data)
  }

  render() {
    
    return (
      <Button
        onClick={this.onButtonClick}
        className="d-flex selected align-items-center shadow-none border-0">
        <img className="img-fluid"
             src={Images.function_icon}
             alt="icon"/>
        Trigonometry
      </Button>
    );
  }
}

const actionCreators = {
};

export default connect(null, actionCreators)(Trigonometry);
