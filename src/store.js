import {applyMiddleware, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import {persistReducer, persistStore} from "redux-persist";
import rootReducer from "./reducers";


const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['user'],
};

const getMiddleware = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(thunkMiddleware);
  } else {
    return applyMiddleware(thunkMiddleware, createLogger())
  }
};
const pReducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(pReducer, composeEnhancers(getMiddleware()));

export const pStore = persistStore(store);